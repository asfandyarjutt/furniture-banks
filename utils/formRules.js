import { phoneRegx, emailRegx } from "../utils/regex";
import { AddManagers } from "../constants";
const {
  REQUIRED_FIRST_NAME,
  REQUIRED_LAST_NAME,
  REQUIRED_PHONE,
  VALID_PHONE,
  REQUIRED_EMAIL,
  VALID_EMAIL,
} = AddManagers;

export const firstName = [
  {
    required: true,
    message: REQUIRED_FIRST_NAME,
  },
];
export const lastName = [
  {
    required: true,
    message: REQUIRED_LAST_NAME,
  },
];
export const phoneNumber = [
  {
    required: true,
    message: REQUIRED_PHONE,
  },
  {
    pattern: phoneRegx,
    message: VALID_PHONE,
  },
];
export const emailAddress = [
  { required: true, message: REQUIRED_EMAIL },
  {
    pattern: emailRegx,
    message: VALID_EMAIL,
  },
];
