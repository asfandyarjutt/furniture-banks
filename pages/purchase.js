import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Button,
  Card,
  Typography,
  Steps,
  Space,
  Popover,
} from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { emailRegx, phoneRegx, zipRegx } from "../constants";
import {
  faAngleLeft,
  faAngleRight,
  faChevronLeft,
  faRegistered,
} from "@fortawesome/free-solid-svg-icons";
import ContactForm from "../components/Forms/ContactForm";
import HouseHoldForm from "../components/Forms/HouseHoldForm";
import BGForm from "../components/Forms/BGForm";
import { useRouter } from "next/router";
import PaymentC from "../components/Forms/PaymentC";
import { currentUser, useCurrentUser } from "../lib/hooks";
import { useCartValue } from "../lib/state";
import { Save_info, CLIENT_APP_FORM } from "../constants";
import { getLocal } from "../components/Forms/functions";
const { Title } = Typography;
const { Step } = Steps;

function appointment() {
  const [currentUser] = useCurrentUser();
  const [currentStep, setStep] = useState(0);
  const [_, dispatch] = useCartValue();
  const [enableModal, setEnableModal] = useState("");
  const [enableRadio, setEnableRadio] = useState("");
  const [stateText, setStateTxt] = useState("");
  const router = useRouter();
  const [form, setForm] = useState({});
  const [valid, setValid] = useState(false);
  const [pass, setPass] = useState(false);
  const [householdpass, setHouseholdPass] = useState(false);
  const [visible, setVisible] = useState(true);
  const [paymentpass, setPaymentpass] = useState(false);
  let contact;
  let payment;
  let household;
  let bg;
  const state = getLocal("State");
  function handleUpdate(value) {
    setPass(value);
  }
  function handleUpdateHouse(value) {
    setHouseholdPass(value);
  }
  function handleUpdatePayment(value) {
    setPaymentpass(value);
  }
  function handleOption(state) {
    if (state === 2) {
      setStateTxt(1);
      setEnableRadio(2);
    } else {
      setStateTxt("");
      setEnableRadio(1);
    }
  }
  useEffect(() => {
    (contact = state?.contact),
      (payment = state?.payment),
      (household = state?.household),
      (bg = state?.bg);
    if (state) {
      if (currentStep === 0) {
        if (Object.keys(contact).length > 0) {
          setForm(contact);
          setVisible(false);
        } else {
          setForm({});
          setVisible(true);
        }
      } else if (currentStep === 1) {
        if (Object.keys(household).length > 0) {
          setForm(household);
          setVisible(false);
        } else {
          setForm({});
          setVisible(true);
        }
      } else if (currentStep === 2) {
        if (Object.keys(bg).length > 0) {
          setForm(bg);
          setVisible(false);
        } else {
          setForm({});
          setVisible(true);
          setValid(false);
        }
      } else if (currentStep === 3) {
        if (Object.keys(payment).length > 0) {
          setForm(payment);
          setVisible(false);
        } else {
          setForm({});
          setVisible(true);
        }
      }
    }
  }, [currentStep, currentUser]);

  useEffect(() => {
    if (currentStep === 0) {
      if (form?.zip) {
        setValid(
          pass === false &&
            Object.values(form).length > 0 &&
            Object.values(form).filter((value) => value === undefined).length <=
              1 &&
            Object.values(form).filter((value) => value === "").length <= 1 &&
            form.clientPhone.length === 10 &&
            emailRegx.test(form.clientEmail) &&
            zipRegx.test(form.zip)
        );
      } else {
        setValid(
          pass === false &&
            Object.values(form).length > 0 &&
            Object.values(form).filter((value) => value === undefined).length <=
              1 &&
            Object.values(form).filter((value) => value === "").length <= 1 &&
            form.clientPhone.length === 10 &&
            emailRegx.test(form.clientEmail)
        );
      }
    } else if (currentStep === 1) {
      setValid(
        householdpass === false &&
          Object.values(form).length > 0 &&
          Object.values(form).filter((value) => value === undefined).length <=
            1 &&
          Object.values(form).filter((value) => value === "").length < 1
      );
    } else if (currentStep === 3) {
      setValid(
        paymentpass === false &&
          Object.values(form).length > 0 &&
          Object.values(form).filter((value) => value === undefined).length <=
            1 &&
          Object.values(form).filter((value) => value === "").length < 1
      );
    } else {
      setValid(
        householdpass === false &&
          paymentpass === false &&
          Object.values(form).length > 0 &&
          Object.values(form).filter((value) => value === undefined).length <= 1
      );
    }
  }, [form, pass, householdpass, paymentpass]);
  const content = (
    <div>
      <p>{Save_info}</p>
    </div>
  );
  return (
    <>
      <Row>
        <Col span={5}>
          <Button
            style={{
              backgroundColor: "white",
              height: "40px",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
            onClick={() => router.back()}
          >
            <Space size="small" direction="horizontal" align="center">
              <FontAwesomeIcon icon={faChevronLeft} /> <span>Back</span>
            </Space>
          </Button>
        </Col>
      </Row>
      <br />
      <br />
      <Row>
        <Col span={24}>
          <Card>
            <center>
              <Title level={3}>{CLIENT_APP_FORM}</Title>
            </center>
            <br />
            <Steps current={currentStep}>
              <Step title="Contact" />
              <Step title="Household" />
              <Step title="Background" />
              <Step title="Payment & Delivery" />
            </Steps>
            <br />
            {currentStep === 0 ? (
              <ContactForm
                enableModal={enableModal}
                updateState={handleUpdate}
                options={handleOption}
                onSaved={(e) => {
                  setForm(e), setVisible(false);
                }}
              />
            ) : currentStep === 1 ? (
              <HouseHoldForm
                handleUpdateHouse={handleUpdateHouse}
                onSaved={(e) => {
                  setForm(e), setVisible(false);
                }}
              />
            ) : currentStep === 2 ? (
              <BGForm
                onSaved={(e) => {
                  setForm(e), setVisible(false);
                }}
              />
            ) : (
              <PaymentC
                disableRadio={enableRadio}
                handleUpdatePayment={handleUpdatePayment}
                onSaved={(e) => {
                  setForm(e), setVisible(false);
                }}
              />
            )}
            <br />
            <Row style={{ paddingTop: "3em", paddingBottom: "3em" }}>
              <Col span={4}>
                <Button
                  onClick={
                    currentStep === 0
                      ? null
                      : () => {
                          setStep(currentStep - 1);

                          stateText !== ""
                            ? setEnableModal(2)
                            : setEnableModal(1);
                        }
                  }
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#69c0ff",
                    border: "1px solid #69c0ff",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: currentStep === 0 ? "none" : "block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faAngleLeft} />
                  &nbsp; Previous
                </Button>
              </Col>
              <Col
                span={4}
                offset={16}
                style={{ display: "flex", justifyContent: "flex-end" }}
              >
                <Popover title={content} visible={visible}>
                  <Button
                    disabled={!valid}
                    style={{
                      backgroundColor: "white",
                      height: "40px",
                      color: "#69c0ff",
                      border: "1px solid #69c0ff",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                    onClick={
                      currentStep === 3
                        ? () => {
                            dispatch({ type: "FINISH" });
                            router.push("/dashboard");
                          }
                        : () => setStep(currentStep + 1)
                    }
                  >
                    {currentStep === 3 ? "Finish" : "Next"} &nbsp;
                    <FontAwesomeIcon icon={faAngleRight} />
                  </Button>
                </Popover>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default appointment;
