import React, { useState, useEffect } from "react";
import { Col, Row, Button, Alert, Spin } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { useFurnitureBanks, useInactiveFurnitureBanks } from "../lib/hooks";
import FBTable from "../components/FBTable";
import { useRouter } from "next/router";
import Link from "next/link";
import { useCurrentUser } from "../lib/hooks";
import {furnitureDelete} from '../components/Forms/constants'
function furniturebanks() {
  const [inactiveFurniturebanks] = useInactiveFurnitureBanks();
  const [active, setActive] = useState(true);
  const [spinIn, setSpin] = useState(false);
  const router = useRouter();
  const { page, keyword } = router.query;
  const [furniturebanks, curPage, maxPage, perPage] = useFurnitureBanks(page);
  const [message, setMessage] = useState(undefined);
  useEffect(() => {
    setMessage(router.query.message);
  });

  const data = furniturebanks?.map((furnitureBank) => ({
    ...furnitureBank,
    key: furnitureBank._id,
    administrator: furnitureBank?.owner
      ? `${furnitureBank?.owner?.firstName} ${furnitureBank?.owner?.lastName}`
      : "None",
  }));

  useEffect(() => {
    setSpin(true);
    if (furniturebanks != undefined) {
      setSpin(false);
    }
  }, [furniturebanks]);

  const inactive = inactiveFurniturebanks?.map((furnitureBank) => ({
    ...furnitureBank,
    key: furnitureBank._id,
    administrator: furnitureBank?.owner
      ? `${furnitureBank?.owner?.firstName} ${furnitureBank?.owner?.lastName}`
      : "None",
  }));
  return (
    <>
      <Spin tip="Loading..." spinning={spinIn}>
        <Alert
          style={{
            marginBottom: "2em",
            width: "100%",
            display: message === undefined ? "none" : "block",
          }}
          message={message}
          type="success"
          showIcon
          closable
          onClose={() => setMessage(undefined)}
        />
        <Row
          align="middle"
          style={{
            marginBottom: active ? "2em" : "0em",
            display: "flex",
            alignItems: "center",
          }}
        >
          <Col span={18} style={{ display: !active ? "none" : "block" }}>
            Click on a Furniture Bank below to view full profile or edit
            information.
          </Col>
          <Col
            span={6}
            style={{
              display: "flex",
              justifyContent: !active ? "flex-start" : "flex-end",
              marginBottom: !active ? "-1em" : "0em",
            }}
          >
            <Button
              style={{
                backgroundColor: "white",
                height: "40px",
                color: "#69c0ff",
                border: "1px solid #69c0ff",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
              onClick={() => setActive(!active)}
            >
              {!active ? <FontAwesomeIcon icon={faChevronLeft} /> : null} &nbsp;
              {active ? "Deleted" : " Back to Active"} Furniture Banks
            </Button>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Link href="/create">
              <a>
                <Button
                  style={{
                    background: "#52C41A",
                    color: "#ffffff",
                    marginRight: "2em",
                    display: !active ? "none" : "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "40px",
                  }}
                  block
                >
                  <FontAwesomeIcon icon={faPlus} />
                  &nbsp; Add Furniture Bank
                </Button>
              </a>
            </Link>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col span={24}>
            <Alert
              style={{
                marginBottom: "3em",
                width: "100%",
                display: "none",
              }}
              message={furnitureDelete}
              type="success"
              showIcon
              closableX
            />
            <FBTable
              data={active ? data : inactive}
              active={active}
              type="furniturebanks"
              curPage={curPage}
              maxPage={maxPage}
              perPage={perPage}
            />
          </Col>
        </Row>
      </Spin>
    </>
  );
}

export default furniturebanks;
