import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Steps,
  Form,
  Input,
  Button,
  message,
  Typography,
  Layout,
  Spin,
} from "antd";
import { useRouter } from "next/router";
import { useCurrentUser } from "../lib/hooks";
import Link from "next/link";
import {
  faCheck,
  faChevronRight,
  faChevronLeft,
  faTimes,
  faPenFancy,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { emailRegx, phoneRegx, textRegx } from "../utils/regex";
import { zipRegx } from "../constants";

const { Step } = Steps;
const { Text, Title } = Typography;
const { Content } = Layout;

function AddFB() {
  const [currentUser] = useCurrentUser();
  const router = useRouter();
  const [step, setStep] = useState(0);
  const [fstepValues, setFstepsValues] = useState({
    name: "",
    address: "",
    zip: "",
    city: "",
    state: "",
    phone: "",
    email: "",
  });
  let passArray = [];
  const [load, setLoad] = useState(false);

  const handleChange = ({ target: { value, name } }) => {
    setFstepsValues({
      ...fstepValues,
      [name]: value,
    });
  };
  passArray.every(Boolean);

  const layout = {
    labelCol: { span: 14 },
    wrapperCol: { span: 24 },
  };
  const onFinish = async (values) => {
    setLoad(true);

    Object.values(fstepValues).some((el) => {
      if (el == "") {
        return message.warning("Missing Fields");
      }
    });

    let res = await fetch("/api/fba", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ ...values, ...fstepValues }),
    });

    if (res.status === 500) {
      res = await fetch("/api/fba", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ ...values, ...fstepValues }),
      });
    }
    if (res.status === 403) {
      setLoad(false);
      return message.warning("Email for manager in use");
    }
    const {
      furnitureBank: { _id },
      info,
    } = await res.json();
    setLoad(false);
    router.push({ pathname: `/furniturebanks/${_id}`, query: { info } });
  };

  const validateNext = (formValues) => {
    let rev = Object.values(formValues);
    passArray = rev.map((value, i) => {
      if (
        value === "" ||
        !formValues.email.match(emailRegx) ||
        !formValues.phone.match(phoneRegx) ||
        !formValues.state.match(textRegx) ||
        !formValues.zip.match(zipRegx) ||
        !formValues.city.match(textRegx)
      ) {
        return false;
      } else if (value === "string" || Number) {
        return true;
      }
    });
  };

  validateNext(fstepValues);
  return (
    <>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <Row style={{ marginBottom: "2em" }}>
          <Col span={12}>
            <Title level={3}>Add Furniture Bank</Title>
          </Col>
          <Col
            span={12}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingRight: "2em",
            }}
          >
            <Link href="/furniturebanks">
              <Button
                style={{ background: "#010101", color: "#ffffff" }}
                shape="circle"
                icon={<FontAwesomeIcon icon={faTimes} />}
              />
            </Link>
          </Col>
        </Row>
        <Row style={{ marginBottom: "3em" }}>
          <Col span={12} offset={6}>
            <Steps current={step}>
              <Step title="General Info" />
              <Step title="Admin Info" />
            </Steps>
          </Col>
        </Row>
        <br />
        <hr />
        <br />
        <Spin spinning={load} delay={500} tip="Adding New Furniture Bank...">
          <Form {...layout} layout="vertical" onFinish={onFinish}>
            {step === 0 && (
              <>
                <Row
                  gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
                  align="middle"
                  justify="space-around"
                >
                  <Col span={24}>
                    <Form.Item
                      label="Name:"
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Name is required.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        name="name"
                        onChange={handleChange}
                        placeholder="Furniture Bank Name"
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row type="flex">
                  <Col span={10}>
                    <Form.Item
                      label="Address:"
                      name="address"
                      rules={[
                        {
                          required: true,
                          message: "Address is required.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        name="address"
                        onChange={handleChange}
                        placeholder="Address"
                      />
                    </Form.Item>
                  </Col>
                  <Col
                    span={12}
                    offset={2}
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Col xxl={7} xl={7} lg={8} md={8}>
                      <Form.Item
                        label="Zip Code:"
                        name="zip"
                        min="0"
                        rules={[
                          {
                            required: true,
                            message: "Zip code is required.",
                          },
                          {
                            pattern: zipRegx,
                            message: "Enter a valid zip code.",
                          },
                        ]}
                      >
                        <Input
                          size="large"
                          name="zip"
                          min="0"
                          maxLength="5"
                          onChange={handleChange}
                          placeholder="Zip Code"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={7}>
                      <Form.Item
                        label="City:"
                        name="city"
                        rules={[
                          {
                            required: true,
                            message: "City is required.",
                          },
                          {
                            pattern: textRegx,
                            message: "Please enter a valid City name.",
                          },
                        ]}
                      >
                        <Input
                          size="large"
                          name="city"
                          onChange={handleChange}
                          placeholder="City"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={7}>
                      <Form.Item
                        label="State: "
                        name="state"
                        rules={[
                          {
                            required: true,
                            message: "State is required.",
                          },
                          {
                            pattern: textRegx,
                            message: "Please enter a valid State name.",
                          },
                        ]}
                      >
                        <Input
                          size="large"
                          name="state"
                          onChange={handleChange}
                          placeholder="State"
                        />
                      </Form.Item>
                    </Col>
                  </Col>
                </Row>
                <Row style={{ marginBottom: "2em" }}>
                  <Col span={10}>
                    <Form.Item
                      label="Phone Number:"
                      name="phone"
                      rules={[
                        {
                          required: true,
                          message: "Phone number is required.",
                        },
                        {
                          pattern: phoneRegx,
                          message: "please enter a valid phone number.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        name="phone"
                        onChange={handleChange}
                        maxLength="10"
                        placeholder="555-123-4444"
                        required
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12} offset={2}>
                    <Form.Item
                      label="E-mail:"
                      name="email"
                      rules={[
                        {
                          required: true,
                          message: "Email is required.",
                        },
                        {
                          pattern: emailRegx,
                          message: "Please enter a valid email",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        required
                        name="email"
                        onChange={handleChange}
                        placeholder="furniture@bank.com"
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col
                    offset={18}
                    span={6}
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Button
                      onClick={() => setStep(step + 1)}
                      style={{
                        backgroundColor: "white",
                        height: "40px",
                        color: "#69c0ff",
                        border: "1px solid #69c0ff",
                        borderRadius: "3px",
                        borderwidth: "1px",
                        display: "inline-block",
                        textAlign: "center",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                      }}
                      disabled={!passArray.every(Boolean)}
                    >
                      Next &nbsp;{" "}
                      <FontAwesomeIcon
                        style={{ color: "#69c0ff" }}
                        icon={faChevronRight}
                      />
                    </Button>
                  </Col>
                </Row>
              </>
            )}
            {step === 1 && (
              <>
                <Row>
                  <Col span={10}>
                    <Form.Item
                      label="Admin First Name:"
                      name="firstName"
                      rules={[
                        {
                          required: true,
                          message: "First Name is required.",
                        },
                      ]}
                    >
                      <Input size="large" placeholder="First Name" />
                    </Form.Item>
                  </Col>
                  <Col span={12} offset={2}>
                    <Form.Item
                      label="Admin Last Name:"
                      name="lastName"
                      rules={[
                        {
                          required: true,
                          message: "Last Name is required.",
                        },
                      ]}
                    >
                      <Input size="large" placeholder="Last Name" />
                    </Form.Item>
                  </Col>
                </Row>
                <Row style={{ marginBottom: "2em" }}>
                  <Col span={10}>
                    <Form.Item
                      label="Phone Number:"
                      name="adminPhone"
                      rules={[
                        { required: true, message: "Phone is required." },
                        {
                          pattern: phoneRegx,
                          message: "please enter a valid phone number.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        maxLength="10"
                        placeholder="555-123-4444"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12} offset={2}>
                    <Form.Item
                      label="E-mail:"
                      name="adminEmail"
                      rules={[
                        {
                          required: true,
                          message: "Email is required.",
                        },
                        {
                          pattern: emailRegx,
                          message: "Please enter a valid email",
                        },
                      ]}
                    >
                      <Input size="large" placeholder="furniture@admin.com" />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col span={5}>
                    <Button
                      style={{
                        backgroundColor: "white",
                        height: "40px",
                        color: "#69c0ff",
                        border: "1px solid #69c0ff",
                        borderRadius: "3px",
                        borderwidth: "1px",
                        display: "inline-block",
                        textAlign: "center",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                      }}
                      onClick={() => setStep(0)}
                    >
                      <FontAwesomeIcon
                        style={{ color: "#69c0ff" }}
                        icon={faChevronLeft}
                      />{" "}
                      &nbsp; Previous
                    </Button>
                  </Col>
                  <Col
                    offset={14}
                    span={5}
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Button
                      htmlType="submit"
                      style={{
                        backgroundColor: "#1890ff",
                        height: "40px",
                        color: "white",
                        border: "1px solid #1890ff",
                        borderRadius: "3px",
                        borderwidth: "1px",
                        display: "inline-block",
                        textAlign: "center",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                      }}
                      disabled={!passArray.every(Boolean)}
                    >
                      <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                    </Button>
                  </Col>
                </Row>
              </>
            )}
          </Form>
        </Spin>
      </Content>
    </>
  );
}

export default AddFB;
