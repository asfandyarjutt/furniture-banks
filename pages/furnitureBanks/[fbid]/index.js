import React from "react";
import { useRouter } from "next/router";
import DetailFB from "../../../components/DetailFB";

function FurnitureDetail() {
  const router = useRouter();
  const { fbid } = router.query;
  return !fbid ? null : <DetailFB fbid={fbid} />;
}

export default FurnitureDetail;
