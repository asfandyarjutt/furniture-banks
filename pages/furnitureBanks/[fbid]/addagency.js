import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import {
  Card,
  Button,
  Row,
  Col,
  Typography,
  Input,
  message as info,
} from "antd";
import { usePartnerAgencies, useOneFB } from "../../../lib/hooks";
import AddAgencyTable from "../../../components/AddAgencyTable";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faChevronLeft,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import { use } from "passport";

const { Title } = Typography;
function addagency() {

  const router = useRouter();
  const { fbid, page } = router.query;
  const [furnitureBank, mutate, trigger] = useOneFB(fbid);
  const [keyword, setKeyword] = useState("")
  const [partnerAgencies, curPage, maxPage, perPage] = usePartnerAgencies(page, keyword, 'displayall');
  const [data, setData] = useState([]);
  const [search, setSearch] = useState("");
  const [searchItems, setSearchItems] = useState([]);
  const [selectedAgencies, setSelectedAgencies] = useState([]);
  useEffect(() => {
    if (partnerAgencies) {
      const mapData = partnerAgencies.map((pa) => ({ ...pa, key: pa._id }));
      setData(mapData);
    }
  }, [partnerAgencies]);
  function handleSearch(value) {
    setSearch(value);
    setSearchItems(
      data.filter((el) => el.name.toLowerCase()?.includes(value.toLowerCase()))
    );
  }

  async function addAgencies() {
    mutate(
      `/api/fba/${fbid}`,
      [...furnitureBank?.partnerAgencies, selectedAgencies],
      false
    );
    const res = await fetch(`/api/fba/${fbid}/agencies`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ agencies: selectedAgencies }),
    });
    const { message } = await res.json();
    trigger(`/api/fba/${fbid}`);
    if (res.status === 200) {
      info.success(`${message}`);
    }
  }


  return (
    <Card>
      <Row align="middle">
        <Col span={6}>
          <Title level={3}>Add Partner Agency</Title>
        </Col>
        <Col
          span={6}
          offset={12}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          <Button
            style={{
              backgroundColor: "white",
              height: "40px",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
            onClick={() => router.back()}
          >
            <FontAwesomeIcon icon={faChevronLeft} /> &nbsp; Back to Furniture
            Bank Profile
          </Button>
        </Col>
      </Row>
      <br />
      <hr />
      <br />
      <Row>
        <Col span={12} style={{ paddingRight: "50px" }}>
          Choose existing Partner Agencies to add to Furniture Bank.
        </Col>
        <Col style={{ paddingLeft: "10px" }} span={2}>
          <Button
            style={{
              backgroundColor: "#52c41a",
              height: "40px",
              color: "white",
              border: "1px solid #52c41a",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
            block
            onClick={addAgencies}
          >
            <FontAwesomeIcon icon={faPlus} /> &nbsp; Add
          </Button>
        </Col>
        <Col style={{ paddingLeft: "10px" }} span={10}>
          <Input
            prefix={<FontAwesomeIcon icon={faSearch} />}
            placeholder="Search"
            style={{ width: "100%", height: "40px" }}
            onChange={(e) => handleSearch(e.target.value)}
          />
        </Col>
      </Row>
      <br />
      <AddAgencyTable
        data={search ? searchItems : data}
        setAgencies={setSelectedAgencies}
        curPage={curPage}
        maxPage={maxPage}
        perPage={perPage}
        page={page}
      />

    </Card>
  );
}

export default addagency;
