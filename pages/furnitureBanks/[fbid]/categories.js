import React, { useState, useEffect } from "react";
import { Card, Form, Input, Button, Typography, Row, Col, message } from "antd";
import {
  faCheck,
  faPlus,
  faTimes,
  faEdit,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { useCurrentUser } from "../../../lib/hooks";
import { useRouter } from "next/router";

const { Title } = Typography;
const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offset: 0 },
    sm: { span: 20, offset: 4 },
  },
};

function categories() {
  const [cUser] = useCurrentUser();
  const [allCategories, setAllCategories] = useState([]);
  const [previousCat, setPreviousCat] = useState([]);
  const [deletedCat, setDeletedCat] = useState([]);
  const [edit, setEdit] = useState({ edit: false, index: null });
  const [value, setValue] = useState("");
  useEffect(() => {
    setAllCategories(cUser?.furnitureBank?.categories || []);
  }, [cUser]);
  const router = useRouter();

  const onFinish = async ({ categories = [] }) => {
    await fetch(`/api/fba/${cUser.furnitureBank._id}/category`, {
      method: "PUT",
      header: { "Content-Type": "application/json" },
      body: JSON.stringify({
        newCategories: [...allCategories, ...categories],
        previousCat,
        deletedCat,
      }),
    });
    message.success("Changes Saved");
    router.push("/dashboard");
  };

  const removeExisting = (index) => {
    setDeletedCat(allCategories.filter((cat, catIndex) => catIndex === index));
    setAllCategories(
      allCategories.filter((cat, catIndex) => catIndex !== index)
    );
  };

  const editExisting = (index) => {
    let cat = allCategories;
    if (cat[index] !== value)
      setPreviousCat([...previousCat, { previous: cat[index], new: value }]);
    cat[index] = value;
    setAllCategories(cat);
  };

  return (
    <Card>
      <Row justify="end" align="middle" style={{ marginBottom: "2em" }}>
        <Col span={12}>
          <Title level={3}>Edit Categories</Title>
        </Col>
        <Col
          span={12}
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingRight: "2em",
          }}
        >
          <Link href="/dashboard">
            <Button
              style={{ background: "#010101", color: "#ffffff" }}
              shape="circle"
              icon={<FontAwesomeIcon icon={faTimes} />}
            />
          </Link>
        </Col>
      </Row>
      {allCategories?.map((cat, i) => (
        <Row key={i}>
          <Col span={6}>
            <p key={i} style={{ display: "flex" }}>
              <span>{i + 1}.</span>&nbsp;{" "}
              {edit.edit && edit.index === i ? (
                <Input
                  value={value}
                  onChange={(e) => setValue(e.target.value)}
                />
              ) : (
                cat
              )}{" "}
            </p>
          </Col>
          {edit.index === i ? (
            <Col span={2} style={{ marginLeft: 20 }}>
              <FontAwesomeIcon
                icon={faCheck}
                style={{ color: "#52C41A", fontSize: 18 }}
                onClick={() => {
                  editExisting(i);
                  setEdit({ edit: false, index: null });
                  setValue("");
                }}
              />
            </Col>
          ) : (
            <Col span={2} style={{ marginLeft: 20 }}>
              <FontAwesomeIcon
                icon={faEdit}
                style={{ color: "#0050B3", fontSize: 18 }}
                onClick={() => {
                  setEdit({ edit: true, index: i });
                  setValue(cat);
                }}
              />
            </Col>
          )}
          <Col span={2}>
            <FontAwesomeIcon
              icon={faTrashAlt}
              style={{ color: "#F5222D", fontSize: 18 }}
              onClick={() => {
                removeExisting(i);
              }}
            />
          </Col>
        </Row>
      ))}
      <Form
        style={{ marginTop: "3em" }}
        name="dynamic_form_item"
        {...formItemLayoutWithOutLabel}
        onFinish={onFinish}
      >
        <Form.List name="categories">
          {(fields, { add, remove }) => {
            return (
              <div>
                {fields.map((field) => (
                  <Row key={field.key}>
                    <Col span={6}>
                      <Form.Item label="New category" required={false}>
                        <Form.Item
                          {...field}
                          validateTrigger={["onChange", "onBlur"]}
                          rules={[
                            {
                              required: true,
                              whitespace: true,
                              message: "Enter New Category.",
                            },
                          ]}
                          noStyle
                        >
                          <Input size="large" placeholder="Category" />
                        </Form.Item>
                      </Form.Item>
                    </Col>
                    <Col span={6}>
                      {fields.length > 1 ? (
                        <div
                          className="dynamic-delete-button"
                          style={{ margin: "0 8px" }}
                          onClick={() => {
                            remove(field.name);
                          }}
                        >
                          <Button
                            className="dynamic-delete-button"
                            onClick={() => {
                              remove(field.name);
                            }}
                          >
                            <FontAwesomeIcon icon={faTimes} /> &nbsp; Discard
                            Category
                          </Button>
                        </div>
                      ) : null}
                    </Col>
                  </Row>
                ))}
                <Row style={{ marginTop: "3em" }}>
                  <Col span={6}>
                    <Form.Item>
                      <Button
                        style={{
                          backgroundColor: "#52c41a",
                          height: "40px",
                          color: "white",
                          border: "1px solid #52c41a",
                          borderRadius: "3px",
                          borderwidth: "1px",
                          display: "inline-block",
                          textAlign: "center",
                          paddingLeft: "20px",
                          paddingRight: "20px",
                        }}
                        onClick={() => {
                          add();
                        }}
                      >
                        <FontAwesomeIcon icon={faPlus} /> &nbsp; Add Category
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </div>
            );
          }}
        </Form.List>
        <Form.Item>
          <Row justify="end">
            <Col
              offset={12}
              xl={6}
              lg={6}
              md={6}
              style={{ display: "flex", justifyContent: "flex-end" }}
            >
              <Button
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#f5222d",
                  border: "1px solid #f5222d",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
                onClick={() => router.back()}
              >
                <FontAwesomeIcon icon={faTimes} /> &nbsp; Discard Changes
              </Button>
            </Col>
            <Col
              xl={4}
              lg={4}
              md={6}
              style={{ display: "flex", justifyContent: "flex-end" }}
            >
              <Button
                style={{
                  backgroundColor: "#1890ff",
                  height: "40px",
                  color: "white",
                  border: "1px solid #1890ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
                htmlType="submit"
              >
                <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
              </Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>
    </Card>
  );
}

export default categories;
