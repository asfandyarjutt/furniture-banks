import React from "react";
import { useRouter } from "next/router";
import AddManager from "../../../components/agency/AddManager";
function newadmin() {
  const router = useRouter();
  const {
    query: { fbid },
  } = router;
  return <AddManager agencyId={fbid} fba />;
}

export default newadmin;
