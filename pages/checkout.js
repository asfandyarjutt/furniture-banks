import React, { useEffect, useState, useRef } from "react";
import { useCurrentUser, useOneAgency } from "../lib/hooks";
import { phoneRegx } from "../constants";
import { useRouter } from "next/router";
import { useCartValue } from "../lib/state";
import { useTimes } from "../lib/hooks";
import {
  Typography,
  Card,
  Row,
  Col,
  Button,
  Table,
  Steps,
  Radio,
  Form,
  Input,
  message,
  Alert,
  Spin,
  Tooltip,
  Modal,
} from "antd";
import { faCheck, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import {
  deliveryQuestion,
  deliveryTitle,
  deliveryToolTip,
  pickUpQuestion,
  pickUpTitle,
  pickUpToolTip,
  stepOne,
  stepOneTitle,
  stepThree,
  stepTwo,
  stepZeroTitle,
  title,
} from "../constants";
import HeartLand from "../lib/heartLand";
import { use } from "passport";
import { getLocal, removeLocal, setLocal } from "../components/Forms/functions";
const { Title, Text } = Typography;
const { Step } = Steps;

function checkout() {
  const [currentUser] = useCurrentUser();
  const [agency] = useOneAgency(currentUser?.partnerAgency);
  const [form] = Form.useForm();
  const [times] = useTimes();
  const [scheduleOpt, setScheduleOpt] = useState({
    selfHaul: [],
    delivery: [],
  });
  const [{ final }, dispatch] = useCartValue();
  const [contact, setContact] = useState({});
  const [payment, setPayment] = useState({});
  const [household, setHousehold] = useState({});
  const [bg, setBg] = useState({});
  const [items, setItems] = useState([]);
  const [authorizedPayment, setauthorizedPayment] = useState({});
  let values;
  let role = currentUser?.role;
  useEffect(() => {
    values = getLocal("State");
    if (values) {
      setItems(values.items);
      setContact(values.contact),
        setHousehold(values.household),
        setBg(values.bg);
      setPayment(values.payment);
      setauthorizedPayment(values.authorizedPayment);
    }
  }, []);

  let _items = items.map((el, i) => ({ ...el, key: i }));
  const [total, setTotal] = useState(0);
  const [step, setStep] = useState(0);
  const today = moment().format("MMMM Do, YYYY");
  const [enabled, setEnabled] = useState(false);
  const [payed, setPayed] = useState(true);
  const [valid, setValid] = useState(true);
  const [spining, setSpin] = useState(false);
  const [schedule, setSchedule] = useState({});
  const [radioValue, setRadioValue] = useState(null);
  const router = useRouter();
  const paypalRef = useRef();
  const [modal, setModal] = useState(false);
  const [paymentError, setPaymentError] = useState(null);
  const phoneRegex = /^(?=.{10}$)([0-9]{3})([0-9]{3})([0-9]{4})/;
  const [deliveryAlert, setDeliveryAlert] = useState([]);
  const [selfHaulAlert, setSelfHaulAlert] = useState([]);

  if (payment.paymentType) {
    const paymentInfo = payment?.paymentType?.split(" ");
    _items.push({
      name: paymentInfo[1] + " " + paymentInfo[2],
      key: _items.length + 1,
      amount: paymentInfo[0].substr(1),
    });
  }
  const covertDayFormateOfSchedule = (schedule) => {
    let days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    for (const keySch in schedule) {
      schedule[keySch] = schedule[keySch].map((date) => {
        let day = moment(date);
        day = day.day();

        return { date, day: days[day] };
      });
    }
    return schedule;
  };

  const mergeScheduleDate = (times, schedule) => {
    for (const key in times) {
      times[key] = times[key]?.map((day) => {
        const scheduleDay = schedule[key]?.find(
          (date) => date?.day === day?.day
        );
        day = { ...day, date: scheduleDay?.date };
        return day;
      });
    }
    return times;
  };
  const setScheduleMapper = () => {
    let selfHaulTime = times && times.selfHaul;
    let deliveryTime = times && times.delivery;
    let time = { selfHaul: selfHaulTime, delivery: deliveryTime };
    const sch = mergeScheduleDate(
      time,
      covertDayFormateOfSchedule(scheduleOpt)
    );
    let { delivery, selfHaul } = sch;
    let days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];
    delivery = delivery?.sort((a, b) => a.date - b.date);
    selfHaul = selfHaul?.sort((a, b) => a.date - b.date);

    const filterDays = (workingDays, currentDay) => {
      let days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
      const daysSchedule = [];
      const startDayIndex =
        days.indexOf(currentDay) !== -1 ? days.indexOf(currentDay) + 1 : 0;
      let flag = true;
      let i = startDayIndex;
      while (flag) {
        if (i === days.length) i = 0;

        daysSchedule.push(days[i]);

        workingDays--;
        if (!workingDays) flag = false;
        i++;
      }

      return daysSchedule;
    };

    let deliveryDaysList = filterDays(
      Number(times?.deliveryDays),
      days[new Date().getDay()]
    );
    let selfHaulDaysList = filterDays(
      Number(times?.selfHaulDays),
      days[new Date().getDay()]
    );

    delivery = delivery?.map((deliveryDay) => {
      let day = deliveryDaysList?.find((day) => deliveryDay.day === day);
      if (day) return deliveryDay;
    });
    delivery = delivery?.filter((d) => d);

    selfHaul = selfHaul?.map((selfHaulDay) => {
      let day = selfHaulDaysList?.find((day) => selfHaulDay.day === day);
      if (day) return selfHaulDay;
    });
    selfHaul = selfHaul?.filter((d) => d);

    const data = { delivery, selfHaul };
    setSchedule(data);
  };

  useEffect(() => {
    if (_items) {
      _items.forEach((el) => {
        setTotal(Number(el.amount) + total);
      });
    }
  }, [items]);

  useEffect(() => {
    setValid(
      step === 0
        ? true
        : (step === 1 && payment.agencyPaymentType !== "heartland") ||
          Object.keys(authorizedPayment).length > 0
        ? true
        : false
    );
  }, [step]);

  useEffect(() => {
    function nextWeekdayDate(date, day_in_week) {
      var ret = new Date(date || new Date());
      ret.setDate(
        ret.getDate() + ((day_in_week - 1 - ret.getDay() + 7) % 7) + 1
      );

      return [ret];
    }
    if (times) {
      function getDays(days, type) {
        if (!days) return;
        return type
          ? days.map(({ day }) => {
              return day === "Monday"
                ? 1
                : day === "Tuesday"
                ? 2
                : day === "Wednesday"
                ? 3
                : day === "Thursday"
                ? 4
                : day === "Friday"
                ? 5
                : day === "Saturday"
                ? 6
                : 7;
            })
          : Object.keys(days).map((day) =>
              day === "Monday"
                ? 1
                : day === "Tuesday"
                ? 2
                : day === "Wednesday"
                ? 3
                : day === "Thursday"
                ? 4
                : day === "Friday"
                ? 5
                : day === "Saturday"
                ? 6
                : 7
            );
      }
      const selfDays = getDays(times.selfHaul, true);
      if (times) {
        Object?.entries(times?.delivery).forEach((el) => {
          let e = el[1].zip.includes(contact.zip);
          if (!e) {
            delete times.delivery[el[0]];
          }
        });
      }
      let deliveryDays = getDays(times.delivery, true);
      deliveryDays = deliveryDays.filter((day) => day);
      if (selfDays && deliveryDays) {
        setScheduleOpt({
          delivery: deliveryDays
            .map((day) => nextWeekdayDate(new Date(), day))
            .flat()
            .sort((a, b) => new Date(a) - new Date(b)),
          selfHaul: selfDays
            .map((day) => nextWeekdayDate(new Date(), day))
            .flat()
            .sort((a, b) => new Date(a) - new Date(b)),
        });
      }
    }
  }, [times]);

  useEffect(() => {
    setScheduleMapper();
    let local_storage = localStorage.getItem("user_Id");
    if (!currentUser && !local_storage) {
      router.push("/");
    }
  }, [scheduleOpt]);

  useEffect(() => {
    const deliveryAlert = schedule?.delivery?.map((day) => {
      if (day.zip.includes(contact.zip)) {
        return `${day.day} from ${day.start} to ${day.end}, Zip Code: ${day.zip}`;
      }
    });
    setDeliveryAlert(deliveryAlert);
    const selfHaulAlert = schedule?.selfHaul?.map(
      (day) => `${day.day} from ${day.start} to ${day.end}`
    );
    setSelfHaulAlert(selfHaulAlert);
  }, [schedule]);

  const columns = [
    {
      title: "Items",
      dataIndex: "name",
    },
    {
      title: "SKU",
      dataIndex: "sku",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
    },
    {
      title: "Amount",
      dataIndex: "amount",
    },
  ];

  async function heartLandToken(values) {
    await dispatch({ type: "AUTHORIZED", payload: values });
    setModal(false);
    if (values?.token_value && values?.exp_month && values?.exp_year)
      setauthorizedPayment(values);
    setValid(true);
  }

  function heartLandError(value) {
    if (value) setPaymentError(value);
  }

  useEffect(() => {
    setInterval(() => {
      setPaymentError(null);
    }, 5000);
  }, []);

  function finish(values) {
    let data = values;
    let date = data?.date?.replace(/,[^,]+$/, "");
    data = { ...data, date: date };
    dispatch({ type: "FINAL", payload: data });
    message.success("Information Saved");
    setValid(true);
  }
  async function sendOrder() {
    setSpin(true);
    let data = {
      items,
      contact,
      role,
      household,
      bg,
      payment,
      final,
      authorizedPayment,
    };
    let { day, start, end, limit, date, zip } = radioValue;
    limit = Number(limit) - 1;
    limit = limit.toString();
    let payload = zip
      ? { day, start, end, limit, zip }
      : { day, start, end, limit };

    let res = await fetch("/api/order", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });

    if (limit) {
      await fetch("api/times", {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(payload),
      });
    }
    const { message: resMessage } = await res.json();

    if (res.status === 504) {
      setValid(false);
      setStep(1);
    }
    if (res.status !== 200) {
      setSpin(false);
      message.warning(`Cannot create order... ${resMessage}`);
    } else {
      message.success(resMessage);
      removeLocal("State");
      dispatch({ type: "RESET" });
      dispatch({ type: "RESET_CART" });
      setSpin(false);
      router.push("/thankyou");
    }
  }

  console.log("payment", authorizedPayment);
  // async function CreateOrder(){
  // if()
  //   hearlandPay
  // }
  async function hearlandPay() {
    const data = { authorizedPayment };
    let res = await fetch("/api/heartest", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    const { _id, resMessage } = await res.json();
    console.log("id", _id);
  }
  return (
    <>
      <Row>
        <Title level={3}>{currentUser?.furnitureBank?.name}</Title>
      </Row>
      <Row style={{ marginBottom: "2em" }}>
        <Button
          style={{
            backgroundColor: "white",
            height: "40px",
            color: "#69c0ff",
            border: "1px solid #69c0ff",
            borderRadius: "3px",
            borderWidth: "1px",
            display: "inline-block",
            textAlign: "center",
            paddingLeft: "20px",
            paddingRight: "20px",
          }}
          onClick={() => router.back()}
          type="primary"
          size="large"
        >
          {" "}
          <FontAwesomeIcon
            style={{ marginRight: "6px" }}
            icon={faChevronLeft}
          />{" "}
          Continue Shopping
        </Button>
      </Row>
      <Card>
        <Title level={3} style={{ marginBottom: "2em" }}>
          {title}
        </Title>
        <Row style={{ padding: "0 7% 0 7%" }}>
          <Steps current={step}>
            <Step title={stepOne} />
            <Step title={stepTwo} />
            <Step title={stepThree} />
          </Steps>
          <Col span={24} style={{ margin: "2rem 0" }}>
            <hr />
          </Col>
          <div>
            <center>
              {step === 0 && (
                <Title
                  level={4}
                  style={{ marginBottom: "4rem", marginTop: "3rem" }}
                >
                  {stepZeroTitle}
                </Title>
              )}
              {step === 1 && (
                <Title
                  level={4}
                  style={{ marginBottom: "4rem", marginTop: "3rem" }}
                >
                  {stepOneTitle}
                </Title>
              )}
            </center>
          </div>
          <br />
          {step === 0 ? (
            <>
              <Col span={12}>
                <Title level={4}>Order:Processing</Title>
              </Col>

              <Col span={24}>
                <p>
                  <b>Date: </b>
                  {today}
                </p>
              </Col>
              <Col span={12}>
                <Row>
                  <Col span={12}>
                    <b>Client:</b>
                  </Col>
                  <Col span={12}>{contact?.clientName}</Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <b>Contact:</b>
                  </Col>
                  <Col span={12}>
                    {contact?.address},{contact?.city},{contact?.zip}
                    <br />
                    {contact?.clientEmail}
                    <br />
                    {contact?.clientPhone}
                  </Col>
                </Row>
              </Col>
              <Col span={12}>
                <Row>
                  <Col span={12}>
                    <b>Partner Agency:</b>
                  </Col>
                  <Col span={12}>{agency?.name}</Col>
                  <Col span={12}>
                    <b>Case Manager:</b>
                  </Col>
                  <Col span={12}>{contact?.cmName}</Col>
                  <Col span={12}>
                    <b>Contact:</b>
                  </Col>
                  <Col span={12}>
                    {contact?.cmEmail}
                    <br />
                    {contact?.cmPhone}
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Table
                  columns={columns}
                  dataSource={_items}
                  pagination={false}
                />
              </Col>
              <Col span={24}>
                <hr />
              </Col>
              <Col span={6} offset={1}>
                <Title level={4}>Total</Title>
              </Col>

              <Col span={4} offset={13}>
                ${total}.00
              </Col>
              <Col span={5} offset={10}>
                Client Amount:{" "}
              </Col>
              <Col span={5}>{payment?.clientPaymentType}</Col>
              <Col span={4}>
                ${payment.clientPayment ? payment.clientPayment : "0"}.00
              </Col>
              <Col span={5} offset={10}>
                Agency Amount:{" "}
              </Col>
              <Col span={5}>
                {payment &&
                payment?.agencyPaymentType &&
                payment?.agencyPaymentType?.charAt(0)?.toUpperCase() +
                  payment?.agencyPaymentType?.slice(1) ===
                  "Heartland"
                  ? "Credit Card"
                  : payment?.agencyPaymentType?.charAt(0)?.toUpperCase() +
                    payment?.agencyPaymentType?.slice(1)}
              </Col>
              <Col span={4}>
                ${payment.agencyPayment ? payment.agencyPayment : "0"}.00
              </Col>
              <Col span={24}>
                <br />
              </Col>
              <Col span={24}>
                <br />
              </Col>
              <Col span={6} offset={18}>
                <Title level={4}>
                  Client Total: $
                  {payment.clientPayment ? payment.clientPayment : "0"}.00
                </Title>
              </Col>
            </>
          ) : step === 1 ? (
            <>
              <Col span={12}>
                <Title level={4}>Order:Processing</Title>
              </Col>
              <Col span={24}>
                <p>
                  <b>Date: </b>
                  {today}
                </p>
              </Col>
              <Col span={12}>
                <Row>
                  <Col span={12}>
                    <b>Client:</b>
                  </Col>
                  <Col span={12}>{contact?.clientName}</Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <b>Contact:</b>
                  </Col>
                  <Col span={12}>
                    {contact?.address}
                    {contact?.zip}
                    <br />
                    {contact?.city}
                    <br />
                    {contact?.clientEmail}
                    <br />
                    {contact?.clientPhone}
                  </Col>
                </Row>
              </Col>
              <Col span={12}>
                <Row>
                  <Col span={12}>
                    <b>Partner Agency:</b>
                  </Col>
                  <Col span={12}>{agency?.name}</Col>

                  <Col span={12}>
                    <b>Case Manager:</b>
                  </Col>
                  <Col span={12}>{contact?.cmName}</Col>
                  <Col span={12}>
                    <b>Contact:</b>
                  </Col>
                  <Col span={12}>
                    {contact?.cmEmail}
                    <br />
                    {contact?.cmPhone}
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Table
                  columns={columns}
                  dataSource={_items}
                  pagination={false}
                />
              </Col>
              <Col span={24}>
                <hr />
              </Col>
              <Col span={6} offset={1}>
                <Title level={4}>Total</Title>
              </Col>

              <Col span={4} offset={13}>
                ${total ? total : "0"}.00
              </Col>
              <Col span={5} offset={10}>
                Client Amount:{" "}
              </Col>
              <Col span={5}>{payment?.clientPaymentType}</Col>
              <Col span={4}>
                ${payment.clientPayment ? payment.clientPayment : "0"}.00
              </Col>
              <Col span={5} offset={10}>
                Agency Amount:{" "}
              </Col>
              <Col span={5}>
                {payment &&
                payment?.agencyPaymentType &&
                payment?.agencyPaymentType?.charAt(0)?.toUpperCase() +
                  payment?.agencyPaymentType?.slice(1) ===
                  "Heartland"
                  ? "Credit Card"
                  : payment?.agencyPaymentType?.charAt(0)?.toUpperCase() +
                    payment?.agencyPaymentType?.slice(1)}
              </Col>
              <Col span={4}>
                ${payment.agencyPayment ? payment.agencyPayment : "0"}.00
              </Col>
              <Col span={24}>
                <br />
              </Col>

              <Col span={6} offset={18}>
                <Title level={4}>
                  Agency Total: $
                  {payment.agencyPayment ? payment.agencyPayment : "0"}.00
                </Title>
              </Col>
              <Col span={4} offset={17}>
                {payment && payment.agencyPaymentType === "heartland" && (
                  <Button
                    type="primary"
                    size="large"
                    style={{ marginLeft: 10, marginTop: 5, width: 200 }}
                    onClick={() => setModal(true)}
                    // onClick={() => hearlandPay()}
                  >
                    Pay $
                    {payment?.agencyPayment ? payment?.agencyPayment : "0.00"}
                  </Button>
                )}
                {modal && (
                  <Modal
                    visible={modal}
                    onCancel={() => setModal(false)}
                    footer={null}
                    width={330}
                  >
                    <div>
                      <HeartLand
                        amount={
                          payment?.agencyPayment
                            ? payment?.agencyPayment
                            : "0.00"
                        }
                        heartLandToken={heartLandToken}
                        heartLandError={heartLandError}
                      />
                    </div>
                    {paymentError && (
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "center",
                        }}
                      >
                        <Text type="danger">
                          {paymentError?.error?.message}
                        </Text>
                      </div>
                    )}
                  </Modal>
                )}
              </Col>
            </>
          ) : (
            <Col span={24}>
              {payment?.paymentType?.includes("Pick Up") ? (
                <b>{pickUpTitle}</b>
              ) : (
                <b>{deliveryTitle}</b>
              )}
              <Form layout="vertical" form={form} onFinish={finish}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: "1rem",
                  }}
                >
                  <Form.Item
                    name="date"
                    label="Selection is required to complete checkout."
                    rules={[
                      {
                        required: true,
                        message: "Please Select an Option",
                      },
                    ]}
                  >
                    <Radio.Group>
                      {payment?.paymentType?.includes("Pick Up") &&
                        schedule.selfHaul.map((day, i) => (
                          <div key={i}>
                            {Number(day.limit) < 1 ? (
                              <Tooltip title={pickUpToolTip} align="top">
                                <Radio
                                  value={`${day.date}, ${day.start} ${day.end}`}
                                  disabled={Number(day.limit) < 1}
                                >
                                  {`${moment(day.date).format("dddd ll")}, ${
                                    day.start
                                  } - ${day.end}`}
                                </Radio>
                              </Tooltip>
                            ) : (
                              <Radio
                                value={`${day.date}, ${day.start} ${day.end}`}
                                disabled={Number(day.limit) < 1}
                                onChange={() => setRadioValue(day)}
                              >
                                {`${moment(day.date).format("dddd ll")}, ${
                                  day.start
                                } - ${day.end}`}
                              </Radio>
                            )}
                            <br />
                          </div>
                        ))}
                      {payment?.paymentType?.includes("Delivery") &&
                        schedule.delivery.map((day, i) => (
                          <div key={i}>
                            {Number(day.limit) < 1 ? (
                              <Tooltip title={deliveryToolTip} align="top">
                                <Radio
                                  value={`${day.date}, ${day.start} ${day.end}`}
                                  disabled={Number(day.limit) < 1}
                                >
                                  {`${moment(day.date).format("dddd ll")}, ${
                                    day.start
                                  } - ${day.end}`}
                                </Radio>
                              </Tooltip>
                            ) : (
                              <Radio
                                value={`${day.date}, ${day.start} ${day.end}`}
                                disabled={Number(day.limit) < 1}
                                onChange={() => setRadioValue(day)}
                              >
                                {`${moment(day.date).format("dddd ll")}, ${
                                  day.start
                                } - ${day.end}`}
                              </Radio>
                            )}
                            <br />
                          </div>
                        ))}
                    </Radio.Group>
                  </Form.Item>
                  <Alert
                    style={{ width: 500, height: "fit-content" }}
                    type="info"
                    message={`${
                      payment?.paymentType?.includes("Pick Up")
                        ? "Pick Up"
                        : "Delivery"
                    } Schedule Options`}
                    showIcon
                    description={
                      payment?.paymentType?.includes("Pick Up")
                        ? selfHaulAlert?.map((time, i) => (
                            <div key={i}>{time}</div>
                          ))
                        : deliveryAlert?.map((time, i) => (
                            <div key={i}>{time}</div>
                          ))
                    }
                  />
                </div>
                {payment?.paymentType?.includes("Pick Up") ? (
                  <b>{pickUpQuestion}</b>
                ) : (
                  <b>{deliveryQuestion}</b>
                )}
                <Form.Item
                  name="name"
                  label="Name:"
                  rules={[{ required: true, message: "Name is Required" }]}
                >
                  <Input placeholder="John Smith" />
                </Form.Item>
                <Form.Item
                  name="phone"
                  label="Phone:"
                  rules={[
                    {
                      required: true,
                      message: "Phone is required",
                    },
                    {
                      pattern: phoneRegx,
                      message: "Enter Valid Phone Number",
                    },
                    {},
                  ]}
                >
                  <Input placeholder="555-123-4444" />
                </Form.Item>
                <Form.Item>
                  <Button
                    htmlType="submit"
                    style={{
                      backgroundColor: "#1890ff",
                      height: "40px",
                      color: "white",
                      border: "1px solid #1890ff",
                      borderRadius: "3px",
                      borderWidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                  >
                    <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                  </Button>
                </Form.Item>
              </Form>
            </Col>
          )}
          <Col span={24}>
            <br />
          </Col>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              width: "95%",
            }}
          >
            <div>
              <Button
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#69c0ff",
                  border: "1px solid #69c0ff",
                  borderRadius: "3px",
                  borderWidth: "1px",
                  display: step === 0 ? "none" : "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
                onClick={() => (step === 0 ? null : setStep(step - 1))}
              >
                <FontAwesomeIcon icon={faChevronLeft} /> &nbsp; Previous
              </Button>
            </div>

            <div>
              <Spin spinning={spining} size="large" tip="Creating order...">
                <Button
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#69c0ff",
                    border: "1px solid #69c0ff",
                    borderRadius: "3px",
                    borderWidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                  onClick={() =>
                    step === 2
                      ? sendOrder().then(() => {
                          localStorage.clear();
                          localStorage.setItem("user_Id", currentUser._id);
                        })
                      : setStep(step + 1)
                  }
                  disabled={!valid}
                >
                  {step === 0
                    ? "Confirm Client Payment"
                    : step === 1
                    ? "Confirm Agency Payment"
                    : "Complete Order"}
                </Button>
              </Spin>
            </div>
          </div>
        </Row>
      </Card>
    </>
  );
}

export default checkout;
