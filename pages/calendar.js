import React from "react";
import { Row, Col, Alert } from "antd";
import Calendar from "../components/Calendar";
import CalendarCard from "../components/Calendar/CalendarCard";
import { useTimes } from "../lib/hooks";

function calendar() {
  const [times] = useTimes();
  return (
    <Row gutter={[24, 24]}>
      <Col span={24}>
        <Calendar />
      </Col>
      <Col span={12}>
        <CalendarCard
          type="Self Haul"
          data={times?.selfHaul}
          selectedDays={times?.selfHaulDays}
        />
      </Col>

      <Col span={12}>
        <CalendarCard
          type="Delivery"
          data={times?.delivery}
          selectedDays={times?.deliveryDays}
        />
      </Col>
    </Row>
  );
}

export default calendar;
