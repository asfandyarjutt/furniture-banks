import React, { useEffect, useState } from "react";
import { Layout, Row, Table, Typography, Input, Spin, Col, Form } from "antd";
import Router from "next/router";
import { currentUser, useCurrentUser } from "../lib/hooks";
import Link from "next/link";
import moment from "moment";
import { Field, Role, Order_Text } from "../constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import ReactPaginate from "react-paginate";
import { useRouter } from "next/router";

const { Content } = Layout;
const { Text, Title } = Typography;
const index = () => {
  const [currentUser] = useCurrentUser();

  const [orders, setOrders] = useState([]);
  const [spinIn, setSpin] = useState(false);
  const [searched, setSearch] = useState("");
  const [curentPage, setCurPage] = useState();
  const [maxPages, setmaxPage] = useState();
  const [dispTable, setDispTable] = useState("all");
  const [search, setSearchResult] = useState("");
  const router = useRouter();
  const { page, keyword } = router.query;
  async function getOrders(keywrd = null) {
    setSpin(true);
    let url = `/api/order?page=${page}`;
    if (keywrd) {
      url = `/api/order?page=1&keyword=${keywrd}`;
      setSpin(false);
    }
    const getorders = await fetch(url, {
      method: "GET",
      headers: { "Conent-Type": "application/json" },
    });

    const { orders, curPage, maxPage } = await getorders.json();

    var filtered = orders.filter(function (el) {
      return el != null;
    });
    setCurPage(curPage);
    setmaxPage(maxPage);
    setOrders(filtered);
    setSpin(false);
  }
  useEffect(() => {
    getOrders();
  }, [currentUser, page]);
  const columns = [
    {
      title: "Order No",
      dataIndex: "order",
      sorter: { compare: (a, b) => b.key - a.key },
      sortDirections: ["ascend", "descend"],
    },
    {
      title: "Date",
      dataIndex: "date",
      sorter: (a, b) => new Date(a.date) - new Date(b.date),
      sortDirections: ["ascend", "descend"],
    },
    {
      title:
        currentUser?.role === Role.FBA
          ? "Agency"
          : currentUser?.role === Role.PAL
          ? Field.Client_Name
          : currentUser?.role === Role.PACM
          ? Field.Client_Name
          : "Agency",
      dataIndex:
        currentUser?.role === Role.FBA
          ? "agency"
          : currentUser?.role === Role.PAL
          ? "client"
          : currentUser?.role === Role.PACM
          ? "client"
          : "agency",
      onFilter: (value, record) => {
        return currentUser?.role === Role.FBA
          ? record.agency.indexof(value) === 0
          : currentUser?.role === Role.PAL
          ? record.client.indexof(value) === 0
          : currentUser?.role === Role.PACM
          ? record.client.indexof(value) === 0
          : record.agency.indexof(value) === 0;
      },
      sorter: {
        compare: (a, b) => {
          return currentUser?.role === Role.FBA
            ? a.agency.length - b.agency.length
            : currentUser?.role === Role.PAL
            ? a.client.length - b.client.length
            : currentUser?.role === Role.PACM
            ? a.client.length - b.client.length
            : a.agency.length - b.agency.length;
        },
      },
      sortDirections: ["ascend", "descend"],
    },
    {
      title: currentUser?.role === Role.FBA ? "Representative" : "Case Manager",
      dataIndex: "manager",
      sorter: { compare: (a, b) => a.manager.length - b.manager.length },
      sortDirections: ["ascend", "descend"],
    },
  ];
  if (currentUser?.role === Role.FBA) {
    columns.push({
      title: "Status",
      dataIndex: "status",
    });
  }
  const data = orders
    ? orders?.map((item, idx) => {
        let tableData = {
          key: idx + 1,
          order: item.number,
          id: item._id,
          date: moment(item?.createdAt).format("dddd, MMMM D, YYYY, hh:mm a"),
          client: item?.clientName,
          agency: item?.agency?.name,
          manager: item.cmName,
          status:
            item.status === "Completed" ? (
              <span
                style={{
                  backgroundColor: "#80DAA2",
                  padding: "8px",
                  color: "white",
                  borderRadius: 5,
                  fontWeight: "bold",
                }}
              >
                {item.status}
              </span>
            ) : (
              <span
                style={{
                  backgroundColor: "#FFC300 ",
                  padding: "8px",
                  color: "white",
                  borderRadius: 5,
                  fontWeight: "bold",
                }}
              >
                {item.status}
              </span>
            ),
        };
        return tableData;
      })
    : null;
  const searchResult = data.filter((obj) => {
    obj["date"] = new Date(obj.date).getTime();
    return Object.keys(obj).some((key) => {
      if (typeof obj[key] === "string") {
        return obj[key].toLowerCase().includes(searched?.toLowerCase());
      } else {
        return false;
      }
    });
  });
  const searchedAt = searchResult.map((data) => {
    let date = new Date(data.date);
    data.date = date.toUTCString();
    return data;
  });
  const handlePagination = (page) => {
    const path = router.pathname;
    const query = router.query;
    query.page = page.selected + 1;
    router.push({
      pathname: path,
      query: query,
    });
  };

  return (
    <div>
      <Row style={{ marginBottom: "3me", padding: " 2em 2em 2em 0" }}>
        <Text>{Order_Text}</Text>
      </Row>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <Row justify="end">
          <Col xxl={12} xl={12} lg={18} md={24} sm={24}>
            <Input
              size="large"
              placeholder="Search by order number, agency name, or case manager."
              prefix={<FontAwesomeIcon icon={faSearch} />}
              style={{ marginBottom: "2em" }}
              onChange={(e) => {
                setSearchResult(e.target.value);
                getOrders(e.target.value);
              }}
            />
          </Col>
        </Row>
        <Spin tip="Loading..." spinning={spinIn}>
          {data.length <= 0 ? (
            <div>
              <h3>No Record Found</h3>
            </div>
          ) : (
            <Table
              tableLayout="fixed"
              columns={columns}
              pagination={false}
              onRow={(record, recordIndex) => ({
                onClick: (event) => {
                  Router.push(`/orders/[orderId]`, `/orders/${record.id}`);
                },
              })}
              style={{ cursor: "pointer" }}
              dataSource={
                dispTable === "all"
                  ? data
                  : null || dispTable === "search"
                  ? searchedAt
                  : null
              }
            />
          )}

          <ReactPaginate
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            previousLabel={"previous"}
            nextLabel={"next"}
            breakLabel={"..."}
            initialPage={0}
            pageCount={maxPages}
            onPageChange={handlePagination}
            containerClassName={"paginate-wrap"}
            subContainerClassName={"paginate-inner"}
            pageClassName={"paginate-li"}
            pageLinkClassName={"paginate-a"}
            activeClassName={"paginate-active"}
            nextLinkClassName={"paginate-next-a"}
            previousLinkClassName={"paginate-prev-a"}
            breakLinkClassName={"paginate-break-a"}
          />
        </Spin>
      </Content>
    </div>
  );
};

export default index;
