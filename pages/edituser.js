import React, { useState, useEffect } from "react";
import { useCurrentUser, useOneAgency } from "../lib/hooks";
import {
  Row,
  Col,
  Typography,
  Form,
  Input,
  Button,
  message,
  Layout,
} from "antd";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { emailRegx, phoneRegx } from "../constants";
const { Content } = Layout;
const EditUser = () => {
  const [form] = Form.useForm();
  const [user] = useCurrentUser();
  const [partnerAgency] = useOneAgency(user?.partnerAgency);
  const [agency, setMyAgency] = useState("");
  const [fEditUser, setFeditUser] = useState({
    firstName: "",
    lastName: "",
    phone: "",
    email: "",
  });

  useEffect(() => {
    form.setFieldsValue({
      firstName: user?.firstName,
      lastName: user?.lastName,
      phone: user?.phone,
      email: user?.email,
    });
    setMyAgency(partnerAgency?.name);
  }, [user]);

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const { Title } = Typography;

  const handleChange = ({ target: { value, name } }) => {
    setFeditUser({
      ...fEditUser,
      [name]: value,
    });
  };

  const onFinish = async (values) => {
    const res = await fetch("/api/user", {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ ...values }),
    });
    const { infoSuccess, infoError } = await res.json();
    if (res.status === 403) {
      onFinishFailed(infoError);
    } else {
      message.success(infoSuccess);
    }
  };
  const onFinishFailed = (errorInfo) => {
    if (typeof errorInfo === "object" && errorInfo !== null) {
      errorInfo?.errorFields?.map((err) =>
        err?.errors?.map((e) => message.warning(e))
      );
    } else {
      message.warning(errorInfo);
    }
  };
  return (
    <>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <Row>
          <Col span={24}>
            <Title level={3}>My Information</Title>
          </Col>
        </Row>
        <br />
        <hr />
        <br />
        <Row style={{ marginBottom: "2em" }}>
          <Title level={3}>
            {user?.role === "FBA" ? user?.furnitureBank?.name : agency}
          </Title>
        </Row>
        <Form
          form={form}
          {...layout}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          style={{ width: "100%" }}
        >
          <Row>
            <Col span={14}>
              <Form.Item
                label="First Name:"
                name="firstName"
                style={{ width: "inherit" }}
                rules={[
                  {
                    required: true,
                    message: "First Name is required.",
                  },
                ]}
              >
                <Input
                  size="large"
                  name="firstName"
                  onChange={handleChange}
                  placeholder="First Name"
                />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item
                label="Last Name:"
                name="lastName"
                style={{ width: "inherit" }}
                rules={[
                  {
                    required: true,
                    message: "Last Name is required.",
                  },
                ]}
              >
                <Input
                  size="large"
                  name="lastName"
                  onChange={handleChange}
                  placeholder="Last Name"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={14}>
              <Form.Item
                label="Phone Number:"
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Phone Number is required.",
                  },
                  {
                    pattern: phoneRegx,
                    message: "Enter a valid Phone Number.",
                  },
                ]}
              >
                <Input
                  size="large"
                  name="phone"
                  maxLength={10}
                  onChange={handleChange}
                  placeholder="123-456-7890"
                />
              </Form.Item>
            </Col>

            {(user?.role === "FBA" || user?.role === "PAL") && (
              <Col span={10}>
                <Form.Item
                  label="Email"
                  name="email"
                  style={{ width: "inherit" }}
                  rules={[
                    {
                      required: true,
                      message: "Email is required.",
                    },
                    {
                      pattern: emailRegx,
                      message: "Enter a valid Email.",
                    },
                  ]}
                >
                  <Input
                    size="large"
                    name="email"
                    onChange={handleChange}
                    placeholder="Email"
                  />
                </Form.Item>
              </Col>
            )}
          </Row>
          <Row>
            <Col offset={18} span={6}>
              <Button
                type="primary"
                htmlType="submit"
                style={{
                  height: "40px",
                  border: "1px solid #69c0ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "flex",
                  justifyContent: "space-around",
                  alignItems: "center",
                  width: "9em",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faCheck} />
                <span>Save&nbsp;</span>
              </Button>
            </Col>
          </Row>
        </Form>
      </Content>
    </>
  );
};

export default EditUser;
