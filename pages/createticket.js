import React, { useState, useEffect } from "react";
import Router from "next/router";

import {
  Layout,
  Row,
  Col,
  Button,
  Dropdown,
  Menu,
  Table,
  Tag,
  Alert,
  Space,
  Modal,
  Input,
  Spin,
} from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faReply,
  faChevronDown,
  faPlus,
  faTrashAlt,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import { useCurrentUser, useHelpDeskTickets } from "../lib/hooks";
import moment from "moment";
const { Content } = Layout;
const { confirm } = Modal;
import { ticketDeleteModal, Role } from "../constants";
const {
  SELECTED_ROW_TITLE,
  NO_ROW_SELECTED_TITLE,
  SELECTED_ROW_CONTENT,
  NO_ROW_SELECTED_CONTENT,
  SELECTED_ROW_OK_TEXT,
  NO_ROW_SELECTED_OK_TEXT,
  SELECTED_ROW_OK_TYPE,
  NO_ROW_SELECTED_OK_TYPE,
  SELECTED_ROW_CANCEL_TEXT,
  NO_ROW_SELECTED_CANCEL_TEXT,
} = ticketDeleteModal;
const Msg_Btn_Style = {
  backgroundColor: "#52c41a",
  height: "40px",
  color: "white",
  border: "1px solid #52c41a",
  borderRadius: "3px",
  borderwidth: "1px",
  display: "inline-block",
  textAlign: "center",
  paddingLeft: "5px",
  paddingRight: "5px",
  marginRight: "5px",
};

const Createticket = () => {
  const [user] = useCurrentUser();
  const [tickets, mutate, trigger] = useHelpDeskTickets();
  const [ticketRep, setTicketRep] = useState(false);
  const [caseManag, setCaseManag] = useState([]);
  const [message, setMessage] = useState(undefined);
  const [warningmessage, setWarmessage] = useState(undefined);
  const [dispTable, setDispTable] = useState("Inbox");
  const [searched, setSearch] = useState("");
  const [fbid, setFbid] = useState("");
  const [menuID, setMenuId] = useState(1);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  async function handleReaded(ticket) {
    const ticketId = ticket._id;
    const data =
      user && user.role === Role.ADMIN
        ? { readed: true, ticketId }
        : user && user.role === Role.FBA
          ? { read: true, ticketId }
          : user && user.role === Role.PAL
            ? { read1: true, ticketId }
            : { read2: true, ticketId };
    await fetch(`/api/hdtickets`, {
      method: "PATCH",
      headers: { "Content-Type": ["application/json"] },
      body: JSON.stringify(data),
    }).then(() => {
      mutate("/api/hdtickets", [...tickets, data]);
      Router.push(`/helpdesk/[ticketId]`, `/helpdesk/${ticketId}`);
    });
  }
  const getCaseManagers = async () => {
    const admin = await fetch(`/api/users`);
    const { users } = await admin.json();

    const data = users?.filter(
      (users) =>
        users.role === Role.PACM &&
        users.furnitureBank?._id === user?.furnitureBank?._id
    );
    setCaseManag(data);
  };

  useEffect(() => {
    if (user) {
      getCaseManagers();
    }
  }, [user]);
  useEffect(() => {
    setFbid(user?.furnitureBank?._id);
    setTicketRep(true);
    setMessage(Router.query.message);
    setTicketRep(false);
    setInterval(() => {
      setMessage(undefined);
    }, 2000);
    Router.prefetch("/newticket");
  }, [user]);
  const columns = [
    {
      title: "Subject",
      dataIndex: "subject",
      sorter: { compare: (a, b) => a.createdAt - b.createdAt },
      render: (text, record) => {
        return (
          <Space onClick={() => handleReaded(record)}>
            {user?.role === Role.FBA
              ? menuID === 1 && (
                <span>
                  {record.read === true ? (
                    <Tag color="green">Read</Tag>
                  ) : record.read === false ? (
                    <Tag color="blue">Unread</Tag>
                  ) : null}
                </span>
              )
              : null}
            {user?.role === Role.PAL
              ? menuID === 1 && (
                <span>
                  {record.read1 === true ? (
                    <Tag color="green">Read</Tag>
                  ) : record.read1 === false ? (
                    <Tag color="blue">Unread</Tag>
                  ) : null}
                </span>
              )
              : null}
            {user?.role === Role.PACM
              ? menuID === 1 && (
                <span>
                  {record.read2 === true ? (
                    <Tag color="green">Read</Tag>
                  ) : record.read2 === false ? (
                    <Tag color="blue">Unread</Tag>
                  ) : null}
                </span>
              )
              : null}
            <span>
              {record.replied ? <FontAwesomeIcon icon={faReply} /> : null}
            </span>
            <span>
              {" "}
              <a>{record.title}</a>
            </span>
          </Space>
        );
      },
    },
    {
      title: "Date",
      dataIndex: "date",
      sorter: { compare: (a, b) => a.date - b.date },
    },
  ];
  const msg = tickets
    ? tickets?.map((ticket, idx) => {
      let date = moment(ticket?.createdAt).format(
        "dddd, MMMM D, YYYY, hh:mm a"
      );

      let some = {
        ...ticket,
        key: idx,
        subject: `${ticket.title}-${ticket.body.slice(0, 100)}...`,
        date: date,
      };
      return some;
    })
    : null;
  let datasent;
  let datainbox;
  let search;
  msg?.map((msg) => { });
  if (user && user.role === Role.FBA) {
    datasent = msg
      ? msg?.filter(
        (ticket) => ticket.replied === false && ticket.from.role === Role.FBA
      )
      : null;
    datainbox = msg
      ? msg?.filter(
        (ticket) =>
          (ticket.replied === true && ticket.from.role === Role.FBA) ||
          ticket.from.role === Role.PAL ||
          ticket.from.role === Role.PACM
      )
      : null;
    search = msg
      ? msg?.filter((ticket) => {
        return (
          ticket.body.toLowerCase().includes(searched.toLowerCase()) ||
          ticket.title.toLowerCase().includes(searched.toLowerCase())
        );
      })
      : null;
  }
  if (user && user.role === Role.PAL) {
    datasent = msg
      ? msg?.filter(
        (ticket) => ticket.replied === false && ticket.from.role === Role.PAL
      )
      : null;
    datainbox = msg
      ? msg?.filter(
        (ticket) =>
          (ticket.replied === true && ticket.from.role === Role.PAL) ||
          ticket.from.role === Role.PACM
      )
      : null;
    search = msg
      ? msg?.filter((ticket) => {
        return (
          ticket.body.toLowerCase().includes(searched.toLowerCase()) ||
          ticket.title.toLowerCase().includes(searched.toLowerCase())
        );
      })
      : null;
  }
  if (user && user.role === Role.PACM) {
    datasent = msg
      ? msg?.filter(
        (ticket) => ticket.replied === false && ticket.from.role === Role.PACM
      )
      : null;
    datainbox = msg
      ? msg?.filter(
        (ticket) =>
          (ticket.replied === true && ticket.from.role === Role.PACM) ||
          ticket.from.role === Role.FBA
      )
      : null;
    search = msg
      ? msg?.filter((ticket) => {
        return (
          ticket.body.toLowerCase().includes(searched.toLowerCase()) ||
          ticket.title.toLowerCase().includes(searched.toLowerCase())
        );
      })
      : null;
  }

  /**
   * @function
   */
  const handleMarkUnread = async () => {
    let markers = !selectedRowKeys
      ? null
      : tickets.map((row, index) => {
        return selectedRowKeys.map((id, i) => {
          if (index === id) {
            return row._id;
          }
        });
      });
    markers = markers.flat();
    markers = markers.filter((m) => m !== undefined);
    const data =
      user && user.role === Role.FBA
        ? { read: false, markers }
        : user && user.role === Role.PAL
          ? { read1: false, markers }
          : { read2: false, markers };

    mutate("/api/hdtickets", [...tickets, data]);
    const updated = await fetch(`/api/hdtickets/updatemany`, {
      method: "PATCH",
      headers: { "Content-Type": ["application/json"] },
      body: JSON.stringify(data),
    });
    const update = await updated.json();
    const { message } = update;
    trigger("/api/hdtickets");
    setWarmessage(message);
    setSelectedRowKeys([]);
    if (updated.status === 200) {
      setMessage("Marked unread");
      mutate("/api/hdtickets", [...tickets, data]);
      setTicketRep(true);
      setInterval(() => {
        setTicketRep(false);
      }, 2000);
    }
  };
  const handleDelete = async () => {
    let markers = !selectedRowKeys
      ? null
      : tickets.map((row, index) => {
        return selectedRowKeys.map((id, i) => {
          if (index === id) {
            return row._id;
          }
        });
      });
    confirm({
      title: selectedRowKeys?.length
        ? SELECTED_ROW_TITLE
        : NO_ROW_SELECTED_TITLE,
      content: selectedRowKeys?.length
        ? SELECTED_ROW_CONTENT
        : NO_ROW_SELECTED_CONTENT,
      okText: selectedRowKeys?.length
        ? SELECTED_ROW_OK_TEXT
        : NO_ROW_SELECTED_OK_TEXT,
      okType: selectedRowKeys?.length
        ? SELECTED_ROW_OK_TYPE
        : NO_ROW_SELECTED_OK_TYPE,
      cancelText: selectedRowKeys?.length
        ? SELECTED_ROW_CANCEL_TEXT
        : NO_ROW_SELECTED_CANCEL_TEXT,

      async onOk() {
        markers = markers.flat();
        markers = markers.filter((m) => m !== undefined);
        if (selectedRowKeys?.length) {
          try {
            mutate("/api/hdtickets", [...tickets, markers]);
            const resTickets = await fetch(`/api/hdtickets/deleteMany`, {
              method: "DELETE",
              headers: { "Content-Type": "application/json" },
              body: JSON.stringify(markers),
            });

            const deleted = await resTickets.json();
            const { message } = deleted;
            trigger("/api/hdtickets");
            setWarmessage(message);

            if (resTickets.status === 200) {
              setTicketRep(true);
              setSelectedRowKeys([]);
              setMessage("Message Deleted");
              setInterval(() => {
                setWarmessage(message);
                setTicketRep(false);
                setWarmessage(undefined);
              }, 2000);
            }
          } catch (error) {
            throw error;
          }
        }
      },
      onCancel() { },
    });
  };
  function handleMenuClick(e) {
    const { key } = e;

    switch (key) {
      case "1":
        setDispTable("Inbox");
        setMenuId(1);
        break;

      case "2":
        setDispTable("Sent");
        setMenuId(2);
        break;

      default:
        "Inbox";
        break;
    }
  }
  function handleCaseManager(e) {
    Router.push({
      pathname: "/helpdesk/newticket",
      query: { msgTo: e.key },
    });
  }
  const onSelectChange = (selectedRowKeys) => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const myMessage = () => (
    <Row>
      <Alert
        message={message}
        type="success"
        showIcon
        closable
        onClose={() => setMessage(undefined)}
        style={{
          marginLeft: "2em",
          width: "96%",
          marginBottom: "2em",
          display: message === undefined ? "none" : "flex",
        }}
      />
    </Row>
  );

  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1">Inbox</Menu.Item>
      <Menu.Item key="2">Sent</Menu.Item>
    </Menu>
  );
  const caseManagers = (
    <Menu onClick={handleCaseManager}>
      {caseManag?.map((caseManager, i) => (
        <Menu.Item key={caseManager._id}>
          {caseManager.firstName} {caseManager.lastName}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <Row
          align="middle"
          style={{ marginBottom: "3em" }}
          justify="start"
          gutter={[48, 24]}
        >
          <Col
            xxl={12}
            xl={12}
            lg={24}
            md={24}
            xs={24}
            style={{ display: "flex", justifyContent: "start" }}
          >
            <Col style={{ padding: 0, margin: 0 }} xxl={6} xl={8} lg={8} md={8}>
              <Dropdown style={{ margin: 0 }} overlay={menu}>
                <Button
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#69c0ff",
                    border: "1px solid #69c0ff",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-around",
                    alignItems: "center",
                    width: "8em",
                  }}
                >
                  {dispTable} <FontAwesomeIcon icon={faChevronDown} />
                </Button>
              </Dropdown>
            </Col>
            <Col
              style={{ padding: 0, padding: 0 }}
              xxl={6}
              xl={8}
              lg={8}
              md={8}
            >
              {menuID != 1 ? (
                ""
              ) : (
                <Button
                  onClick={handleMarkUnread}
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#69c0ff",
                    border: "1px solid #69c0ff",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  Mark as Unread
                </Button>
              )}
            </Col>
            <Col xxl={6} xl={6} lg={8} md={8}>
              <Button
                onClick={handleDelete}
                style={{
                  backgroundColor: "#f5222d",
                  height: "40px",
                  color: "white",
                  border: "1px solid #f5222d",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Delete
              </Button>
            </Col>
          </Col>
          <Col
            xxl={12}
            xl={12}
            lg={24}
            md={24}
            style={{ display: "flex", justifyContent: "space-evenly" }}
          >
            <Col
              xxl={12}
              xl={12}
              lg={12}
              md={12}
              style={{
                display: "flex",
                justifyContent: "space-around",
                paddingRight: "0em",
              }}
            >
              <Button
                style={Msg_Btn_Style}
                onClick={() =>
                  Router.push({
                    pathname: "/helpdesk/newticket",
                  })
                }
              >
                <FontAwesomeIcon icon={faPlus} /> &nbsp;{" "}
                {user?.role === Role.PACM
                  ? "Message PAL"
                  : user?.role === Role.PAL
                    ? "New Message"
                    : "Message Super"}
              </Button>
              {user?.role === Role.PACM && (
                <Button
                  style={Msg_Btn_Style}
                  onClick={() =>
                    Router.push({
                      pathname: "/helpdesk/newticket",
                      query: { msgTo: Role.FBA },
                    })
                  }
                >
                  <FontAwesomeIcon icon={faPlus} /> &nbsp;Message FBA
                </Button>
              )}
              {user?.role === Role.FBA && (
                <Dropdown style={{ margin: 0 }} overlay={caseManagers}>
                  <Button
                    style={{
                      backgroundColor: "#52c41a",
                      height: "40px",
                      color: "white",
                      border: "1px solid #52c41a",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-around",
                      alignItems: "center",
                      width: "8em",
                    }}
                  >
                    Message CW
                    <FontAwesomeIcon
                      style={{ marginLeft: 5 }}
                      icon={faChevronDown}
                    />
                  </Button>
                </Dropdown>
              )}
            </Col>
            <Col xxl={12} xl={12} lg={12} md={12} style={{ paddingRight: 0 }}>
              <Input
                size="large"
                prefix={<FontAwesomeIcon icon={faSearch} />}
                placeholder="Search"
                name="searchBar"
                onChange={(e) => {
                  setSearch(e.target.value), setDispTable("Search");
                }}
                style={{ width: "100%" }}
              />
            </Col>
          </Col>
        </Row>
        {myMessage()}
        <Row>
          <Col span={24}>
            <Spin size="large" tip="Loading..." spinning={ticketRep}>
              <Table
                rowSelection={rowSelection}
                columns={columns}
                dataSource={
                  dispTable === "Inbox"
                    ? datainbox
                    : null || dispTable === "Sent"
                      ? datasent
                      : null || dispTable === "Search"
                        ? search
                        : null
                }
              />
            </Spin>
          </Col>
        </Row>
      </Content>
    </>
  );
};
export default Createticket;
