import React from "react";
import { Card, Row, Input, Button, Typography, Col, Form, message } from "antd";
function recover() {
  async function recoverPassword({ email }) {
    try {
      await fetch("/api/user/password/reset", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ email }),
      });
      message.success(
        "Recover request successful, we send you an email to continue"
      );
    } catch (error) {
      message.error("Error: verify your e-mail or contact your admin");
    }
  }
  return (
    <Card style={{ height: "100%" }} bodyStyle={{ height: "100%" }}>
      <Typography.Title>Recover password</Typography.Title>
      <Row
        gutter={[16, 16]}
        align="middle"
        justify="center"
        style={{ height: "100%" }}
      >
        <Col span={12}>
          <Form onFinish={recoverPassword}>
            <Form.Item label="E-mail" name="email">
              <Input />
            </Form.Item>
            <br />
            <Form.Item>
              <Button
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#69c0ff",
                  border: "1px solid #69c0ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
                htmlType="submit"
                block
              >
                Recover
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </Card>
  );
}

export default recover;
