import React, { useEffect } from "react";
import { useCurrentUser } from "../lib/hooks";
import Link from "next/link";
import { useRouter } from "next/router";
import { Card, Col, Row, Input, Typography, Form, Button, message } from "antd";
import { trigger } from "swr";

const layout = {
  labelCol: {
    sm: { span: 8 },
    span: 8,
  },
  wrapperCol: {
    span: 24,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 9,
    span: 24,
  },
};
const Login = () => {
  const router = useRouter();
  const { Title, Text } = Typography;
  const [user, { mutate }] = useCurrentUser();

  async function activate(userId) {
    try {
      const response = await fetch("/api/user/activate", {
        method: "PATCH",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({ userId }),
      });
      if (response.status === 400) {
        setError(true);
      }
    } catch (err) {

      console.log(err)
    }
  }

  const onFinish = async (values) => {
    try {
      const res = await fetch("/api/auth", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(values),
      });
      if (res.status === 403) {
        message.warning("Your Account is Blocked Please Contact Admin");
      }
      if (res.status === 200) {
        const userObj = await res.json();
        localStorage.setItem("user_Id", userObj.user._id);
        mutate(userObj);
        activate(userObj.user._id)
        router.push("/dashboard");
      }
      if (res.status === 401) {
        message.warning("Wrong Credentials");
      }
    } catch (error) {
      throw error;
    }
  };
  return (
    <div
      style={{
        background: "#F5F5F5",
        display: "flex",
        flexDirection: "column",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Card
        style={{
          textAlign: "center",
          width: "35em",
          height: "35em",
        }}
      >
        <Row
          gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
          style={{ margin: "4em 1em 4em 1em" }}
        >
          <Col span={24}>
            <Title level={3}>FURNITURE CATALOG</Title>
          </Col>
        </Row>
        <Row
          gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
          style={{ justifyContent: "flex-start", width: "100%" }}
        >
          <Col
            span={24}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Form
              {...layout}
              name="basic"
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              style={{ width: "100vw" }}
            >
              <Form.Item
                label="E-mail:"
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Please enter your e-mail.",
                  },
                ]}
              >
                <Input size="large" placeholder="E-mail" />
              </Form.Item>

              <Form.Item
                label="Password:"
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please enter your password.",
                  },
                ]}
              >
                <Input.Password size="large" placeholder="Password" />
              </Form.Item>
              <Row gutter={{ xs: 6, sm: 16, md: 24, lg: 32 }}>
                <Col span={8} offset={16}>
                  <Link href="/recover">
                    <a>Forgot password?</a>
                  </Link>
                </Col>
              </Row>
              <Row>
                <Col span={10}>
                  <Form.Item {...tailLayout}>
                    <Button
                      size="large"
                      className="blueWhiteButton"
                      type="primary"
                      htmlType="submit"
                      block
                    >
                      Login
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default Login;
