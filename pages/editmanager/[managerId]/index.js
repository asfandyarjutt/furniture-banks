import React from "react";
import { useRouter } from "next/router";
import EditCaseManager from "../../../components/pal/casemanager/addCaseManager";

function CaseManDetail() {
  const router = useRouter();
  const { managerId } = router.query;
  return !managerId ? null : <EditCaseManager managerId={managerId} />;
}

export default CaseManDetail;
