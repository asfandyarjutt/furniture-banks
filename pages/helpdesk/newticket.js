import React, { useState, useEffect } from "react";
import Link from "next/link";
import { Role } from "../../constants";
import Router from "next/router";
import {
  Layout,
  Row,
  Col,
  Button,
  Typography,
  Spin,
  Select,
  Pagination,
  Space,
  Input,
  Form,
  message,
} from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faReply,
  faTrash,
  faChevronLeft,
  faCheck,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import {
  useCurrentUser,
  useOneAgency,
  useHelpDeskTickets,
} from "../../lib/hooks";
import { useRouter } from "next/router";
const { Content } = Layout;
const { Text, Title } = Typography;
const { TextArea } = Input;
const { Option } = Select;
const newticket = () => {
  const router = useRouter();
  const {
    query: { msgTo },
  } = router;
  let myManager = "";
  const [tickets, mutate, trigger] = useHelpDeskTickets();
  const [ticket, setTicket] = useState({ title: "", body: "", agency: "" });
  const [currentUser] = useCurrentUser();
  const [partnerAgency] = useOneAgency(currentUser?.partnerAgency);
  const [myfurniture, setMyFurni] = useState();
  const [myadmin, setMyAdmin] = useState();
  const [myadminFurniId, setMyAdminFurniId] = useState();
  const [agencies, setAgencies] = useState([]);
  const [backAgencies, setBackAgen] = useState([]);
  const myFurnitureId = currentUser?.furnitureBank?._id;
  const furniIds = myadminFurniId?.furnitureBanks.map((furni) => {
    return furni._id;
  });
  const matchId = furniIds?.filter((id) =>
    myFurnitureId === id ? true : false
  );

  if (matchId) {
    myManager = myadmin;
  }
  useEffect(() => {
    async function getMyadmin() {
      const admin = await fetch(`/api/users`);
      const { users } = await admin.json();
      const dataa = users?.filter((user) => user.role === "PACM");

      if (currentUser?.role === Role.PACM) {
        let PAL = currentUser?.partnerAgency;
        let myAdmin;
        let adminId;

        if (msgTo === Role.FBA) {
          myAdmin = users.find(
            ({ role, furnitureBank }) =>
              role === Role.FBA &&
              furnitureBank &&
              furnitureBank._id === currentUser?.furnitureBank._id
          );
          adminId = myAdmin?._id;
        } else {
          myAdmin = users.find(
            ({ role, partnerAgency }) =>
              role === Role.PAL && partnerAgency && partnerAgency._id === PAL
          );
          adminId = myAdmin?.partnerAgency?.liaison;
        }
        setMyAdminFurniId(myAdmin);
        setMyAdmin(adminId);

        setMyFurni(currentUser?.furnitureBank?.name);
      }
      if (currentUser?.role === Role.PAL) {
        const myAdmin = users.find(
          ({ role, furnitureBank }) =>
            role === Role.FBA &&
            furnitureBank &&
            furnitureBank._id === currentUser?.furnitureBank?._id
        );
        const adminId = myAdmin?._id;

        setMyAdmin(adminId);
        setMyAdminFurniId(myAdmin);
        setMyFurni(currentUser?.furnitureBank?.name);
      }
      if (currentUser?.role === Role.FBA) {
        let myAdmin;
        let myAdminId;
        if (msgTo) {
          myAdminId = msgTo;
          myAdmin = users.find(({ role }) => role === Role.ADMIN);
        } else {
          myAdmin = users.find(({ role }) => role === Role.ADMIN);
          myAdminId = myAdmin._id;
        }

        setMyAdmin(myAdminId);
        setMyAdminFurniId(myAdmin);
        setMyFurni(currentUser?.furnitureBank?.name);
      }
    }

    if (currentUser !== undefined) {
      getMyadmin();
    }

    Router.prefetch("/createticket");
  }, [partnerAgency, currentUser]);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setTicket({ ...ticket, [name]: value });
  };

  const handleSelect = (value) => {
    setTicket({ ...ticket, agency: value });
  };

  const onFinish = async () => {
    const data = {
      ...ticket,
      myManager,
      replied: false,
      read: true,
      agency:
        currentUser?.role === Role.FBA ? myfurniture : partnerAgency?.name,
    };
    mutate("/api/hdtickets", [...tickets, data]);
    const newTicket = await fetch(`/api/hdtickets`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    trigger("/api/hdtickets");
    const saved = await newTicket.json();

    const { message } = saved;
    if (newTicket.status == 200) {
      Router.push({ pathname: "/createticket", query: { message } });
    } else if (newTicket.status > 300) {
      onFinishFailed("There is some error quering tickets");
    }
  };

  const onFinishFailed = () => {
    message.warning("There is no info to send");
  };

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 24 },
  };

  return (
    <div>
      <Row
        style={{
          marginBottom: "3em",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Link href="/createticket">
          <Button
            style={{
              backgroundColor: "white",
              height: "40px",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            <FontAwesomeIcon icon={faChevronLeft} /> &nbsp; Back
          </Button>
        </Link>
      </Row>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          height: "auto",
          background: "#ffffff",
          marginBottom: "2em",
        }}
      >
        {" "}
        <Form
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          layout="vertical"
          {...layout}
        >
          <Row style={{ marginBottom: "2em" }}>
            <Col span={12}>
              <Title level={4}>New Message</Title>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={12} style={{ display: "none" }}>
              <Spin tip="Loading..." spinning={!myFurnitureId}>
                <Select
                  size="large"
                  placeholder="Choose your Agency"
                  onChange={handleSelect}
                >
                  {agencies.length
                    ? agencies?.map((agency, idx) => {
                        return (
                          <Option name="agency" key={idx} value={agency}>
                            {agency}
                          </Option>
                        );
                      })
                    : backAgencies?.map((agency, idx) => {
                        return (
                          <Option name="agency" key={idx} value={agency}>
                            {agency}
                          </Option>
                        );
                      })}
                </Select>
              </Spin>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                label="Title"
                name="title"
                rules={[
                  {
                    required: true,
                    message: "Please input Subject",
                  },
                ]}
              >
                <Input
                  onChange={handleChange}
                  name="title"
                  size="large"
                  placeholder="Subject"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row style={{ marginBottom: "3em" }}>
            <Col span={24}>
              <Form.Item
                label="Body Text"
                name="body"
                rules={[
                  {
                    required: true,
                    message: "Please input Subject",
                  },
                ]}
              >
                <TextArea
                  onChange={handleChange}
                  name="body"
                  placeholder="Describe the problem you are experiencing here."
                  autoSize={{ minRows: 3, maxRows: 5 }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xxl={4} xl={4} lg={5} md={7} sm={8}>
              <Button htmlType="submit" type="primary" size="large">
                <FontAwesomeIcon icon={faCheck} /> &nbsp; Send
              </Button>
            </Col>
            <Col span={4}>
              <Link href={`/createticket`}>
                <Button
                  size="large"
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#f5222d",
                    border: "1px solid #f5222d",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  {" "}
                  <FontAwesomeIcon icon={faTimes} /> &nbsp; Discard
                </Button>
              </Link>
            </Col>
          </Row>
        </Form>
      </Content>
    </div>
  );
};

export default newticket;
