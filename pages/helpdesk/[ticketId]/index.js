import React from "react";
import { useRouter } from "next/router";
import TDetail from "../../../components/deskhelpComplements/ticketDetail";

function TicketDetail() {
  const router = useRouter();
  const { ticketId } = router.query;
  return !ticketId ? null : <TDetail ticketId={ticketId} />;
}

export default TicketDetail;
