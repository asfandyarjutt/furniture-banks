import React, { useEffect, useState } from "react";
import {
  Layout,
  Row,
  Col,
  Alert,
  Table,
  Typography,
  Input,
  Button,
  Spin,
  Popconfirm,
} from "antd";
import { useCurrentUser, useOneAgency } from "../lib/hooks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faPlus,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import Router, { useRouter } from "next/router";
import Link from "next/link";
const { Content } = Layout;
const index = () => {
  const router = useRouter();
  const [currentUser] = useCurrentUser();
  const [partnerAgency, mutate, trigger] = useOneAgency(
    currentUser?.partnerAgency
  );
  const [users, setUsers] = useState();
  const [message, setMessage] = useState(undefined);
  const [spinin, setSpin] = useState(false);
  const [spinning, setSpining] = useState(false);
  useEffect(() => {
    setSpining(true);
    if (partnerAgency?.caseManagers != undefined) {
      setSpining(false);
    }
  }, [partnerAgency?.caseManagers]);

  const [searched, setSearch] = useState("");
  const [dispTable, setDispTable] = useState("All");
  useEffect(() => {
    setUsers(partnerAgency?.caseManagers);
    setMessage(Router.query.message);
    Router.prefetch("/orders");
    Router.prefetch("/pal/addCase");
  }, [partnerAgency, partnerAgency?.caseManagers]);
  async function removeAgency(id) {
    mutate(
      `/api/pa/${partnerAgency?._id}`,
      partnerAgency?.caseManagers.filter((user) => user.id !== id)
    );
    const res = await fetch(`/api/user`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ userId: id }),
    });
    const { message } = await res.json();
    trigger(`/api/pa/${partnerAgency?._id}`);
    if (res.status === 200 && message) {
      setMessage("Case Manager has been deleted.");
    } else {
      setMessage("There is some error on delete Case Manager.");
    }
  }

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      sorter: { compare: (a, b) => a.firstName - b.firstName },
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "Phone Number",
      dataIndex: "phone",
      sorter: { compare: (a, b) => a.phone - b.phone },
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "Email",
      dataIndex: "email",
      sorter: { compare: (a, b) => a.email - b.email },
      sortDirections: ["descend", "ascend"],
    },
    {
      title: "",
      key: "action",
      render: (record) => (
        <Popconfirm
          placement="left"
          title="Are you sure you want to delete？"
          okText="Delete"
          cancelText="Cancel"
          onConfirm={() => removeAgency(record._id)}
        >
          <Button
            style={{
              backgroundColor: "#f5222d",
              height: "40px",
              color: "white",
              border: "1px solid #f5222d",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
            type="text"
          >
            <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Delete
          </Button>
        </Popconfirm>
      ),
      width: 120,
    },
  ];
  const data = users
    ? users.map((usr) => {
        let dta = {
          ...usr,
          key: usr._id,
          name: usr.firstName +" "+ usr.lastName,
          phone: usr.phone,
          email: usr.email,
        };
        return dta;
      })
    : null;

  const searchedAt = data?.filter((item) => {
    return (
      item?.name.toLowerCase().includes(searched.toLowerCase()) ||
      item?.email.toLowerCase().includes(searched.toLowerCase()) ||
      item?.lastName.toLowerCase().includes(searched.toLowerCase())
    );
  });

  return (
    <Spin tip="Loading..." spinning={spinning}>
      <div>
        <Row style={{ height: "4em" }} justify="space-between">
          <Col>
            <Link href={`/pal/addCase`}>
              <a>
                <Button
                  style={{
                    backgroundColor: "#52c41a",
                    height: "40px",
                    color: "white",
                    border: "1px solid #52c41a",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faPlus} /> &nbsp; Add Case Manager
                </Button>
              </a>
            </Link>
          </Col>
        </Row>
        <Content
          className="site-layout-background"
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
            background: "#ffffff",
          }}
        >
          <Row justify="end">
            <Col xxl={12} xl={12} lg={12} md={24} sm={24}>
              <Input
                placeholder="Search by name, last name, or e-mail."
                size="large"
                prefix={<FontAwesomeIcon icon={faSearch} />}
                style={{ marginBottom: "3em" }}
                onChange={(e) => {
                  setSearch(e.target.value), setDispTable("search");
                }}
              />
            </Col>
          </Row>
          <Spin spinning={spinin} tip="Loading...">
            <Table
              tableLayout="fixed"
              columns={columns}
              dataSource={
                dispTable === "All"
                  ? data
                  : null || dispTable === "search"
                  ? searchedAt
                  : null
              }
            />
          </Spin>
        </Content>
      </div>
    </Spin>
  );
};

export default index;
