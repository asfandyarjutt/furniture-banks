import React from "react";
import { useRouter } from "next/router";
import AddManager from "../../../components/agency/AddManager";

function newliaison() {
  const router = useRouter();
  const {
    query: { agencyId },
  } = router;
  return <AddManager agencyId={agencyId} liaison />;
}

export default newliaison;
