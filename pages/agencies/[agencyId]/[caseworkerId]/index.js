import React from "react";
import { useRouter } from "next/router";
import CaseworkerInfo from "../../../../components/caseworkerInfo";

function CaseworkerDetail() {
  const router = useRouter();
  const { agencyId, caseworkerId } = router.query;
  return !agencyId && !caseworkerId ? null : (
    <CaseworkerInfo agencyId={agencyId} caseworkerId={caseworkerId} />
  );
}

export default CaseworkerDetail;
