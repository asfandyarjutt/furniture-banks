import React from "react";
import AddManager from "../../../components/agency/AddManager";
import { useRouter } from "next/router";

function addmanager() {
  const router = useRouter();
  const {
    query: { agencyId, agencyName },
  } = router;
  return <AddManager agencyId={agencyId} agencyname={agencyName} />;
}

export default addmanager;
