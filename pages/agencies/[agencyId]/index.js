import React from "react";
import { useRouter } from "next/router";
import AgencyInfo from "../../../components/AgencyInfo";

function agencyDetail() {
  const router = useRouter();
  const { agencyId } = router.query;
  return !agencyId ? null : <AgencyInfo agencyId={agencyId} />;
}

export default agencyDetail;
