import React, { useState, useEffect } from "react";
import { Button, Col, Row, Popconfirm, Alert, Spin } from "antd";
import { usePartnerAgencies, useInactiveAgencies, useCurrentUser } from "../../lib/hooks";
import FBTable from "../../components/FBTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import { useRouter } from 'next/router'

function index() {
  const router = useRouter()
  const { page, keyword } = router.query;
  const [agencies, curPage, maxPage, perPage] = usePartnerAgencies(page);
  const [user] = useCurrentUser();
  const [inactiveAgencies] = useInactiveAgencies();
  const [active, setActive] = useState(true);
  const [message, setMessage] = useState(undefined)
  const [spinIn, setSpin] = useState(false);
  const [data, setData] = useState([])
  useEffect(() => {
    setMessage(router.query.message)
  })

  useEffect(() => {
    let data = [];
    agencies?.forEach((agency) => {
      data.push({
        ...agency, 
        key: agency._id,
        liaison: agency.liaison
          ? `${agency?.liaison?.firstName} ${agency?.liaison?.lastName}`
          : "None",
      })
    })

    setData(data)

  }, [user, agencies])

  useEffect(() => {
    setSpin(true)
    if (agencies != undefined) {
      setSpin(false)

    }
  }, [agencies])

  const inactive = inactiveAgencies?.map((agency) => ({
    ...agency,
    key: agency._id,
    liaison: agency.liaison
      ? `${agency?.liaison?.firstName} ${agency?.liaison?.lastName}`
      : "None",
  }));

  return (
    <>
      <Spin tip="Loading..." spinning={spinIn}>
        <Alert
          style={{
            marginBottom: "2em",
            width: "100%",
            display: message === undefined ? "none" : "block",
          }}
          message={message}
          type="success"
          showIcon
          closable
          onClose={() => setMessage(undefined)}
        />
        <Row align="middle" style={{ marginBottom: active ? "2em" : "0em", display: "flex", alignItems: "center" }}>
          <Col span={18} style={{ display: !active ? "none" : "block" }}>
            Click on a Partner Agency below to view full profile or edit
            information.
          </Col>
          <Col span={6} style={{ display: "flex", justifyContent: !active ? "flex-start" : "flex-end", marginBottom: !active ? "-1em" : "0em" }}>
            <Button
              style={{
                backgroundColor: "white",
                height: "40px",
                color: "#69c0ff",
                border: "1px solid #69c0ff",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
              onClick={() => setActive(!active)}
            >
              {!active ? <FontAwesomeIcon icon={faChevronLeft} /> : null}  &nbsp;
              {active ? "Deleted" : "Active"} Partner Agencies
            </Button>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Link href={`/agencies/add`}>
              <a>
                <Button
                  style={{
                    background: "#52C41A",
                    color: "#ffffff",
                    marginRight: "2em",
                    display: !active ? "none" : "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "40px"
                  }}
                  block
                >
                  <FontAwesomeIcon icon={faPlus} />
                  &nbsp; Add Partner Agency
                </Button>
              </a>
            </Link>
          </Col>
        </Row>
        <br />
        <br />
        <Row></Row>
        <FBTable
          data={active ? data : inactive}
          type="agencies"
          active={active}
          curPage={curPage}
          maxPage={maxPage}
          perPage={perPage}

        />

      </Spin>




    </>
  );
}

export default index;
