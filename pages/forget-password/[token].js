import React from "react";
import Head from "next/head";
import nextConnect from "next-connect";
import { useRouter } from "next/router";
import database from "../../middlewares/database";
import { Card, Input, Button, Form, message } from "antd";
import { Token } from "../../models";

const ResetPasswordTokenPage = ({ valid, token }) => {
  const router = useRouter();
  async function handleSubmit({ password,confirm }) {
    event.preventDefault();
    const body = {
      password,
      token,
    };
    try {
     if(password===confirm){
      await fetch("/api/user/password/reset", {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      })
      message.success("New password saved");
      setTimeout(() => {
        router.push("/");
      }, 2000)

     }
     else{
     message.warning("Password Dont Match");
     }
    
    } catch (err) {
      throw (err);
    }
  }

  return (
    <Card>
      <Head>
        <title>Forget password</title>
      </Head>
      <h2>Forget password</h2>
      {valid ? (
        <>
          <p>Enter your new password.</p>
          <Form onFinish={handleSubmit}>
            <Form.Item name="password">
              <Input.Password
                type="password"
                placeholder="New password"
                style={{ width: "30%" }}
              />
            </Form.Item>
            <Form.Item name="confirm">
              <Input.Password
                type="password"
                placeholder="Confirm password"
                style={{ width: "30%" }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#69c0ff",
                  border: "1px solid #69c0ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
                htmlType="submit"
              >
                Set new password
              </Button>
            </Form.Item>
          </Form>
        </>
      ) : (
          <p>This link may have been expired</p>
        )}
    </Card>
  );
};

export async function getServerSideProps(ctx) {
  const handler = nextConnect();
  handler.use(database);
  await handler.apply(ctx.req, ctx.res);
  const { token } = ctx.query;
  const tokenDoc = await Token.findOne({
    token: ctx.query.token,
    type: "passwordReset",
  });

  return { props: { token, valid: !!tokenDoc } };
}

export default ResetPasswordTokenPage;
