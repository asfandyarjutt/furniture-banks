import React, { useEffect } from "react";
import { useCurrentUser } from "../lib/hooks";
import { useRouter } from "next/router";
import { Col, Row, Spin } from "antd";
import {
  AdminDashboard,
  Pacmdashboard,
  PalDashboard,
  FbaDashboard,
} from "../components/dashboard";
import { useCartValue } from "../lib/state";

function dashboard() {
  const [user] = useCurrentUser();
  const [_, dispatch] = useCartValue();
  const router = useRouter();

  useEffect(() => {
    if (user?.furnitureBank?.purchaselimits) {
      dispatch({
        type: "SET_LIMITS",
        payload: user.furnitureBank.purchaselimits,
      });
    }
  }, [user]);

  switch (user?.role) {
    case "ADMIN":
      return <AdminDashboard />;
    case "FBA":
      return <FbaDashboard />;
    case "PAL":
      return <PalDashboard />;
    case "PACM":
      return <Pacmdashboard />;
    default:
      return (
        <Row
          align="middle"
          justify="center"
          style={{ width: "100%", height: "100%" }}
        >
          <Col>
            <Spin />
          </Col>
        </Row>
      );
  }
}

export default dashboard;
