import React, { useState } from "react";
import {
  Card,
  Row,
  Col,
  Button,
  Form,
  Input,
  Typography,
  Select,
  message,
  InputNumber,
} from "antd";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { useRouter } from "next/router";
import UploadPhotos from "../../components/UploadPhotos";
import { useCurrentUser } from "../../lib/hooks";
import useInput from "../../lib/useInput";

const { Title } = Typography;
const { Option } = Select;

function add() {
  const [photos, setPhotos] = useState([]);
  const [description] = useInput("");
  const [category, setCategory] = useState("");
  const [quantity] = useInput("");
  const [color] = useInput("");
  const [material] = useInput("");
  const [high] = useInput("");
  const [wide] = useInput("");
  const [deep] = useInput("");
  const router = useRouter();
  const [currentUser] = useCurrentUser();

  function addPhoto(photo) {
    setPhotos([...photos, photo]);
  }
  const handleSelect = (value) => {
    setCategory(value);
  };

  async function createItem() {
    const data = {
      description: description.value,
      category: category,
      quantity: quantity.value,
      images: photos,
      isActive: true,
      color: color.value,
      material: material.value,
      dimensions: {
        high: high.value,
        wide: wide.value,
        deep: deep.value,
      },
    };

    const res = await fetch("/api/furniture", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    const { message } = await res.json();
    router.push({ pathname: "/dashboard", query: { message } });
  }
  return (
    <Card>
      <Row justify="end" align="middle" style={{ marginBottom: "2em" }}>
        <Col span={12}>
          <Title level={3}>Add New Item</Title>
        </Col>
        <Col
          span={12}
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingRight: "2em",
          }}
        >
          <Link href="/dashboard">
            <Button
              style={{ background: "#010101", color: "#ffffff" }}
              shape="circle"
              icon={<FontAwesomeIcon icon={faTimes} />}
            />
          </Link>
        </Col>
      </Row>
      <UploadPhotos setPhotos={addPhoto} />
      <Row>
        <p>Item Description:</p>
        <Input
          size="large"
          placeholder="Wooden Bed Frame"
          name="description"
          {...description}
        />
      </Row>
      <br />
      <Row gutter={16}>
        <Col span={16}>
          <p>Category: </p>
          <Select
            size="large"
            style={{ width: "100%" }}
            onChange={handleSelect}
            value={category}
          >
            {currentUser?.furnitureBank?.categories?.map((category, key) => (
              <Option key={key} value={category}>
                {category}
              </Option>
            ))}
          </Select>
        </Col>
        <Col span={8}>
          <p>Quantity: </p>
          <Input
            size="large"
            {...quantity}
            min={1}
            defaultValue={1}
            type="number"
            placeholder="7"
          />
        </Col>
      </Row>
      <br />
      <Row gutter={16}>
        <Col span={12}>
          <p style={{ margin: 0 }}>Color:</p>
          <Input size="large" placeholder="White" {...color} />
        </Col>
        <Col span={12}>
          <p style={{ margin: 0 }}>Material:</p>
          <Input size="large" placeholder="Wood" {...material} />
        </Col>
      </Row>
      <br />
      <p>Dimensions (inches):</p>
      <Row align="middle" gutter={16}>
        <Col span={4}>
          <Input size="large" placeholder="High" {...high} type="number" />
        </Col>

        <Col span={4}>
          <Input size="large" placeholder="Wide" {...wide} type="number" />
        </Col>

        <Col span={4}>
          <Input size="large" placeholder="Deep" {...deep} type="number" />
        </Col>
      </Row>
      <br />
      <Row style={{ padding: "3em" }}>
        <Col
          offset={18}
          span={6}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          <Button
            style={{
              backgroundColor: "white",
              height: "40px",
              color: "#f5222d",
              border: "1px solid #f5222d",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
              marginRight: "10px",
            }}
            onClick={() => router.push(`/dashboard`)}
          >
            <FontAwesomeIcon icon={faTimes} /> &nbsp; Cancel
          </Button>
          <Button
            style={{
              backgroundColor: "#1890ff",
              height: "40px",
              color: "white",
              border: "1px solid #1890ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
            disabled={
              !description.value ||
              !category ||
              !quantity.value ||
              !high.value ||
              !wide.value ||
              !deep.value
            }
            onClick={createItem}
          >
            <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
          </Button>
        </Col>
      </Row>
    </Card>
  );
}

export default add;
