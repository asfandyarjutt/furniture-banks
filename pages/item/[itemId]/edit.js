import React from "react";
import { useRouter } from "next/router";
import ItemDetail from "../../../components/item/ItemDetail";

function edit() {
  const router = useRouter();
  const {
    query: { itemId },
  } = router;
  return itemId ? <ItemDetail itemId={itemId} /> : null;
}

export default edit;
