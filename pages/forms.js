import React from "react";
import { Card, Typography, Row, Col } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faInbox } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";

const { Title } = Typography;
function forms() {
  return (
    <Row gutter={16}>
      <Col span={12}>
        <Card style={{ cursor: "pointer", height: "200px" }}>
          <Link href="/forms/appointment">
            <center>
              <FontAwesomeIcon color="#1890FF" size="3x" icon={faInbox} />
              <Title level={4}>Client Appointment Form</Title>
              <p>
                Customize information requested from Client and Case Manager at
                time of purchase.
              </p>
            </center>
          </Link>
        </Card>
      </Col>
      <Col span={12}>
        <Card style={{ cursor: "pointer", height: "200px" }}>
          <Link href="/forms/email">
            <center>
              <FontAwesomeIcon color="#1890FF" size="3x" icon={faEnvelope} />
              <Title level={4}>E-mail Confirmation</Title>
              <p>
                Customize email confirmation sent to Client and Case Manager.
              </p>
            </center>
          </Link>
        </Card>
      </Col>
    </Row>
  );
}

export default forms;
