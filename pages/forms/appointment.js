import React, { useEffect } from "react";
import { Row, Col, Button, Card, Typography, Tabs } from "antd";
import ContactForm from "../../components/Forms/ContactForm";
import HouseHoldForm from "../../components/Forms/HouseHoldForm";
import BackgroundForm from "../../components/Forms/BackgroundForm";
import PaymentForm from "../../components/Forms/PaymentForm";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { useCurrentUser } from "../../lib/hooks";

const { Title } = Typography;
const { TabPane } = Tabs;

function appointment() {
  const [user, { mutate }] = useCurrentUser();
  const router = useRouter();
  return (
    <>
      <Row>
        <Col span={5}>
          <Button
            style={{
              backgroundColor: "white",
              height: "40px",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
            onClick={() => router.back()}
          >
            <FontAwesomeIcon icon={faChevronLeft} /> &nbsp; Back
          </Button>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Card>
            <Title level={3}>Client Appointment Form</Title>
            <Tabs defaultActiveKey="1">
              <TabPane tab="Contact Information" key="1">
                <ContactForm onEdit />
              </TabPane>
              <TabPane tab="Household Information" key="2">
                <HouseHoldForm onEdit />
              </TabPane>
              <TabPane tab="Background Information" key="3">
                <BackgroundForm />
              </TabPane>
              <TabPane tab="Payment and Delivery" key="4">
                <PaymentForm />
              </TabPane>
            </Tabs>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default appointment;
