import React from "react";
import DetailEmail from "../../components/Forms/DetailEmail";
import { useEmailTexts } from "../../lib/hooks";

function email() {
  const [emailTexts, emailSubject, mutate] = useEmailTexts();
  return <DetailEmail emailSubject={emailSubject} emailTexts={emailTexts} />;
}

export default email;
