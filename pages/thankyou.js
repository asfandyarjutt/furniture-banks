import React from "react";
import { Layout, Row, Typography, Button } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
const { Content } = Layout;
const { Title } = Typography;
const Thankyou = () => {
  return (
    <div>
      <Row style={{ marginBottom: "2em" }}>
        <Title level={3}>Furniture name</Title>
      </Row>
      <Row style={{ marginBottom: "3em" }}>
        <Link href="/dashboard">
          <Button type="primary" size="large">
            <FontAwesomeIcon icon={faChevronLeft} />
            <span> &nbsp; Return to Catalog</span>
          </Button>
        </Link>
      </Row>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "300px",
          }}
        >
          <Title level={3} style={{ textAlign: "center" }}>
            Thank you for your purchase! A confirmation email will be sent to
            both
            <br /> Client and Case Manager.
          </Title>
        </div>
      </Content>
    </div>
  );
};

export default Thankyou;
