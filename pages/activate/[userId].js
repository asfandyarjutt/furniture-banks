import React, { useState, useEffect } from "react";
import { Row, Spin, Alert, Card } from "antd";
import { useRouter } from "next/router";

function Activation() {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const {
    query: { userId },
    push,
  } = useRouter();

  useEffect(() => {
    async function activate() {
      try {
        const response = await fetch("/api/user/activate", {
          method: "PATCH",
          headers: { "Content-type": "application/json" },
          body: JSON.stringify({ userId }),
        });
        if (response.status === 400) {
          setLoading(false);
          setError(true);
        }
        setLoading(false);
        setTimeout(() => push("/"), 3000);
      } catch (err) {
        setLoading(false);
        setError(true);
      }
    }

    if (userId) {
      activate();
    }
  }, [userId]);
  return (
    <Card>
      <Row align="middle" justify="center">
        {loading ? (
          <Spin />
        ) : error ? (
          <Alert
            type="error"
            showIcon
            message="Invalid token or user is already active"
          />
        ) : (
          <Alert type="success" showIcon message="Activation successful" />
        )}
      </Row>
    </Card>
  );
}

export default Activation;
