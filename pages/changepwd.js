import React from "react";
import { Row, Card, Input, Form, Button, message } from "antd";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";

function changepwd() {
  const [form] = Form.useForm();
  const router = useRouter();
  async function finish(values) {
    const r = await fetch("/api/changepwd", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(values),
    });
    if (r.status === 403) {
      const res = await r.json();
      message.warning(res.message);
    } else {
      const res = await r.json();
      message.success(res.message);
      router.push("/");
    }
  }

  return (
    <Card bodyStyle={{ height: "100%" }} style={{ height: "100%" }}>
      <Row align="middle" justify="center" style={{ height: "100%" }}>
        <Form form={form} layout="vertical" onFinish={finish}>
          <Form.Item label="Old Password" name="oldPwd">
            <Input.Password />
          </Form.Item>
          <Form.Item label="New Password" name="newPwd">
            <Input.Password />
          </Form.Item>
          <Form.Item>
            <Button
              style={{
                backgroundColor: "#1890ff",
                height: "40px",
                color: "white",
                border: "1px solid #1890ff",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
              htmlType="submit"
            >
              <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
            </Button>
          </Form.Item>
        </Form>
      </Row>
    </Card>
  );
}

export default changepwd;
