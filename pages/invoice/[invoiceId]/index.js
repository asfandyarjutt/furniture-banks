import React from "react";
import { useRouter } from "next/router";
import InvoiceDetails from "../../../components/InvoiceDetail";

function InvoiceDetail() {
  const router = useRouter();
  const { invoiceId } = router.query;
  return !invoiceId ? null : <InvoiceDetails invoiceId={invoiceId} />;
}

export default InvoiceDetail;
