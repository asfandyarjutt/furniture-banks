import React from "react";
import { useRouter } from "next/router";
import OrderDetails from "../../../components/pal/orders/orderDetails";

function OrderDetail() {
  const router = useRouter();
  const { orderId } = router.query;
  return !orderId ? null : <OrderDetails orderId={orderId} />;
}

export default OrderDetail;
