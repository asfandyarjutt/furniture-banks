import React, { createContext, useReducer, useEffect, useState } from "react";
import "antd/dist/antd.css";
import "../styles/vars.css";
import "../styles/global.css";
import Layout from "../components/Layout";

import { reducer, initialState, CartContext } from "../lib/state";
import { SWRConfig } from "swr";
import { useRouter } from "next/router";
import { useCurrentUser } from "../lib/hooks";

export default function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [user] = useCurrentUser();
  let local_storage = "";
  if (typeof window !== "undefined") {
    local_storage = localStorage.getItem("user_Id");
  }

  useEffect(() => {
    if (
      !user &&
      !local_storage &&
      router.pathname !== "/forget-password/[token]"
    ) {
      router.push("/");
    }
    if (user && local_storage && router.pathname === "/") {
      router.push("/dashboard");
    }
  }, [user]);
  return (
    <CartContext.Provider value={useReducer(reducer, initialState)}>
      <SWRConfig value={{ fetcher: (url) => fetch(url).then((r) => r.data) }}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </SWRConfig>
    </CartContext.Provider>
  );
}
