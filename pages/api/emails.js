import nextConnect from "next-connect";
import middleware from "../../middlewares";
import mongoose from "mongoose";
import { furnitureBankSchema } from "../../models/FurnitureBank";
import { extractUser } from "../../lib/api-helpers";

const handler = nextConnect();
let FurnitureBank;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);
handler.get(async (req, res) => {
  const currentUser = extractUser(req);
  if (!currentUser) {
    return res.status(403).json({ message: "You must login" });
  }
  const { emailTexts, emailSubject } = await FurnitureBank.findById(
    currentUser.furnitureBank._id
  );
  res.status(200).json({ emailTexts, emailSubject });
});

handler.put(async (req, res) => {
  const currentUser = extractUser(req);
  const {
    client,
    caseManager,
    partnerAgency,
    paSubject,
    caseSubject,
    clientSubject,
  } = req.body;
  if (!currentUser) {
    return res.status(403).json({ message: "You must login" });
  }
  await FurnitureBank.findByIdAndUpdate(currentUser.furnitureBank._id, {
    $set: {
      emailTexts: { client, caseManager, partnerAgency },
      emailSubject: { paSubject, caseSubject, clientSubject },
    },
  });
  res.status(200).json({ message: "updated" });
});

export default handler;
