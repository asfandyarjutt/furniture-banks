import nextConnect from 'next-connect'
import bcrypt from 'bcrypt'
import mongoose from 'mongoose'
import middleware from '../../middlewares'
import { userSchema } from '../../models/User'
import { extractUser } from '../../lib/api-helpers'

const handler = nextConnect()

handler.use(middleware)

let User
try {
  User = mongoose.model('User')
} catch (error) {
  User = mongoose.model('User', userSchema)
}

handler.post(async (req, res) => {
  const { oldPwd, newPwd } = req.body
  const currentUser = extractUser(req);
  const user = await User.findById(currentUser._id)

  if (!user) {
    res.status(403).json({ message: 'You must login' })
  }

  if (bcrypt.compareSync(oldPwd, user.password)) {
    const hashedPassword = bcrypt.hashSync(newPwd, 10);
    await User.findByIdAndUpdate(user._id, { $set: { password: hashedPassword } })
    res.status(200).json({ message: 'Password Changed' })
  } else {
    res.status(403).json({ message: 'Old Password Incorrect' })
  }
})

export default handler
