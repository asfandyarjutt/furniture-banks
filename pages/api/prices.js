import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../middlewares";
import { extractUser } from "../../lib/api-helpers";
import { furnitureBankSchema } from "../../models/FurnitureBank";
import Dinero from "dinero.js";

const handler = nextConnect();
let FurnitureBank;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);
handler.put(async (req, res) => {
  const currentUser = extractUser(req);
  const { selfHaul, curbsideDelivery, installationDelivery } = req.body.data;

  try {
    const self = Dinero({
      amount: Number(selfHaul.split(".").join("")),
      currency: "USD",
    });
    const curbside = Dinero({
      amount: Number(curbsideDelivery.split(".").join("")),
      currency: "USD",
    });
    const installation = Dinero({
      amount: Number(installationDelivery.split(".").join("")),
      currency: "USD",
    });
    if (!currentUser) {
      return res.status(403).json({ message: "You must login" });
    }

    await FurnitureBank.findByIdAndUpdate(currentUser.furnitureBank._id, {
      $set: {
        prices: {
          self: self.getAmount(),
          curbside: curbside.getAmount(),
          installation: installation.getAmount(),
        },
      },
    });

    return res.status(200).json({ message: "updated" });
  } catch (err) {
    throw (err);
  }
});

export default handler;
