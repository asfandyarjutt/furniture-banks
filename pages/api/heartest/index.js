import {
    ServicesConfig,
    ServicesContainer,
    Address,
    CreditCardData,
    
} from "globalpayments-api";
import mongoose from "mongoose";
import nextConnect from "next-connect";
import { heartlandSchema } from "../../../models/Heartland";
import middleware from "../../../middlewares";

const handler = nextConnect();
const config = new ServicesConfig();
config.secretApiKey = "skapi_cert_MU6vBQDFsm8Ausb4s9DWXYZFjWFGB2BMNEauLR2rfA";
config.serviceUrl = "https://cert.api2.heartlandportico.com";
config.developerId = "002914";
config.versionNumber = "5105";
ServicesContainer.configure(config);
let HeartLand
try {
    HeartLand = mongoose.model("Heartland");

} catch (error) {
    HeartLand = mongoose.model("Heartland", heartlandSchema);

}
handler.use(middleware)
handler.post(async (req, res) => {
    const { authorizedPayment } = req.body
    console.log('payment', authorizedPayment)
    try {
        const card = new CreditCardData();
        card.token = authorizedPayment.token_value
        card.expMonth = authorizedPayment.exp_month;
        card.expYear = authorizedPayment.exp_year;
        card.cardHolderName = "Joe Smith";
        const address = new Address();
        address.streetAddress1 = '6860 Dallas Pkwy'
        address.postalCode = "75024";

        card
            .tokenize()
            .withCurrency("USD")
            .withAddress(address)
            .execute()
            .then(async (response) => {
                console.log('resssssssss', response);
                const token = new CreditCardData();
                token.token = response.token;
                token.expMonth = "12";
                token.expYear = "2025";


                if (response && response.responseCode == "00") {
                    await HeartLand.create({
                        heartLandInfo: response,
                    }).then(savedDoc => {
                        res.status(200).json({
                            _id: savedDoc._id,
                            message: "Payment Success"

                        })

                    });
                }


                else {
                    return res.status(504).json({ message: "Payment Error, Please repay." });
                }



                token.charge(17.02)
                    .withCurrency("USD")
                    .execute()
                    .then(async (authorization) => {
                        console.log("auth:", authorization);


                    })
                    .catch((err) => console.log(err));


            })

            .catch((error) => console.log(error));


    } catch (err) {
        throw new Error(`Error ${err}`);
    }
});

export default handler;