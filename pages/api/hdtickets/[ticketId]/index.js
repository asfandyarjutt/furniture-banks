import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import { ticketSchema } from "../../../../models/Ticket";
import mongoose from "mongoose";
import { extractUser } from "../../../../lib/api-helpers";
const handler = nextConnect();

handler.use(middleware);
let Ticket;
try {
  Ticket = mongoose.model("Ticket");
} catch (error) {
  Ticket = mongoose.model("Ticket", ticketSchema);
}

handler.get(async (req, res) => {
  const { ticketId } = req.query;
  try {
    const oneTicket = await Ticket.findById(ticketId)
      .populate("from")
      .populate("to")
      .populate("postId");
    res.status(200).json({ oneTicket });
  } catch (error) {
    throw new Error(error);
  }
});

handler.delete(async (req, res) => {
  const currentUser = extractUser(req);
  const { ticketId } = req.query;
  if (currentUser.role !== "ADMIN") {
    return res
      .status(403)
      .json({ message: `Sorry you can't perform that action` });
  }
  try {
    const deleteTicket = await Ticket.findByIdAndDelete(ticketId);
    res.status(302).json({
      message: `This ticket has been successfully deleted
    `,
    });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
