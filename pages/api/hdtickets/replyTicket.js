import nextConnect from "next-connect";
import { ticketSchema } from "../../../models/Ticket";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { extractUser } from "../../../lib/api-helpers";
const handler = nextConnect();

handler.use(middleware);
let Ticket;
try {
  Ticket = mongoose.model("Ticket");
} catch (error) {
  Ticket = mongoose.model("Ticket", ticketSchema);
}

handler.post(async (req, res) => {
  const currentUser = extractUser(req);
 
  try {
    const { title, body, to, ticketId } = req.body;
    const from = currentUser._id;
    let read =
      currentUser.role === "ADMIN"
        ? { readed: true, read: false, read1: false, read2: false }
        : currentUser.role === "FBA"
        ? { readed: false, read: true, read1: false, read2: false }
        : currentUser.role === "PAL"
        ? { readed: false, read: false, read1: true, read2: false }
        : currentUser.role === "PACM"
        ? { readed: false, read: false, read1: false, read2: true }
        : null;
    await Ticket.create({
      title,
      body,
      replied: true,
      ...read,
      to,
      from,
    }).then((ticket) => {
      Ticket.findByIdAndUpdate(ticketId, {
        $push: { postId: ticket._id },
        ...read,
      })
        .populate("postId")
        .sort({ date: -1 })
        .exec((err) => {
          if (err) return res.json({ success: false, err });
          return res
            .status(200)
            .json({ success: true, message: "Your reply has been sent." });
        });
    });
  } catch (error) {
  }
});

export default handler;
