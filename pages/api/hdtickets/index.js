import nextConnect from "next-connect";
import { ticketSchema } from "../../../models/Ticket";
import { userSchema } from "../../../models/User";
import middleware from "../../../middlewares";
import mongoose from "mongoose";
import { extractUser } from "../../../lib/api-helpers";
const handler = nextConnect();

handler.use(middleware);
let Ticket;
let User;
try {
  User = mongoose.model("User");
  Ticket = mongoose.model("Ticket");
} catch (error) {
  User = mongoose.model("User", userSchema);
  Ticket = mongoose.model("Ticket", ticketSchema);
}

handler.get(async (req, res) => {
  const currentUser = extractUser(req);
  const userId = req?.query?.userId;
  if (!!userId) {
    try {
      const usr = await User.findById({ _id: userId })
        .populate({
          path: "HelpDeskTickets",
          populate: {
            path: "from",
            model: "User",
            populate: { path: "partnerAgency", model: "PartnerAgency" },
          },
        })
        .sort({ date: -1 });
      const { HelpDeskTickets } = usr;
      res.status(200).json({
        tickets: HelpDeskTickets.sort((a, b) => b.createdAt - a.createdAt),
        user: usr,
      });
    } catch (error) {
      throw new Error({ message: error });
    }
  }
});

handler.post(async (req, res) => {
  const currentUser = extractUser(req);
  let read =
    currentUser.role === "FBA"
      ? { readed: false, read: true, read1: false, read2: false }
      : currentUser.role === "PAL"
      ? { readed: false, read: false, read1: true, read2: false }
      : currentUser.role === "PACM"
      ? { readed: false, read: false, read1: false, read2: true }
      : null;
  const { title, body, agency, myManager, belongTo } = req.body;
  const from = currentUser._id;
  try {
    const ticket = await Ticket.create({
      title,
      body,
      to: myManager,
      from,
      agency,
      belongTo,

      replied: false,
      ...read,
    });
    await User.findByIdAndUpdate(
      { _id: currentUser._id },
      { $push: { HelpDeskTickets: ticket._id } }
    );
    await User.findByIdAndUpdate(
      { _id: myManager },
      { $push: { HelpDeskTickets: ticket._id } }
    );
    res.status(200).json({ message: "Message Sent" });
  } catch (error) {
    throw error;
  }
});

handler.patch(async (req, res) => {
  const currentUser = extractUser(req);
  const { ticketId } = req.body;
  let t = await Ticket.findByIdAndUpdate(
    { _id: ticketId },
    { $set: { ...req.body } },
    {
      new: true,
    }
  )
    .populate("from")
    .populate("to")
    .sort({ date: -1 })
    .then(() =>
      res.status(200).json({ message: "Your message has been sent." })
    );
  res.redirect("/helpdesk");
});

handler.get(async (req, res) => {
  const { ticketId } = req.query;
  try {
    const oneTicket = await Ticket.findById(ticketId)
      .populate("from")
      .populate("to");
    res.status(200).json({ oneTicket });
  } catch (error) {
    throw new Error(error);
  }
});

handler.delete(async (req, res) => {
  const currentUser = extractUser(req);
  if (currentUser.role !== "ADMIN") {
    return res
      .status(403)
      .json({ message: `Sorry you can't perform that action` });
  }
  try {
    res.status(302).json({
      message: `This ticket has been successfully deleted
    `,
    });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
