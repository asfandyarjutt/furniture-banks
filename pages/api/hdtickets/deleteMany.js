import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { ticketSchema } from "../../../models/Ticket";
import mongoose from "mongoose";
const handler = nextConnect();

handler.use(middleware);
let Ticket;
try {
  Ticket = mongoose.model("Ticket");
} catch (error) {
  Ticket = mongoose.model("Ticket", ticketSchema);
}
handler.delete(async (req, res) => {

  try {
    await Ticket.deleteMany({ _id: { $in: req.body } });
    res
      .status(200)
      .json({
        message: `Messages have been deleted`,
      })
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
