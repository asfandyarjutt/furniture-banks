import nextConnect from "next-connect";
import { ticketSchema } from "../../../models/Ticket";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { extractUser } from "../../../lib/api-helpers";
const handler = nextConnect();

handler.use(middleware);

let Ticket;
try {
  Ticket = mongoose.model("Ticket");
} catch (error) {
  Ticket = mongoose.model("Ticket", ticketSchema);
}

handler.patch(async (req, res) => {
  const currentUser = extractUser(req);
  const { markers } = req.body;
  if (!currentUser) {
    return res.status(403).json({ message: `This is not your ticket!` });
  }
  await Ticket.updateMany(
    { _id: { $in: markers } },
    { $set: { ...req.body } },
    { new: true }
  ).then((response) =>
    res
      .status(200)
      .json({ message: "The ticket's has been successfully updated", response })
      .catch((error) => console.error(error))
  );
});

export default handler;
