import nextConnect from "next-connect";
import middleware from "../../middlewares";
import mongoose from "mongoose";
import { userSchema } from "../../models/User";

const handler = nextConnect();
let User;

try {
  User = mongoose.model("User");
} catch (error) {
  User = mongoose.model("User", userSchema);
}

handler.use(middleware);

handler.get(async (req, res) => {
  const users = await User.find()
    .populate("furnitureBanks")
    .populate("furnitureBank")
    .populate("partnerAgency")
    const filteredUsers = users.sort((a,b) => b.createdAt - a.createdAt)
  res.status(200).json({ users:filteredUsers });
});

export default handler;
