import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { partnerAgencySchema } from "../../../models/PartnerAgency";

let PartnerAgency;

try {
  PartnerAgency = mongoose.model("PartnerAgency");
} catch (error) {
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
}
const handler = nextConnect();
handler.use(middleware);

handler.post(async (req, res) => {
  const [recoverfbs] = req.body;
  recoverfbs.forEach(async (fbid) => {
    await PartnerAgency.findByIdAndUpdate(fbid, { active: true });
  });
  res.json({ message: "Agency Returned to Active" });
});

export default handler;
