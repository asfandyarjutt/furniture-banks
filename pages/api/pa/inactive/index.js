import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../../middlewares";
import { partnerAgencySchema } from "../../../../models/PartnerAgency";
import { furnitureBankSchema } from "../../../../models/FurnitureBank";
import { extractUser } from "../../../../lib/api-helpers";

const handler = nextConnect();

let PartnerAgency;
let FurnitureBank;

try {
  PartnerAgency = mongoose.model("PartnerAgency");
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);
handler.get(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    if (!currentUser) {
      return res.status(403).json({ message: "You need to login" });
    }

    if (currentUser.role === "ADMIN") {
      const partnerAgencies = await PartnerAgency.find({ active: false })
        .populate("caseManagers")
        .populate("liaison");
      return res.json({ partnerAgencies });
    }

    const { partnerAgencies } = await FurnitureBank.findById(
      currentUser.furnitureBank._id
    )
      .populate("partnerAgencies")
      .populate({
        path: "partnerAgencies",
        populate: { path: "liaison", model: "User" },
      })
      .populate({
        path: "partnerAgencies",
        populate: { path: "caseManagers", model: "User" },
      });

    return res
      .status(200)
      .json({ partnerAgencies: partnerAgencies.filter((pa) => !pa.active) });
  } catch (error) {
    throw (error);
  }
});

export default handler;
