import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { extractUser } from "../../../lib/api-helpers";
import createUser from "../../../lib/createUser";
import { userSchema } from "../../../models/User";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import { partnerAgencySchema } from "../../../models/PartnerAgency";
import sendMails from "../../../lib/sendMails";
import { max } from "moment";
import { limitValue, lookUp, skipValue, windValue } from "../order/aggregation";
import { ADMIN, FBA, UNDEFINED } from "../../../components/Forms/constants";
const handler = nextConnect();

let User;
let FurnitureBank;
let PartnerAgency;

try {
  User = mongoose.model("User");
  FurnitureBank = mongoose.model("FurnitureBank");
  PartnerAgency = mongoose.model("PartnerAgency");
} catch (error) {
  User = mongoose.model("User", userSchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
}

handler.use(middleware);
handler.get(async (req, res) => {
  const curPage = req.query.page || 1;
  let keyword = {};
  const perPage = 10;
  const display = req.query.display
  let userid = req?.query?.userid;
  const userrole = req.query.userrole;
  let regex = new RegExp(req.query.keyword, "i");
  let count;
  let nid;
  if (userid != UNDEFINED) {
    nid = mongoose.Types.ObjectId(userid);
  }


  try {
    if (userrole === ADMIN) {
      count = await PartnerAgency.find({ active: true })
        .populate("liaison")
        .populate("caseManagers");
      const partnerAgencies = await PartnerAgency.find({ active: true })
        .populate("liaison")
        .populate("caseManagers")
        .skip((curPage - 1) * perPage)
        .limit(perPage);

      const searchAgencies = await PartnerAgency.find({ active: true, name: regex })
        .populate("liaison")
        .populate("caseManagers")


      const partnerFiltered = partnerAgencies.sort(
        (a, b) => b.createdAt - a.createdAt
      );


      const nullFurniture = await PartnerAgency.find({ active: true })
        .populate({
          path: "liaison",
          furnitureBank: null
        }).populate("caseManagers")

      if (display === 'undefined') {
        if (req?.query?.keyword === '' || req?.query?.keyword === 'undefined') {
          return res.status(200).json({
            partnerAgencies: partnerFiltered,
            curPage: curPage,
            maxPage: count.length / perPage,
            perPage: perPage,
          });
        }

        else {
          return res.status(200).json({
            partnerAgencies: searchAgencies,
            curPage: curPage,
            maxPage: 1,
            perPage: perPage,
          });
        }

      }
      else {
        return res.status(200).json({
          partnerAgencies: nullFurniture,
          curPage: curPage,
          maxPage: 1,
          perPage: perPage,
        });
      }

    } else {
      const countPipe = await FurnitureBank.aggregate([
        { $match: { _id: nid } },
        lookUp("partneragencies", "partnerAgencies", "_id", "partnerAgencies"),
        windValue("$partnerAgencies"),
        { $match: { "partnerAgencies.active": true } },
      ]);

      const PartnerAgencyPipe = await FurnitureBank.aggregate([
        { $match: { _id: nid } },
        lookUp("partneragencies", "partnerAgencies", "_id", "partnerAgencies"),
        windValue("$partnerAgencies"),
        { $match: { "partnerAgencies.active": true } },
        lookUp(
          "users",
          "partnerAgencies.liaison",
          "_id",
          "partnerAgencies.liaison"
        ),
        windValue("$partnerAgencies.liaison"),
        skipValue(curPage, perPage),
        limitValue(perPage),
      ]);

      const newPartnerAgencyPipe = PartnerAgencyPipe.map((partnerAgency) => {
        return partnerAgency.partnerAgencies;
      });

      return res.status(200).json({
        partnerAgencies: newPartnerAgencyPipe,
        curPage: curPage,
        maxPage: countPipe.length / perPage,
        perPage: perPage,
      });
    }
  } catch (err) {
    return err;
  }
});

handler.post(async (req, res) => {
  const currentUser = extractUser(req);
  if (currentUser?.role !== "ADMIN" && currentUser?.role !== "FBA") {
    return res.json({ message: `Sorry you can't perform that action` });
  }
  const { name, address, phone, user, city, state, zip, email, services } =
    req.body;

  if (!user.firstName || !user.lastName)
    return res.status(400).send("Missing field(s) for a user");

  if (await User.findOne({ email }))
    return res.status(403).send("Case Manager email is already in use");

  const { newUser, password } = await createUser(
    user,
    currentUser.furnitureBank,
    undefined,
    currentUser.role
  );
  const newAgency = new PartnerAgency({
    name,
    address,
    phone,
    city,
    state,
    zip,
    email,
    services,
  });

  if (user.liaison) {
    newAgency.liaison = newUser?._id;
  } else {
    newAgency.caseManagers.push(newUser._id);
  }
  if (currentUser.role === FBA) {
    await FurnitureBank.findByIdAndUpdate(currentUser.furnitureBank, {
      $push: { partnerAgencies: newAgency._id },
    });
  }
  sendMails(newUser, currentUser.furnitureBank, newAgency, undefined, password);
  await newAgency.save(async (error) => {
    if (error) return console.log(error);
    await User.findByIdAndUpdate(
      newUser._id,
      { $set: { partnerAgency: newAgency._id } },
      { new: true }
    );
  });

  res.status(201).json({
    partnerAgency: newAgency,
    info: "New Partner Agency has been added.",
  });
});

export default handler;
