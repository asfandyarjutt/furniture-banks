import mongoose from "mongoose";
import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import createUser from "../../../../lib/createUser";
import { extractUser } from "../../../../lib/api-helpers";
import { partnerAgencySchema } from "../../../../models/PartnerAgency";
import { userSchema } from "../../../../models/User";
import sendMails from "../../../../lib/sendMails";

const handler = nextConnect();
let PartnerAgency;
let User;

try {
  User = mongoose.model("User");
  PartnerAgency = mongoose.model("PartnerAgency");
} catch (error) {
  User = mongoose.model("User", userSchema);
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
}

handler.use(middleware);
handler.post(async (req, res) => {
  const currentUser = extractUser(req);
  const {
    body: user,
    query: { agencyId },
  } = req;
  try {
    if (
      currentUser?.role !== "ADMIN" &&
      currentUser?.role !== "FBA" &&
      currentUser?.role !== "PAL"
    ) {
      return res.json({ message: `Sorry you can't perform that action` });
    }

    if (!user.firstName || !user.lastName) {
      return res.status(400).json({ message: "Missing field(s) for a user" });
    }

    if (await User.findOne({ email: user.email })) {
      return res
        .status(403)
        .json({ message: "Case Manager email is already in use" });
    }
    const { newUser, password } = await createUser(
      user,
      currentUser.furnitureBank,
      agencyId,
      currentUser.role
    );
    if (newUser.role == "PAL") {
      await PartnerAgency.findByIdAndUpdate(agencyId, {
        $set: { liaison: newUser._id },
      });
      const agency = await PartnerAgency.findById(agencyId);
      sendMails(newUser, undefined, agency, undefined, password);
    } else {
      await PartnerAgency.findByIdAndUpdate(agencyId, {
        $push: { caseManagers: newUser._id },
      });
      const agency = await PartnerAgency.findById(agencyId);
      if (agency.liaison === undefined || null) {
        return res
          .status(403)
          .json({ message: "Case Manager Must have a Liaison!" });
      }
      const miPal = await User.findById(agency?.liaison);
      sendMails(newUser, undefined, agency, miPal, password);
    }
    res.status(201).json({ message: `New Case Mananager has been added.` });
  } catch (error) {
    res.status(400).json({ message: `There is an Error ${error}` });
    throw new Error(error);
  }
});

export default handler;
