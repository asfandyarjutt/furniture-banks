import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import { partnerAgencySchema } from "../../../../models/PartnerAgency";
import mongoose from "mongoose";

const handler = nextConnect();
let PartnerAgency;

try {
  PartnerAgency = mongoose.model("PartnerAgency");
} catch (error) {
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
}

handler.use(middleware);
handler.get(async (req, res) => {
  const { agencyId } = req.query
  try {
    const partnerAgency = await PartnerAgency.findById({ _id: agencyId })
      .populate("liaison")
      .populate("caseManagers");
    res.status(200).json({ partnerAgency });
  } catch (error) {
    throw new Error(error);
  }
});

handler.patch(async (req, res) => {
  const { name, address, phone, email, services } = req.body;
  try {
    const {
      query: { agencyId },
    } = req;
    await PartnerAgency.findByIdAndUpdate(agencyId, {
      name,
      address,
      phone,
      email,
      services,
    });
    res.status(200).json({ info: "Partner Agency information has been updated." });
  } catch (error) {
    throw new Error(error);
  }
});

handler.delete(async (req, res) => {
  const {
    query: { agencyId },
  } = req;
  try {
    await PartnerAgency.findByIdAndUpdate(agencyId, { active: false });
    res.status(200).json({ message: "Partner Agency has been deleted. You can retrieve this information by going to the Deleted Partner Agencies section." });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
