import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import mongoose from "mongoose";
import { partnerAgencySchema } from "../../../models/PartnerAgency";
import { extractUser } from "../../../lib/api-helpers";

const handler = nextConnect();

let PartnerAgency;

try {
  PartnerAgency = mongoose.model("PartnerAgency");
} catch (error) {
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
}

handler.use(middleware);
handler.put(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    const { credit, agencyId } = req.body;
    const newAmount = Number(String(credit).split(".").join(""));
    if (!currentUser) {
      return res.status(403).json({ message: "You must login" });
    }
    await PartnerAgency.findByIdAndUpdate(agencyId, {
      $inc: { credit: newAmount },
    });

    res.send("ok");
  } catch (err) {
    throw err;
  }
});

export default handler;
