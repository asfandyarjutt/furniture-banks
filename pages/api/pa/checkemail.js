import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { extractUser } from "../../../lib/api-helpers";
import { userSchema } from "../../../models/User";
import { partnerAgencySchema } from "../../../models/PartnerAgency";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
const handler = nextConnect();

let User;
let PartnerAgency;
let FurnitureBank;
try {
  User = mongoose.model("User");
  PartnerAgency = mongoose.model("PartnerAgency");
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  User = mongoose.model("User", userSchema);
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);
handler.post(async (req, res) => {
  const currentUser = extractUser(req);
  if (currentUser?.role !== "ADMIN" && currentUser?.role !== "FBA") {
    return res.json({ message: `Sorry you can't perform that action` });
  }
  const { email } = req.body;
  // const type = req.query.type;
  // if (type) {
    // if (type == 0) {
      if (await PartnerAgency.findOne({ email })) {
        return res.status(403).send("Case Manager email is already in use");
      } else if (await FurnitureBank.findOne({ email })) {
        return res.status(403).send("Case Manager email is already in use");
      } else if (await User.findOne({ email })) {
        return res.status(403).send("Case Manager email is already in use");
      } else {
        return res.status(200).send("email not in use");
      // }
    // }
    //  else {
    //   if (await PartnerAgency.findOne({ email })) {
    //     return res.status(403).send("Case Manager email is already in use");
    //   } else if (await FurnitureBank.findOne({ email })) {
    //     return res.status(403).send("Case Manager email is already in use");
    //   } else if (await User.findOne({ email })) {
    //     return res.status(403).send("Case Manager email is already in use");
    //   } else {
    //     return res.status(200).send("email not in use");
    //   }
    // }
  }
});

export default handler;
