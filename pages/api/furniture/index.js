import nextConnect from "next-connect";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import { furnitureItemSchema } from "../../../models/FurnitureItem";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { extractUser } from "../../../lib/api-helpers";
import generator from "generate-password";

const handler = nextConnect();

handler.use(middleware);
let FurnitureBank;
let FurnitureItem;
try {
  FurnitureBank = mongoose.model("FurnitureBank");
  FurnitureItem = mongoose.model("FurnitureItem");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  FurnitureItem = mongoose.model("FurnitureItem", furnitureItemSchema);
}

handler.get(async (req, res) => {
  const currentUser = extractUser(req);

  if (currentUser.role !== "ADMIN") {
    return res
      .status(403)
      .json({ message: `Sorry you can't perform that action` });
  }
  try {
    let allFurnitures = await FurnitureItem.find().sort({ date: -1 });
    res.status(200).json({
      allFurnitures,
    });
  } catch (error) {
    throw new Error(error);
  }
});

handler.post(async (req, res) => {
  const currentUser = extractUser(req);
  try {
    const sku = generator
      .generate({ length: 8, numbers: true })
      .toLocaleUpperCase();
    const furniture = await FurnitureItem.create({
      ...req.body,
      sku,
    });

    const FB = await FurnitureBank.findOne({
      _id: currentUser.furnitureBank._id,
    });

    FB.catalogue.push(furniture._id);
    await FB.save();
    res
      .status(200)
      .json({ furniture, message: `New item has been added to catalog.` });
  } catch (error) {
    res.status(400);
    throw new Error(error);
  }
});

handler.patch(async (req, res) => {
  const currentUser = extractUser(req);
  if (currentUser.role !== "ADMIN") {
    return res
      .status(403)
      .json({ message: `Sorry you can't perform that action` });
  }
  const { furnitureId } = req.query;
  try {
    const furniture = await FurnitureItem.findOneAndUpdate(
      furnitureId,
      req.body,
      {
        new: true,
      }
    );
    res
      .status(200)
      .json({ furniture, message: `Furniture successfuly updated` });
  } catch (error) {
    throw new Error(error);
  }
});

handler.delete(async (req, res) => {
  const currentUser = extractUser(req);
  if (currentUser.role !== "ADMIN") {
    return res
      .status(403)
      .json({ message: `Sorry you can't perform that action` });
  }
  const { furnitureId } = req.query;
  try {
    res
      .status(302)
      .json({ message: `This furniture has been successfully deleted` });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
