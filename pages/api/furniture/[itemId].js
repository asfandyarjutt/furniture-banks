import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { furnitureItemSchema } from "../../../models/FurnitureItem";
import mongoose from "mongoose";
const handler = nextConnect();

handler.use(middleware);

let FurnitureItem;
try {
  FurnitureItem = mongoose.model("FurnitureItem");
} catch (error) {
  FurnitureItem = mongoose.model("FurnitureItem", furnitureItemSchema);
}
handler.get(async (req, res) => {
  const {
    query: { itemId },
  } = req;
  const item = await FurnitureItem.findById(itemId);
  res.json({ item });
});

handler.patch(async (req, res) => {
  const {
    query: { itemId },
  } = req;
  await FurnitureItem.findByIdAndUpdate(itemId, { ...req.body });
  res.send("ok");
  res.status(200).json({ status: "Item Updated" });
});

handler.delete(async (req, res) => {
  const {
    query: { itemId },
  } = req;
  await FurnitureItem.findByIdAndDelete(itemId);
  res.status(200).json({ info: "Item Deleted" });
});

export default handler;
