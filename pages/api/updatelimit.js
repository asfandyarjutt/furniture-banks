import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../middlewares";
import { extractUser } from "../../lib/api-helpers";
import { furnitureBankSchema } from "../../models/FurnitureBank";

const handler = nextConnect();
let FurnitureBank;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);
handler.put(async (req, res) => {
  const currentUser = extractUser(req);
  const { limitper } = req.body;
  try {
    await FurnitureBank.findByIdAndUpdate(currentUser.furnitureBank._id, {
      limitper,
    });
    return res.status(200).json({ message: "updated" });
  } catch (err) {
    throw (err);
  }
});

export default handler;
