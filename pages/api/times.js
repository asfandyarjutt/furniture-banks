import nextConnect from "next-connect";
import middleware from "../../middlewares";
import mongoose from "mongoose";
import { furnitureBankSchema } from "../../models/FurnitureBank";
import { timesSchema } from "../../models/Times";
import { extractUser } from "../../lib/api-helpers";

const handler = nextConnect();
let FurnitureBank, Times;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
  Times = mongoose.model("Times");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  Times = mongoose.model("Times", timesSchema);
}
handler.use(middleware);
handler.get(async (req, res) => {
  const currentUser = extractUser(req);
  if (!currentUser) {
    return res.status(403).json({ message: "you must login" });
  }
  const times = await Times.findById(currentUser.furnitureBank.times);

  res.status(200).json(times);
});

handler.put(async (req, res) => {
  const currentUser = extractUser(req);
  const { start, end, zip, day, limit, selfHaulDays, deliveryDays } = req.body;

  try {
    const times = await Times.findOne({
      _id: currentUser.furnitureBank.times,
    });
    if (zip) {
      const filterValue = times.delivery.find((deliveryDate) => {
        return (
          deliveryDate.start === start &&
          deliveryDate.end === end &&
          deliveryDate.zip === zip &&
          deliveryDate.day === day
        );
      });
      if (filterValue) {
        times.delivery = times.delivery.map((deliveryDate) =>
          deliveryDate.start === start &&
          deliveryDate.end === end &&
          deliveryDate.zip === zip &&
          deliveryDate.day === day
            ? { ...req.body }
            : deliveryDate
        );
      } else if (start && end && zip && day && limit) {
        times.delivery.push({ start, end, zip, day, limit });
      }
    } else {
      const filterValue = times.selfHaul.find((deliveryDate) => {
        return (
          deliveryDate.start === start &&
          deliveryDate.end === end &&
          deliveryDate.day === day
        );
      });
      if (filterValue) {
        times.selfHaul = times.selfHaul.map((deliveryDate) =>
          deliveryDate.start === start &&
          deliveryDate.end === end &&
          deliveryDate.day === day
            ? { ...req.body }
            : deliveryDate
        );
      } else if (start && end && day && limit) {
        times.selfHaul.push({ start, end, day, limit });
      }
    }
    if (selfHaulDays) {
      times.selfHaulDays = selfHaulDays;
    }
    if (deliveryDays) {
      times.deliveryDays = deliveryDays;
    }
    await times.save();
    res.status(200).json({ message: "Times Created" });
  } catch (error) {
    throw new Error(`${error}`);
  }
});

handler.delete(async (req, res) => {
  const currentUser = extractUser(req);
  const { days, type } = req.body;
  try {
    if (currentUser.role !== "FBA") {
      return res.status(403).json({
        message: `You can't perform this action`,
      });
    }
    const times = await Times.findOne({
      _id: currentUser.furnitureBank.times,
    });
    if (type === "delivery") {
      times.delivery = times.delivery.filter(
        (el, i) => !days.includes(String(i))
      );
    } else {
      times.selfHaul = times.selfHaul.filter(
        (el, i) => !days.includes(String(i))
      );
    }
    times.save();

    res.status(200).json({ message: "deleted" });
  } catch (err) {
    throw err;
  }
});

export default handler;
