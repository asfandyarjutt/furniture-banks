export function lookUp(from, localField, foreignField, as) {
  return {
    $lookup: {
      from: from,
      localField: localField,
      foreignField: foreignField,
      as: as,
    },
  };
}

export function sortValue() {
  return { $sort: { date: -1 } };
}

export function skipValue(curPage, perPage) {
  return { $skip: (curPage - 1) * perPage };
}

export function windValue(agency) {
  return { $unwind: agency };
}

export function limitValue(perPage) {
  return { $limit: perPage };
}
