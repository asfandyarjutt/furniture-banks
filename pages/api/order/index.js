import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { send_mail } from "../../../lib/mailer";
import { extractUser } from "../../../lib/api-helpers";
import { orderSchema } from "../../../models/Order";
import { furnitureItemSchema } from "../../../models/FurnitureItem";
import { partnerAgencySchema } from "../../../models/PartnerAgency";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import Dinero from "dinero.js";
import {
  ServicesConfig,
  ServicesContainer,
  Address,
  CreditCardData,
} from "globalpayments-api";
import User from "../../../models/User";
import {
  limitValue,
  lookUp,
  skipValue,
  windValue,
  sortValue,
} from "./aggregation";
const handler = nextConnect();
let Order, PartnerAgency, FurnitureItem, FurnitureBank;
const config = new ServicesConfig();
// config.secretApiKey = process.env.NEXT_PUBLIC_SECRETKEY;
config.secretApiKey = 'pskapi_cert_MU6vBQDFsm8Ausb4s9DWXYZFjWFGB2BMNEauLR2rfA';
config.serviceUrl = "https://cert.api2.heartlandportico.com";
config.developerId = "002914";
config.versionNumber = "5105";

ServicesContainer.configure(config);
try {
  Order = mongoose.model("Order");
  PartnerAgency = mongoose.model("PartnerAgency");
  FurnitureItem = mongoose.model("FurnitureItem");
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  Order = mongoose.model("Order", orderSchema);
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
  FurnitureItem = mongoose.model("FurnitureItem", furnitureItemSchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);

handler.get(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    const curPage = req.query.page || 1;

    const perPage = 10;
    let orders;
    let count;
    let keyword = {};
    let maxPage;

    let regex = new RegExp(req.query.keyword, "i");
    if (currentUser?.furnitureBank != null) {
      const allOrders = await Order.aggregate([
        { $match: { furnitureBank: currentUser.furnitureBank._id } },
        lookUp("partneragencies", "agency", "_id", "agency"),
        windValue("$agency"),
        sortValue(),
        skipValue(curPage, perPage),
        limitValue(perPage),
      ]);

      const searchPipe = await Order.aggregate([
        { $match: { furnitureBank: currentUser.furnitureBank._id } },
        lookUp("partneragencies", "agency", "_id", "agency"),
        windValue("$agency"),
        {
          $match: {
            $or: [
              { number: regex },
              { "agency.name": regex },
              { cmName: regex },
            ],
          },
        },
        sortValue(),
        skipValue(curPage, perPage),
      ]);
      if (currentUser.role === "FBA") {
        count = await Order.find({
          furnitureBank: currentUser.furnitureBank._id,
        })
          .populate("agency")
          .sort({ date: -1 });

        if (req?.query?.keyword === undefined) {
          orders = allOrders;
          maxPage = Math.ceil(count.length / perPage);
        } else {
          orders = searchPipe;
          maxPage = 1;
        }
      } else {
        keyword = {
          $and: [
            {
              $or: [
                { clientName: regex },
                { number: regex },
                { cmName: regex },
              ],
            },
          ],
        };
        if (req?.query?.keyword === undefined) {
          count = await Order.find({
            cmEmail: currentUser.email,
          })
            .populate("agency")
            .sort({ date: -1 });

          orders = await Order.find({
            cmEmail: currentUser.email,
          })
            .populate("agency")
            .sort({ date: -1 })
            .skip((curPage - 1) * perPage)
            .limit(perPage);
          maxPage = count.length / perPage;
        } else {
          orders = await Order.find({
            cmEmail: currentUser.email,
            ...keyword,
          })
            .populate("agency")
            .sort({ date: -1 })
            .skip((curPage - 1) * perPage);
          maxPage = 1;
        }
      }
      const newOrders = orders.map((order) => {
        let currentDate = new Date();
        let prevDate = new Date(order.deliverDate);
        if (currentDate > prevDate) {
          Object.assign(order, { status: "Completed" });
        } else {
          Object.assign(order, { status: "In-Progress" });
        }
        return order;
      });
      let deliveryorder = newOrders.map((order) => {
        let newDate = new Date();
        let orderDate = order.deliverDate;
        let deliverystatus = order.deliveryStatus;

        if (
          order.status === "Completed" &&
          newDate === orderDate &&
          deliverystatus !== undefined &&
          deliverystatus === false
        ) {
          send_mail(
            `Order Number: ${order.number}`,
            `${currentUser.email}`,
            `Order Number: ${order.number}`,
            `<div>
              <h1>Order Number: ${order.number}</h1>
            <h2>Number of Items: ${order.items.length}</h2>
            <h1>Order Status: ${order.status}</h1>
            <h3>Client information</h3>
               <p>
              ${order._doc.clientName}
              <br/> email: ${order.clientEmail}
              <br/> phone: ${order.clientPhone}
              <br/> address: ${order.clientAddress}, ${order.clientZip !== undefined ? order.clientZip : null
            }, ${order.clientCity}
            </p>
               <h3>Case Manager information</h3>
               <p>
                 ${order.cmName}
                 <br/> email: ${order.cmEmail}
                 <br/> phone: ${order.cmPhone}
               </p>
               <h4>Client payment: ${order.clientAmount}, type: ${order.clientPaymentType
            }</h4>
               <h4>Agency payment: ${order.agencyAmount}, type: ${order &&
              order?.agencyPaymentType &&
              order?.agencyPaymentType?.charAt(0)?.toUpperCase() +
              order?.agencyPaymentType?.slice(1) ===
              "Heartland"
              ? "Credit Card"
              : order?.agencyPaymentType?.charAt(0)?.toUpperCase() +
              order?.agencyPaymentType?.slice(1)
            }</h4>
               <table cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; margin: 0;">
               <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                 <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">ITEM</td>
             <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">QUANTITY</td>
                 <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">SKU</td>
               </tr>
               ${order.items.map((item) => {
              return `<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                 <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.sku}</td>
                 <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.quantity}</td>
                <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.sku}</td>
                 </tr>`;
            })}
               </table>
            
            
            
            
            
            </div>`
          );
          Order.findByIdAndUpdate(
            { _id: order._id },
            { $set: { deliveryStatus: true } }
          );
        }
      });
      return res.status(200).json({
        message: "Fetched Orders",
        orders: newOrders,
        curPage: curPage,
        maxPage: maxPage,
        perPage: perPage,
      });
    } else {
      return res.status(200).json({
        message: "Fetched Orders",
        orders: [],
      });
    }
  } catch (error) {
    throw error;
  }
});

handler.post(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    let emailTexts = {};
    let emailSubjects = {};
    let fbaEmail;
    const furnitureBank = await FurnitureBank.findById({
      _id: currentUser?.furnitureBank?._id,
    });
    const [owner] = await User.find({ _id: furnitureBank.owner });
    emailTexts = furnitureBank.emailTexts;
    emailSubjects = furnitureBank.emailSubject;
    fbaEmail = owner.email;
    const orders = await Order.find();
    const number = orders.length >= 0 ? String(orders.length + 1) : "1";
    const {
      items,
      contact: {
        clientName,
        clientEmail,
        clientPhone,
        zip,
        city,
        apt,
        address,
        state,
        cmPhone,
        cmEmail,
        cmName,
      },
      household: houseHold,
      role,
      bg,
      payment: {
        paymentType,
        agencyPayment,
        clientPayment,
        agencyPaymentType,
        clientPaymentType,
      },
      final,
      authorizedPayment,
    } = req.body;




    const total = new Dinero({
      amount: Number(agencyPayment.replace(".", "")),
      currency: "USD",
    }).add(Dinero({ amount: Number(clientPayment.replace(".", "")) }));
    const agency = await PartnerAgency.findOne({
      $or: [
        { caseManagers: { $all: [currentUser._id] } },
        { liaison: currentUser._id },
      ],
    });
    if (agencyPaymentType === "Prepay") {
      const oldValue = Number(agencyPayment);
      const availableCredit = Number(agency.credit);
      if (Number(agencyPayment) > Number(agency.credit)) {
        return res.status(403).json({ message: "insufficient credit" });
      }
      agency.credit = availableCredit - oldValue;
    }
    items.forEach(async (item) => {
      await FurnitureItem.findByIdAndUpdate(item.item, {
        $inc: { quantity: -Number(item.quantity) },
      });
    });

    await Order.create({
      date: new Date(),
      clientName,
      clientAddress: address,
      clientAppartment: apt,
      clientZip: zip,
      clientCity: city,
      clientState: state,
      clientPhone: clientPhone,
      clientEmail: clientEmail,
      deliveryStatus: false,
      agency: agency._id,
      number,
      cmName,
      cmEmail,
      cmPhone,
      receiverName: final.name,
      receiverPhone: final.phone,
      items,
      type: `${paymentType.split(" ")[1]} ${paymentType.split(" ")[2]}`,
      total: total.getAmount(),
      clientAmount: clientPayment,
      agencyAmount: agencyPayment,
      agencyPaymentType,
      clientPaymentType,
      houseHold,
      role,
      bg,
      deliverDate: new Date(final.date),
      furnitureBank: currentUser.furnitureBank._id,
    })
      .then(() => {
        send_mail(
          `Order Number: ${number}`,
          `${cmEmail}`,
          `${role === "PAL"
            ? emailSubjects?.paSubject
            : emailSubjects?.caseSubject
          }`,
          `<div>
        <h1>Order Number: ${number}</h1>
        <h2>Number of Items: ${items.length}</h2>
        <p> ${role === "PAL" ? emailTexts?.partnerAgency : emailTexts?.caseManager
          }</p>
        <h3>Client information</h3>
        <p>
          ${clientName}
          <br/> email: ${clientEmail}
          <br/> phone: ${clientPhone}
          <br/> address: ${address}, ${zip !== undefined ? zip : null}, ${city}
        </p>
        <h3>Case Manager information</h3>
        <p>
          ${cmName}
          <br/> email: ${cmEmail}
          <br/> phone: ${cmPhone}
        </p>
        <h4>Client payment: ${clientPayment}, type: ${clientPaymentType}</h4>
        <h4>Agency payment: ${agencyPayment}, type: ${agencyPaymentType &&
            agencyPaymentType?.charAt(0)?.toUpperCase() +
            agencyPaymentType?.slice(1) ===
            "Heartland"
            ? "Credit Card"
            : agencyPaymentType?.charAt(0)?.toUpperCase() +
            agencyPaymentType?.slice(1)
          }</h4>
        <table cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; margin: 0;">
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">ITEM</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">QUANTITY</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">SKU</td>
        </tr>
        ${items.map((item) => {
            return `<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.name}</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.quantity}</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.sku}</td>
          </tr>`;
          })}
        </table>
      </div>`
        ); //first email

        //second email
        send_mail(
          `Order Number: ${number}`,
          `${clientEmail}`,
          `${emailSubjects?.clientSubject}`,
          `<div>
        <h1>Order Number: ${number}</h1>
        <h2>Number of Items: ${items.length}</h2>
        <p>${emailTexts.client}</p>
        <h3>Client information</h3>
        <p>
          ${clientName}
          <br/> email: ${clientEmail}
          <br/> phone: ${clientPhone}
          <br/> address: ${address}, ${zip !== undefined ? zip : null}, ${city}
        </p>
        <h3>Case Manager information</h3>
        <p>
          ${cmName}
          <br/> email: ${cmEmail}
          <br/> phone: ${cmPhone}
        </p>
        <h4>Client payment: ${clientPayment}, type: ${clientPaymentType}</h4>
        <h4>Agency payment: ${agencyPayment}, type: ${agencyPaymentType &&
            agencyPaymentType?.charAt(0)?.toUpperCase() +
            agencyPaymentType?.slice(1) ===
            "Heartland"
            ? "Credit Card"
            : agencyPaymentType?.charAt(0)?.toUpperCase() +
            agencyPaymentType?.slice(1)
          }</h4>
        <table cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; margin: 0;">
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">ITEM</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">QUANTITY</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">SKU</td>
        </tr>
        ${items.map((item) => {
            return `<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.name}</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.quantity}</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.sku}</td>
          </tr>`;
          })}
        </table>
      </div>`
        );
        //third email
        send_mail(
          `Order Number: ${number}`,
          `${fbaEmail}`,
          `Order Number: ${number}`,
          `<div>
        <h1>Order Number: ${number}</h1>
        <h2>Number of Items: ${items.length}</h2>
        <h3>Client information</h3>
        <p>
          ${clientName}
          <br/> email: ${clientEmail}
          <br/> phone: ${clientPhone}
          <br/> address: ${address}, ${zip !== undefined ? zip : null}, ${city}
        </p>
        <h3>Case Manager information</h3>
        <p>
          ${cmName}
          <br/> email: ${cmEmail}
          <br/> phone: ${cmPhone}
        </p>
        <h4>Client payment: ${clientPayment}, type: ${clientPaymentType}</h4>
        <h4>Agency payment: ${agencyPayment}, type: ${agencyPaymentType &&
            agencyPaymentType?.charAt(0)?.toUpperCase() +
            agencyPaymentType?.slice(1) ===
            "Heartland"
            ? "Credit Card"
            : agencyPaymentType?.charAt(0)?.toUpperCase() +
            agencyPaymentType?.slice(1)
          }</h4>
        <table cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; margin: 0;">
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">ITEM</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">QUANTITY</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">SKU</td>
        </tr>
        ${items.map((item) => {
            return `<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.name}</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.quantity}</td>
          <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item.sku}</td>
          </tr>`;
          })}
        </table>
      </div>`
        );
      })
      .catch((error) => {
        console.log(error);
        res.status(400).json({ message: "There is a Problem Creating Order" });
      }); //end of then
    await agency.save();

    res.status(200).json({ message: "Order Completed" });
  } catch (err) {
    throw new Error(`Error Creating Order, ${err}`);
  }
});

export default handler;
