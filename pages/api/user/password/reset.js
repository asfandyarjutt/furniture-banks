import crypto from "crypto";
import bcrypt from "bcrypt";
import mongoose from "mongoose";
import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import { send_mail } from "../../../../lib/mailer";
import { userSchema } from "../../../../models/User";
import { tokenSchema } from "../../../../models/Token";

const handler = nextConnect();
let User, Token;

try {
  User = mongoose.model("User");
  Token = mongoose.model("Token");
} catch (error) {
  User = mongoose.model("User", userSchema);
  Token = mongoose.model("Token", tokenSchema);
}

handler.use(middleware);
handler.post(async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(401).json({ message: "Email not found" });
    const token = crypto.randomBytes(32).toString("hex");
    await Token.create({
      token,
      user: user._id,
      type: "passwordReset",
      expireAt: new Date(Date.now() + 1000 * 60 * 20),
    });
    send_mail(
      "Reset your password",
      user.email,
      "Reset your password",
      `
    <div>
      <p>Hello ${user.email}</p>
      <p>Please follow <a href="${process.env.WEB_URI}/forget-password/${token}">this link</a> to reset your password.</p>
    </div>
    `
    );
    res
      .status(200)
      .json({ message: "You should receive an email soon, check you inbox." });
  } catch (error) {
    res.status(500);
    throw new Error({ message: error });
  }
});

handler.put(async (req, res) => {
  try {
    if (!req.body.password) {
      return res.status(400).json({ message: "Password not provided" });
    }
    const tokenDoc = await Token.findOne({
      token: req.body.token,
      type: "passwordReset",
    });
    if (!tokenDoc) {
      return res
        .status(403)
        .json({ message: "This link may have been expired" });
    }

    const password = bcrypt.hashSync(req.body.password, 12);
    await User.findByIdAndUpdate(tokenDoc.user._id, { $set: { password } });
    await Token.findOneAndDelete({
      token: req.body.token,
      type: "passwordReset",
    });
    res.status(201).json({ message: "Password reset successfully" });
  } catch (error) {
    res.status(500);
    throw new Error({ message: error });
  }
});

export default handler;
