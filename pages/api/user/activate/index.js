import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../../middlewares";
import { userSchema } from "../../../../models/User";

const handler = nextConnect();

let User;

try {
  User = mongoose.model("User");
} catch (error) {
  User = mongoose.model("User", userSchema);
}

handler.use(middleware);
handler.patch(async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.body.userId });
    if (user.active) {
      return res.status(400).json({ message: "User is already active" });
    }
    user.active = true;
    await user.save();
    res.status(200).json({ message: "User is now active" });
  } catch (error) {
    res.status(500);
    throw new Error({ message: error });
  }
});

export default handler;
