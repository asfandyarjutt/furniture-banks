import nextConnect from "next-connect";
import middleware from "../../middlewares";
import passport from "../../lib/passport";
import { extractUser } from "../../lib/api-helpers";
import {
  ADMIN,
  FailMessage,
  FBA,
  PACM,
  PAL,
} from "../../components/Forms/constants";

const handler = nextConnect();

handler.use(middleware);

handler.post(passport.authenticate("local"), (req, res) => {
  const user = extractUser(req);
  if (user.role === FBA) {
    user.furnitureBank.active == false
      ? res.status(403).json({
          statusMessage: FailMessage,
        })
      : res.json({ user: user });
  }
  if (user.role === PAL) {
    
    if (user.partnerAgency.active == false) {
      res.status(403).json({
        statusMessage: FailMessage,
      });
    } else {
      res.json({ user: user });
    }
  }
  if (user.role === ADMIN || PACM) {
    res.json({ user: user });
  }
});

handler.delete((req, res) => {
  req.logOut();
  req.session.destroy();
  res.status(204).end();
});

export default handler;
