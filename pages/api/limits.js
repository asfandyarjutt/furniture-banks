import nextConnect from "next-connect";
import middleware from "../../middlewares";
import mongoose from "mongoose";
import { extractUser } from "../../lib/api-helpers";
import { furnitureBankSchema } from "../../models/FurnitureBank";

const handler = nextConnect();
let FurnitureBank;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model("FurnitureBank", furnitureBankSchema);
}

handler.use(middleware);
handler.put(async (req, res) => {
  const { from, to } = req.body;
  const currentUser = extractUser(req);
  if (!from || !to) {
    return res.status(400).json("Empty fields");
  }
  try {
    if (!currentUser) {
      return res.status(403).json({ message: "You must login" });
    }
    const fb = await FurnitureBank.findOne({
      _id: currentUser.furnitureBank._id,
    });
    if (!fb.purchaselimits) {
      fb.purchaselimits = [];
    }

    fb.purchaselimits.push({
      to: req.body.to,
      from: req.body.from,
      bycategory: req.body.bycategory,
      total: req.body.total,
    });

    await fb.save();
  } catch (err) {
    throw err;
  }
  res.status(200).json({
    info: "Purchase limit has been updated.",
  });
});

export default handler;
