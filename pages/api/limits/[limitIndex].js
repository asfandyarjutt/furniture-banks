import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { extractUser } from "../../../lib/api-helpers";
import { furnitureBankSchema } from "../../../models/FurnitureBank";

const handler = nextConnect();
handler.use(middleware);
let FurnitureBank;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.delete(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    const fb = await FurnitureBank.findOne({
      _id: currentUser.furnitureBank._id,
    });
    const index = Number(req.query.limitIndex);
    fb.purchaselimits = fb.purchaselimits.filter((_, i) => i !== index);
    await fb.save();
    res.status(200).json({ message: "deleted" });
  } catch (error) {
    throw error;
  }
});

handler.put(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    const fb = await FurnitureBank.findOne({
      _id: currentUser.furnitureBank._id,
    });
    const index = Number(req.query.limitIndex);
    const { to, from, total, bycategory } = req.body;
    fb.purchaselimits = fb.purchaselimits.map((limit, i) =>
      i === index ? { to, from, total, bycategory } : limit
    );
    await fb.save();
    res.status(200).json({
      info: "Purchase limit has been updated.",
    });
  } catch (error) {
    throw error;
  }
});

export default handler;
