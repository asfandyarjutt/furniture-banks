import nextConnect from "next-connect";
import mongoose from "mongoose";
import middleware from "../../../middlewares";
import { send_mail } from "../../../lib/mailer";
import { extractUser } from "../../../lib/api-helpers";
import { orderSchema } from "../../../models/Order";
import { furnitureItemSchema } from "../../../models/FurnitureItem";
import { partnerAgencySchema } from "../../../models/PartnerAgency";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import Dinero from "dinero.js";

import User from "../../../models/User";
import {
  limitValue,
  lookUp,
  skipValue,
  windValue,
  sortValue,
} from "./aggregation";
const handler = nextConnect();
let Order, PartnerAgency, FurnitureItem, FurnitureBank;

try {
  Order = mongoose.model("Order");
  PartnerAgency = mongoose.model("PartnerAgency");
  FurnitureItem = mongoose.model("FurnitureItem");
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  Order = mongoose.model("Order", orderSchema);
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
  FurnitureItem = mongoose.model("FurnitureItem", furnitureItemSchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);

handler.get(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    const curPage = req.query.page || 1;

    const perPage = 10;
    let orders;
    let count;
    let keyword = {};
    let maxPage;

    let regex = new RegExp(req.query.keyword, "i");

    if (currentUser?.furnitureBank != null) {
      const allOrders = await Order.aggregate([
        {
          $match: {
            furnitureBank: currentUser.furnitureBank._id,
            agencyPaymentType: "invoice",
          },
        },
        lookUp("partneragencies", "agency", "_id", "agency"),
        windValue("$agency"),
        sortValue(),
        skipValue(curPage, perPage),
        limitValue(perPage),
      ]);

      const searchPipe = await Order.aggregate([
        {
          $match: {
            furnitureBank: currentUser.furnitureBank._id,
            agencyPaymentType: "invoice",
          },
        },
        lookUp("partneragencies", "agency", "_id", "agency"),
        windValue("$agency"),
        {
          $match: {
            $or: [
              { number: regex },
              { "agency.name": regex },
              { cmName: regex },
            ],
          },
        },
        sortValue(),
        skipValue(curPage, perPage),
      ]);
      if (currentUser.role === "FBA") {
        count = await Order.find({
          furnitureBank: currentUser.furnitureBank._id,
          agencyPaymentType: "invoice",
        })
          .populate("agency")
          .sort({ date: -1 });

        if (req?.query?.keyword === undefined) {
          orders = allOrders;
          maxPage = Math.ceil(count.length / perPage);
        } else {
          orders = searchPipe;
          maxPage = 1;
        }
      } else {
        keyword = {
          $and: [
            {
              $or: [
                { clientName: regex },
                { number: regex },
                { cmName: regex },
              ],
            },
          ],
        };
        if (req?.query?.keyword === undefined) {
          count = await Order.find({
            cmEmail: currentUser.email,
            agencyPaymentType: "invoice",
          })
            .populate("agency")
            .sort({ date: -1 });
          orders = await Order.find({
            cmEmail: currentUser.email,
            agencyPaymentType: "invoice",
          })
            .populate("agency")
            .sort({ date: -1 })
            .skip((curPage - 1) * perPage)
            .limit(perPage);
          maxPage = count.length / perPage;
        } else {
          orders = await Order.find({
            cmEmail: currentUser.email,
            agencyPaymentType: "invoice",
            ...keyword,
          })
            .populate("agency")
            .sort({ date: -1 })
            .skip((curPage - 1) * perPage);
          maxPage = 1;
        }
      }
      return res.status(200).json({
        message: "Fetched Orders",
        orders: orders,
        curPage: curPage,
        maxPage: maxPage,
        perPage: perPage,
      });
    } else {
      return res.status(200).json({
        message: "Fetched Orders",
        orders: [],
      });
    }
  } catch (error) {
    throw error;
  }
});

export default handler;
