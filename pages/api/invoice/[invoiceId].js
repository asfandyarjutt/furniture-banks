import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { orderSchema } from "../../../models/Order";
import { furnitureItemSchema } from "../../../models/FurnitureItem";
import mongoose from "mongoose";

const handler = nextConnect();
handler.use(middleware);

let Order;
let FurnitureItem;
try {
  Order = mongoose.model("Order");
  FurnitureItem = mongoose.model("FurnitureItem");
} catch (error) {
  Order = mongoose.model("Order", orderSchema);
  FurnitureItem = mongoose.model("FurnitureItem", furnitureItemSchema);
}

handler.get(async (req, res) => {
  const { invoiceId } = req.query;
  try {
    const oneInvoice = await Order.findById(invoiceId)
      .populate("agency")
      .populate({
        path: "items",
        populate: {
          path: "item",
          model: "FurnitureItem",
        },
      });
    res.status(200).json({ oneInvoice });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
