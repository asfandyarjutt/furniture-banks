import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { orderSchema } from "../../../models/Order";
import { send_mail } from "../../../lib/mailer";
import { userSchema } from "../../../models/User";
import { extract_user } from "../../../lib/api-helpers";
import mongoose from "mongoose";

const handler = nextConnect();
handler.use(middleware);

let Order, User;
try {
  Order = mongoose.model("Order");
  User = mongoose.model("User");
} catch (error) {
  Order = mongoose.model("Order", orderSchema);
  User = mongoose.model("User", userSchema);
}

handler.post(async (req, res) => {
  try {
    const { orderId, userId } = req.body;

    let user = await User.findById({ _id: userId }).populate("furnitureBank");
    user = extract_user(user);

    const order = await Order.findById(orderId)
      .populate("agency")
      .populate({
        path: "items",
        populate: {
          path: "item",
          model: "FurnitureItem",
        },
      });

    const data = {
      emailSubject: user?.furnitureBank?.emailSubject,
      emailTexts: user?.furnitureBank?.emailTexts,
      role: order?.role ? order?.role : "both",
      number: order?.number,
      cmEmail: order?.cmEmail,
      cmName: order?.cmName,
      cmPhone: order?.cmPhone,
      agencyPaymentType: order?.agencyPaymentType,
      agencyPayment: order?.agencyAmount,
      clientPayment: order?.clientAmount,
      clientPaymentType: order?.clientPaymentType,
      clientName: order?.clientName,
      clientEmail: order?.clientEmail,
      clientPhone: order?.clientPhone,
      address: order?.clientAddress,
      city: order?.clientCity,
      zip: order?.clientZip,
      state: order?.clientState,
      items: order?.items,
      emails: [order?.cmEmail, order?.clientEmail],
    };

    data?.emails?.map((email, i) => {
      send_mail(
        `Order Number: ${data?.number}`,
        `${email}`,
        ` ${
          i === 0
            ? data?.role === "PAL"
              ? data?.emailSubject?.paSubject
                ? data?.emailSubject?.paSubject
                : "Hi Partner Agency"
              : data?.role === "PACM"
              ? data?.emailSubject?.caseSubject
                ? data?.emailSubject?.caseSubject
                : "Hi Case Worker"
              : "Hi Case Worker"
            : i === 1
            ? data?.emailSubject?.clientSubject
            : "Hi Client"
        }`,
        `<div>
      <h1>Order Number: ${data?.number}</h1>
      <h2>Number of Items: ${data?.items.length}</h2>
    <p> ${
      i === 0
        ? data?.role === "PAL"
          ? data?.emailTexts?.partnerAgency
            ? data?.emailTexts?.partnerAgency
            : "Thank You!"
          : data?.role === "PACM"
          ? data?.emailTexts?.caseManager
            ? data?.emailTexts?.caseManager
            : "Thank You!"
          : "Thank You!"
        : i === 1
        ? data?.emailTexts?.client
        : "Thank You!"
    }</p>
    <h3>Client information</h3>
    <p>
    ${data?.clientName}
    <br/> email: ${data?.clientEmail}
      <br/> phone: ${data?.clientPhone}
      <br/> address: ${data?.address}, ${
          data?.zip !== undefined ? data?.zip : null
        }, ${data?.city}
    </p>
    <h3>Case Manager information</h3>
    <p>
    ${data?.cmName}
    <br/> email: ${data?.cmEmail}
    <br/> phone: ${data?.cmPhone}
    </p>
    <h4>Client payment: ${data?.clientPayment}, type: ${
          data?.clientPaymentType
        }</h4>
    <h4>Agency payment: ${data?.agencyPayment}, type: ${
          data &&
          data?.agencyPaymentType &&
          data?.agencyPaymentType?.charAt(0)?.toUpperCase() +
            data?.agencyPaymentType?.slice(1) ===
            "Heartland"
            ? "Credit Card"
            : data?.agencyPaymentType?.charAt(0)?.toUpperCase() +
              data?.agencyPaymentType?.slice(1)
        }</h4>
    <table cellpadding="0" cellspacing="0" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; margin: 0;">
    <thead>
    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <th style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">ITEM</th>
    <th style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">QUANTITY</th>
    <th style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">SKU</th>
    </tr>
    </thead>
    <tbody>
    ${data?.items
      ?.map(
        (item) =>
          `<tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
      <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item?.item[0]?.description}</td>
      <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item?.quantity}</td>
      <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; text-align: left; border-top-width: 1px; border-top-color: #eee; border-top-style: solid; margin: 0; padding: 5px 0;" valign="top">${item?.sku}</td>
      </tr>`
      )
      .join("")}
    </tbody>
    </table>
    </div>`
      );
    });

    res.status(200).json({ msg: "Email Sent Successfully" });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
