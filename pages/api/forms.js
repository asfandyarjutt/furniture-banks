import nextConnect from "next-connect";
import middleware from "../../middlewares";
import { extractUser } from "../../lib/api-helpers";
import mongoose from "mongoose";
import { furnitureBankSchema } from "../../models/FurnitureBank";

const handler = nextConnect();
let FurnitureBank;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.use(middleware);
handler.put(async (req, res) => {
  const currentUser = extractUser(req);
  const data = req.body.data;

  try {
    if (!currentUser) {
      return res.status(403).json({ message: "You must login" });
    }
    await FurnitureBank.findByIdAndUpdate(currentUser.furnitureBank._id, {
      $set: { bg: data },
    });
    return res.status(200).json({ message: "updated" });
  } catch (err) {
    throw err;
  }
});

export default handler;
