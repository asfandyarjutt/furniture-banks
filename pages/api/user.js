import bcrypt from "bcrypt";
import nextConnect from "next-connect";
import middleware from "../../middlewares";
import mongoose from "mongoose";
import isEmail from "validator/lib/isEmail";
import { send_mail } from "../../lib/mailer";
import { furnitureBankSchema } from "../../models/FurnitureBank";
import { userSchema } from "../../models/User";
import { orderSchema } from "../../models/Order";
import { partnerAgencySchema } from "../../models/PartnerAgency";
import { extractUser } from "../../lib/api-helpers";
import {
  ADMIN,
  EmailAlreadyUse,
  FailMessage,
  FBA,
  InvalidEmail,
  MissingFields,
  PACM,
  PAL,
  updateError,
  changesSaved,
  userDeleted,
} from "../../components/Forms/constants";
import Item from "antd/lib/list/Item";

const handler = nextConnect();
let FurnitureBank, User, PartnerAgency, Order;

try {
  Order = mongoose.model("Order");
  FurnitureBank = mongoose.model("FurnitureBank");
  User = mongoose.model("User");
  PartnerAgency = mongoose.model("PartnerAgency");
} catch (error) {
  Order = mongoose.model("Order", orderSchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  User = mongoose.model("User", userSchema);
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
}

handler.use(middleware);
handler.get(async (req, res) => {
  try {
    const user = extractUser(req);
    if (user.role === FBA) {
      user.furnitureBank.active == false
        ? res.status(403).json({
            statusMessage: FailMessage,
          })
        : res.json({ user: user });
    }
    if (user.role === PAL) {
      const partnerData = await PartnerAgency.find({ _id: user.partnerAgency });
      if (partnerData[0].active == false) {
        res.status(403).json({
          statusMessage: FailMessage,
        });
      } else {
        res.json({ user: user });
      }
    }
    if (user.role === ADMIN || PACM) {
      res.json({ user: user });
    }
  } catch (err) {
    throw err;
  }
});

handler.post(async (req, res) => {
  try {
    const { firstName, lastName } = req.body;
    const email = req.body.email;
    if (!isEmail(email)) {
      res.status(400).send(InvalidEmail);
      return;
    }
    if (!firstName || !lastName) {
      res.status(400).send(MissingFields);
      return;
    }
    if (await User.findOne({ email })) {
      res.status(403).send(EmailAlreadyUse);
      return;
    }
    const password = generator.generate({ length: 10, numbers: true });
    const hashedPassword = bcrypt.hashSync(password, 10);
    const user = await User.create({
      email,
      password: hashedPassword,
      firstName,
      lastName,
    });

    send_mail(
      "Welcome to your furniture bank account",
      email,
      "Confirmation",
      `
    <div>
      <h1>Welcome ${firstName} to your furniture bank account</h1>

      <p>Here you have 2 steps to start using your account</p>
      <ul>
        <li>
        Activate your account by visiting <a href="${process.env.WEB_URI}/activate/${user._id}">this link</a>
        </li>
        <li>
        Once you've been validate your account, sign in with your email and your temporal password: <b>${password}</b>. Change it as soon as you can.
        </li>
      </ul>
    </div>
    `
    );

    req.logIn(user, (err) => {
      if (err) throw err;

      res.status(201).json({
        user: extractUser(req),
      });
    });
  } catch (err) {
    throw err;
  }
});

handler.delete(async (req, res) => {
  const { userId } = req.body;
  const furnitureBank = await FurnitureBank.findOne({ owner: userId });
  const partnerAgency = await PartnerAgency.findOne({ liaison: userId });

  if (furnitureBank) furnitureBank["owner"] = undefined;
  if (partnerAgency) partnerAgency["liaison"] = undefined;

  await User.findByIdAndRemove(userId);
  res.status(200).json({ message: userDeleted });
});

handler.patch(async (req, res) => {
  const user = extractUser(req);

  try {
    if (user.role === "PAL") {
      let orders = await Order.find({
        cmEmail: user.email,
      })
        .populate("agency")
        .lean();

      orders = orders?.map((order) =>
        order.cmEmail === user?.email
          ? { ...order, cmEmail: req?.body?.email }
          : order
      );
      orders?.forEach(async function (order) {
        await Order.findByIdAndUpdate({ _id: order._id }, order, { new: true });
      });
    }

    await User.findByIdAndUpdate(
      { _id: user._id },
      { ...req.body },
      { new: true }
    )
      .then((response) =>
        res.status(200).json({
          infoSuccess: changesSaved,
        })
      )
      .catch((e) => res.status(403).json({ infoError: updateError }));
  } catch (error) {
    console.log(error);
  }
});

export default handler;
