import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import mongoose from "mongoose";
const handler = nextConnect();

handler.use(middleware);
let FurnitureBank;
try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}
handler.post(async (req, res) => {
  const [recoverfbs] = req.body;
  recoverfbs.forEach(async (fbid) => {
    await FurnitureBank.findByIdAndUpdate(fbid, { active: true });
  });
  res.json({ message: "Furniture Returned to Active" });
});

export default handler;
