import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import { furnitureBankSchema } from "../../../../models/FurnitureBank";
import mongoose from "mongoose";
const handler = nextConnect();

handler.use(middleware);
let FurnitureBank;
try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.patch(async (req, res) => {
  const {
    query: { fbid },
    body: { agencies },
  } = req;

  await FurnitureBank.findByIdAndUpdate(fbid, {
    $push: { partnerAgencies: agencies },
  });

  res.send("ok");
});

handler.delete(async (req, res) => {
  const {
    query: { fbid },
  } = req;

  const furnitureBank = await FurnitureBank.findOne({ _id: fbid });

  if (!furnitureBank) return res.send("Furniture Bank not found");

  const partnerAgencies = furnitureBank.partnerAgencies.filter(
    (agency) => agency != req.body.agency
  );
  furnitureBank.partnerAgencies = partnerAgencies;
  await furnitureBank.save();
  res.send("deleted");
});

export default handler;
