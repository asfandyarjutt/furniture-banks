import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { userSchema } from "../../../models/User";
import mongoose from "mongoose";
import { extractUser } from "../../../lib/api-helpers";

const handler = nextConnect();

handler.use(middleware);
let User;
try {
  User = mongoose.model("User");
} catch (error) {
  User = mongoose.model("User", userSchema);
}
handler.get(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    if (!currentUser) {
      return res.status(403).json({ message: "You need to login" });
    }
    const { furnitureBanks } = await User.findById(currentUser._id)
      .populate("furnitureBanks")
      .populate({
        path: "furnitureBanks",
        populate: { path: "owner", model: "User" },
      });
    const filtered = furnitureBanks.filter((fb) => !fb.active);
    res.json({ furnitureBanks: filtered });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
