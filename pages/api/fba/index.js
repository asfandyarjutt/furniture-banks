import mongoose from "mongoose";
import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { userSchema } from "../../../models/User";
import createUser from "../../../lib/createUser";
import { extractUser } from "../../../lib/api-helpers";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import { timesSchema } from "../../../models/Times";

import sendMails from "../../../lib/sendMails";
import { limitValue, lookUp, skipValue, windValue } from "../order/aggregation";

const handler = nextConnect();
let User;
let FurnitureBank;
let Times;
try {
  User = mongoose.model("User");
  FurnitureBank = mongoose.model("FurnitureBank");
  Times = mongoose.model("Times");
} catch (error) {
  User = mongoose.model("User", userSchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  Times = mongoose.model("Times", timesSchema);
}

handler.use(middleware);

handler.post(async (req, res) => {
  const currentUser = extractUser(req);
  if (currentUser?.role !== "ADMIN") {
    return res.json({ message: `Sorry you can't perform that action` });
  }
  const {
    name,
    address,
    phone,
    zip,
    adminEmail,
    adminPhone,
    firstName,
    lastName,
    city,
    email,
    state,
  } = req.body;

  const user = {
    email: adminEmail,
    phone: adminPhone,
    firstName,
    lastName,
    fba: true,
  };

  if (!user.firstName || !user.lastName) {
    return res.status(400).send("Missing field(s) for a user");
  }
  if (await User.findOne({ email: user.email })) {
    return res.status(403).send("Case manager email is already in use.");
  }
  const { newUser, password } = await createUser(
    user,
    undefined,
    undefined,
    currentUser.role
  );
  try {
    const furnitureBank = await FurnitureBank.create({
      name,
      address,
      phone,
      email,
      owner: newUser._id,
      city,
      state,
      zip,
    });
    const times = await Times.create({});
    const fb = await FurnitureBank.findOne({
      _id: furnitureBank._id,
    });
    fb.times = times;
    await fb.save();
    await User.findByIdAndUpdate(
      newUser._id,
      {
        $set: { furnitureBank: furnitureBank._id },
      },
      { new: true }
    );

    await User.findByIdAndUpdate(currentUser._id, {
      $push: { furnitureBanks: furnitureBank._id },
    });
    sendMails(newUser, furnitureBank, undefined, undefined, password);
    res
      .status(201)
      .json({ furnitureBank, info: "New Furniture Bank has been added." });
  } catch (error) {
    throw new Error(error.message);
  }
});

handler.get(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    const curPage = req.query.page || 1;
    const perPage = 10;
    const count = await User.aggregate([
      { $match: { _id: currentUser._id } },
      lookUp("furniturebanks", "furnitureBanks", "_id", "furnitureBanks"),
      windValue("$furnitureBanks"),
      { $match: { "furnitureBanks.active": true } },
    ]);
    const allFurniture = await User.aggregate([
      { $match: { _id: currentUser._id } },
      lookUp("furniturebanks", "furnitureBanks", "_id", "furnitureBanks"),
      windValue("$furnitureBanks"),
      { $match: { "furnitureBanks.active": true } },
      lookUp('users',"furnitureBanks.owner","_id","furnitureBanks.owner"),
      windValue("$furnitureBanks.owner"),
      skipValue(curPage, perPage),
      limitValue(perPage),
    ]);

    res.json({
      furnitureBanks: allFurniture,
      curPage: curPage,
      maxPage: count.length / perPage,
      perPage: perPage,
    });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
