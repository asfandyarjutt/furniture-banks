import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import mongoose from "mongoose";
import { extractUser } from "../../../lib/api-helpers";
import { lookUp, sortValue, windValue } from "../order/aggregation";

const handler = nextConnect();

handler.use(middleware);
let FurnitureBank;
try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.get(async (req, res) => {
  try {
    const currentUser = extractUser(req);
    const curPage = req.query.page || 1;
    const perPage = 10;
    const userId = currentUser?._id;
    let maxPage;
    let regex = new RegExp(req.query.keyword, "i");
    if (!!userId) {
      if (currentUser.furnitureBank != null) {
        const count = await FurnitureBank.findById(
          currentUser?.furnitureBank?._id
        ).populate({
          path: "catalogue",
        });
        maxPage = count.catalogue.length / perPage;
        if (req.query.keyword !== undefined) {
          maxPage = 1;
        }
        const furnitureBank = await FurnitureBank.findById(
          currentUser?.furnitureBank?._id
        ).populate({
          path: "catalogue",
          options: {
            limit: perPage,
            skip: (curPage - 1) * perPage,
          },
          match: {
            $or: [{ description: regex }],
          },
        });
        res.status(200).json({
          furnitureBank: furnitureBank,
          catalogue: furnitureBank.catalogue.sort(
            (a, b) => b.isActive - a.isActive
          ),
          curPage: curPage,
          maxPage: maxPage,
          perPage: perPage,
        });
      } else {
        res.status(200).json({
          furnitureBank: [],
          catalogue: [],
        });
      }
    }
  } catch (err) {
    throw err;
  }
});

export default handler;
