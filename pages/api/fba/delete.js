import nextConnect from "next-connect";
import middleware from "../../../middlewares";
import { furnitureBankSchema } from "../../../models/FurnitureBank";
import { userSchema } from "../../../models/User";
import mongoose from "mongoose";
const handler = nextConnect();

handler.use(middleware);
let FurnitureBank;
let User;
try {
  FurnitureBank = mongoose.model("FurnitureBank");
  User = mongoose.model("User");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  User = mongoose.model("User", userSchema);
}
handler.delete(async (req, res) => {
  const [deletefbs] = req.body;
  deletefbs.forEach(async (fbid) => {
    await FurnitureBank.findByIdAndRemove(fbid.furniture_id);
    await User.findByIdAndRemove(fbid.user_id);
  });
  res.json({ message: "Furniture Removed Permanently" });
});

export default handler;
