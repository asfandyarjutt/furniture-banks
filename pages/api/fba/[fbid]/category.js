import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import mongoose from "mongoose";
import { furnitureBankSchema } from "../../../../models/FurnitureBank";
import { furnitureItemSchema } from "../../../../models/FurnitureItem";

const handler = nextConnect();
handler.use(middleware);
let FurnitureBank, FurnitureItem;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
  FurnitureItem = mongoose.model("FurnitureItem");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  FurnitureItem = mongoose.model("FurnitureItem", furnitureItemSchema);
}
handler.put(async (req, res) => {
  let { newCategories, previousCat, deletedCat } = JSON.parse(req.body);
  let fb = await FurnitureBank.findOne({
    _id: req.query.fbid,
  })
    .populate("catalogue")
    .lean();

  fb.categories = newCategories;

  let purchaselimits;
  let catalogue = [];
  if (previousCat?.length > 0) {
    previousCat?.forEach((cat) => {
      purchaselimits = fb?.purchaselimits?.map((limit) => {
        let limitKeys = Object.keys(limit.bycategory);
        let key = limitKeys?.find((key) => key === cat.previous);
        if (Object.keys(limit.bycategory).length > 0 && key === cat.previous) {
          limit.bycategory[cat.new] = limit.bycategory[cat.previous];
          delete limit.bycategory[cat.previous];
          return limit;
        } else {
          return limit;
        }
      });
    });
    fb.purchaselimits = purchaselimits;

    previousCat?.forEach((cat) => {
      fb?.catalogue?.map((item) => {
        if (item.category === cat.previous) {
          catalogue.push({ ...item, category: cat.new });
        }
      });
    });
    catalogue?.map(async function (item) {
      await FurnitureItem.findByIdAndUpdate({ _id: item._id }, item);
    });
  }
  if (deletedCat?.length > 0) {
    deletedCat?.forEach((cat) => {
      purchaselimits = fb?.purchaselimits?.map((limit) => {
        let limitKeys = Object.keys(limit.bycategory);
        let key = limitKeys?.find((key) => key === cat);
        if (Object.keys(limit.bycategory).length > 0 && key === cat) {
          delete limit.bycategory[`${cat}`];
          return limit;
        } else {
          return limit;
        }
      });
    });
    fb.purchaselimits = purchaselimits;

    deletedCat?.forEach((cat) => {
      fb?.catalogue?.map((item) => {
        if (item.category === cat) {
          catalogue.push({ ...item, category: "All" });
        }
      });
    });
    catalogue?.map(async function (item) {
      await FurnitureItem.findByIdAndUpdate({ _id: item._id }, item);
    });
  }

  await FurnitureBank.findByIdAndUpdate({ _id: fb._id }, fb);
  res.json({ message: "done" });
});

export default handler;
