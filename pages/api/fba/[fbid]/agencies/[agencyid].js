import nextConnect from "next-connect";
import middleware from "../../../../../middlewares";
import { furnitureBankSchema } from "../../../../../models/FurnitureBank";
import mongoose from "mongoose";
import { partnerAgencySchema } from "../../../../../models/PartnerAgency";
const handler = nextConnect();

handler.use(middleware);
let FurnitureBank;
let PartnerAgency;
try {
  FurnitureBank = mongoose.model("FurnitureBank");
  PartnerAgency = mongoose.model("PartnerAgency")
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  PartnerAgency = mongoose.model(
    "PartnerAgency",
    partnerAgencySchema,
    "partneragency"
  )
}

handler.delete(async (req, res) => {
  const {
    query: { fbid, agencyid },
  } = req;
  try {
    await PartnerAgency.findByIdAndUpdate(agencyid, { $unset: { furnitureBank: 1 } }, { new: true });
    const furniturebank = await FurnitureBank.findOne({ _id: fbid });
    const withDelete = furniturebank.partnerAgencies.filter(
      (agency) => agency != agencyid
    );
    furniturebank.partnerAgencies = withDelete;
    await furniturebank.save();
    res.status(200).json({ info: "Partner Agency Removed" })
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
