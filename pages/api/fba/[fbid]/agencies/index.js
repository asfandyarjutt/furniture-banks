import nextConnect from "next-connect";
import middleware from "../../../../../middlewares";
import { furnitureBankSchema } from "../../../../../models/FurnitureBank";
import { userSchema } from "../../../../../models/User";
import { partnerAgencySchema } from "../../../../../models/PartnerAgency";
import mongoose from "mongoose";

const handler = nextConnect();
handler.use(middleware);
let FurnitureBank, User, PartnerAgency;

try {
  FurnitureBank = mongoose.model("FurnitureBank");
  User = mongoose.model("User");
  PartnerAgency = mongoose.model("PartnerAgency");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
  PartnerAgency = mongoose.model("PartnerAgency", partnerAgencySchema);
  User = mongoose.model("User", userSchema);
}

handler.post(async (req, res) => {
  try {
    const {
      body: { agencies },
      query: { fbid },
    } = req;
    const myAgencies = await FurnitureBank.findById(fbid)
    agencies.map(async (agency) => {
      if (myAgencies.partnerAgencies.includes(agency.id) === true) {
        return res.status(400)
      }
      await FurnitureBank.findByIdAndUpdate(fbid, {
        $push: { partnerAgencies: agency.id },
      });

      const ag = await PartnerAgency.findByIdAndUpdate(agency.id, { furnitureBank: fbid }, { new: true });

      await User.findByIdAndUpdate(ag.liaison, { furnitureBank: fbid });
      ag.caseManagers.forEach(async (manager) => {
        await User.findByIdAndUpdate(manager, { furnitureBank: fbid });
      });
    });
    res.status(200).json({ message: "Partner Agency Added" });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
