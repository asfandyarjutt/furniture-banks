import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import { furnitureBankSchema } from "../../../../models/FurnitureBank";
import { userSchema } from "../../../../models/User";
import mongoose from "mongoose";
import createUser from "../../../../lib/createUser";
import { extractUser } from "../../../../lib/api-helpers";
import sendMails from "../../../../lib/sendMails";
const handler = nextConnect();

handler.use(middleware);
let User;
let FurnitureBank;
try {
  User = mongoose.model("User");
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  User = mongoose.model("User", userSchema);
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.post(async (req, res) => {
  const currentUser = extractUser(req);
  const { fbid } = req.query;
  const { firstName, lastName, email, phone, partnerAgency, fba } = req.body;
  const user = {
    firstName,
    lastName,
    email,
    phone,
    partnerAgency,
    fba,
  };

  if (currentUser?.role !== "ADMIN" && currentUser?.role !== "FBA") {
    return res.json({ message: `Sorry you can't perform that action` });
  }

  if (!user.firstName || !user.lastName) {
    return res.status(400).send("Missing field(s) for a user");
  }

  if (await User.findOne({ email: user.email })) {
    return res.status(403).send("Case manager email is already in use.");
  }
  const { newUser, password } = await createUser(
    user,
    undefined,
    undefined,
    currentUser?.role
  );

  try {
    const fb = await FurnitureBank.findByIdAndUpdate(
      fbid,
      { $set: { owner: newUser?._id } },
      { new: true }
    );
    const setUser = await User.findByIdAndUpdate(
      newUser._id,
      { $set: { furnitureBank: fbid } },
      { new: true }
    );
    sendMails(newUser, fb, undefined, undefined, password);
    res
      .status(201)
      .json({ message: "New Furniture Admin Added", user: newUser });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
