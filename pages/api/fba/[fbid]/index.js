import nextConnect from "next-connect";
import middleware from "../../../../middlewares";
import { furnitureBankSchema } from "../../../../models/FurnitureBank";
import mongoose from "mongoose";
const handler = nextConnect();

handler.use(middleware);
let FurnitureBank;
try {
  FurnitureBank = mongoose.model("FurnitureBank");
} catch (error) {
  FurnitureBank = mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks"
  );
}

handler.get(async (req, res) => {
  const { fbid } = req.query;
  try {
    const furnitureBank = await FurnitureBank.findById(fbid)
      .populate("catalogue")
      .populate("partnerAgencies")
      .populate("owner");

    res.json({ furnitureBank });
  } catch (error) {
    throw new Error(error);
  }
});

handler.patch(async (req, res) => {
  const {
    query: { fbid },
    body: { name, address, phone, email },
  } = req;
  try {
    await FurnitureBank.findByIdAndUpdate(fbid, {
      name,
      address,
      phone,
      email,
    });
  } catch (error) {
    throw new Error(error);
  }
  res
    .status(200)
    .json({ message: "Furniture Bank information has been updated." });
});

handler.put((req, res) => res.send("ok"));

handler.delete(async (req, res) => {
  const { fbid } = req.query;
  try {
    await FurnitureBank.findByIdAndUpdate(fbid, { $set: { active: false } });
    res.status(200).json({
      message:
        "Furniture Bank has been deleted. You can retrieve this information by going to the Deleted Furniture Banks section.",
    });
  } catch (error) {
    throw new Error(error);
  }
});

export default handler;
