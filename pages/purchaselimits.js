import React, { useEffect, useState } from "react";
import {
  Card,
  Col,
  Row,
  Button,
  Collapse,
  Typography,
  Popconfirm,
  Alert,
  Form,
  Radio,
  message as ms,
  notification,
  Divider,
} from "antd";
import { useCurrentUser } from "../lib/hooks";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faMinus,
  faCheck,
  faEdit,
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";

import { useRouter } from "next/router";

const { Text } = Typography;

function purchaselimits() {
  const router = useRouter();
  const [form] = Form.useForm();
  async function handleDelete(i) {
    await fetch(`/api/limits/${i}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });
    ms.warning("Limit Deleted");
    setTimeout(() => router.reload(), 200);
  }
  const [currentUser] = useCurrentUser();
  const [limitper, setLimitPer] = useState("");
  const [loading, setLoading] = useState(true);
  const [active, setactive] = useState(false);
  const [message, setMessage] = useState(undefined);
  const { Panel } = Collapse;

  useEffect(() => {
    if (currentUser) {
      setLimitPer(currentUser?.furnitureBank?.limitper);
      setLoading(false);
    }
  }, [currentUser]);
  useEffect(() => {
    if (router.query.info) {
      setMessage(router.query.info);
      setTimeout(() => {
        router.replace(router.pathname);
        setMessage(undefined);
      }, 3500);
    }
  }, [router.query.info]);

  async function finish(values) {
    await fetch("/api/updatelimit", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ ...values }),
    });
    ms.success("Change Confirmed");
  }
  const openNotification = (type) => {
    notification[type]({
      message: "Confirm Change",
      description:
        "All purchase limits must be made using the same method. Using both methods will cause problems at time of client purchase. Click save to confirm.",
      duration: 0,
    });
  };
  const openNotificationSave = (type) => {
    notification[type]({
      message: "Change Confirmed",
      description: `Purchase limits has changed`,
    });
  };
  return (
    <>
      <Card>
        <Row>
          <Col span={18} style={{ marginBottom: "2em" }}>
            <Text>
              Choose a method by which to limit client purchases. Press save
              only after saving method.
            </Text>
          </Col>
        </Row>
        <Row>
          {limitper && (
            <Row>
              <Form name="setLimit" form={form} onFinish={finish}>
                <Col span={24}>
                  <Form.Item name="limitper">
                    <Radio.Group
                      defaultValue={limitper}
                      value={limitper}
                      onChange={() => setactive(true)}
                    >
                      <Radio
                        value="category"
                        onClick={() => openNotification("warning")}
                      >
                        Limit Per Category
                      </Radio>
                      <Radio
                        value="total"
                        onClick={() => openNotification("warning")}
                      >
                        Limit Per Total Items
                      </Radio>
                    </Radio.Group>
                  </Form.Item>
                </Col>
                <Row>
                  <Col span={24}>
                    <Form.Item>
                      <Button
                        disabled={!active}
                        htmlType="submit"
                        size="large"
                        block
                        style={{
                          backgroundColor: "#1890ff",
                          color: "white",
                          border: "1px solid #1890ff",
                          borderRadius: "3px",
                          borderwidth: "1px",
                          display: "inline-block",
                          textAlign: "center",
                          paddingLeft: "20px",
                          paddingRight: "20px",
                        }}
                      >
                        <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Row>
          )}
        </Row>
        <Divider />
        <br />
        <Alert
          style={{
            marginBottom: "2em",
            width: "100%",
            display: message === undefined ? "none" : "flex",
          }}
          message={message}
          type="success"
          showIcon
          closable
          onClose={() => setMessage(undefined)}
        />
        <Row style={{ marginBottom: "3em" }}>
          <Col span={24}>
            <Link href="/purchaselimits/add">
              <a>
                <Button
                  size="large"
                  style={{
                    backgroundColor: "#52c41a",
                    color: "white",
                    border: "1px solid #52c41a",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                  block
                >
                  <FontAwesomeIcon icon={faPlus} />
                  &nbsp; Add Purchase Limit
                </Button>
              </a>
            </Link>
          </Col>
        </Row>
        <Row></Row>
        {!currentUser?.furnitureBank?.purchaselimits ? (
          <p>There are no purchase limits.</p>
        ) : (
          <Collapse>
            {!loading &&
              currentUser.furnitureBank.purchaselimits?.map((limit, i) => (
                <Panel
                  header={
                    (limit?.from == 1 && limit?.to == 1) || !limit?.to
                      ? `${limit?.from} person`
                      : `${limit?.from} - ${limit?.to} people`
                  }
                  key={i}
                >
                  <p>
                    <b>Total:</b>{" "}
                    {limit?.bycategory
                      ? ` ${
                          Object.entries(limit?.bycategory).length
                        } Categories `
                      : `${limit.total} Items`}
                  </p>
                  {limit?.bycategory &&
                    Object.entries(limit?.bycategory).map((category) => (
                      <p key={category[0]}>
                        <b>{category[0]}:</b>&nbsp;
                        {category[1]}
                      </p>
                    ))}
                  <Link
                    href={{
                      pathname: "/purchaselimits/edit",
                      query: {
                        data: JSON.stringify({ limit, id: i }),
                      },
                    }}
                  >
                    <a>
                      <Button
                        style={{
                          backgroundColor: "white",
                          height: "40px",
                          color: "#5FB2FF",
                          border: "1px solid #5FB2FF",
                          borderRadius: "3px",
                          borderwidth: "1px",
                          display: "inline-block",
                          textAlign: "center",
                          paddingLeft: "20px",
                          paddingRight: "20px",
                          marginRight: 10,
                        }}
                      >
                        <FontAwesomeIcon icon={faEdit} /> &nbsp; Edit
                      </Button>
                    </a>
                  </Link>
                  <Popconfirm
                    title="Are you sure you want to delete？"
                    okText="Delete"
                    cancelText="Cancel"
                    onConfirm={() => handleDelete(i)}
                  >
                    <Button
                      style={{
                        backgroundColor: "white",
                        height: "40px",
                        color: "#f5222d",
                        border: "1px solid #f5222d",
                        borderRadius: "3px",
                        borderwidth: "1px",
                        display: "inline-block",
                        textAlign: "center",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                      }}
                    >
                      <FontAwesomeIcon icon={faMinus} /> &nbsp; Remove
                    </Button>
                  </Popconfirm>
                </Panel>
              ))}
          </Collapse>
        )}
        <br />
      </Card>
    </>
  );
}

export default purchaselimits;
