import mongoose from 'mongoose'
const {Schema}=mongoose
mongoose.Promise = global.Promise;

 const userSchema = new Schema(
  {
    email: {
      required: true,
      type: String,
    },
    active: {
      type: Boolean,
      default: false,
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: String,
    phone: String,
    role: {
      type: String,
      default: "PACM",
      enum: ["ADMIN", "FBA", "PAL", "PACM"],
    },
    belongTo: String,

    furnitureBanks: [
      {
        type: Schema.Types.ObjectId,
        ref: "FurnitureBank",
      },
    ],
    partnerAgency: {
      type: Schema.Types.ObjectId,
      ref: "PartnerAgency",
    },
    HelpDeskTickets: [
      {
        type: Schema.Types.ObjectId,
        ref: "Ticket",
      },
    ],
    furnitureBank: {
      type: Schema.Types.ObjectId,
      ref: "FurnitureBank",
    },
    password: {
      required: true,
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

module.exports= mongoose.models.User || mongoose.model("User", userSchema);
