import mongoose from 'mongoose'
const {Schema}=mongoose
mongoose.Promise = global.Promise;


const formsSchema = new Schema(
  {
    background: [
      {
        question: String,
        answers: [String],
        type: Number,
      },
    ],
    payment: {
      selfHaul: Number,
      curbsideDelivery: Number,
      installationDelivery: Number,
    },
  },
  {
    timestamps: true,
  }
);

module.exports= mongoose.models.Forms || mongoose.model('Forms',formsSchema)
