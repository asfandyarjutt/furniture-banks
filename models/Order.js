import mongoose from "mongoose";
const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const orderSchema = new Schema(
  {
    date: Date,
    clientName: String,
    clientAddress: String,
    clientAppartment: String,
    clientZip: String,
    clientCity: String,
    clientState: String,
    clientPhone: String,
    clientEmail: String,
    number: String,
    agency: {
      type: Schema.Types.ObjectId,
      ref: "PartnerAgency",
    },
    cmName: String,
    cmEmail: String,
    cmPhone: String,
    receiverName: String,
    receiverPhone: String,
    items: [
      {
        item: [
          {
            type: Schema.Types.ObjectId,
            ref: "FurnitureItem",
          },
        ],
        quantity: Number,
        sku: String,
      },
    ],
    type: String,
    total: Number,
    clientAmount: Number,
    deliveryStatus: false,
    agencyAmount: Number,
    agencyPaymentType: String,
    clientPaymentType: String,

    heartLandInfo: {},
    houseHold: {},
    bg: {},
    role: String,
    deliverDate: Date,
    deliverType: String,
    furnitureBank: {
      type: Schema.Types.ObjectId,
      ref: "FurnitureBank",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.models.Order || mongoose.model("Order", orderSchema);
