import mongoose from 'mongoose'
const {Schema}=mongoose
mongoose.Promise = global.Promise;

const ticketSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    },
    agency: {
      type: String,
    },
    belongTo: {
      type: String,
    },
    readed: false,
    replied: false,
    read: {
      type: Boolean,
      default: true,
    },
    read1: {
      type: Boolean,
      default: true,
    },
    read2: {
      type: Boolean,
      default: true,
    },
    from: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    to: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    // to: [{
    //   type: Schema.Types.ObjectId,
    //   ref: "User",
    // },],
    postId: [
      {
        type: Schema.Types.ObjectId,
        ref: "Ticket",
      },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.models.Ticket || mongoose.model("Ticket", ticketSchema);
