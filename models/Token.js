import mongoose from 'mongoose'
const {Schema}=mongoose
mongoose.Promise = global.Promise;

const tokenSchema = new Schema(
  {
    token: String,
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    type: String,
    expireAt: Date,
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.models.Token || mongoose.model("Token", tokenSchema);
