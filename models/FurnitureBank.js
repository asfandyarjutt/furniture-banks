import mongoose from "mongoose";
const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const furnitureBankSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    catalogue: [
      {
        type: Schema.Types.ObjectId,
        ref: "FurnitureItem",
      },
    ],
    bg: [],
    purchaselimits: [],
    prices: {},
    address: String,
    phone: String,
    categories: [String],
    email: String,
    city: String,
    state: String,
    zip: Number,
    active: {
      type: Boolean,
      default: true,
    },
    partnerAgencies: [
      {
        type: Schema.Types.ObjectId,
        ref: "PartnerAgency",
      },
    ],
    owner: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    times: {
      type: Schema.Types.ObjectId,
      ref: "Times",
    },
    emailTexts: {
      client: String,
      caseManager: String,
      partnerAgency: String,
    },
    emailSubject: {
      paSubject: String,
      caseSubject: String,
      clientSubject: String,
    },
    limitper: {
      type: String,
      default: "total",
    },
    paypalId: String,
  },
  {
    timestamps: true,
  }
);
module.exports =
  mongoose.models.FurnitureBank ||
  mongoose.model(
    "FurnitureBank",
    furnitureBankSchema,
    "furniturebanks",
    "furniturebankss"
  );
