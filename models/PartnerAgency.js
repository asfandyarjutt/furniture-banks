import mongoose from 'mongoose'
const {Schema}=mongoose
mongoose.Promise = global.Promise;

 const partnerAgencySchema = new Schema(
  {
    name: String,
    address: String,
    phone: String,
    email: String,
    city: String,
    state: String,
    zip: Number,
    credit: { type: Number, default: 0 },
    active: {
      type: Boolean,
      default: true,
    },
    services: [String],
    liaison: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    caseManagers: [
      {
        type: Schema.Types.ObjectId,
        ref: "User",
      },
    ],
    furnitureBank: {
      type: Schema.Types.ObjectId,
      ref: "FurnitureBank",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.models.PartnerAgency || mongoose.model("PartnerAgency", partnerAgencySchema);
