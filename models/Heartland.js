import mongoose from "mongoose";
const { Schema } = mongoose;
mongoose.Promise = global.Promise;


const heartlandSchema = new Schema(
    {

        heartLandInfo: {},

    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.models.Heartland || mongoose.model("Heartland", heartlandSchema);