import mongoose from "mongoose";
const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const timesSchema = new Schema(
  {
    selfHaul: [],
    delivery: [],
    selfHaulDays: { type: String },
    deliveryDays: { type: String },
  },
  {
    timestamps: true,
    minimize: false,
  }
);

module.exports = mongoose.models.Times || mongoose.model("Times", timesSchema);
