import mongoose from "mongoose";
const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const furnitureItemSchema = new Schema(
  {
    description: {
      type: String,
      required: true,
    },
    images: [String],
    quantity: {
      type: Number,
      min: 0,
      default: 1,
    },
    sku: {
      type: String,
      required: true,
    },
    category: {
      type: String,
      enum: [],
    },
    isActive: Boolean,
    color: String,
    material: String,
    dimensions: {
      high: Number,
      wide: Number,
      deep: Number,
    },
  },
  {
    timestamps: true,
  }
);

module.exports =
  mongoose.models.FurnitureItem ||
  mongoose.model("FurnitureItem", furnitureItemSchema);
