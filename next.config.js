require("dotenv").config();

const compose = (plugins) => ({
  webpack(config, options) {
    return plugins.reduce((config, plugin) => {
      if (plugin instanceof Array) {
        const [_plugin, ...args] = plugin;
        plugin = _plugin(...args);
      }
      if (plugin instanceof Function) {
        plugin = plugin();
      }
      if (plugin && plugin.webpack instanceof Function) {
        return plugin.webpack(config, options);
      }
      return config;
    }, config);
  },

  webpackDevMiddleware(config) {
    return plugins.reduce((config, plugin) => {
      if (plugin instanceof Array) {
        const [_plugin, ...args] = plugin;
        plugin = _plugin(...args);
      }
      if (plugin instanceof Function) {
        plugin = plugin();
      }
      if (plugin && plugin.webpackDevMiddleware instanceof Function) {
        return plugin.webpackDevMiddleware(config);
      }
      return config;
    }, config);
  },
});

const withBundleAnalyzer = require("@next/bundle-analyzer");

module.exports = compose([
  [
    withBundleAnalyzer,
    {
      enabled: process.env.ANALYZE === "true",
    },
    {
      env: {
        EMAIL: process.env.EMAIL,
        X: process.env.X,
        WEB_URI: process.env.WEB_URI,
        EMAIL_PWD: process.env.EMAIL_PWD,
        DB: process.env.DB,
        HEARTLAND_PUBLIC_KEY: process.env.HEARTLAND_PUBLIC_KEY,
        HEARTLAND_SECRET_KEY: process.env.HEARTLAND_SECRET_KEY,
      },
    },
  ],
]);
