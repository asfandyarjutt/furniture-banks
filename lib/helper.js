import Dinero from "dinero.js";

export const formatPayment = (payment) => {
  return Dinero({ amount: Number(payment + "00") }).toFormat("0,0");
};
