import { useContext, createContext } from "react";
import { getLocal, setLocal } from "../components/Forms/functions";

export const CartContext = createContext();
const initial = getLocal("State");
let initials;
if (initial) {
  initials = initial;
} else {
  initials = {
    items: [],
    purchase: false,
    contact: {},
    household: {},
    bg: {},
    payment: {},
    final: {},
    authorizedPayment: {},
    limits: {},
    limit: {},
    limitReach: false,
    deliver: true,
    limitTotalC: 0,
    active: true,
    totalcart: 0,
  };
}
export const initialState = initials;

export const reducer = (state, action) => {
  switch (action.type) {
    case "ADD":
      const localState = getLocal("State");
      const totalI = localState.items.reduce(
        (prev, current) => prev + current.quantity,
        0
      );
      if (
        totalI > localState.limit.total ||
        action.payload.quantity > localState.limit.total
      ) {
      }
      const itemState = {
        ...localState,
        items: [...localState.items, action.payload],
      };
      setLocal("State", itemState);
      return itemState;
    case "SET_LIMITS":
      return {
        ...state,
        limits: action.payload,
      };
    case "REMOVE":
      const removeState = getLocal("State");
      const itemtoRemove = {
        ...removeState,
        items: removeState.items.filter(({ item }) => item !== action.payload),
      };
      setLocal("State", itemtoRemove);
      return {
        ...removeState,
        items: removeState.items.filter(({ item }) => item !== action.payload),
      };
    case "CANCEL_DELIVER":
      return {
        ...state,
        deliver: false,
      };
    case "SET_DELIVER":
      return {
        ...state,
        deliver: true,
      };
    case "REP_CONTACT":
      const Contacttoget = getLocal("State");
      const stateContactset = {
        ...Contacttoget,
        contact: action.payload,
      };
      const returnState = {
        ...state,
        contact: action.payload,
      };
      if (Contacttoget) {
        setLocal("State", stateContactset);
        return Contacttoget;
      } else {
        setLocal("State", returnState);
        return returnState;
      }

    case "BG":
      const bgState = getLocal("State");
      const bgStatetoset = {
        ...bgState,
        bg: action.payload,
      };
      setLocal("State", bgStatetoset);
      return bgStatetoset;
    case "PAYMENT":
      const localPayment = getLocal("State");
      const localPaymenttoset = {
        ...localPayment,
        payment: action.payload,
        disableRadio: action.disableRadio,
      };
      setLocal("State", localPaymenttoset);
      return localPaymenttoset;
    case "FINISH":
      const newState = { ...state, purchase: true };
      setLocal("State", newState);
      return { ...state, purchase: true };
    case "HOUSEHOLD":
      const localhousehold = getLocal("State");
      const total =
        Number(action.payload.adultMales) +
        Number(action.payload.adultFemales) +
        Number(action.payload.minors);
      const sethousehold = {
        ...localhousehold,
        household: action.payload,
        limit: state?.limits?.filter(
          (limit) => total >= Number(limit.from) && total <= Number(limit.to)
        ),
      };
      setLocal("State", sethousehold);
      return {
        ...localhousehold,
        household: action.payload,
        limit: state?.limits?.filter(
          (limit) => total >= Number(limit.from) && total <= Number(limit.to)
        ),
      };
    case "FINAL":
      const finalState = getLocal("State");
      const finaltoSet = {
        ...finalState,
        final: action.payload,
      };
      setLocal("State", finaltoSet);
      return finaltoSet;
    case "AUTHORIZED":
      const values = getLocal("State");
      const newValues = {
        ...values,
        authorizedPayment: action.payload,
      };
      setLocal("State", newValues);
      return {
        ...values,
        authorizedPayment: action.payload,
      };
    case "REACH":
      return {
        ...state,
        limitReach: true,
      };
    case "TOTALCART":
      return {
        ...state,
        totalcart: action.payload,
      };
    case "RESET_CART": {
      return {
        items: [],
        purchase: false,
        contact: {},
        household: {},
        bg: {},
        payment: {},
        final: {},
        authorizedPayment: {},
        limits: {},
        limit: {},
        limitReach: false,
        deliver: true,
        limitTotalC: 0,
        active: true,
        totalcart: 0,
      };
    }
    case "RESET":
      return initialState;

    default:
      return state;
  }
};

export const useCartValue = () => useContext(CartContext);
