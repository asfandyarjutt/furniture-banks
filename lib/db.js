import { userSchema } from "../models/User";
import mongoose from "mongoose";

export async function getUser(req, userId) {
  let User;
  try {
    User = mongoose.model("User");
  } catch (error) {
    User = mongoose.model("User", userSchema);
  }
  const user = await User.findById(userId)
    .populate({
      path: "furnitureBank",
      populate: {
        path: "owner",
        model: "User",
      },
    });
  if (!user) return null;
  const { _id, name, email, furnitureBank } = user;
  const isAuth = _id === req.user?._id;

  return {
    _id,
    name,
    furnitureBank,
    email: isAuth ? email : null,
  };
}
