import useSWR, { trigger } from "swr";

export const fetcher = (url) => fetch(url).then((r) => r.json());

export function useCurrentUser() {
  const { data, mutate } = useSWR("/api/user", fetcher, {
    onErrorRetry: (error, key, config, revalidate, { retryCount }) => {
      setTimeout(() => revalidate({ retryCount }), 5000);
    },
  });

  const user = data?.user;
  return [user, { mutate }, trigger];
}

export function useHelpDeskTickets() {
  const [user] = useCurrentUser();
  let userId;
  user == undefined ? (userId = "") : (userId = user._id);
  const { data, mutate } = useSWR(`/api/hdtickets?userId=${userId}`, fetcher, {
    onErrorRetry: (error, key, config, revalidate, { retryCount }) => {
      setTimeout(() => revalidate({ retryCount }), 5000);
    },
  });
  const tickets = data?.tickets;
  return [tickets, mutate, trigger];
}

export function useFurnitureBanks(page) {
  const { data, mutate } = useSWR(`/api/fba?page=${page}`, fetcher);
  const furnitureBanks = data?.furnitureBanks.map((furniture) => {
    return furniture.furnitureBanks;
  });

  const curPage = data?.curPage;
  const maxPage = data?.maxPage;
  const perPage = data?.perPage;
  return [furnitureBanks, curPage, maxPage, perPage, mutate, trigger];
}
export function useTimes() {
  const { data, mutate } = useSWR("/api/times", fetcher, {});
  const times = data;
  return [times, mutate, trigger];
}
export function useEmailTexts() {
  const { data, mutate } = useSWR("/api/emails", fetcher);
  const emailTexts = data?.emailTexts;
  const emailSubject = data?.emailSubject;
  return [emailTexts, emailSubject, mutate];
}

export function useInactiveFurnitureBanks() {
  const { data, mutate } = useSWR("/api/fba/inactive", fetcher, {});
  const furnitureBanks = data?.furnitureBanks;
  return [furnitureBanks, mutate];
}

export function usePartnerAgencies(page, keyword, display) {
  const [user] = useCurrentUser();
  const { data, mutate } = useSWR(
    `/api/pa?page=${page}&userid=${user?.furnitureBank?._id}&userrole=${user?.role}&keyword=${keyword}&display=${display}`,
    fetcher,
    {}
  );
  let partnerAgencies;
  partnerAgencies = data?.partnerAgencies;
  const curPage = data?.curPage;
  const maxPage = data?.maxPage;
  const perPage = data?.perPage;
  const returnData = [
    partnerAgencies,
    curPage,
    maxPage,
    perPage,
    mutate,
    trigger,
  ];
  return returnData;
}

export function useOneFB(fbid) {
  const { data, mutate } = useSWR(`/api/fba/${fbid}`, fetcher, {});
  const furnitureBank = data?.furnitureBank;
  return [furnitureBank, mutate, trigger];
}
export function useOneItem(itemId) {
  const { data, mutate } = useSWR(`/api/furniture/${itemId}`, fetcher);
  const item = data?.item;
  return [item, mutate];
}

export function useOneTicket(ticketId) {
  const { data, mutate } = useSWR(`/api/hdtickets/${ticketId}`, fetcher, {});
  const ticketInfo = data?.oneTicket;
  return [ticketInfo, mutate];
}
export function useOneOrder(orderId) {
  const { data, mutate } = useSWR(`/api/order/${orderId}`, fetcher);
  const order = data?.oneOrder;
  return [order, mutate];
}
export function useOneInvoice(invoiceId) {
  const { data, mutate } = useSWR(`/api/invoice/${invoiceId}`, fetcher);
  const invoice = data?.oneInvoice;
  return [invoice, mutate];
}
export function useOneAgency(agencyId) {
  const { data, mutate } = useSWR(`/api/pa/${agencyId}`, fetcher, {});
  const partnerAgency = data?.partnerAgency;
  return [partnerAgency, mutate, trigger];
}

export function useInactiveAgencies() {
  const { data, mutate } = useSWR("/api/pa/inactive", fetcher, {});
  const partnerAgencies = data?.partnerAgencies;
  return [partnerAgencies, mutate];
}
