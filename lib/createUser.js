import bcrypt from "bcrypt";
import mongoose from "mongoose";
import { userSchema } from "../models/User";
import PartnerAgency from "../models/PartnerAgency"
import { furnitureBankSchema } from "../models/FurnitureBank"
import generator from "generate-password";
import sendMails from './sendMails';
export default async function createUser(user, furnitureBank, partnerAgency, admin) {
  let User;
  let FurnitureBank;
  try {
    User = mongoose.model("User");
    FurnitureBank = mongoose.model("FurnitureBank");
  } catch (error) {
    User = mongoose.model("User", userSchema);
    FurnitureBank = mongoose.model(
      "FurnitureBank",
      furnitureBankSchema,
      "furniturebanks"
    );
  }

  const password = generator.generate({ length: 10, numbers: true });
  const hashedPassword = bcrypt.hashSync(password, 10);
  const newUser = new User({
    ...user,
    furnitureBank: furnitureBank == undefined || null ? null : furnitureBank,
    password: hashedPassword,
    partnerAgency: partnerAgency == undefined || null ? null : partnerAgency,
    originalPassword: password,
    role: user.liaison ? "PAL" : user.fba ? "FBA" : "PACM",
  })

  if (admin === "ADMIN" || admin === "FBA") {
    newUser.save()
    return { newUser, password }
  }
  newUser.save()
  return {newUser,password}
}

