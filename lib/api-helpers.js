export function extractUser(req) {
  if (!req.user) return null;
  const {
    _id,
    email,
    role,
    furnitureBank,
    firstName,
    lastName,
    phone,
    HelpDeskTickets,
    partnerAgency,
  } = req.user;
  return {
    _id,
    firstName,
    lastName,
    email,
    role,
    phone,
    furnitureBank,
    HelpDeskTickets,
    partnerAgency,
  };
}

export function extract_user(user) {
  if (!user) return null;
  const {
    _id,
    email,
    role,
    furnitureBank,
    firstName,
    lastName,
    phone,
    HelpDeskTickets,
    partnerAgency,
  } = user;
  return {
    _id,
    firstName,
    lastName,
    email,
    role,
    phone,
    furnitureBank,
    HelpDeskTickets,
    partnerAgency,
  };
}
