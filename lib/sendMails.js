import { send_mail } from './mailer'


export default async function sendMails(newUser, furnitureBank, agency, miPal, password) {
  switch (newUser.role) {
    case "FBA":
      send_mail(
        "Welcome to your furniture bank account",
        newUser.email,
        "Furniture Bank Catalog Account Confirmation.",
        `
          <div>
            <p>Welcome ${furnitureBank.name} to the Furniture Bank Catalog.
            </p>
            <p>Here you have 2 steps to start using your account</p>
            <ul>
              <li>
              Activate your account by visiting <a href="${process.env.WEB_URI}/activate/${newUser._id}">this link</a>.
              </li>
              <li>
              Sign in with your email and your temporary password: <b>${password}</b>. Change it as soon as you can.
              </li>
            </ul>
            <p>Please email furniturebankcatalog@gmail.com with any questions about the platform.</p>
          </div>
          `
      )
      break;
    case "PAL":
      send_mail(
        "Welcome to your furniture bank account",
        newUser.email,
        "Furniture Bank Catalog Account Confirmation.",
        `
       <div>
         <p>Welcome ${agency.name} online catalog</p>
         <p>Here you have 2 steps to start using your account</p>
         <ul>
           <li>
           Activate your account by visiting <a href="${process.env.WEB_URI}/activate/${newUser._id}">this link</a>.
           </li>
           <li>
             Sign in with your email and your temporary password: <b>${password}</b>. Change it as soon as you can.
           </li>
         </ul>
         <p>Please email to ${agency.email} with any questions about the platform.
         </p>
       </div>
       `)
      break;
    case "PACM":
      send_mail(
        "Welcome to your furniture bank account",
        newUser.email,
        "Furniture Bank Catalog Account Confirmation.",
        `
      <div>
        <p>Welcome ${furnitureBank === undefined || null ? agency.name : furnitureBank.name} online platform. The main liaison for ${agency.name} is ${miPal.firstName} ${miPal.lastName}. Please contact this person at ${miPal.email} with any questions.
        </p>
        <p>Here you have 2 steps to start using your account</p>
        <ul>
          <li>
          Activate your account by visiting <a href="${process.env.WEB_URI}/activate/${newUser._id}">this link</a>.
          </li>
          <li>
          Sign in with your email and your temporary password: <b>${password}</b>. Change it as soon as you can.
          </li>
        </ul>
      </div>
      `
      );
      break;
  }
}
