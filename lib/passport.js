import passport from "passport";
import { compareSync } from "bcrypt";
import { Strategy as LocalStrategy } from "passport-local";
import mongoose from "mongoose";
import { userSchema } from "../models/User";

let User;
try {
  User = mongoose.model("User");
} catch (error) {
  User = mongoose.model("User", userSchema);
}

passport.serializeUser((user, done) => done(null, user._id));
passport.deserializeUser(async (id, done) =>
  done(null, await User.findById(id).populate("furnitureBank"))
);

passport.use(
  new LocalStrategy(
    { usernameField: "email" },
    async (email, password, done) => {
      try {
        const user = await User.findOne({ email })
          .populate("furnitureBank")
          .populate("partnerAgency");
        if (user && compareSync(password, user.password)) done(null, user);
        else done(null, false, { message: "Email or passsword incorrect" });
      } catch (error) {
        throw new Error(error);
      }
    }
  )
);

export default passport;
