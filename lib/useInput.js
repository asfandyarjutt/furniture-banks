import { message } from "antd";
import { useState } from "react";

function useInput(defaultValue, qty) {
  let select = undefined;
  const [value, setValue] = useState(defaultValue);
  const onChange = (e) => {
    if (select) {
      if (qty < e.target.value || e.target.value < 0) {
        message.warning('Item Quantity is invalid')
      }
      else {
        setValue(e);

      }
    } else {
      if (qty < e.target.value || e.target.value < 0) {
        message.warning('Item Quantity is invalid')

      }
      else {
        setValue(e.target.value);

      }
    }
  };

  return [
    {
      value,
      onChange,
    },
    setValue,
  ];
}
export default useInput;
