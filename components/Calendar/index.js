import React, { useEffect, useState } from "react";
import Link from "next/link";
import { Card, Calendar, Badge } from "antd";
import moment from "moment";

function CalendarComp() {
  const [orders, setOrders] = useState([]);
  const today = new Date();
  const [deliveries, setDeliveries] = useState([]);
  const [currentMonth, setCurrentMonth] = useState(today.getMonth() + 1);
  const [currentYear, setCurrentYear] = useState(today.getFullYear());
  const [currentDeliveries, setCurrentDeliveries] = useState([]);
  const actualmonth = moment().month();

  useEffect(() => {
    async function getOrders() {
      let ord = await fetch(`/api/order`);
      const order = await ord.json();
      setOrders(order);
    }
    getOrders();
  }, []);

  useEffect(() => {
    setDeliveries(
      orders.orders?.map((order) => {
        let date = moment(new Date(order?.deliverDate)).format("D M Y");
        let some = {
          type: order.type,
          date: parseInt(date),
          month: Number(date.split(" ")[1]),
          year: Number(date.split(" ")[2]),
          content: order.clientName,
          id: order._id,
        };
        return some;
      })
    );
  }, [orders]);

  useEffect(() => {
    setCurrentDeliveries(
      deliveries?.filter(
        ({ month, year }) => month == currentMonth && year == currentYear
      )
    );
  }, [deliveries, currentMonth]);

  function getListData(value) {
    let listData = [];
    let events = [];
    currentDeliveries?.map((caso) => {
      if (
        value.date() === caso.date &&
        value.month() + 1 === caso.month &&
        value.year() === caso.year
      ) {
        if (!listData.hasOwnProperty(caso.date)) {
          listData[caso.date] = { eventos: [] };
        }
        listData[caso.date].eventos.push({
          type:
            caso.type === "Curbside Delivery" ||
            caso.type === "Installation delivery"
              ? "success"
              : "warning",
          content: caso.content,
          id: caso.id,
        });

        switch (value.date()) {
          case caso.date:
            events = listData[String(caso.date)].eventos.map((event) => event);
            break;
          default:
        }
      }
    });
    return events || [];
  }

  function dateCellRender(value) {
    const listData = getListData(value);
    return (
      <ul className="events" style={{ padding: 0 }}>
        {listData.map((item, idx) => (
          <li
            key={idx}
            style={{
              listStyle: "none",
              fontSize: 12,
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
            }}
          >
            <Link href={`/orders/${item.id}`}>
              <a>
                <Badge status={item.type} text={item.content} />
              </a>
            </Link>
          </li>
        ))}
      </ul>
    );
  }

  function getMonthData(value) {
    if (value.month() === actualmonth) {
      return actualmonth;
    }
  }

  function monthCellRender(value) {
    const num = getMonthData(value);
    let same;
    deliveries.map((ord) => {
      ord.year === value.year() ? (same = true) : (same = false);
    });
    return num ? (
      <div className="notes-month">
        {same && <section>{`${deliveries.length}  Deliveries`}</section>}
        {deliveries.map((ord, idx) => {
          if (ord.year === value.year())
            return (
              <div key={idx}>
                <span>{ord.content}</span>
              </div>
            );
        })}
      </div>
    ) : null;
  }
  return (
    <Card>
      <Calendar
        dateCellRender={dateCellRender}
        monthCellRender={monthCellRender}
        onPanelChange={(value, mode) => {
          setCurrentMonth(Number(value.format("D M Y").split(" ")[1]));
          setCurrentYear(Number(value.format("D M YYYY").split(" ")[2]));
        }}
      />
    </Card>
  );
}

export default CalendarComp;
