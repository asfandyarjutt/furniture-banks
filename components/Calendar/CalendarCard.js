import React, { useState, useEffect, useRef } from "react";
import {
  Card,
  Typography,
  Button,
  Table,
  Select,
  Row,
  Col,
  Modal,
  Input,
  TimePicker,
  Popconfirm,
  Space,
  message as ms,
  Tag,
  Form,
} from "antd";
import useInput from "../../lib/useInput";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import Highlighter from "react-highlight-words";
import { useTimes } from "../../lib/hooks";
import { calendarCard } from "../../constants";
import {
  numberRegx,
  zipRegx,
  zipCodeRegx,
  duplicateZipRegx,
} from "../../constants";

const {
  DELIVERY,
  TYPE_DELIVERY,
  SELF_HAUL,
  TYPE_SELF_HAUL,
  PICK_UP,
  PICK_UP_PURCHASE,
  RECEIVE_DELIVERY,
} = calendarCard;

const { Title } = Typography;
const { Option } = Select;
const { RangePicker } = TimePicker;

function CalendarCard({ type, data, selectedDays }) {
  const [times, mutate, trigger] = useTimes();
  const [state, setState] = useState({
    selectedRowKeys: [],
    loading: false,
  });
  const [modal, setModal] = useState(false);
  const [day, setDay] = useState("");
  const [selectedDay, setSelectedDay] = useState("");
  const [limit, setLimit] = useInput("");
  const [zip, setZip] = useInput("");
  const [to, setTo] = useState();
  const [from, setFrom] = useState();
  const [dataSource, setDataSource] = useState([]);
  const [daysToDelete, setDaysToDelete] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchColumn] = useState("");
  const searchInput = useRef(null);
  const [form] = Form.useForm();

  const seconds = [];

  for (let i = 0; i <= 59; i++) {
    seconds.push(i);
  }

  const days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];

  const capitalizeString = (string) => {
    if (!string) return null;
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  const availableDays = days.filter(
    (day) => !Object.keys(data || {}).includes(day)
  );

  const onSelectChange = (selectedRowKeys) => {
    const removeDays = selectedRowKeys.map((index) => Object.keys(data)[index]);
    setState({ ...state, selectedRowKeys });
    setDaysToDelete(removeDays);
  };
  useEffect(() => {
    if (data) {
      setDataSource(
        Object.entries(data).map((entries, i) => {
          return {
            day: capitalizeString(entries[0]),
            schedule: `${entries[1]?.start
              ?.replace(":00 ", " ")
              ?.replace("am", "AM")
              ?.replace("pm", "PM")} - ${entries[1]?.end
              ?.replace(":00 ", " ")
              ?.replace("am", "AM")
              ?.replace("pm", "PM")}`,
            key: i,
            ...(entries[1].limit ? { limit: entries[1].limit } : null),
            ...(entries[1].zip
              ? { zip: entries[1].zip?.split(/[ ,-]+/) }
              : null),
          };
        })
      );
    }
  }, [data]);
  useEffect(() => {
    setSelectedDay(selectedDays);
  }, [selectedDays]);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            size="small"
            style={{
              width: 90,
              backgroundColor: "white",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderWidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
              backgroundColor: "white",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderWidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),

    onFilter: (value, record) =>
      record[dataIndex]?.toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  function handleSearch(selectedKeys, confirm, dataIndex) {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchColumn(dataIndex);
  }

  function handleReset(clearFilters) {
    clearFilters();
    setSearchText("");
  }

  const columns = [
    {
      title: "Day",
      dataIndex: "day",
      key: "day",
      render: (record) => {
        return data && <span>{capitalizeString(data[record]?.day)}</span>;
      },
    },
    {
      title: "Schedule",
      dataIndex: "schedule",
      key: "schedule",
    },
    {
      title: "Appt Limit",
      dataIndex: "limit",
      key: "limit",
    },
  ];
  if (type === DELIVERY) {
    columns.unshift({
      title: "Zip Code",
      dataIndex: "zip",
      key: "zip",
      render: (zip) => (
        <>
          {zip.map((zip) => {
            let colors = [
              "magenta",
              "red",
              "volcano",
              "orange",
              "gold",
              "lime",
              "green",
              "cyan",
              "blue",
              "geekblue",
              "purple",
            ];
            return (
              <div>
                <Tag
                  style={{ marginBottom: 3 }}
                  color={colors[Math.floor(Math.random() * colors.length)]}
                  key={zip}
                >
                  {zip}
                </Tag>
              </div>
            );
          })}
        </>
      ),
    });
  }

  function extractHours([from, to]) {
    const start = from.format("hh:mm:ss a");
    const end = to.format("hh:mm:ss a");
    setFrom(start);
    setTo(end);
  }

  async function saveNewTime() {
    if (!from || !to || !day || !limit) {
      return;
    }
    const resTime = await fetch("/api/times", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        start: from,
        end: to,
        type: type === SELF_HAUL ? TYPE_SELF_HAUL : TYPE_DELIVERY,
        day: day,
        zip: type === SELF_HAUL ? null : zip.value,
        limit: limit.value,
      }),
    });
    setDay("");
    setZip("");
    setLimit("");
    setModal(false);
    const { message } = await resTime.json();
    trigger("/api/times");
    ms.success(`${message}`);
    form.resetFields();
  }

  async function deleteDays() {
    if (daysToDelete.length === 0) return;
    await fetch("/api/times", {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        days: daysToDelete,
        type: type === SELF_HAUL ? TYPE_SELF_HAUL : TYPE_DELIVERY,
      }),
    }).then(() => {
      trigger("/api/times");
      setState({ ...state, selectedRowKeys: [] });
    });
  }

  const { selectedRowKeys } = state;
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const handleSelect = (value) => {
    setDay(value);
  };
  const handleSelectDay = async (value) => {
    setSelectedDay(value);
    let data =
      type === SELF_HAUL
        ? {
            selfHaulDays: value,
          }
        : type === DELIVERY
        ? {
            deliveryDays: value,
          }
        : null;
    await fetch("/api/times", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
  };

  return (
    <Card>
      <Title level={3}>{type === SELF_HAUL ? PICK_UP : DELIVERY}</Title>
      <span>
        Client must {type === SELF_HAUL ? PICK_UP_PURCHASE : RECEIVE_DELIVERY}{" "}
        within{" "}
      </span>
      <Select
        style={{ marginLeft: 5, width: "30%" }}
        placeholder="Day"
        onChange={handleSelectDay}
        value={selectedDay}
      >
        <Option value="1">1 Business Day</Option>
        <Option value="2">2 Business Days</Option>
        <Option value="3">3 Business Days</Option>
        <Option value="4">4 Business Days</Option>
        <Option value="5">5 Business Days</Option>
      </Select>
      <p>
        <b>
          Add days and times when client can{" "}
          {type === SELF_HAUL ? PICK_UP_PURCHASE : RECEIVE_DELIVERY}
        </b>
      </p>
      <Row>
        <Col span={24}>
          {data && (
            <Table
              rowSelection={rowSelection}
              dataSource={dataSource}
              columns={columns}
              pagination={false}
            />
          )}
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={8}>
          <Popconfirm
            title="Are you sure you want to delete？"
            okText="Yes"
            cancelText="No"
            onConfirm={deleteDays}
          >
            <Button
              style={{
                backgroundColor: "#f5222d",
                height: "40px",
                color: "white",
                border: "1px solid #f5222d",
                borderRadius: "3px",
                borderWidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
            >
              <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Delete
            </Button>
          </Popconfirm>
        </Col>
        <Col span={8} offset={8}>
          <Button
            style={{
              backgroundColor: "#52c41a",
              height: "40px",
              color: "white",
              border: "1px solid #52c41a",
              borderRadius: "3px",
              borderWidth: "1px",
              display: "inline-block",
              textAlign: "center",
              marginLeft: -8,
            }}
            onClick={() => setModal(true)}
          >
            <FontAwesomeIcon icon={faPlus} /> &nbsp; Add{" "}
            {type === SELF_HAUL ? PICK_UP : DELIVERY} Time
          </Button>
          <Modal
            title={`Add New ${type === SELF_HAUL ? PICK_UP : DELIVERY} Time`}
            visible={modal}
            onOk={form.submit}
            onCancel={() => {
              setModal(false);
              form.resetFields();
            }}
            okText={"Save"}
          >
            <Form form={form} onFinish={saveNewTime}>
              <Row
                gutter={8}
                style={{ marginBottom: "2em" }}
                justify="space-around"
              >
                <Col span={10}>
                  <Form.Item
                    name="day"
                    rules={[
                      {
                        required: true,
                        message: "Day is Required.",
                      },
                    ]}
                  >
                    <Select
                      name="day"
                      style={{ width: "100%" }}
                      placeholder="Day"
                      onChange={handleSelect}
                    >
                      <Option value="">Day</Option>
                      {type === DELIVERY
                        ? days.map((day, i) => (
                            <Option key={i} value={day}>
                              {day.charAt(0).toUpperCase() + day.slice(1)}
                            </Option>
                          ))
                        : days.map((day, i) => (
                            <Option key={i} value={day}>
                              {day.charAt(0).toUpperCase() + day.slice(1)}
                            </Option>
                          ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={10} offset={4}>
                  <Form.Item
                    name="time"
                    rules={[
                      {
                        required: true,
                        message: "Time is Required.",
                      },
                    ]}
                  >
                    <RangePicker
                      name="time"
                      onChange={extractHours}
                      disabledSeconds={() => seconds}
                      format="hh:mm A"
                      okText={"OK"}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={type === SELF_HAUL ? 24 : 10}>
                  <Form.Item
                    name="appointmentLimit"
                    rules={[
                      {
                        required: true,
                        message: "Appointment Limit is Required.",
                      },
                      {
                        pattern: numberRegx,
                        message: "Enter a valid Appointment Limit.",
                      },
                    ]}
                  >
                    <Input
                      name="appointmentLimit"
                      placeholder="Appointments Limit"
                      {...limit}
                    />
                  </Form.Item>
                </Col>
                {type === DELIVERY && (
                  <Col span={10} offset={4}>
                    <Form.Item
                      name="zipCode"
                      rules={[
                        {
                          required: true,
                          message: "Zip Code is Required.",
                        },
                        {
                          pattern: zipCodeRegx,
                          message: "Enter a valid Zip Code.",
                        },
                        {
                          pattern: duplicateZipRegx,
                          message: "Zip Code is already entered.",
                        },
                      ]}
                    >
                      <Input name="zipCode" placeholder="Zip Code" {...zip} />
                    </Form.Item>
                  </Col>
                )}
              </Row>
            </Form>
          </Modal>
        </Col>
      </Row>
    </Card>
  );
}

export default CalendarCard;
