const styles = {
  backButtonContainer: {
    marginBottom: "3em",
    display: "flex",
    justifyContent: "space-between",
  },
  backButton: {
    backgroundColor: "white",
    height: "40px",
    color: "#69c0ff",
    border: "1px solid #69c0ff",
    borderRadius: "3px",
    borderwidth: "1px",
    display: "inline-block",
    textAlign: "center",
    paddingLeft: "20px",
    paddingRight: "20px",
  },
  backIconColor: { color: "#69c0ff" },
  backTextColor: { color: "#69c0ff" },
  mainContainer: {
    padding: 24,
    margin: 0,
    minHeight: 280,
    background: "#ffffff",
  },
  headerContainer: { display: "flex", justifyContent: "center" },
  upperContainer: {
    marginTop: "2em",
    display: "flex",
  },
  tableContainer: { marginBottom: "3em" },
  tableStyle: { borderBottom: "1px solid black" },
  orderTotalContainer: { marginBottom: "2em" },
};
export default styles;
