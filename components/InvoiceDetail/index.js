import React, { useState, useEffect } from "react";
import Router from "next/router";
import { OrderDetails } from "../../constants";
import { useOneInvoice } from "../../lib/hooks";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import {
  Descriptions,
  Layout,
  Button,
  Row,
  Col,
  Table,
  Typography,
  Space,
} from "antd";
import Dinero from "dinero.js";
import styles from "./styles";
import { invoiceDetail } from "../../constants";

const {
  CLIENT,
  CLIENT_ADDRESS,
  CLIENT_EMAIL,
  CLIENT_PHONE,
  PARTNER_AGENCY,
  CASE_MANGER,
  CASE_MANGER_EMAIL,
  CASE_MANGER_PHONE,
  DELIVERY_RECEIVED_BY,
  PHONE_NUMBER,
} = invoiceDetail;

const { Content } = Layout;
const { Title, Text } = Typography;

const invoicesDetails = ({ invoiceId }) => {
  const [invoice] = useOneInvoice(invoiceId);
  let tableHeading = [];
  let date = moment(invoice?.createdAt).format("dddd, MMMM D, YYYY, hh:mm a");
  useEffect(() => {
    Router.prefetch("/invoice");
  });

  let data = invoice?.items?.map((item, idx) => {
    tableHeading = {
      key: idx,
      item: item?.item[0]?.description,
      sku: item.sku,
      quantity: item.quantity,
      amount: "N/A",
    };
    return tableHeading;
  });

  if (invoice?.type) {
    const paymentInfo = invoice?.type;
    data.push({
      item: paymentInfo,
      key: data.length + 1,
      amount: Dinero({ amount: Number(invoice?.total + "00") }).toFormat(
        "$0,0.00"
      ),
    });
  }

  const columns = [
    {
      title: "Item",
      dataIndex: "item",
      key: "item",
    },
    {
      title: "SKU",
      dataIndex: "sku",
      key: "sku",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
    },
  ];

  return (
    <div>
      <Row style={styles.backButtonContainer}>
        <Space value="middle" size={40} direction="horizontal">
          <Button onClick={() => Router.back()} style={styles.backButton}>
            <FontAwesomeIcon
              style={styles.backIconColor}
              icon={faChevronLeft}
            />
            <span style={styles.backTextColor}> &nbsp; Back</span>
          </Button>
        </Space>
      </Row>
      {invoice && (
        <Content
          className="site-layout-background"
          style={styles.mainContainer}
        >
          <Row style={styles.headerContainer}>
            <Title>Invoice</Title>
          </Row>
          <Row style={styles.upperContainer} gutter={[8, 16]}>
            <Col span={12}>
              <Col span={16}>
                <Title level={4}>{`Order ${invoice?.number}`}</Title>
              </Col>
            </Col>
            <Col span={12}>
              <Col span={18}>
                <Title level={4}>{date}</Title>
              </Col>
            </Col>
          </Row>
          <Row gutter={[8, 16]}>
            <Col span={12}>
              <Descriptions size="middle">
                <Descriptions.Item span={4} label={CLIENT}>
                  {invoice?.clientName}
                </Descriptions.Item>
                <Descriptions.Item span={4} label={CLIENT_ADDRESS}>
                  {invoice?.clientAddress} - {invoice.clientAppartment},{" "}
                  {invoice?.clientCity}, {invoice.clientState}{" "}
                  {invoice.clientZip} <br />
                </Descriptions.Item>
                <Descriptions.Item span={4} label={CLIENT_EMAIL}>
                  {invoice?.clientEmail} <br />
                </Descriptions.Item>
                <Descriptions.Item label={CLIENT_PHONE}>
                  {invoice?.clientPhone}
                </Descriptions.Item>
              </Descriptions>
            </Col>
            <Col span={12}>
              <Descriptions>
                <Descriptions.Item span={4} label={PARTNER_AGENCY}>
                  {invoice?.agency.name}
                </Descriptions.Item>
                <Descriptions.Item span={4} label={CASE_MANGER}>
                  {invoice?.cmName}
                </Descriptions.Item>
                <Descriptions.Item span={4} label={CASE_MANGER_EMAIL}>
                  {invoice?.cmEmail}
                </Descriptions.Item>
                <Descriptions.Item label={CASE_MANGER_PHONE}>
                  {invoice?.cmPhone}
                </Descriptions.Item>
              </Descriptions>
            </Col>
          </Row>

          <Row>
            <Col span={6}>
              <Descriptions
                title={
                  invoice?.type === "Pick Up"
                    ? "To be pick up by:"
                    : "Delivery to be received by:"
                }
              ></Descriptions>
            </Col>
            <Col span={6}>
              <Descriptions.Item>
                {`${invoice?.receiverName}`}
              </Descriptions.Item>
            </Col>
            <Col span={6}>
              <Descriptions title={PHONE_NUMBER}></Descriptions>
            </Col>
            <Col span={6}>
              <Descriptions.Item>
                {` ${invoice?.receiverPhone}`}
              </Descriptions.Item>
            </Col>
          </Row>
          <Row style={styles.tableContainer}>
            <Col span={20}>
              <Table
                style={styles.tableStyle}
                columns={columns}
                dataSource={data}
                pagination={false}
              />
            </Col>
          </Row>
          <Row gutter={[16, 16]} style={styles.orderTotalContainer}>
            <Col span={16}>
              <Title level={4}>{OrderDetails.Order_Total}</Title>
            </Col>
            <Col span={8}>
              <Title level={4}>
                {Dinero({ amount: Number(invoice?.total + "00") }).toFormat(
                  "$0,0.00"
                )}
              </Title>
            </Col>
          </Row>
          <Row gutter={[24, 48]}>
            <Col span={4}>
              <Text strong>{OrderDetails.Client_Amount}</Text>
            </Col>
            <Col offset={4} span={5}>
              <Text>{invoice?.clientPaymentType}</Text>
            </Col>
            <Col offset={3}>
              <Text>
                {Dinero({
                  amount: Number(invoice?.clientAmount + "00"),
                }).toFormat("$0,0.00")}
              </Text>
            </Col>
          </Row>
          <Row gutter={[24, 48]}>
            <Col span={4}>
              <Text strong>{OrderDetails.Agency_Amount}</Text>
            </Col>
            <Col offset={4} span={5}>
              <Text>
                {invoice?.agencyPaymentType?.charAt(0)?.toUpperCase() +
                  invoice?.agencyPaymentType?.slice(1)}
              </Text>
            </Col>
            <Col offset={3}>
              <Text>
                {Dinero({
                  amount: Number(invoice?.agencyAmount + "00"),
                }).toFormat("$0,0.00")}
              </Text>
            </Col>
          </Row>
        </Content>
      )}
    </div>
  );
};

export default invoicesDetails;
