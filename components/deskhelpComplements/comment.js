import React from "react";
import { Layout, Typography, Row, Divider, Space } from "antd";
import moment from 'moment'
const { Content } = Layout;
const { Text, Title } = Typography;

const Comment = ({ info, writer, agency, manager }) => {
  return (
    <div>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          height: "auto",
          background: "#ffffff",
          marginBottom: "2em",
        }}
      >
        <Row>
          <Space size="middle" direction="horizontal">
            <Title level={2}> <span>RE:</span></Title>
            <Title level={2}>{info?.title}</Title>
          </Space>
        </Row>
        <Text type="secondary">{moment(info?.createdAt).format("dddd, MMMM D, YYYY, hh:mm a")}</Text>
        <Divider />
        <Text strong style={{ marginRight: "7em" }}>
          {`To: ${writer?.firstName} ${writer?.lastName}`}
        </Text>
        <Text>{`Agency: ${agency}`}</Text>
        <Divider />
        <Row>
          <Text></Text>
          <Text style={{ marginBottom: "3em" }}>{info.body}</Text>
        </Row>
      </Content>
    </div>
  );
};

export default Comment;
