import React, { useState, useEffect } from "react";
import Router from "next/router";
import { useCurrentUser, useHelpDeskTickets } from "../../lib/hooks";
import {
  Layout,
  Row,
  Col,
  Button,
  Dropdown,
  Menu,
  Table,
  Tag,
  Alert,
  Space,
  Modal,
  Input,
  Spin,
} from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronDown,
  faReply,
  faTrashAlt,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
import moment from "moment";
import { ticketDeleteModal } from "../../constants";
import { funcFetchTickets } from "../Layout";
const {
  SELECTED_ROW_TITLE,
  NO_ROW_SELECTED_TITLE,
  SELECTED_ROW_CONTENT,
  NO_ROW_SELECTED_CONTENT,
  SELECTED_ROW_OK_TEXT,
  NO_ROW_SELECTED_OK_TEXT,
  SELECTED_ROW_OK_TYPE,
  NO_ROW_SELECTED_OK_TYPE,
  SELECTED_ROW_CANCEL_TEXT,
  NO_ROW_SELECTED_CANCEL_TEXT,
} = ticketDeleteModal;
const { confirm } = Modal;
const TicketsAll = () => {
  const [tickets, mutate, trigger] = useHelpDeskTickets();
  const [user] = useCurrentUser();
  const [spining, setSpin] = useState(false);
  const [dispTable, setDispTable] = useState("All Messages");
  const [searched, setSearch] = useState("");
  const [message, setMessage] = useState(undefined);
  const [messagewar, setMessageWar] = useState(undefined);
  const router = useRouter();
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  useEffect(() => {
    setMessage(router.query.message);
  }, [tickets]);
  useEffect(() => {
    setSpin(true);
    if (tickets != undefined) {
      setSpin(false);
    }
  }, [tickets]);
  async function handleDelete() {
    let markers = !selectedRowKeys
      ? null
      : tickets.map((row, index) => {
        return selectedRowKeys.map((id, i) => {
          if (index === id) {
            return row._id;
          }
        });
      });
    confirm({
      title: selectedRowKeys?.length
        ? SELECTED_ROW_TITLE
        : NO_ROW_SELECTED_TITLE,
      content: selectedRowKeys?.length
        ? SELECTED_ROW_CONTENT
        : NO_ROW_SELECTED_CONTENT,
      okText: selectedRowKeys?.length
        ? SELECTED_ROW_OK_TEXT
        : NO_ROW_SELECTED_OK_TEXT,
      okType: selectedRowKeys?.length
        ? SELECTED_ROW_OK_TYPE
        : NO_ROW_SELECTED_OK_TYPE,
      cancelText: selectedRowKeys?.length
        ? SELECTED_ROW_CANCEL_TEXT
        : NO_ROW_SELECTED_CANCEL_TEXT,
      async onOk() {
        if (selectedRowKeys?.length) {
          markers = markers.flat();
          markers = markers.filter((m) => m !== undefined);
          try {
            mutate("/api/hdtickets", [...tickets, markers], false);
            const resTickets = await fetch(`/api/hdtickets/deleteMany`, {
              method: "DELETE",
              headers: { "Content-Type": "application/json" },
              body: JSON.stringify(markers),
            });
            const deleted = await resTickets.json();
            const { message } = deleted;
            trigger("/api/hdtickets");
            if (resTickets.status === 200) {
              setMessageWar(message);
              setSelectedRowKeys([]);
              setInterval(() => {
                setMessageWar(undefined);
              }, 2000);
            }
          } catch (error) {
            throw error;
          }
        }
      },
    });
  }
  async function handleMarkUnread(e) {
    e.preventDefault();
    let markers = !selectedRowKeys
      ? null
      : tickets.map((row, index) => {
        return selectedRowKeys.map((id, i) => {
          if (index === id) {
            return row._id;
          }
        });
      });
    if (selectedRowKeys?.length) {
      markers = markers.flat();
      markers = markers.filter((m) => m !== undefined);
      const data = { readed: false, markers };
      mutate("/api/hdtickets", [...tickets, data]);
      const updated = await fetch(`/api/hdtickets/updatemany`, {
        method: "PATCH",
        headers: { "Content-Type": ["application/json"] },
        body: JSON.stringify(data),
      });
      const update = await updated.json();
      const { message } = update;

      trigger("/api/hdtickets");

      if (updated.status === 200) {
        mutate("/api/hdtickets", [...tickets, data]);

        setMessage(message);
        setSelectedRowKeys([]);
        setInterval(() => {
          setMessage(undefined);
        }, 2000);
      }
    } else {
      setMessageWar("Please select a message");
      setInterval(() => {
        setMessageWar(undefined);
      }, 2000);
    }
  }
  function handleMenuClick(e) {
    const { key } = e;
    switch (key) {
      case "1":
        setDispTable("All Messages");
        break;
      case "2":
        setDispTable("Answered");
        break;
      case "3":
        setDispTable("Unanswered");
        break;
      default:
        "All Messages";
        break;
    }
  }
  let data = tickets
    ? tickets.map((item, idx) => {
      let date = moment(item?.createdAt).format(
        "dddd, MMMM D, YYYY, hh:mm a"
      );
      return {
        ...item,
        key: idx,
        id: item?._id,
        user: `${item?.from?.firstName} ${item?.from?.lastName}`,
        agency: item?.agency ? item?.agency : item?.belongTo,
        title: item?.title,
        subject: item?.body,
        readed: item?.readed,
        replied: item?.replied,
        createdAt: date,
      };
    })
    : null;
  const unanswered = data?.filter((item) => !item.replied);
  const answered = data?.filter((item) => item.replied);
  let searchedAt = data?.filter((item) => {
    return (
      item?.user?.toLowerCase()?.includes(searched.toLowerCase()) ||
      item?.agency?.toLowerCase()?.includes(searched.toLowerCase()) ||
      item?.subject?.toLowerCase()?.includes(searched.toLowerCase()) ||
      item?.title?.toLowerCase()?.includes(searched.toLowerCase())
    );
  });

  async function handleReaded(ticketId) {
    const data =
      user && user.role === "ADMIN"
        ? { readed: true, ticketId }
        : { read: true, ticketId };
    await fetch(`/api/hdtickets`, {
      method: "PATCH",
      headers: { "Content-Type": ["application/json"] },
      body: JSON.stringify(data),
    }).then(() => {
      mutate("/api/hdtickets", [...tickets, data]);
      Router.push(`/helpdesk/[ticketId]`, `/helpdesk/${ticketId}`);
    });
  }
  const columns = [
    {
      title: "User",
      dataIndex: "user",
      render: (text, record) => {
        return (
          <Space>
            <span>
              {record.readed ? (
                <Tag color="green">Read</Tag>
              ) : (
                <Tag color="blue">Unread</Tag>
              )}
            </span>
            <span>
              {record.replied ? <FontAwesomeIcon icon={faReply} /> : null}
            </span>
            <span> {text}</span>
          </Space>
        );
      },
      width: 25,
    },
    {
      title: "Organization",
      dataIndex: "agency",
      render: (text) => <span>{text}</span>,
      width: 25,
    },
    {
      title: "Subject",
      dataIndex: "subject",
      render: (title, record) => {
        return (
          <Space onClick={() => handleReaded(record._id)}>
            <a>{record.title}</a>
          </Space>
        );
      },
      width: 25,
    },
    {
      title: "Date",
      dataIndex: "createdAt",
      key: "createdAt",
      width: 25,
    },
  ];

  const onSelectChange = (selectedRowKeys) => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const { Content } = Layout;

  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1">All Messages</Menu.Item>
      <Menu.Item key="2">Answered</Menu.Item>
      <Menu.Item key="3">Unanswered </Menu.Item>
    </Menu>
  );
  return (
    <Spin tip="Loading..." spinning={spining}>
      <div>
        <Content
          className="site-layout-background"
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
            background: "#ffffff",
          }}
        >
          <Row style={{ marginBottom: "3em" }} justify="space-around">
            <Dropdown overlay={menu}>
              <Button
                style={{ marginLeft: "2em", marginRight: "35em" }}
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#69c0ff",
                  border: "1px solid #69c0ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center",
                  width: "11em",
                }}
              >
                {dispTable} <FontAwesomeIcon icon={faChevronDown} />
              </Button>
            </Dropdown>
            <Button
              onClick={handleMarkUnread}
              style={{
                backgroundColor: "white",
                height: "40px",
                color: "#69c0ff",
                border: "1px solid #69c0ff",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
              type="primary"
            >
              Mark as Unread
            </Button>
            <Button
              onClick={handleDelete}
              style={{
                backgroundColor: "#f5222d",
                height: "40px",
                color: "white",
                border: "1px solid #f5222d",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
            >
              <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Delete
            </Button>
            <Input
              size="large"
              prefix={<FontAwesomeIcon icon={faSearch} />}
              placeholder="Search by user, organization, or subject."
              name="searchBar"
              onChange={(e) => {
                setSearch(e.target.value), setDispTable("Search");
              }}
              style={{ width: "40%", height: "40px" }}
            />
          </Row>
          <Row justify="center" align="middle">
            <Alert
              style={{
                marginLeft: "2em",
                marginBottom: "2em",
                width: "96%",
                display: message === undefined ? "none" : "flex",
              }}
              message={message}
              type="success"
              showIcon
              closable
              onClose={() => setMessage(undefined)}
            />
            <Alert
              style={{
                marginLeft: "2em",
                marginBottom: "2em",
                width: "96%",
                display: messagewar === undefined ? "none" : "flex",
              }}
              message={messagewar}
              type="warning"
              showIcon
              closable
              onClose={() => setMessageWar(undefined)}
            />
          </Row>
          <Row>
            <Col span={24}>
              <Spin size="large" tip="Loading..." spinning={spining}>
                <Table
                  pagination="bottomRight"
                  rowSelection={rowSelection}
                  columns={columns}
                  dataSource={
                    dispTable === "All Messages"
                      ? data
                      : null || dispTable === "Unanswered"
                        ? unanswered
                        : null || dispTable === "Answered"
                          ? answered
                          : null || dispTable === "Search"
                            ? searchedAt
                            : null
                  }
                />
              </Spin>
            </Col>
          </Row>
        </Content>
      </div>
    </Spin>
  );
};

export default TicketsAll;
