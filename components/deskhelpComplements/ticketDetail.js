import React, { useState, useEffect } from "react";
import {
  useOneTicket,
  useCurrentUser,
  useHelpDeskTickets,
} from "../../lib/hooks";
import { useRouter } from "next/router";
import moment from "moment";
import Comment from "./comment";
import {
  Layout,
  Row,
  Col,
  Button,
  Typography,
  Divider,
  Pagination,
  Space,
  Input,
  Form,
  Skeleton,
} from "antd";
import { Role } from "../../constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faReply,
  faTrash,
  faChevronLeft,
  faCheck,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
const { Content } = Layout;
const { Text, Title } = Typography;
const { TextArea } = Input;

const TicketTemplate = ({ ticketId }) => {
  const router = useRouter();
  const [ticket] = useOneTicket(ticketId);
  const [tickets, mutate, trigger] = useHelpDeskTickets();
  const [load, setLoad] = useState(false);
  const [user] = useCurrentUser();
  const [viewResponse, SetViewResponse] = useState("none");
  const [toDay, setToday] = useState(ticket?.createdAt);
  const [answer, setAnswer] = useState({
    title: ticket?.title,
    agency: ticket?.agency,
    to: ticket?.from,
    from: ticket?.from,
    ticketId,
    replied: true,
  });
  const [spining, setSpin] = useState(false);
  let date = new Date(toDay);
  const today = moment().format("MMMM D, YYYY");

  useEffect(() => {
    setSpin(true);
    setToday(ticket?.createdAt);
    setAnswer({
      title: ticket?.title,
      to: ticket?.from,
      from: ticket?.from,
      agency: ticket?.agency,
      ticketId,
    });
    router.prefetch("/helpdesk");
    setSpin(false);
  }, [ticket]);
  useEffect(() => {
    if (!tickets) {
      setSpin(true);
    }
  }, []);

  const handleChange = (e) => {
    const { value, name } = e.target;
    setAnswer({
      ...answer,
      [name]: value,
    });
  };

  const onFinish = async (values) => {
    const data =
      user && user.role === Role.ADMIN
        ? {
            replied: true,
            readed: true,
            read: false,
            read1: false,
            read2: false,
            ticketId,
          }
        : user && user.role === Role.FBA
        ? {
            replied: true,
            readed: false,
            read: true,
            read1: false,
            read2: false,
            ticketId,
          }
        : user && user.role === Role.PAL
        ? {
            replied: true,
            readed: false,
            read: false,
            read1: true,
            read2: false,
            ticketId,
          }
        : {
            replied: true,
            readed: false,
            read: false,
            read1: false,
            read2: true,
            ticketId,
          };
    const nuevot = await fetch(`/api/hdtickets/replyTicket`, {
      method: "POST",
      headers: { "Content-Type": ["application/json"] },
      body: JSON.stringify(answer),
    });
    const replied = await nuevot.json();
    const { message } = replied;

    if (nuevot.status === 200) {
      await fetch(`/api/hdtickets`, {
        method: "PATCH",
        headers: { "Content-Type": ["application/json"] },
        body: JSON.stringify(data),
      }).then(() => {
        handleReaded(ticketId);
        router.push({ pathname: "/helpdesk", query: { message } });
      });
    }
  };
  async function removeTicket(ticketId) {
    await fetch(`/api/hdtickets/${ticketId}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });
    router.push("/helpdesk");
  }

  async function handleReaded(ticketId) {
    const data =
      user && user.role === Role.ADMIN
        ? { readed: true, ticketId }
        : user && user.role === Role.FBA
        ? { read: true, ticketId }
        : user && user.role === Role.PAL
        ? { read1: true, ticketId }
        : { read2: true, ticketId };
    await fetch(`/api/hdtickets`, {
      method: "PATCH",
      headers: { "Content-Type": ["application/json"] },
      body: JSON.stringify(data),
    }).then(() => {
      user?.role === Role.ADMIN
        ? router.push("/helpdesk")
        : router.push("/createticket");
    });
  }

  return (
    <div>
      <Row
        style={{
          marginBottom: "3em",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Button
          onClick={() => handleReaded(ticketId)}
          type="primary"
          style={{
            backgroundColor: "white",
            height: "40px",
            color: "#69c0ff",
            border: "1px solid #69c0ff",
            borderRadius: "3px",
            borderwidth: "1px",
            display: "inline-block",
            textAlign: "center",
            paddingLeft: "20px",
            paddingRight: "20px",
          }}
        >
          <FontAwesomeIcon icon={faChevronLeft} /> &nbsp; Back
        </Button>

        <Pagination defaultCurrent={1} />
      </Row>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          height: "auto",
          background: "#ffffff",
          marginBottom: "2em",
        }}
      >
        <Skeleton size="large" active loading={spining}>
          <Row style={{ display: "flex", justifyContent: "space-between" }}>
            <Title level={2}>{ticket?.title}</Title>
            <div style={{ display: "flex" }}>
              <Space align="center" size="large">
                {user && user.role === Role.ADMIN && (
                  <Button
                    style={{
                      backgroundColor: "#f5222d",
                      height: "40px",
                      color: "white",
                      border: "1px solid #f5222d",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                    onClick={() => removeTicket(ticketId)}
                  >
                    <FontAwesomeIcon icon={faTrash} /> &nbsp; Delete
                  </Button>
                )}
                <Button
                  onClick={() => SetViewResponse("reply")}
                  style={{
                    backgroundColor: "#1890ff",
                    height: "40px",
                    color: "white",
                    border: "1px solid #1890ff",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faReply} /> &nbsp; Reply
                </Button>
              </Space>
            </div>
          </Row>
          <Text type="secondary">
            {moment(ticket?.createdAt).format("dddd, MMMM D, YYYY, hh:mm a")}
          </Text>
          <Divider />
          <Row>
            <Text strong style={{ marginRight: "7em" }}>
              From: {`${ticket?.from?.firstName} ${ticket?.from?.lastName}`}{" "}
            </Text>{" "}
            <Text>{` Agency: ${ticket?.agency}`}</Text>
          </Row>
          <Divider />
          <Row>
            <Text style={{ marginBottom: "3em" }}>{ticket?.body}</Text>
          </Row>
        </Skeleton>
      </Content>
      {ticket?.postId.map((item, idx) => {
        return (
          <div key={idx}>
            <Comment
              agency={ticket?.agency}
              info={item}
              writer={ticket?.from}
              manager={ticket?.to}
            />
          </div>
        );
      })}
      {viewResponse === "reply" ? (
        <Content
          className="site-layout-background"
          style={{
            padding: 24,
            margin: 0,
            height: "auto",
            background: "#ffffff",
          }}
        >
          <Row>
            <Space size="middle" direction="horizontal">
              <Title level={2}>
                {" "}
                <span>RE:</span>
              </Title>
              <Title level={2}>{ticket?.title}</Title>
            </Space>
          </Row>
          <Row>
            <Space size="large" direction="vertical">
              <Text type="secondary">{today}</Text>
            </Space>
          </Row>
          <Divider />
          <Text strong style={{ marginRight: "7em" }}>
            To: {` ${ticket?.from?.firstName} ${ticket?.from?.lastName}`}{" "}
          </Text>{" "}
          <Text>{`Agency: ${ticket?.agency}`}</Text>
          <Divider />
          <Form onFinish={onFinish}>
            <Row style={{ marginBottom: "2em" }}>
              <Col span={24}>
                <Form.Item
                  name="body"
                  rules={[
                    {
                      required: true,
                      message: "Please input your text",
                    },
                  ]}
                >
                  <TextArea
                    onChange={handleChange}
                    name="body"
                    placeholder={`Dear ${ticket?.from?.firstName},`}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Space size="large">
                <Button
                  htmlType="submit"
                  type="primary"
                  style={{
                    height: "40px",
                  }}
                >
                  <FontAwesomeIcon icon={faCheck} /> &nbsp; Send
                </Button>
                <Button
                  onClick={() => SetViewResponse("none")}
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#f5222d",
                    border: "1px solid #f5222d",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faTimes} /> &nbsp; Discard
                </Button>
              </Space>
            </Row>
          </Form>
        </Content>
      ) : null}
    </div>
  );
};

export default TicketTemplate;
