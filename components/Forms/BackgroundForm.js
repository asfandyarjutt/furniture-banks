import React, { useState, useEffect } from "react";
import { Row, Col, Input, Form, Button, Select, message, Popover } from "antd";
import { MinusCircleOutlined } from "@ant-design/icons";
import {
  faCheck,
  faPlus,
  faEdit,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useCurrentUser } from "../../lib/hooks";
import {
  HEADING_BG_INFO,
  MULTI_CHOICE,
  ALL_APPLY,
  FILL_FIELDS,
  Bg_Updated,
  Bg_Added,
  Bg_Deleted,
  ANSWERS_KEY,
  IN_COMPLETE,
  ADD_OPTION,
  ADD_QUESTION,
  OPTION_ERROR,
  DELETE_QUESTION,
} from "../../constants";
import styles from "./FormStyles";
import { obj } from "pumpify";
import { set } from "mongoose";

const { Option } = Select;

function BackgroundForm() {
  const [form] = Form.useForm();
  const [questions, setQuestions] = useState([]);
  const [currentUser] = useCurrentUser();
  const [disableSave, setDisableSave] = useState(false);
  const [closeFields, setCloseFields] = useState(true);
  const [editQuestion, setEditQuestion] = useState({
    edit: false,
    key: "",
  });
  const formItemLayoutWithOutLabel = {
    wrapperCol: {
      xs: { span: 6, offset: 0 },
      sm: { span: 20, offset: 4 },
    },
  };
  const content = (
    <div>
      <p>{FILL_FIELDS}</p>
    </div>
  );
  useEffect(() => {
    if (currentUser) {
      setQuestions(currentUser.furnitureBank.bg);
    }
  }, [currentUser]);

  const updateQuestion = async (key, index) => {
    const result = questions.filter((_, j) => index !== j);
    setQuestions(result);
    await fetch("/api/forms", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ data: result }),
    });
    message.success(key !== undefined ? Bg_Updated : Bg_Deleted);
  };

  async function finish(values) {
    let removeEmptyAns = [];
    const questionKeys = Object.keys(values);
    const filteredArray = questionKeys.filter((val) =>
      val.includes(ANSWERS_KEY)
    );

    filteredArray.forEach((answer, i) => {
      if (
        answer.includes(`${i}`) &&
        !!values[answer] &&
        values[answer].length > 0
      ) {
        const copy = questions;
        if (!(ANSWERS_KEY in copy[i])) {
          copy[i].answers = values[answer];
          setQuestions(copy);
          message.success(Bg_Added);
        } else {
          copy[i].answers = [...copy[i].answers, ...values[answer]];
          setQuestions(copy);
          message.success(Bg_Updated);
        }
      }
    });
    questions?.map((question) => {
      if (ANSWERS_KEY in question && question[ANSWERS_KEY].length > 0) {
        removeEmptyAns.push(question);
      }
      if (!(ANSWERS_KEY in question) || question[ANSWERS_KEY].length == 0) {
        message.warn(IN_COMPLETE);
      }
    });
    setQuestions(removeEmptyAns);

    await fetch("/api/forms", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ data: removeEmptyAns }),
    });

    message.success("Saved");
    window.location.reload();
    setCloseFields(!closeFields);
  }
  function handleInputChange({ target: { value } }, i) {
    const copy = questions;
    copy[i].question = value;
    setQuestions(copy);
  }
  function validateFields({ target: { value } }) {
    let status;
    if (!value || value === " " || value === "") {
      status = true;
    } else {
      status = false;
    }
    setDisableSave(status);
  }
  function editOptions({ target: { value } }, index, i) {
    const copy = questions;
    copy[index].answers[i] = value;
    setQuestions(copy);
  }
  function handleSelect(e, i) {
    const copy = questions;
    copy[i].type = e;
    setQuestions(copy);
  }
  const addQuestion = async () => {
    setQuestions([...questions, { question: "", type: 1 }]);
    setDisableSave(!disableSave);

    !closeFields ? setCloseFields(!closeFields) : null;
  };

  const storeState = editQuestion;
  if (typeof window !== "undefined" && editQuestion.edit) {
    localStorage.setItem("EditQue", JSON.stringify(storeState));
  } else if (!editQuestion.edit) {
    localStorage.removeItem("EditQue");
  }
  var getEditQue = localStorage.getItem("EditQue");
  var retrievedState = JSON.parse(getEditQue);
  return (
    <div>
      <p>{HEADING_BG_INFO}</p>
      <Form
        form={form}
        name="dynamic_form_item"
        {...formItemLayoutWithOutLabel}
        onFinish={finish}
      >
        <Row gutter={[32, 32]}>
          <Col span={4}>
            <Button onClick={addQuestion} style={styles.Add_questions}>
              <FontAwesomeIcon icon={faPlus} /> &nbsp; {ADD_QUESTION}
            </Button>
          </Col>
          {questions.map((question, index) => (
            <Row
              key={index}
              style={{
                width: "100%",
                marginBottom: "10px",
                paddingLeft: "20px",
              }}
            >
              <Col span={17} style={{ marginRight: "20px" }}>
                <Form.Item
                  key={index}
                  name={`${questions[index].question}_${index}`}
                  label={`Question ${index + 1}: `}
                  initialValue={questions[index].question}
                  onChange={(e) => {
                    validateFields(e);
                  }}
                >
                  <Input
                    onBlur={(e) => {
                      handleInputChange(e, index);
                    }}
                    size="large"
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Select
                  size="large"
                  defaultValue={`${questions[index].type}` || "1"}
                  onChange={(e) => handleSelect(e, index)}
                >
                  <Option value="1">{MULTI_CHOICE}</Option>
                  <Option value="2">{ALL_APPLY}</Option>
                </Select>
              </Col>
              <Col span={24}>
                <Col style={{ display: "flex" }}>
                  Previous Options: &nbsp;
                  {questions[index]?.answers?.map((el, i) =>
                    i + 1 === questions[index]?.answers.length
                      ? ` ${el}.`
                      : `${el}, `
                  )}
                  {retrievedState?.edit &&
                  retrievedState?.key === index ? null : (
                    <>
                      {questions[index]?.answers?.length >= 0 && (
                        <FontAwesomeIcon
                          key={index}
                          icon={faEdit}
                          style={{
                            color: "#0050B3",
                            fontSize: 18,
                            cursor: "pointer",
                            marginLeft: 20,
                          }}
                          onClick={() => {
                            setEditQuestion({
                              edit: true,
                              key: index,
                            });
                          }}
                        />
                      )}
                    </>
                  )}
                </Col>
                {/* Edit questions */}
                {retrievedState?.edit && retrievedState?.key === index && (
                  <Col>
                    <Row>
                      {questions[index]?.answers?.map((el, i) => (
                        <>
                          <Col key={i} span={18}>
                            <Form.Item
                              key={i}
                              name={el}
                              onChange={(e) => {
                                editOptions(e, index, i);
                                validateFields(e);
                              }}
                            >
                              <Input size="large" defaultValue={el} />
                            </Form.Item>
                          </Col>
                          {/* Delete button */}
                          {questions[index]?.answers?.length > 1 && (
                            <Col
                              span={2}
                              style={{ textAlign: "right", paddingTop: "10px" }}
                            >
                              <FontAwesomeIcon
                                key={i}
                                icon={faTrashAlt}
                                style={{
                                  color: "#F5222D",
                                  fontSize: 18,
                                  cursor: "pointer",
                                }}
                                onClick={() => {
                                  let copy = questions;
                                  let filteredAns = questions[
                                    index
                                  ]?.answers.filter((_, j) => j !== i);
                                  copy[index].answers = filteredAns;
                                  setQuestions(copy);
                                  setEditQuestion({
                                    edit: true,
                                    key: index,
                                  });
                                }}
                              />
                            </Col>
                          )}
                        </>
                      ))}
                      {/* update button */}
                      {retrievedState?.edit && (
                        <Col span={18}>
                          <Form.Item>
                            <Button
                              onClick={() => {
                                const key = "update";
                                updateQuestion(key);
                                closeFields
                                  ? setCloseFields(!closeFields)
                                  : null;
                                setEditQuestion({
                                  key: "",
                                });
                              }}
                              disabled={disableSave}
                              style={styles.updateQuestion}
                            >
                              <FontAwesomeIcon icon={faCheck} /> &nbsp; Update
                            </Button>
                          </Form.Item>
                        </Col>
                      )}
                    </Row>
                  </Col>
                )}
              </Col>
              <Col span={24}>
                {!retrievedState?.edit && (
                  <Form.List name={`answers${index}`}>
                    {(fields, { add, remove }, { errors }) => {
                      return (
                        <div>
                          {fields.map((field, i) => (
                            <Form.Item
                              {...formItemLayoutWithOutLabel}
                              label={`Option ${i + 1}`}
                              required={true}
                              key={field.key}
                              onChange={(e) => validateFields(e)}
                            >
                              <Row>
                                <Col span={18}>
                                  <Form.Item
                                    {...field}
                                    rules={[
                                      {
                                        required: true,
                                        whitespace: true,
                                        message: OPTION_ERROR,
                                      },
                                    ]}
                                  >
                                    <Input
                                      placeholder="Enter Option "
                                      size="large"
                                    />
                                  </Form.Item>
                                </Col>
                                <Col span={6}>
                                  {fields.length > 1 ? (
                                    <MinusCircleOutlined
                                      style={styles.minusCircle}
                                      className="dynamic-delete-button"
                                      onClick={() => remove(field.name)}
                                    />
                                  ) : null}
                                </Col>
                              </Row>
                            </Form.Item>
                          ))}
                          <br />
                          {editQuestion.edit == true &&
                          editQuestion.key === index ? null : (
                            <Form.Item>
                              <Button
                                type="dashed"
                                onClick={() => {
                                  add();
                                  !closeFields
                                    ? setCloseFields(!closeFields)
                                    : null;
                                  setDisableSave(!disableSave);
                                }}
                                style={styles.AddOption}
                              >
                                <FontAwesomeIcon icon={faPlus} /> &nbsp;
                                {ADD_OPTION}
                              </Button>
                              {fields.length > 0 &&
                                !closeFields &&
                                fields?.map((field) => remove(field.name))}
                            </Form.Item>
                          )}
                        </div>
                      );
                    }}
                  </Form.List>
                )}
              </Col>
              <Col span={8}>
                <Button
                  style={styles.DeleteQuestion}
                  onClick={() => {
                    let key = undefined;

                    updateQuestion(key, index);
                    setDisableSave(disableSave);
                  }}
                >
                  <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; {DELETE_QUESTION}
                </Button>
              </Col>
            </Row>
          ))}
          <Col offset={18} span={6}>
            <Form.Item>
              <Popover title={content} visible={disableSave}>
                <Button
                  htmlType="submit"
                  disabled={disableSave}
                  style={styles.saveBtn}
                >
                  <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                </Button>
              </Popover>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default BackgroundForm;
