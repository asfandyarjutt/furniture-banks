import React, { useEffect } from "react";
import { Row, Col, Input, Typography, Form, Button, message } from "antd";
import { useCartValue } from "../../lib/state";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useCurrentUser } from "../../lib/hooks";
import { Not_Editable_Txt, HOUSEHOLD_INFORMATION } from "../../constants";
import { getLocal, setLocal } from "./functions";

const { Text, Title } = Typography;

function HouseHoldForm({ onEdit, onSaved, handleUpdateHouse }) {
  const [form] = Form.useForm();
  const [{ household }, dispatch] = useCartValue();
  const [user] = useCurrentUser();

  useEffect(() => {
    if (user?.furnitureBank?.purchaselimits) {
      dispatch({
        type: "SET_LIMITS",
        payload: user.furnitureBank.purchaselimits,
      });
    }
  }, [user]);

  useEffect(() => {
    if (household) {
      form.setFieldsValue(household);
    }
  }, [form]);
  let values;
  let householdvalues;
  values = getLocal("State");
  householdvalues = values?.household;
  useEffect(() => { });
  if (values) {
    var size = Object.keys(householdvalues).length;
  }
  useEffect(() => {
    if (householdvalues && size) {
      if (householdvalues == 0) {
        form.setFieldsValue({
          adultMales: 0,
          adultFemales: 0,
          adultBeds: 0,
          childBeds: 0,
          minors: 0,
        });
      } else {
        form.setFieldsValue({
          adultMales: values?.household?.adultMales,
          adultFemales: values?.household?.adultFemales,
          adultBeds: values?.household?.adultBeds,
          childBeds: values?.household?.childBeds,
          minors: values?.household?.minors,
        });
      }
    }
  }, [form, size]);

  function finish(values) {
    let adultMales = Number(values.adultMales)
    let adultFemales = Number(values.adultFemales)
    let minors = Number(values.minors)
    let sum = adultMales + adultFemales + minors
    if (sum === 0) {
      message.warning("Please Enter a Valid Input")
      handleUpdateHouse(true)
    }
    else {
      dispatch({ type: "HOUSEHOLD", payload: values })
      handleUpdateHouse(false)
      message.success("Information Saved");
    }
  }
  const defValue = 0;

  return (
    <div>
      {onEdit && (
        <p>
          <Text style={{ color: "red" }} type="secondary">
            {Not_Editable_Txt}
          </Text>
        </p>
      )}
      <Title level={4}>{HOUSEHOLD_INFORMATION}</Title>
      <Form
        form={form}
        layout="horizontal"
        onFinish={finish}
        initialValues={{
          adultMales: "0",
          adultBeds: "0",
          adultFemales: "0",
          childBeds: "0",
          minors: "0",
        }}
      >
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item
              name="adultMales"
              label="Adult Males (18 and over)"
              rules={[{ required: true, message: "Required Field" }]}
            >
              <Input size="large" min={0} type="number" disabled={onEdit} />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="adultBeds"
              label="Adult Beds Needed"
              rules={[{ required: true, message: "Required Field" }]}
            >
              <Input size="large" min={0} type="number" disabled={onEdit} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item
              name="adultFemales"
              label="Adult Females (18 and over)"
              rules={[{ required: true, message: "Required Field" }]}
            >
              <Input size="large" min={0} type="number" disabled={onEdit} />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="childBeds"
              label="Child Beds Needed"
              rules={[{ required: true, message: "Required Field" }]}
            >
              <Input size="large" min={0} type="number" disabled={onEdit} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Form.Item
              name="minors"
              label="Minors (17 and under)"
              rules={[{ required: true, message: "Required Field" }]}
            >
              <Input size="large" min={0} type="number" disabled={onEdit} />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form.Item>
              <Button
                onClick={() => onSaved(form.getFieldsValue({}))}
                block
                size="large"
                htmlType="submit"
                style={{
                  backgroundColor: "#1890ff",
                  color: "white",
                  border: "1px solid #1890ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: onEdit ? "none" : "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
              </Button>
            </Form.Item>
          </Col>
        </Row>
        {!onEdit ||
          (undefined && (
            <Col offset={12} span={12}>
              <Form.Item>
                <Button
                  onClick={() => onSaved(form.getFieldsValue({}))}
                  htmlType="submit"
                  style={{
                    backgroundColor: "#1890ff",
                    height: "40px",
                    color: "white",
                    border: "1px solid #1890ff",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                </Button>
              </Form.Item>
            </Col>
          ))}
      </Form>
    </div>
  );
}

export default HouseHoldForm;
