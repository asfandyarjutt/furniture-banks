import React, { useEffect, useState, useRef } from "react";
import {
  Row,
  Alert,
  Col,
  Input,
  Typography,
  Form,
  Button,
  message,
  Radio,
  Checkbox,
} from "antd";
import {
  BG_Info,
  ErrorMSg,
  CHECKBOX,
  SELECT_REASON,
  MS_STATUS,
  ANNUAL_HH,
  FALSE,
  INFO_SAVED,
  OTHER,
} from "../../constants";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useCurrentUser } from "../../lib/hooks";
import { useCartValue } from "../../lib/state";
import { getLocal, setLocal } from "./functions";

const { Text, Title } = Typography;
function BGForm({ onSaved }) {
  const [form] = Form.useForm();
  const [isEmpty, setIsEmpty] = useState(false);
  const [currentUser] = useCurrentUser();
  const [{ bg }, dispatch] = useCartValue();
  const [state, setState] = useState({});
  const [enableCheckBox, setCheckBox] = useState({});
  const [isChecked, setIsChecked] = useState({
    answers: "",
    questions: "",
    prevAns: "",
    key: "",
  });
  const inputEl = useRef(null);
  let enableSave = [];
  let contain_emepty_val;
  const [furniture, setFurniture] = useState([]);
  const [defaultFormValues, setDefaultFormValues] = useState(null);
  enableSave = Object.values(form.getFieldsValue({}))?.map((key) => {
    const length =
      typeof key !== "boolean" && !Array.isArray(key)
        ? key?.replace(/\s/g, "").length
        : null;
    if (key !== "" && key != null && length !== 0) {
      return true;
    } else return false;
  });
  function finish(values) {
    for (const key in values) {
      if (Array.isArray(values[key])) {
        values[key] = values[key].map((val, i) => {
          if (val?.includes("other-")) {
            val = val?.replace("other-", "");
            return val;
          } else {
            return val;
          }
        });
        const other = values[key].filter((val) => val.includes(OTHER));
        const rest = values[key].filter((val) => val !== OTHER);
        other.length > 0 && rest.push(other[0]);
        values[key] = rest;
        console.log("rest:", values[key]);
      }
    }
    dispatch({ type: "BG", payload: values });
    message.success(`${INFO_SAVED}`);
    setDefaultFormValues(form.getFieldsValue({}));
  }
  useEffect(() => {
    if (bg) {
      form.setFieldsValue({ ...bg });
    }
  }, [currentUser]);
  const handleOnBlur = (question) => {
    if (isChecked.answers) {
      const ans = isChecked.prevAns.filter((element) =>
        isChecked.answers.includes(element)
      );

      const others_arr = isChecked?.answers?.filter((val) =>
        val.includes(OTHER)
      );
      let property = isChecked.questions;
      const all_arr = isChecked?.answers?.filter((val) => !val.includes(OTHER));

      const otherValue = others_arr[others_arr.length - 1];
      all_arr.push(otherValue);
      ans.push(otherValue);
      let index = all_arr.indexOf(OTHER);
      isChecked.key ? ans.filter((val) => !val.includes("other-")) : null;

      const formObj = {
        ...form.values,
        [`${isChecked.questions}`]: ans,
      };
      if (index) {
        formObj[property] = [...ans, OTHER];
      }
      form.setFieldsValue(formObj);
      setDefaultFormValues(form.getFieldsValue({}));
      console.log("Handlechange");
    }
  };
  async function othersChange(e, label, answer) {
    let prevValues;
    if (e.target.id !== "" && !e.target.value.includes("other-")) {
      prevValues = [
        ...form.getFieldsValue({})[`${label}`],
        `other-${e.target.value}`,
      ];
    } else if (e.target.id !== "") {
      prevValues = [
        ...form.getFieldsValue({})[`${label}`],
        `${e.target.value}`,
      ];
    }
    setIsChecked({
      answers: prevValues,
      questions: label,
      prevAns: answer,
      key: e.target.type === CHECKBOX && e.target.id && !e.target.checked,
    });
    if (e.target.type === "text" && e.target.value === "") {
      setIsEmpty(true);
    } else setIsEmpty(false);
    if (e.target.type === CHECKBOX) {
      setCheckBox({ ...enableCheckBox, [e.target.value]: e.target.checked });
      setState({
        ...state,
        [label]: e.target.value,
      });
    } else {
      setState({
        ...state,
        [label]: e.target.value,
      });
    }
  }
  const enable = Object.keys(enableCheckBox)?.map((key) => {
    if (enableCheckBox[key] !== FALSE) {
      return enableCheckBox[key];
    } else {
      return enableCheckBox[key];
    }
  });

  if (enable.length > 0 && enable.includes(true)) {
    contain_emepty_val = true;
  } else if (enable.length > 0 && !enable.includes(true)) {
    contain_emepty_val = false;
  }
  useEffect(() => {
    setFurniture([...currentUser.furnitureBank.bg]);
    const dummy = {
      ANNUAL_HH: undefined,
      "Family Status": undefined,
      Gender: undefined,
      MS_STATUS: undefined,
      Race: undefined,
      SELECT_REASON: undefined,
    };

    let values;
    let localBg;
    values = getLocal("State");
    localBg = values?.bg;
    let defaultFormValues = {};
    currentUser.furnitureBank.bg.map((obj) => {
      defaultFormValues[obj.question] = obj.answers[0];
    });
    if (localBg && Object.keys(localBg).length === 0) {
      setDefaultFormValues(defaultFormValues);
    } else {
      setDefaultFormValues(localBg);
    }
  }, []);
  if (!defaultFormValues) return <div>loading...</div>;
  const isEnable =
    isEmpty ||
    !enableSave?.every(Boolean) ||
    (contain_emepty_val !== undefined && !contain_emepty_val);

  return (
    <div>
      <Row style={{ marginBottom: "2em" }}>
        <Title level={4}>{BG_Info}</Title>
      </Row>
      <Form
        form={form}
        layout="horizontal"
        onFinish={finish}
        initialValues={defaultFormValues}
      >
        <Row gutter={[16, 16]} align="middle" style={{ marginBottom: "2em" }}>
          {currentUser?.furnitureBank?.bg?.map(
            ({ question, answers, type, idx }, id) => {
              let textfieldValues;
              if (Array.isArray(defaultFormValues[question])) {
                const result = defaultFormValues[question].filter(
                  (element) => !answers.includes(element)
                );
                textfieldValues = result[0];
              } else {
                const result = answers.includes(defaultFormValues[question]);
                if (!result) {
                  textfieldValues = defaultFormValues[question];
                } else {
                  textfieldValues = "";
                }
              }

              return (
                <Col span={18} offset={3}>
                  <p key={idx}>
                    <b>{question}</b>
                  </p>
                  {type === 1 ? (
                    <Form.Item name={question}>
                      <Radio.Group
                        style={{ display: "block" }}
                        onChange={(e) => {
                          othersChange(e, question);
                        }}
                      >
                        <Row>
                          {answers.map((answer, i) => {
                            return (
                              <Col span={8} key={i}>
                                <Radio key={i} value={answer}>
                                  {answer}
                                </Radio>
                              </Col>
                            );
                          })}

                          <Row span={8}>
                            <Radio
                              key={3}
                              value={
                                !answers.includes(state[`${question}`]) &&
                                !state[`${question}`]
                                  ? textfieldValues
                                  : !answers.includes(state[`${question}`])
                                  ? state[`${question}`]
                                  : textfieldValues
                              }
                            >
                              Other &nbsp;
                            </Radio>
                            {!answers.includes(state[`${question}`]) && (
                              <Col>
                                <Input
                                  key={id}
                                  defaultValue={textfieldValues}
                                  value={state[`${question}`]}
                                  onChange={(e, i) => {
                                    form.setFieldsValue({
                                      ...form.values,
                                      [`${question}`]: e.target.value,
                                    });
                                    setState({
                                      ...state,
                                      [`${question}`]: e.target.value,
                                    });
                                  }}
                                />
                              </Col>
                            )}
                          </Row>
                        </Row>
                      </Radio.Group>
                    </Form.Item>
                  ) : (
                    <Form.Item
                      name={question}
                      onChange={(e) => {
                        othersChange(e, question, answers);
                      }}
                    >
                      <Checkbox.Group>
                        <Row gutter={[16, 16]}>
                          {answers?.map((answer, i) => (
                            <Col span={8} key={i}>
                              <Checkbox key={i} checked="" value={answer}>
                                {answer}
                              </Checkbox>
                            </Col>
                          ))}
                          <Col span={8}>
                            <Checkbox
                              id={question}
                              name={question}
                              value={textfieldValues ? textfieldValues : OTHER}
                            >
                              Other &nbsp;
                              <Input
                                onBlur={handleOnBlur}
                                id={question}
                                defaultValue={textfieldValues}
                                value={
                                  !!answers.includes(state[`${question}`])
                                    ? textfieldValues
                                    : state[`${question}`]
                                }
                              />
                            </Checkbox>
                          </Col>
                        </Row>
                      </Checkbox.Group>
                    </Form.Item>
                  )}
                </Col>
              );
            }
          )}
          <Col span={24}>
            <Form.Item style={{ textAlign: "center" }}>
              {!enableSave.every(Boolean) && (
                <Col>
                  <Alert
                    message="Please Fill the text Filled"
                    type="error"
                    showIcon
                    style={{
                      marginLeft: "2em",
                      width: "96%",
                      marginBottom: "2em",
                      display: "flex",
                    }}
                  />
                </Col>
              )}
              <Button
                block
                onClick={async () => {
                  handleOnBlur();
                  onSaved(form.getFieldsValue({}));
                }}
                size="large"
                htmlType="submit"
                disabled={isEnable}
                style={{
                  backgroundColor: "#1890ff",
                  color: "white",
                  border: "1px solid #1890ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default BGForm;
