import React, { useEffect, useState } from "react";
import { Card, Form, Input, Typography, Col, Row, Button, message } from "antd";
import { faCheck, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useInput from "../../lib/useInput";
import {
  EMAIL_CONFORMATION_ORDER,
  EMAIL_CONFORMATION_CM,
  EMAIL_CONFORMATION_PA,
} from "../../constants";
import { useRouter } from "next/router";

const { Title } = Typography;
const { TextArea } = Input;

function DetailEmail({ emailTexts, emailSubject }) {
  const [form] = Form.useForm();
  const router = useRouter();
  const [clientText, setClientText] = useInput("");
  const [caseText, setCaseText] = useInput("");
  const [paText, setPaText] = useInput("");
  const [paSubject, setPaSubject] = useState("");
  const [caseSubject, setCaseSubject] = useState("");
  const [clientSubject, setClientSubject] = useState("");
  useEffect(() => {
    if (emailTexts) {
      setClientText(emailTexts.client);
      setCaseText(emailTexts.caseManager);
      setPaText(emailTexts?.partnerAgency);
    }
  }, [emailTexts]);
  useEffect(() => {
    if (emailSubject) {
      setPaSubject(emailSubject?.paSubject);
      setCaseSubject(emailSubject?.caseSubject);
      setClientSubject(emailSubject?.clientSubject);
    }
    form.setFieldsValue({
      clientSubject: emailSubject?.clientSubject,
      caseWorkerSubject: emailSubject?.caseSubject,
      paSubject: emailSubject?.paSubject,
    });
  }, [emailSubject]);

  const hanadleChange = (e) => {
    if (e.target.id === "clientSubject") {
      setClientSubject(e.target.value);
    } else if (e.target.id === "caseWorkerSubject") {
      setCaseSubject(e.target.value);
    } else if (e.target.id === "paSubject") {
      setPaSubject(e.target.value);
    }
  };
  async function updateTexts() {
    const data = {
      client: clientText.value,
      caseManager: caseText.value,
      partnerAgency: paText.value,
      paSubject: paSubject,
      caseSubject: caseSubject,
      clientSubject: clientSubject,
    };

    await fetch("/api/emails", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    message.success("E-mail Information Saved");
    router.push("/forms");
  }

  return (
    <>
      <Button
        style={{
          backgroundColor: "white",
          height: "40px",
          color: "#69c0ff",
          border: "1px solid #69c0ff",
          borderRadius: "3px",
          borderwidth: "1px",
          display: "inline-block",
          textAlign: "center",
          paddingLeft: "20px",
          paddingRight: "20px",
        }}
        onClick={() => router.back()}
      >
        <FontAwesomeIcon icon={faChevronLeft} /> &nbsp; Back
      </Button>
      <Card style={{ marginTop: "2em" }}>
        <Title level={3}>E-mail Confirmation</Title>
        <Row gutter={[16, 16]}>
          <Col span={24}>
            <Title level={5}>{EMAIL_CONFORMATION_ORDER}</Title>
          </Col>
          <Col span={24}>
            <Form
              form={form}
              layout="vertical"
              initialValues={{
                clientSubject:
                  emailSubject != undefined
                    ? emailSubject.clientSubject
                    : clientSubject,
              }}
            >
              <Form.Item
                label="Subject For Client"
                name="clientSubject"
                rules={[
                  {
                    required: true,
                    message: "Please Type your Subject!",
                  },
                ]}
              >
                <Input onChange={hanadleChange} />
              </Form.Item>
            </Form>
          </Col>
          <Col span={24}>
            <TextArea
              defaultValue={emailTexts && emailTexts.client}
              rows="5"
              {...clientText}
            />
          </Col>
          <Col span={24}>
            <Title level={5}>{EMAIL_CONFORMATION_CM}</Title>
          </Col>
          <Col span={24}>
            <Form
              form={form}
              layout="vertical"
              initialValues={{
                caseWorkerSubject: emailSubject && emailSubject.caseSubject,
              }}
            >
              <Form.Item
                label="Subject For Case Worker"
                name="caseWorkerSubject"
                rules={[
                  {
                    required: true,
                    message: "Please Type your Subject!",
                  },
                ]}
              >
                <Input onChange={hanadleChange} />
              </Form.Item>
            </Form>
          </Col>
          <Col span={24}>
            <TextArea
              defaultValue={emailTexts && emailTexts.caseManager}
              rows="5"
              {...caseText}
            />
          </Col>
          <Col span={24}>
            <Title level={5}>{EMAIL_CONFORMATION_PA}</Title>
          </Col>
          <Col span={24}>
            <Form
              form={form}
              layout="vertical"
              initialValues={{
                paSubject: emailSubject && emailSubject.paSubject,
              }}
            >
              <Form.Item
                label="Subject For Partner Agency"
                name="paSubject"
                rules={[
                  {
                    required: true,
                    message: "Please Type your Subject!",
                  },
                ]}
              >
                <Input onChange={hanadleChange} />
              </Form.Item>
            </Form>
          </Col>
          <Col span={24}>
            <TextArea
              defaultValue={emailTexts && emailTexts.partnerAgency}
              rows="5"
              {...paText}
            />
          </Col>
          <Col
            span={6}
            offset={18}
            style={{ display: "flex", justifyContent: "flex-end" }}
          >
            <Button
              onClick={updateTexts}
              style={{
                backgroundColor: "#1890ff",
                height: "40px",
                color: "white",
                border: "1px solid #1890ff",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
            >
              <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
            </Button>
          </Col>
        </Row>
      </Card>
    </>
  );
}

export default DetailEmail;
