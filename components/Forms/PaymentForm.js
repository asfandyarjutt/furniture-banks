import React, { useEffect } from "react";
import { Typography, Input, Row, Col, Button, message } from "antd";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useInput from "../../lib/useInput";
import { useCurrentUser } from "../../lib/hooks";
import { formatPayment } from "../../lib/helper";
import { paymentForm } from "../../constants";

const { EDIT_COST, PICK_UP, CURBSIDE_DELIVERY, INSTALLATION_DELIVERY } =
  paymentForm;

function PaymentForm() {
  const [selfHaul, setSelf] = useInput("");
  const [curbsideDelivery, setCurb] = useInput("");
  const [installationDelivery, setInstall] = useInput("");
  const [currentUser] = useCurrentUser();
  async function savePrices() {
    const data = {
      selfHaul: selfHaul.value,
      curbsideDelivery: curbsideDelivery.value,
      installationDelivery: installationDelivery.value,
    };

    await fetch("/api/prices", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ data }),
    });

    message.success("Payment Information Saved");
  }

  useEffect(() => {
    if (currentUser.furnitureBank.prices) {
      const { self, curbside, installation } =
        currentUser?.furnitureBank?.prices;
      setSelf(formatPayment(self));
      setCurb(formatPayment(curbside));
      setInstall(formatPayment(installation));
    }
  }, []);

  return (
    <div>
      <Row style={{ marginBottom: "2em" }}>
        <Typography.Text>{EDIT_COST}</Typography.Text>
      </Row>
      <Row gutter={16} align="middle">
        <Col span={6}>
          <Input placeholder="200" {...selfHaul} />
        </Col>
        <Col span={8}>
          <Typography.Text>{PICK_UP}</Typography.Text>
        </Col>
      </Row>
      <br />
      <Row gutter={16} align="middle">
        <Col span={6}>
          <Input placeholder="200" {...curbsideDelivery} />
        </Col>
        <Col span={8}>
          <Typography.Text>{CURBSIDE_DELIVERY}</Typography.Text>
        </Col>
      </Row>
      <br />
      <Row gutter={16} align="middle">
        <Col span={6}>
          <Input placeholder="200" {...installationDelivery} />
        </Col>
        <Col span={8}>
          <Typography.Text>{INSTALLATION_DELIVERY}</Typography.Text>
        </Col>
        <br />
        <Col span={6} offset={18}>
          <Button
            onClick={savePrices}
            style={{
              backgroundColor: "#1890ff",
              height: "40px",
              color: "white",
              border: "1px solid #1890ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
          </Button>
        </Col>
      </Row>
    </div>
  );
}

export default PaymentForm;
