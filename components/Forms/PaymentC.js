import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Input,
  Typography,
  Form,
  Button,
  message,
  Radio,
  Alert,
  Modal,
} from "antd";
import {
  Deliver_Info,
  reciver_furniture,
  not_applicable_txt,
  payment_change,
  curbs_deli,
  instal_deli,
} from "../../constants";
import { useCurrentUser } from "../../lib/hooks";
import { useCartValue } from "../../lib/state";
import Dinero from "dinero.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faDollarSign } from "@fortawesome/free-solid-svg-icons";
import {
  curbsideText,
  ErrorRequired,
  InformationSaved,
  installText,
  PaymentWarn,
  PaymentWarning,
  selfhaulText,
} from "./constants";
import { getLocal } from "./functions";

const { Text, Title } = Typography;

function PaymentC({ onSaved, disableRadio, handleUpdatePayment }) {
  const [form] = Form.useForm();
  const [currentUser] = useCurrentUser();
  const [{ payment, deliver }, dispatch] = useCartValue();
  const [self, setSelf] = useState("");
  const [install, setInstall] = useState("");
  const [curb, setCurb] = useState("");
  const [clientpayment, setClientPayment] = useState("Cash");
  const [paymenttype, Setpaymenttype] = useState("0");
  const [deliverypayment, setDeliverypayment] = useState();
  const [agentpayment, setAgentayment] = useState();
  const [type, setType] = useState("");
  const [modal, setModal] = useState(false);
  const [Enable, setEnable] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [naDisbaled, setnaDisabld] = useState(false);
  const [cashDisabled, setCashDisabled] = useState(false);
  let pickUp = disableRadio === 1;
  let delivery = disableRadio === 2;
  let values;
  values = getLocal("State");

  let selfHaul = self + "" + selfhaulText;
  let curbside = curb + "" + curbsideText;
  let installtion = install + "" + installText;

  const numberExtraction = deliverypayment && deliverypayment.match(/(\d+)/);
  const numberConvert = numberExtraction && numberExtraction[0];
  useEffect(() => {
    form.setFieldsValue({
      paymentType: deliverypayment,
    });
  });
  useEffect(() => {
    if (values.payment.agencyPayment == 0) {
      setDisabled(true);
      setnaDisabld(true);
    }

    if (Number(values.payment.agencyPayment) === Number(numberConvert)) {
      setCashDisabled(true);
      setnaDisabld(false);
    } else {
      setCashDisabled(false);
      setnaDisabld(true);
    }
  }, [numberConvert]);

  useEffect(() => {
    if (paymenttype === "0" && agentpayment === "0") {
      setEnable(true);
    } else {
      setEnable(false);
    }
  }, [agentpayment, paymenttype]);

  function handleCancel() {
    setModal(false);
  }
  useEffect(() => {
    Object.keys(values?.payment).length > 0
      ? disableRadio === 1
        ? setDeliverypayment(selfHaul)
        : setDeliverypayment(curbside)
      : setDeliverypayment(disableRadio === 1 ? selfHaul : curbside);
    Object.keys(values?.payment).length > 0
      ? setAgentayment(`runned.${values.payment.agencyPayment}`)
      : setAgentayment("0");
    Object.keys(values?.payment).length > 0
      ? Setpaymenttype(`eeee.${values.payment.clientPayment}`)
      : Setpaymenttype("0");
  }, [self]);

  function finish(values) {
    const agencyPayment = Number(values && values.agencyPayment);
    const client = Number(values && values.clientPayment);
    const total = agencyPayment + client;
    if (total < numberConvert) {
      message.warning(PaymentWarning);
      handleUpdatePayment(true);
    } else {
      dispatch({
        type: "PAYMENT",
        payload: values,
        disableRadio: disableRadio,
      });
      handleUpdatePayment(false);
      message.success(InformationSaved);
    }
  }
  function onBlur() {
    const numberPayment = Number(paymenttype);
    const numberagency = Number(agentpayment);
    let client = "client";
    if (type === client) {
      const agencyamount = numberConvert - numberPayment;
      if (agencyamount === 0) {
        form.setFieldsValue({
          ["agencyPaymentType"]: "invoice",
        });

        Setpaymenttype("invoice");
        setDisabled(true);
      } else {
        setDisabled(false);
      }
      if (numberPayment != 0) {
        form.setFieldsValue({
          ["clientPaymentType"]: "Cash",
        });
        setClientPayment("Cash");
        setCashDisabled(false);
        setnaDisabld(true);
      } else {
        setnaDisabld(false);
        setCashDisabled(true);
        form.setFieldsValue({
          ["clientPaymentType"]: "N/A",
        });
        setClientPayment("N/A");
      }

      if (numberPayment > numberConvert) {
        form.setFieldsValue({
          ["agencyPayment"]: "",
        });
        message.warning(PaymentWarn);
      } else {
        setModal(true);
        form.setFieldsValue({
          ["agencyPayment"]: agencyamount.toString(),
        });
        setAgentayment(agencyamount.toString());
      }
    } else {
      const agencyamount = numberConvert - numberagency;
      if (agencyamount == numberConvert) {
        form.setFieldsValue({
          ["agencyPaymentType"]: "invoice",
        });

        Setpaymenttype("invoice");
        setDisabled(true);
      } else {
        setDisabled(false);
      }
      if (numberagency == numberConvert) {
        form.setFieldsValue({
          ["clientPaymentType"]: "N/A",
        });
        setClientPayment("N/A");
        setCashDisabled(true);
        setnaDisabld(false);
      } else {
        setCashDisabled(false);
        setnaDisabld(true);
        form.setFieldsValue({
          ["clientPaymentType"]: "Cash",
        });
        setClientPayment("Cash");
      }
      if (numberagency > numberConvert) {
        form.setFieldsValue({
          ["clientPayment"]: "",
        });
      } else {
        setModal(true);
        Setpaymenttype(agencyamount.toString());
        form.setFieldsValue({
          ["clientPayment"]: agencyamount.toString(),
        });
      }
    }
  }

  function onFinishFailed() {
    message.warning(ErrorRequired);
  }

  useEffect(() => {
    form.setFieldsValue({
      ["paymentType"]:
        Object.keys(values?.payment).length > 0
          ? values?.payment.paymentType
          : selfHaul,
      ["clientPayment"]:
        Object.keys(values?.payment).length > 0 &&
        values.disableRadio === disableRadio
          ? values?.payment.clientPayment
          : "0",
      ["agencyPayment"]:
        Object.keys(values?.payment).length > 0 &&
        values.disableRadio === disableRadio
          ? values?.payment.agencyPayment
          : "0",
      ["clientPaymentType"]:
        Object.keys(values?.payment).length > 0
          ? values?.payment.clientPaymentType
          : "Cash",
      ["agencyPaymentType"]:
        Object.keys(values?.payment).length > 0
          ? values?.payment.agencyPaymentType
          : "heartland",
    });
  }, [self]);
  useEffect(() => {
    if (currentUser.furnitureBank.prices) {
      const { self, curbside, installation } =
        currentUser?.furnitureBank?.prices;
      setSelf(Dinero({ amount: Number(self + "00") }).toFormat("$0,0.00"));
      setCurb(Dinero({ amount: Number(curbside + "00") }).toFormat("$0,0.00"));
      setInstall(
        Dinero({ amount: Number(installation + "00") }).toFormat("$0,0.00")
      );
    }
  }, []);

  function onKeyDown(evt) {
    if (evt.key === "e" || evt.key === "+" || evt.key === "-") {
      evt.preventDefault();
    }
  }
  return (
    <Form
      form={form}
      onFinish={finish}
      onFinishFailed={onFinishFailed}
      layout="vertical"
    >
      <Col span={24}>
        <Title level={4}>{Deliver_Info}</Title>
      </Col>
      <Row>
        <Col span={24}>
          <p style={{ color: "red" }}>
            <Text>{reciver_furniture}</Text>
          </p>
        </Col>
        <Form.Item name="paymentType">
          <Radio.Group
            defaultValue={disableRadio === 1 ? selfHaul : curbside}
            value={deliverypayment}
            onChange={(e) => {
              setDeliverypayment(e.target.value);
            }}
          >
            <Radio disabled={!pickUp} value={selfHaul}>
              {self} Pick Up
            </Radio>
            <br />
            <Radio value={curbside} disabled={!delivery}>
              {curb}
              {curbs_deli}
            </Radio>
            <br />
            <Radio value={installtion} disabled={!delivery}>
              {install} {instal_deli}
            </Radio>
          </Radio.Group>
        </Form.Item>
        <Col span={24}></Col>
        <Col span={8}>
          <Form.Item
            label="Client Payment:"
            name="clientPayment"
            rules={[{ required: true, message: "Field required." }]}
          >
            <Input
              addonBefore={<FontAwesomeIcon icon={faDollarSign} />}
              placeholder="200"
              defaultValue="heartland"
              type="number"
              onFocus={() => setType("client")}
              onBlur={onBlur}
              value={paymenttype}
              onKeyDown={(evt) => {
                onKeyDown(evt);
              }}
              onChange={(e) => Setpaymenttype(e.target.value)}
            />
          </Form.Item>
          <Form.Item name="clientPaymentType" finish>
            <Radio.Group
              defaultValue="Cash"
              value={clientpayment}
              onChange={(e) => setClientPayment(e.target.value)}
            >
              <Radio disabled={cashDisabled} value="Cash">
                Cash
              </Radio>
              <br />
              <Radio disabled={cashDisabled} value="Money order">
                Money Order
              </Radio>
              <br />
              <Radio disabled={naDisbaled} value="N/A">
                {not_applicable_txt}
              </Radio>
            </Radio.Group>
          </Form.Item>
        </Col>
        <Col span={8} offset={6}>
          <Alert
            type="warning"
            message="Payment"
            description="Payment Amount Should be Equal to Service Cost."
            showIcon
          />
        </Col>
        <Col span={24}></Col>

        <Col span={8}>
          <Form.Item
            label="Agency Payment:"
            name="agencyPayment"
            rules={[{ required: true, message: "Field required." }]}
          >
            <Input
              addonBefore={<FontAwesomeIcon icon={faDollarSign} />}
              placeholder="200"
              defaultValue="heartland"
              type="number"
              onFocus={() => setType("agency")}
              onBlur={onBlur}
              value={agentpayment}
              onKeyDown={(evt) => {
                onKeyDown(evt);
              }}
              onChange={(e) => {
                setAgentayment(e.target.value);
              }}
            />
          </Form.Item>
          <Form.Item
            name="agencyPaymentType"
            rules={[{ required: true, message: "Field required." }]}
          >
            <Radio.Group
              defaultValue="heartland"
              value={paymenttype}
              onChange={(e) => Setpaymenttype(e.target.value)}
            >
              <Radio disabled={disabled} value="heartland">
                Credit Card
              </Radio>
              <br />
              <Radio value="invoice">Invoice</Radio>
              <br />
              <Radio value="Prepay">Prepay</Radio>
            </Radio.Group>
          </Form.Item>
        </Col>

        <Col span={24}>
          <Form.Item>
            <Button
              onClick={() => onSaved(form.getFieldsValue({}))}
              disabled={Enable}
              block
              size="large"
              htmlType="submit"
              style={{
                backgroundColor: "#1890ff",
                color: "white",
                border: "1px solid #1890ff",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
            >
              <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
            </Button>
          </Form.Item>
        </Col>
      </Row>
      <Modal
        title="Payment"
        centered
        visible={modal}
        onCancel={handleCancel}
        onOk={handleCancel}
      >
        <p>{payment_change}</p>
      </Modal>
    </Form>
  );
}

export default PaymentC;
