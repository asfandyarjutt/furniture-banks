import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Input,
  Typography,
  Form,
  Button,
  message,
  Radio,
  Modal,
} from "antd";
import { faCheck, fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import {
  emailRegx,
  phoneRegx,
  zipRegx,
  ClientContactInfo,
  Not_Editable_Txt,
  CM_Info,
  SELECT_PICK_UP,
  RequiredZip,
  Validzip,
  Role,
  ZIP_CODE,
  INFO_SAVED,
} from "../../constants";
import { useCurrentUser, useTimes } from "../../lib/hooks";
import { useCartValue } from "../../lib/state";
import { getLocal, setLocal } from "./functions";

const { Text, Title } = Typography;
const radioStyle = {
  display: "block",
  height: "30px",
  lineHeight: "30px",
};
function ContactForm({ onEdit, onSaved, updateState, enableModal, options }) {
  const RadioGroup = Radio.Group;
  const [modal, setModal] = useState(true);
  const [changeModalTxt, setModalTxt] = useState("");
  const [selectOption, setSelectOption] = useState("");
  const [enableOk, setEnableOk] = useState(true);
  const [times] = useTimes();
  const [form] = Form.useForm();
  const [currentUser] = useCurrentUser();
  const [{ contact }, dispatch] = useCartValue();

  let values;
  useEffect(() => {
    values = getLocal("State");
  });
  const [fstepValues, setFstepsValues] = useState({});
  const router = useRouter();
  let passArray = [];
  function finish(values) {
    let good = false;
    if (selectOption !== 1) {
      times?.delivery.forEach(({ zip }) => {
        if (zip.includes(values.zip)) good = true;
      });
    } else {
      good = true;
    }
    if (good) {
      dispatch({ type: "REP_CONTACT", payload: values });
      message.success(`${INFO_SAVED}`);
      dispatch({ type: "SET_DELIVER" });
      updateState(false);
    } else {
      updateState(true);
      dispatch({ type: "REP_CONTACT", payload: values });
      message.success(`${INFO_SAVED}`);
      selectOption !== 1 && message.warning(`${ZIP_CODE}`);
      setModal(true);
      setModalTxt(SELECT_PICK_UP);
      dispatch({ type: "CANCEL_DELIVER" });
    }
  }

  const handleCancel = () => {
    router.push("/dashboard");
  };
  const handleOK = () => {
    setModal(false);
  };
  const handleChange = ({ target: { value, name } }) => {
    setFstepsValues({
      ...fstepValues,
      [name]: value,
    });
  };

  const validateNext = (formValues) => {
    const myObj = form.getFieldsValue({});
    passArray = Object.keys(myObj).map((value) => {
      if (myObj[value] == undefined && value !== "apt") {
        return false;
      } else if (value === "clientEmail" && !emailRegx.test(myObj[value])) {
        return false;
      } else if (value === "clientPhone" && !phoneRegx.test(myObj[value])) {
        return false;
      } else if (
        value === "zip" &&
        selectOption !== 1 &&
        !zipRegx.test(myObj[value])
      ) {
        return false;
      } else if (value === "apt") {
        return true;
      } else return true;
    });
  };
  validateNext(fstepValues);

  useEffect(() => {
    if (currentUser) {
      form.setFieldsValue({
        cmName: `${currentUser.firstName} ${currentUser.lastName}`,
        cmEmail: currentUser.email,
        cmPhone: currentUser.phone,
        clientName: values?.contact?.clientName,
        address: values?.contact?.address,
        city: values?.contact?.city,
        clientEmail: values?.contact?.clientEmail,
        clientPhone: values?.contact?.clientPhone,
        state: values?.contact?.state,
        zip: values?.contact?.zip,
      });
    }
    if (contact) {
      form.setFieldsValue({ ...contact });
    }
  }, [currentUser, form]);
  const deliveryModal = (
    <Modal
      title={changeModalTxt !== "" ? changeModalTxt : "Select Option"}
      centered
      visible={modal}
      onCancel={handleCancel}
      onOk={handleOK}
      okButtonProps={{ disabled: enableOk }}
    >
      <RadioGroup
        onChange={(e) => {
          setEnableOk(false);
          setSelectOption(e.target.value);
          options(e.target.value);
        }}
      >
        <Radio style={radioStyle} value={1}>
          Pick Up
        </Radio>

        {changeModalTxt === "" && (
          <Radio style={radioStyle} value={2}>
            Delivery
          </Radio>
        )}
      </RadioGroup>
    </Modal>
  );
  return (
    <div>
      {onEdit && (
        <p>
          <Text style={{ color: "red" }} type="secondary">
            {Not_Editable_Txt}
          </Text>
        </p>
      )}
      <Title level={4}>{ClientContactInfo}</Title>
      <div>
        <Form
          form={form}
          onChange={handleChange}
          layout="horizontal"
          onFinish={finish}
        >
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Form.Item
                label="Name:"
                name="clientName"
                rules={[{ required: true, message: "Name is required." }]}
              >
                <Input
                  onChange={handleChange}
                  size="large"
                  disabled={onEdit}
                  name="clientName"
                  placeholder="First and Last"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={16}>
              <Form.Item
                label="Address:"
                name="address"
                rules={[{ required: true, message: "Address is required." }]}
              >
                <Input
                  size="large"
                  onChange={handleChange}
                  name="address"
                  disabled={onEdit}
                  placeholder="Number and Street Name"
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="Apt:" name="apt">
                <Input
                  onChange={handleChange}
                  name="apt"
                  size="large"
                  disabled={onEdit}
                  placeholder="Appartment, Unit, Etc."
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={10}>
              <Form.Item
                label="City:"
                name="city"
                rules={[{ required: true, message: "City is required." }]}
              >
                <Input
                  size="large"
                  name="city"
                  onChange={handleChange}
                  disabled={onEdit}
                  placeholder="City"
                />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                label="State:"
                name="state"
                rules={[{ required: true, message: "State is required." }]}
              >
                <Input
                  size="large"
                  name="state"
                  onChange={handleChange}
                  disabled={onEdit}
                />
              </Form.Item>
            </Col>

            <Col span={8}>
              <Form.Item
                label="Zip Code:"
                name="zip"
                rules={[
                  { required: true, message: RequiredZip },
                  {
                    pattern: zipRegx,
                    message: Validzip,
                  },
                ]}
              >
                <Input
                  size="large"
                  name="zip"
                  maxLength="5"
                  onChange={handleChange}
                  disabled={onEdit}
                  placeholder="12345"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={12}>
              <Form.Item
                label="Phone:"
                name="clientPhone"
                rules={[
                  { required: true, message: "Phone is required." },
                  {
                    pattern: phoneRegx,
                    message: "please enter a valid phone number.",
                  },
                  {
                    min: 10,
                    message: "Value must be 10 digits ",
                  },
                ]}
              >
                <Input
                  onChange={handleChange}
                  name="clientPhone"
                  maxLength={10}
                  size="large"
                  disabled={onEdit}
                  placeholder="(123) 456-7890"
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="E-mail:"
                name="clientEmail"
                rules={[
                  {
                    pattern: emailRegx,
                    message: "Please enter a valid email",
                  },
                  { required: true, message: "E-mail is required." },
                ]}
              >
                <Input
                  onChange={handleChange}
                  name="clientEmail"
                  size="large"
                  disabled={onEdit}
                  placeholder="johnny@appleseed.com"
                />
              </Form.Item>
            </Col>
          </Row>
          <Title level={4}>&nbsp;&nbsp;{CM_Info}</Title>
          <Row gutter={[16, 16]}>
            <Col span={24}>
              <Form.Item
                label="Name:"
                name="cmName"
                rules={[{ required: true, message: "Name is required." }]}
              >
                <Input
                  size="large"
                  disabled={onEdit}
                  placeholder="First and Last"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col span={12}>
              <Form.Item
                label="Phone:"
                name="cmPhone"
                rules={[{ required: true, message: "Phone is required." }]}
              >
                <Input
                  size="large"
                  disabled={onEdit}
                  placeholder="(123) 456-7890"
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="E-mail:"
                name="cmEmail"
                rules={[{ required: true, message: "E-mail is required." }]}
              >
                <Input
                  size="large"
                  disabled={onEdit}
                  placeholder="johnny@appleseed.com"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item>
                <Button
                  onClick={() => onSaved(form.getFieldsValue({}))}
                  size="large"
                  disabled={!passArray.every(Boolean)}
                  htmlType="submit"
                  block
                  style={{
                    backgroundColor: "#1890ff",
                    color: "white",
                    border: "1px solid #1890ff",
                    borderRadius: "3px",
                    borderWidth: "1px",
                    display: onEdit ? "none" : null,
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                </Button>
              </Form.Item>
            </Col>
          </Row>
          {!onEdit ||
            (undefined && (
              <Col offset={12} span={24}>
                <Form.Item>
                  <Button
                    onClick={() => onSaved(form.getFieldsValue({}))}
                    htmlType="submit"
                    style={{
                      backgroundColor: "#1890ff",
                      height: "40px",
                      color: "white",
                      border: "1px solid #1890ff",
                      borderRadius: "3px",
                      borderWidth: "1px",
                      display: !onEdit ? "inline-block" : null,
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                  >
                    <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                  </Button>
                </Form.Item>
              </Col>
            ))}
        </Form>
      </div>
      {}
      {currentUser &&
        currentUser?.role !== Role.FBA &&
        enableModal !== 1 &&
        enableModal !== 2 &&
        deliveryModal}
      {currentUser &&
        currentUser?.role !== Role.FBA &&
        changeModalTxt !== "" &&
        deliveryModal}
    </div>
  );
}

export default ContactForm;
