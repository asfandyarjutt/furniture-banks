import React, { useState, useEffect } from "react";
import { Card, Row, Col, Button, Typography, Divider, Spin } from "antd";
import moment from "moment";
import { useOneAgency } from "../../lib/hooks";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import styles from "./styles";

const { Title } = Typography;

const CaseworkerInfo = ({ agencyId, caseworkerId }) => {
  const [agency] = useOneAgency(agencyId);
  const [caseworker, setCaseworker] = useState({});
  const [PAinfo, setPAInfo] = useState({});
  const [loading, setLoading] = useState(true);
  let day = moment(caseworker?.createdAt);
  let now = moment();
  useEffect(() => {
    if (agency) setLoading(false);
  }, [agency]);
  useEffect(() => {
    setPAInfo({ email: agency?.liaison?.email, name: agency?.name });
    let caseworker = agency?.caseManagers?.find(
      (caseworker) => caseworker._id === caseworkerId
    );
    setCaseworker(caseworker);
  }, [agency]);

  return (
    <Card style={styles.container}>
      <Spin spinning={loading} style={styles.loader}>
        {!loading && (
          <>
            <Row>
              <Col span={12}>
                {<Title>Caseworker Info</Title>}
                <p>
                  Added&nbsp;
                  {day.calendar(now, {
                    sameDay: "[Today] [at] hh:mm a",
                    nextDay: "[Tomorrow]",
                    nextWeek: "dddd [at] hh:mm a",
                    lastDay: "dddd [at] hh:mm a",
                    lastWeek: "dddd",
                    sameElse: "MMMM DD, YYYY",
                  })}
                </p>
              </Col>
              <Col span={6} offset={6} style={styles.closeButtonContainer}>
                <Link href={`/agencies/${agencyId}`}>
                  <Button
                    style={styles.closeButton}
                    shape="circle"
                    icon={<FontAwesomeIcon icon={faTimes} />}
                  />
                </Link>
              </Col>
            </Row>
            <Divider />
            <Row>
              <Col span={5} style={styles.dataCol}>
                <b>First Name :</b>
              </Col>
              <Col span={5}>
                <p>{caseworker?.firstName}</p>
              </Col>
            </Row>
            <Row>
              <Col span={5} style={styles.dataCol}>
                <b>Last Name :</b>
              </Col>
              <Col span={5}>
                <p>{caseworker?.lastName}</p>
              </Col>
            </Row>
            <Row>
              <Col span={5} style={styles.dataCol}>
                <b>Email :</b>
              </Col>
              <Col span={5}>
                <p>{caseworker?.email}</p>
              </Col>
            </Row>
            <Row>
              <Col span={5} style={styles.dataCol}>
                <b>Phone :</b>
              </Col>
              <Col span={5}>
                <p>{caseworker?.phone}</p>
              </Col>
            </Row>
            <Row>
              <Col span={5} style={styles.dataCol}>
                <b>Partner Agency Name :</b>
              </Col>
              <Col span={5}>
                <p>{PAinfo?.name}</p>
              </Col>
            </Row>
            <Row>
              <Col span={5} style={styles.dataCol}>
                <b>Partner Agency Email :</b>
              </Col>
              <Col span={5}>
                <p>{PAinfo?.email}</p>
              </Col>
            </Row>
          </>
        )}
      </Spin>
    </Card>
  );
};

export default CaseworkerInfo;
