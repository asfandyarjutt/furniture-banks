const styles = {
  container: { height: "100%" },
  loader: {
    display: "flex",
    justifyContent: "center",
    marginTop: 200,
  },
  closeButtonContainer: { display: "flex", justifyContent: "flex-end" },
  closeButton: { background: "#010101", color: "#ffffff" },
  dataCol: {
    display: "flex",
    paddingRight: "2em",
  },
};

export default styles;
