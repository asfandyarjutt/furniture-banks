import React, { useEffect, useState } from "react";
import { Layout, Menu, Badge } from "antd";
import { useCurrentUser, useHelpDeskTickets } from "../lib/hooks";
import Link from "next/link";
import { useRouter } from "next/router";
import { URL_HDTICKETS, HELP_DESK, Role, URL_AUTH } from "../constants";
import { useCartValue } from "../lib/state";

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const adminOptions = [
  ["Help Desk", "/helpdesk"],
  ["Furniture Banks", "/furniturebanks"],
  ["Partner Agencies", "/agencies"],
];
const fbaOptions = [
  ["Catalog", "/dashboard"],
  ["Purchase Limits", "/purchaselimits"],
  ["Orders", `/orders`],
  ["Invoices", `/invoice`],
  ["Calendar", "/calendar"],
  ["Partner Agencies", "/agencies"],
  ["Forms", "/forms"],
  ["Help Desk", "/createticket"],
];
const palOptions = [
  ["Catalog", "/dashboard"],
  ["Orders", "/orders"],
  ["Invoices", `/invoice`],
  ["Case Managers", "/casemanagers"],
  ["Help Desk", "/createticket"],
];
const pacmOptions = [
  ["Catalog", "/dashboard"],
  ["Orders", "/orders"],
  ["Invoices", `/invoice`],
  ["Help Desk", "/createticket"],
];
function LayoutComponent({ children, role }) {
  const [user, { mutate }] = useCurrentUser();
  const [tickets] = useHelpDeskTickets();
  const [{ final }, dispatch] = useCartValue();
  const [unreadState, setUnread] = useState({
    unreaded: [],
    unread: [],
    unread1: [],
    unread2: [],
  });
  useEffect(() => {
    setUnread({
      ...unreadState,
      unreaded: tickets?.filter((ticket) => ticket.readed === false),
      unread: tickets?.filter((ticket) => ticket && ticket.read === false),
      unread1: tickets?.filter((ticket) => ticket && ticket.read1 === false),
      unread2: tickets?.filter((ticket) => ticket && ticket.read2 === false),
    });
  }, [tickets, user]);
  const [rou, setRou] = useState("");
  const router = useRouter();
  const { page, keyword } = router.query;

  const hadleLogout = async () => {
    await fetch(URL_AUTH, {
      method: "DELETE",
    });
    mutate(null);
    router.push("/");
    setUnread({ unreaded: [], unread: [], unread1: [], unread2: [] });
    localStorage.removeItem("user_Id");
    dispatch({ type: "RESET_CART" });
    localStorage.removeItem("State");
    localStorage.clear();
  };
  useEffect(() => {
    const handleRouteChange = (url) => {
      setRou(url);
    };
    router.events.on("routeChangeStart", handleRouteChange);
    return () => {
      router.events.off("routeChangeStart", handleRouteChange);
    };
  }, []);
  return (
    <Layout style={{ height: "100vh" }}>
      <Header
        className="header"
        style={{
          display: "Flex",
          justifyContent: "space-between",
          backgroundColor: "white",
        }}
      >
        <h2>Furniture Bank</h2>
        <Menu
          theme="light"
          mode="horizontal"
          style={{ paddingLeft: 10, marginRight: -20 }}
        >
          <SubMenu key="sub1" title={user?.email}>
            <Menu.Item key="1">
              <Link href="/edituser">
                <a>My Information</a>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link href="/changepwd">
                <a>Change Password</a>
              </Link>
            </Menu.Item>
            <Menu.Item key="3" onClick={hadleLogout}>
              Logout
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Header>
      <Layout>
        {user && (
          <Sider width={200} className="site-layout-background">
            <Menu
              mode="inline"
              defaultSelectedKeys={rou}
              style={{ height: "100%", borderRight: 0 }}
            >
              {user?.role === "ADMIN" &&
                adminOptions.map((option, i) => (
                  <Menu.Item key={i}>
                    <Link href={option[1]}>
                      <a style={{ marginRight: "10px" }}>{option[0]}</a>
                    </Link>
                    <Badge
                      count={unreadState.unreaded?.length}
                      style={{
                        display: option[0] === HELP_DESK ? "block" : "none",
                      }}
                    ></Badge>
                  </Menu.Item>
                ))}
              {user?.role === Role.FBA &&
                fbaOptions.map((option, i) => (
                  <Menu.Item key={i}>
                    <Link href={option[1]}>
                      <a style={{ marginRight: "10px" }}>{option[0]}</a>
                    </Link>
                    <Badge
                      count={unreadState.unread?.length}
                      style={{
                        display: option[0] === HELP_DESK ? "block" : "none",
                      }}
                    ></Badge>
                  </Menu.Item>
                ))}
              {user?.role === Role.PAL &&
                palOptions.map((option, i) => (
                  <Menu.Item key={i}>
                    <Link href={option[1]}>
                      <a style={{ marginRight: "10px" }}>{option[0]}</a>
                    </Link>
                    <Badge
                      count={unreadState.unread1?.length}
                      style={{
                        display: option[0] === HELP_DESK ? "block" : "none",
                      }}
                    ></Badge>
                  </Menu.Item>
                ))}
              {user?.role === Role.PACM &&
                pacmOptions.map((option, i) => (
                  <Menu.Item key={i}>
                    <Link href={option[1]}>
                      <a style={{ marginRight: "10px" }}>{option[0]}</a>
                    </Link>
                    <Badge
                      count={unreadState.unread2?.length}
                      style={{
                        display: option[0] === HELP_DESK ? "block" : "none",
                      }}
                    ></Badge>
                  </Menu.Item>
                ))}
            </Menu>
          </Sider>
        )}
        <Layout style={{ padding: "0 24px 24px" }}>
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            {children}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
}

export default LayoutComponent;
