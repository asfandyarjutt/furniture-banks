import React, { useState, useRef, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  Table,
  Input,
  Button,
  Space,
  Layout,
  Typography,
  message as info,
  Modal,
  Col,
} from "antd";
import Highlighter from "react-highlight-words";
import { useInactiveFurnitureBanks } from "../lib/hooks";
import ReactPaginate from "react-paginate";

const { Content } = Layout;
const { Title } = Typography;
function FBTable({ data, active, type, curPage, maxPage, perPage }) {
  const router = useRouter();
  const { page, keyword } = router.query;
  const [searchText, setSearchText] = useState("");
  const [fbToRcover, setFbToRecover] = useState([]);
  const [modal, setModal] = useState(false);
  const [searchedColumn, setSearchedColumn] = useState("");
  const [fbtodelete, setFBtoDelete] = useState([]);
  const [localData, setLocatData] = useState();
  const [inactiveFurniturebanks, mutate] = useInactiveFurnitureBanks();
  const searchInput = useRef(null);
  function handleCancel() {
    setModal(false);
  }

  useEffect(() => {
    setLocatData(data);
  }, [data]);
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            size="small"
            style={{
              width: 90,
              backgroundColor: "white",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
              backgroundColor: "white",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),

    onFilter: (value, record) =>
      record[dataIndex]?.toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  function handleSearch(selectedKeys, confirm, dataIndex) {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  }

  function handleReset(clearFilters) {
    clearFilters();
    setSearchText("");
  }

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "40%",
      ...getColumnSearchProps("name"),
      render: (text, record) => (
        <Link href={`/${type}/${record?._id}`}>
          <a>{text}</a>
        </Link>
      ),
    },
    {
      title: "City",
      dataIndex: "city",
      key: "city",
      width: "20%",
      ...getColumnSearchProps("city"),
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",
      width: "20%",
      ...getColumnSearchProps("state"),
    },
  ];

  if (type === "agencies") {
    columns.push({
      title: "Liaison",
      dataIndex: "liaison",
      key: "liaison",
      width: "20%",
      ...getColumnSearchProps("liaison"),
    });
  } else {
    columns.push({
      title: "Administrator",
      dataIndex: "administrator",
      key: "administrator",
      width: "20%",
      ...getColumnSearchProps("administrator"),
    });
  }

  const rowSelection = {
    onChange: (selectedRowKeys) => {
      if (type === "furniturebanks") {
        const selectedDataObect = data.filter((d) => {
          return (
            d.key ===
            selectedRowKeys.find((selected) => {
              return selected === d.key;
            })
          );
        });
        const dataDelete = selectedDataObect.map((selected) => {
          return { furniture_id: selected.key, user_id: selected.owner._id };
        });
        setFBtoDelete(dataDelete);
      }

      setFbToRecover([selectedRowKeys]);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User",
      name: record.name,
    }),
  };

  async function recoverFbs() {
    if (type === "furniturebanks") {
      const res = await fetch("/api/fba/active", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(fbToRcover),
      });
      const { message } = await res.json();
      info.success(`${message}`);
    } else {
      const res = await fetch("/api/pa/active", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(fbToRcover),
      });
      const { message } = await res.json();
      info.success(`${message}`);
    }
  }
  async function PermanentDelete() {
    if (type === "furniturebanks") {
      try {
        const res = await fetch("/api/fba/delete", {
          method: "Delete",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify([fbtodelete]),
        });
        const newdata = mutate(`/api/fba/inactive`);
        setLocatData(data);
        handleCancel();
      } catch (error) {
        throw error;
      }
    }
  }

  const handlePagination = (page) => {
    const path = router.pathname;
    const query = router.query;
    query.page = page.selected + 1;
    router.push({
      pathname: path,
      query: query,
    });
  };

  return (
    <>
      {!active && (
        <div>
          <Col span={6} style={{ justifyContent: "center" }}>
            <Button
              style={{
                backgroundColor: "white",
                height: "40px",
                color: "#69c0ff",
                border: "1px solid #69c0ff",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
                marginBottom: "2em",
              }}
              block
              size="large"
              onClick={recoverFbs}
            >
              Recover{" "}
              {type === "furniturebanks"
                ? "Furniture Bank(s)"
                : "Partner Agencies"}
            </Button>
            {type === "furniturebanks" ? (
              <Button
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#69c0ff",
                  border: "1px solid #69c0ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                  marginBottom: "2em",
                }}
                block
                size="sm"
                onClick={() => setModal(true)}
              >
                Permanent Delete Furniture Bank(s)
              </Button>
            ) : (
              ""
            )}
          </Col>
        </div>
      )}
      <Modal
        title="Add Credit"
        centered
        visible={modal}
        onOk={PermanentDelete}
        onCancel={handleCancel}
      >
        <p>Are You Sure You want to Permanently Delete?</p>
      </Modal>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <Title style={{ display: !active ? "block" : "none" }} level={3}>
          {type === "agencies"
            ? "Deleted Partner Agencies"
            : "Deleted Furniture Banks"}
        </Title>
        <Table
          rowSelection={!active ? { type: "checkbox", ...rowSelection } : false}
          columns={columns}
          dataSource={localData}
          pagination={false}
          onRow={(record, recordIndex) => ({
            onClick: (event) => {
              router.push(`/${type}/${record?._id}`);
            },
          })}
        />
        {active===true?
        <ReactPaginate
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        previousLabel={"previous"}
        nextLabel={"next"}
        breakLabel={"..."}
        initialPage={0}
        pageCount={maxPage}
        onPageChange={handlePagination}
        containerClassName={"paginate-wrap"}
        subContainerClassName={"paginate-inner"}
        pageClassName={"paginate-li"}
        pageLinkClassName={"paginate-a"}
        activeClassName={"paginate-active"}
        nextLinkClassName={"paginate-next-a"}
        previousLinkClassName={"paginate-prev-a"}
        breakLinkClassName={"paginate-break-a"}
      />:""
    
    }
        
      </Content>
    </>
  );
}

export default FBTable;
