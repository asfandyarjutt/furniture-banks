import React, { useState } from "react";
import { Row, Col, Steps, Form, Input, Button, Spin, message } from "antd";
import { useRouter } from "next/router";
import {
  faCheck,
  faChevronRight,
  faChevronLeft,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const { Step } = Steps;

function AddFB() {
  const router = useRouter();
  const [step, setStep] = useState(0);
  const [fstepValues, setFstepsValues] = useState({
    name: "",
    address: "",
    zip: "",
    city: "",
    state: "",
    phone: "",
    email: "",
  });
  const [load, setLoad] = useState(false)

  const handleChange = ({ target: { value, name } }) => {
    setFstepsValues({
      ...fstepValues,
      [name]: value,
    });
  };

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  const onFinish = async (values) => {
    setLoad(true)
    const res = await fetch("/api/fba", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ ...values, ...fstepValues }),
    });
    const {
      furnitureBank: { _id },
    } = await res.json();
    
    setLoad(false)
    router.push(`/furniturebanks/${_id}`);

  };

  const onFinishFailed = (errorInfo) => {
    message.error("Failed to create Furniture Bank" + `${errorInfo}`)
  };

  return (
    <>
      <Row>
        <Col span={12} offset={6}>
          <Steps current={step}>
            <Step title="General Info" />
            <Step title="Admin Info" />
          </Steps>
        </Col>
      </Row>
      <br />
      <hr />
      <br />
      <Spin spinning={load} delay={500} tip='Adding New Furniture Bank...'>
        <Form
          {...layout}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          style={{ width: "100%" }}
        >
          {step === 0 && (
            <>
              <Row>
                <Form.Item label="Name" name="name" style={{ width: "inherit" }}>
                  <Input
                    name="name"
                    onChange={handleChange}
                    placeholder="Furniture Bank Name"
                    style={{ width: "calc(100% + 20vw)" }}
                  />
                </Form.Item>
              </Row>
              <Row>
                <Col span={10}>
                  <Form.Item label="Address" name="address">
                    <Input
                      name="address"
                      onChange={handleChange}
                      placeholder="address"
                    />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item label="Zip" name="zip">
                    <Input
                      name="zip"
                      onChange={handleChange}
                      placeholder="00000"
                    />
                  </Form.Item>
                </Col>
                <Col span={6}>
                  <Form.Item label="City" name="city">
                    <Input
                      name="city"
                      onChange={handleChange}
                      placeholder="City"
                    />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item label="State" name="state">
                    <Input
                      name="state"
                      onChange={handleChange}
                      placeholder="State"
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={10}>
                  <Form.Item label="Phone number" name="phone">
                    <Input
                      name="phone"
                      onChange={handleChange}
                      placeholder="(123) 456-7890 Ext. 123"
                    />
                  </Form.Item>
                </Col>
                <Col span={14}>
                  <Form.Item label="E-mail" name="email">
                    <Input
                      name="email"
                      onChange={handleChange}
                      placeholder="furniture@bank.com"
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col offset={18} span={6}>
                  <Button
                    onClick={() => setStep(step + 1)}
                    style={{
                      backgroundColor: "white",
                      height: "40px",
                      color: "#69c0ff",
                      border: "1px solid #69c0ff",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                  >
                    Next &nbsp;{" "}
                    <FontAwesomeIcon
                      style={{ color: "#69c0ff" }}
                      icon={faChevronRight}
                    />
                  </Button>
                </Col>
              </Row>
            </>
          )}
          {step === 1 && (
            <>
              <Row>
                <Col span={12}>
                  <Form.Item label="Admini first name" name="firstName">
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item label="Admin last name" name="lastName">
                    <Input />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={8}>
                  <Form.Item label="Admin phone" name="adminPhone">
                    <Input placeholder="(123) 456-7890 Ext. 123" />
                  </Form.Item>
                </Col>
                <Col span={16}>
                  <Form.Item label="Admin E-mail" name="adminEmail">
                    <Input placeholder="furniture@admin.com" />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={6}>
                  <Button
                    style={{
                      backgroundColor: "white",
                      height: "40px",
                      color: "#69c0ff",
                      border: "1px solid #69c0ff",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                    onClick={() => setStep(0)}
                  >
                    <FontAwesomeIcon
                      style={{ color: "#69c0ff" }}
                      icon={faChevronLeft}
                    />{" "}
                  &nbsp; Previous
                </Button>
                </Col>
                <Col offset={12} span={6}>
                  <Button
                    htmlType="submit"
                    style={{
                      backgroundColor: "#1890ff",
                      height: "40px",
                      color: "white",
                      border: "1px solid #1890ff",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                  >
                    <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                </Button>
                </Col>
              </Row>
            </>
          )}
        </Form>
      </Spin>
    </>
  );
}

export default AddFB;
