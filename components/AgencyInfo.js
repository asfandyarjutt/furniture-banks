import React, { useState, useEffect } from "react";
import moment from "moment";
import AgenciesTable from "./AgenciesTable";
import { useOneAgency } from "../lib/hooks";
import useInput from "../lib/useInput";
import { useCurrentUser } from "../lib/hooks";
import Dinero from "dinero.js";
import {
  Card,
  Row,
  Col,
  Button,
  Typography,
  Alert,
  Input,
  Space,
  Spin,
  Modal,
  Popconfirm,
  Divider,
} from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import {
  faTrashAlt,
  faPlus,
  faMinus,
  faEdit,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import { useRouter } from "next/router";

const { Title } = Typography;

function AgencyInfo({ agencyId }) {
  const [agency, mutate, trigger] = useOneAgency(agencyId);

  const router = useRouter();
  const [editing, setEditing] = useState(false);
  const [name, setname] = useInput(agency?.name);
  const [address, setaddress] = useInput(agency?.address);
  const [phone, setphone] = useInput(agency?.phone);
  const [email, setemail] = useInput(agency?.email);
  const [credit, setCredit] = useInput("");
  const [services, setServices] = useInput(agency?.services);
  const [managers, setManagers] = useState();
  const [currentUser] = useCurrentUser();
  const [modal, setModal] = useState(false);
  const [successAdd, setSuccessAdd] = useState(false);
  const [failureAdd, setFailureAdd] = useState(false);
  const [message, setMessage] = useState(undefined);
  const [messageManager, setMessagemanager] = useState(undefined);
  const [messageInfo, setMessageInfo] = useState(undefined);
  useEffect(() => {
    setname(agency?.name);
    setaddress(agency?.address);
    setphone(agency?.phone);
    setemail(agency?.email);
    setServices(agency?.services);
    let manas = agency?.caseManagers
      ? agency?.caseManagers.map((manager) => {
          return {
            key: manager?._id,
            _id: manager?._id,
            name: `${manager?.firstName} ${manager?.lastName}`,
          };
        })
      : null;
    setManagers(manas);
    setMessage(router.query.info);
    setMessagemanager(router.query.message);
  }, [agency]);
  async function handleOk() {
    mutate(`/api/pa/${agencyId}`, [agency.credit]);
    try {
      await fetch("/api/pa/credit", {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ credit: credit.value, agencyId }),
      });
      setModal(false);
      setCredit(0);
      setSuccessAdd(true);
    } catch (err) {
      setFailureAdd(true);
    }
    trigger(`/api/pa/${agencyId}`);
  }

  function handleCancel() {
    setModal(false);
  }
 
  async function handleEdit(e) {
    e.preventDefault();
    const updates = {
      name: name.value,
      address: address.value,
      phone: phone.value,
      email: email.value,
      services: services.value,
    };
    mutate(`/api/pa/${agencyId}`, [agency, updates]);
    const res = await fetch(`/api/pa/${agencyId}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(updates),
    });
    const { info } = await res.json();
    trigger(`/api/pa/${agencyId}`);
    if (res.status === 200) {
      setMessageInfo(info);
    }
  }

  async function handleDelete() {
    mutate(`/api/pa/${agencyId}`, [agency]);
    const res = await fetch(`/api/pa/${agencyId}`, {
      method: "DELETE",
    });
    const { message } = await res.json();
    trigger(`/api/pa/${agencyId}`);
    router.push({ pathname: "/agencies", query: { message } });
  }
  async function removeAdmin(id) {
    mutate(`/api/pa/${agencyId}`, [agency?.liaison]);
    await fetch(`/api/user`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ userId: id }),
    });
    trigger(`/api/pa/${agencyId}`);
    setEditing(false);
  }

  let day = moment(agency?.createdAt);
  let now = moment();
  return agency ? (
    <>
      <Alert
        style={{
          marginBottom: "2em",
          width: "100%",
          display: message === undefined ? "none" : "block",
        }}
        message={message}
        type="success"
        showIcon
        closable
        onClose={() => setMessage(undefined)}
      />
      <Alert
        style={{
          marginBottom: "2em",
          width: "100%",
          display: messageManager === undefined ? "none" : "block",
        }}
        message={messageManager}
        type={
          messageManager === "New Case Mananager has been added."
            ? "success"
            : "error"
        }
        showIcon
        closable
        onClose={() => setMessagemanager(undefined)}
      />
      <Alert
        style={{
          marginBottom: "2em",
          width: "100%",
          display: messageInfo === undefined ? "none" : "block",
        }}
        message={messageInfo}
        type="success"
        showIcon
        closable
        onClose={() => setMessage(undefined)}
      />
      <Card>
        <Row>
          <Col span={12}>
            {editing ? <Input {...name} /> : <Title>{agency?.name}</Title>}
            <p>
              Added&nbsp;
              {day.calendar(now, {
                sameDay: "[Today] [at] hh:mm a",
                nextDay: "[Tomorrow]",
                nextWeek: "dddd [at] hh:mm a",
                lastDay: "dddd [at] hh:mm a",
                lastWeek: "dddd",
                sameElse: "MMMM DD, YYYY",
              })}
            </p>
          </Col>
          <Col
            span={6}
            offset={6}
            style={{ display: "flex", justifyContent: "flex-end" }}
          >
            <Link href="/agencies">
              <Button
                style={{ background: "#010101", color: "#ffffff" }}
                shape="circle"
                icon={<FontAwesomeIcon icon={faTimes} />}
              />
            </Link>
          </Col>
        </Row>
        <Divider />
        <Row style={{ marginBottom: "3em" }}>
          <Col span={editing ? 3 : 2} lg={4} xxl={2} xl={4}>
            {editing ? (
              <Button
                onClick={() => setEditing(!editing)}
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#f5222d",
                  border: "1px solid #f5222d",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                {editing ? (
                  <FontAwesomeIcon icon={faTimes} />
                ) : (
                  <FontAwesomeIcon icon={faEdit} />
                )}
                &nbsp;
                {editing ? "Cancel" : "   Edit info"}
              </Button>
            ) : (
              <Button
                onClick={() => setEditing(!editing)}
                style={{
                  backgroundColor: "#0050b3",
                  height: "40px",
                  color: "white",
                  border: "1px solid #0050b3",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                {editing ? (
                  <FontAwesomeIcon icon={faTimes} />
                ) : (
                  <FontAwesomeIcon icon={faEdit} />
                )}
                &nbsp;
                {editing ? "Cancel" : "   Edit info"}
              </Button>
            )}
          </Col>
          {editing && (
            <Col span={8} onClick={(e) => handleEdit(e)}>
              <Button
                onClick={() => setEditing(!editing)}
                style={{
                  backgroundColor: "#1890ff",
                  height: "40px",
                  color: "white",
                  border: "1px solid #1890ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
              </Button>
            </Col>
          )}
          {!editing && (
            <Col span={editing ? 8 : 12}>
              <Popconfirm
                placement="right"
                title="Are you sure you want to delete?"
                onConfirm={handleDelete}
                onCancel={() => null}
                okText="Delete"
                cancelText="Cancel"
              >
                <Button
                  style={{
                    backgroundColor: "#f5222d",
                    height: "40px",
                    color: "white",
                    border: "1px solid #f5222d",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Delete
                </Button>
              </Popconfirm>
            </Col>
          )}
        </Row>
        <Row>
          <Col
            span={4}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingRight: "4em",
            }}
          >
            <b>Contact:</b>
          </Col>
          <Col span={5}>
            {editing ? (
              <Input {...address} />
            ) : (
              <p>
                {agency?.address}, {agency?.city}, {agency?.state},{" "}
                {agency?.zip}
              </p>
            )}
            {editing ? <Input {...phone} /> : <p>{agency?.phone}</p>}
            {editing ? <Input {...email} /> : <p>{agency?.email}</p>}
          </Col>
          {!agency?.liaison?.active && (
            <Col span={8} offset={6}>
              <Alert
                type="warning"
                message="Pending Login"
                description="An email has been sent to the Partner Agency Liaison so that they may begin using their online Furniture Bank Catalogue. This message will disappear after their
first login."
                showIcon
              />
            </Col>
          )}
        </Row>
        <Row>
          <Col
            span={4}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingRight: "4em",
            }}
          >
            <b>Liaison:</b>
          </Col>
          <Col span={4}>
            {agency?.liaison && editing ? (
              <Popconfirm
                placement="right"
                title="Are you sure want to delete？"
                okText="Delete"
                cancelText="Cancel"
                onConfirm={() => removeAdmin(agency?.liaison?._id)}
              >
                <Button
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#f5222d",
                    border: "1px solid #f5222d",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon
                    style={{ color: "#f5222d" }}
                    icon={faMinus}
                  />
                  &nbsp; Remove Liaison
                </Button>
              </Popconfirm>
            ) : agency?.liaison ? (
              <>
                <p>
                  {agency?.liaison?.firstName} {agency.liaison?.lastName}
                </p>
                <p>{agency?.liaison?.phone}</p>
                <p>{agency?.liaison?.email}</p>
              </>
            ) : (
              <Link href={`/agencies/${agencyId}/newliaison`}>
                <a>Add a Liaison</a>
              </Link>
            )}
          </Col>
        </Row>
        <Row>
          <Col
            span={4}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingRight: "4em",
            }}
          >
            <b>Services:</b>
          </Col>
          <Col
            span={4}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingRight: "4em",
            }}
          >
            {editing ? (
              <Input.TextArea {...services} />
            ) : (
              <p>{agency?.services}</p>
            )}
          </Col>
        </Row>
        <br />
        <hr />
        <br />
        {currentUser?.role === "FBA" && (
          <Row>
            {successAdd && (
              <Col span={24} style={{ marginBottom: "2em" }}>
                <Alert
                  message="Additional credit has been added to the Partner Agency's balance."
                  type="success"
                  showIcon
                  closable
                  onClose={() => setSuccessAdd(false)}
                />
              </Col>
            )}
            {failureAdd && (
              <Col span={24}>
                <Alert
                  message="Additional credit has not been added to the Partner Agency, try again. if persist send a ticket to your admin."
                  type="error"
                  showIcon
                  closable
                />
              </Col>
            )}
            <Col span={3} style={{ marginBottom: "2em" }}>
              <p>Prepaid Credit: </p>
            </Col>
            <Col span={21}>
              {Dinero({ amount: Number(agency?.credit + "00") }).toFormat(
                "$0,0.00"
              )}
            </Col>
            <Col offset={3} span={4}>
              <Button
                onClick={() => setModal(true)}
                style={{
                  backgroundColor: "#52c41a",
                  height: "40px",
                  color: "white",
                  border: "1px solid #52c41a",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faPlus} /> &nbsp;Add Credit
              </Button>

              <Modal
                title="Add Credit"
                centered
                visible={modal}
                onOk={handleOk}
                onCancel={handleCancel}
              >
                <p>
                  Current Balance: &nbsp;
                  {Dinero({ amount: Number(agency?.credit + "00") }).toFormat(
                    "$0,0.00"
                  )}
                </p>
                Add Credit: &nbsp;{" "}
                <Input type='number' style={{ width: "40%" }} {...credit} />
                <hr />
                <p>
                  New Balance:{" "}
                  {Dinero({
                    amount: Number(
                      agency?.credit +
                        Number(String(credit.value).split(".").join("")) +
                        "00"
                    ),
                  }).toFormat("$0,0.00")}
                </p>
              </Modal>
            </Col>
            <Divider style={{ border: "1px solid #cccccc" }} />
          </Row>
        )}
        <Row style={{ marginTop: "2em", padding: "1em" }}>
          <Col span={20}>
            <Title level={4}>Case Managers:</Title>
          </Col>
          <Col span={4} style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button
              onClick={() =>
                router.push({
                  pathname: `/agencies/${agencyId}/addmanager`,
                  query: { agencyId },
                })
              }
              disabled={!agency?.liaison ? true : false}
              style={{
                backgroundColor: "#52c41a",
                height: "40px",
                color: "white",
                border: "1px solid #52c41a",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
            >
              <FontAwesomeIcon icon={faPlus} /> &nbsp; Add Case Manager
            </Button>
          </Col>
        </Row>
        {!agency?.liaison ? (
          <Row>
            <Col span={24}>
              <Alert
                type="warning"
                message="Liaison Missing"
                description="In order to add a Case Manager you must first add a Partner Agency Liaison."
                showIcon
              />
            </Col>
          </Row>
        ) : (
          <Row>
            <Col span={24}>
              <AgenciesTable agencyDetail data={managers} agencyId={agencyId} />
            </Col>
          </Row>
        )}
      </Card>
    </>
  ) : (
    <Row justify="center" align="middle" style={{ height: "100%" }}>
      <Space size="middle">
        <Spin size="large" />
      </Space>
    </Row>
  );
}

export default AgencyInfo;
