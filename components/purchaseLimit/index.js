import React, { useState, useEffect } from "react";
import {
  Card,
  Row,
  Col,
  Button,
  Input,
  Collapse,
  Typography,
  message,
  Divider,
} from "antd";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { useCurrentUser } from "../../lib/hooks";
import { useRouter } from "next/router";
import styles from "./styles";
import { purchaseLimit } from "../../constants";
const { ADD, EDIT } = purchaseLimit;

const { Title } = Typography;
const { Panel } = Collapse;
function PurchaseLimit({ type }) {
  const [currentUser] = useCurrentUser();
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [bycategory, setByCategory] = useState({});
  const [limitByCategory, setLimitByCategory] = useState({});
  const [total, setTotal] = useState("");
  const [limitper, setLimitPer] = useState("");
  const [limit, setLimit] = useState({});
  const [id, setId] = useState("");
  const [categoryLimit, setCategoryLimit] = useState([]);
  const router = useRouter();
  useEffect(() => {
    if (currentUser) {
      setLimitPer(currentUser?.furnitureBank?.limitper);
    }
    if (currentUser && type === EDIT) {
      const { data } = router.query;
      let { limit, id } = JSON.parse(data);
      setLimit(limit);
      setId(id);
    }
  }, [currentUser]);

  useEffect(() => {
    if (limit) {
      setFrom(limit?.from);
      setTo(limit?.to);
      setTotal(limit?.total);
      setLimitByCategory(limit?.bycategory);
    }
  }, [limit]);

  useEffect(() => {
    if (limitByCategory) {
      let categories = currentUser?.furnitureBank?.categories.map(
        (category) => {
          return { [category]: "" };
        }
      );
      let result = Object.keys(limitByCategory)?.map((key) => ({
        [key]: limitByCategory[key],
      }));

      let merged = [];

      for (let i = 0; i < categories?.length; i++) {
        const el = categories[i];
        merged.push({
          ...categories[i],
        });
        for (let j = 0; j < result?.length; j++) {
          const res = result[j];
          if (Object.keys(el)[0] === Object.keys(res)[0]) {
            merged.pop({ ...categories[i] });
            merged.push({ ...result[j] });
          }
        }
      }
      setCategoryLimit(merged);
    }
  }, [limitByCategory]);

  async function handleUpdate() {
    const data = {
      from: from,
      to: to,
      bycategory: bycategory,
      total: total,
    };
    if (data.from === "" || data.to === "") {
      return message.warning("Empty fields");
    }
    const res = await fetch("/api/limits", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    const { info } = await res.json();
    if (res.status === 400) {
      return message.warning(`${info}`);
    }
    message.success("Limit Saved");
    router.push({ pathname: "/purchaselimits", query: { info } });
  }

  async function updateHandler(i) {
    const data = {
      from: from,
      to: to,
      bycategory: limitByCategory,
      total: total,
    };
    if (data.from === "" || data.to === "") {
      return message.warning("Empty fields");
    }
    const res = await fetch(`/api/limits/${i}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    const { info } = await res.json();
    if (res.status !== 200) {
      return message.warning(`${info}`);
    }
    message.success("Limit Updated");
    router.push({ pathname: "/purchaselimits", query: { info } });
  }

  function handleByCategory({ target: { name, value } }) {
    type === EDIT
      ? setLimitByCategory({ ...limitByCategory, [name]: value })
      : setByCategory({ ...bycategory, [name]: value });
  }

  return (
    <Card>
      <Row justify="end" align="middle" style={{ marginBottom: "2em" }}>
        <Col span={12}>
          <Title level={3}>
            {type === ADD
              ? "Add Purchase Limit"
              : type === EDIT
              ? "Edit Purchase Limit"
              : null}
          </Title>
        </Col>
        <Col span={12} style={styles.closeButtonContainer}>
          <Link href="/purchaselimits">
            <Button
              style={styles.closeButton}
              shape="circle"
              icon={<FontAwesomeIcon icon={faTimes} />}
            />
          </Link>
        </Col>
      </Row>

      <Row style={{ marginTop: "2em" }}>
        <Col span={12}>
          <Title level={4}>Household Size:</Title>
          <Row gutter={16}>
            <Col span={12}>
              <p>From:</p>
              <Input
                value={from}
                onChange={(e) => setFrom(e.target.value)}
                size="large"
                type="number"
                min={1}
              />
            </Col>
            <Col span={12}>
              <p>To:</p>
              <Input
                value={to}
                onChange={(e) => setTo(e.target.value)}
                size="large"
                type="number"
                min={2}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Divider style={styles.divider} />
      <Row style={{ marginTop: "2em" }}>
        <Col span={24}>
          <Collapse>
            {type === ADD && (
              <>
                {limitper === "category" ? (
                  <Panel header="Limit by items per category" key={1}>
                    {currentUser?.furnitureBank?.categories?.map(
                      (category, i) => (
                        <div key={i}>
                          <p>{category}:</p>
                          <Input
                            size="large"
                            name={category}
                            onChange={handleByCategory}
                            type="number"
                            min={1}
                            style={{ marginBottom: "1em" }}
                          />
                        </div>
                      )
                    )}
                  </Panel>
                ) : (
                  <Panel header="Limit by total number of items" key={2}>
                    <Input
                      size="large"
                      value={total}
                      onChange={(e) => setTotal(e.target.value)}
                      type="number"
                      min={1}
                    />
                  </Panel>
                )}
              </>
            )}
            {type === EDIT && (
              <>
                {limit?.bycategory ? (
                  <Panel header="Limit by items per category" key={1}>
                    {categoryLimit?.map((category, i) => (
                      <div key={i}>
                        <p>{Object.keys(category)[0]}:</p>
                        <Input
                          size="large"
                          name={Object.keys(category)[0]}
                          value={Object.values(category)[0]}
                          onChange={handleByCategory}
                          type="number"
                          min={1}
                          style={{ marginBottom: "1em" }}
                        />
                      </div>
                    ))}
                  </Panel>
                ) : (
                  <Panel header="Limit by total number of items" key={2}>
                    <Input
                      size="large"
                      value={total}
                      onChange={(e) => setTotal(e.target.value)}
                      type="number"
                      min={1}
                    />
                  </Panel>
                )}
              </>
            )}
          </Collapse>
        </Col>
      </Row>
      <br />
      <Row>
        <Col
          span={6}
          offset={18}
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <Link href={`/purchaselimits`}>
            <Button style={styles.cancelButton}>
              {" "}
              <FontAwesomeIcon icon={faTimes} /> &nbsp; Cancel
            </Button>
          </Link>
          <Button
            style={styles.updateButton}
            onClick={type === ADD ? handleUpdate : () => updateHandler(id)}
          >
            <FontAwesomeIcon icon={faCheck} /> &nbsp;{" "}
            {type === ADD ? "Save" : type === EDIT ? "Update" : null}
          </Button>
        </Col>
      </Row>
    </Card>
  );
}

export default PurchaseLimit;
