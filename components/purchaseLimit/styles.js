const styles = {
  closeButtonContainer: {
    display: "flex",
    justifyContent: "flex-end",
    paddingRight: "2em",
  },
  closeButton: { background: "#010101", color: "#ffffff" },
  divider: { border: "1px solid #cccccc" },
  cancelButton: {
    backgroundColor: "white",
    height: "40px",
    color: "#f5222d",
    border: "1px solid #f5222d",
    borderRadius: "3px",
    borderwidth: "1px",
    display: "inline-block",
    textAlign: "center",
    paddingLeft: "20px",
    paddingRight: "20px",
  },
  updateButton: {
    backgroundColor: "#1890ff",
    height: "40px",
    color: "white",
    border: "1px solid #1890ff",
    borderRadius: "3px",
    borderwidth: "1px",
    display: "inline-block",
    textAlign: "center",
    paddingLeft: "20px",
    paddingRight: "20px",
  },
};

export default styles;
