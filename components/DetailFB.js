import React, { useState, useEffect } from "react";
import moment from "moment";
import AgenciesTable from "./AgenciesTable";
import { useOneFB } from "../lib/hooks";
import useInput from "../lib/useInput";
import {
  Card,
  Row,
  Col,
  Button,
  Typography,
  Alert,
  Input,
  Space,
  Spin,
  Popconfirm,
  Divider,
} from "antd";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { faEdit, faTrashAlt, faTimes } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";

const { Title } = Typography;

function DetailFB({ fbid }) {
  const [fb, mutate, trigger] = useOneFB(fbid);
  const router = useRouter();
  const [editing, setEditing] = useState(false);
  const [name, setname] = useInput(fb?.name);
  const [address, setaddress] = useInput(fb?.address);
  const [phone, setphone] = useInput(fb?.phone);
  const [email, setemail] = useInput(fb?.email);
  const [messageC, setMessage] = useState(undefined);
  const [visible, setVisible] = useState(true);
  useEffect(() => {
    setname(fb?.name);
    setaddress(fb?.address);
    setphone(fb?.phone);
    setemail(fb?.email);
    setMessage(router.query.info);
    setMessage(router.query.message);
  }, [fb]);

  async function handleEdit() {
    const updates = {
      name: name.value,
      address: address.value,
      phone: phone.value,
      email: email.value,
    };
    mutate(`/api/fba/${fbid}`, [fb, updates]);
    const call = await fetch(`/api/fba/${fbid}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(updates),
    });
    const { message } = await call.json();
    trigger(`/api/fba/${fbid}`);

    if (call.status === 200) {
      setMessage(message);
    }
  }
  async function handleDelete() {
    mutate(`/api/fba/${fbid}`, [fb], false);
    const res = await fetch(`/api/fba/${fbid}`, {
      method: "DELETE",
    });
    const { message } = await res.json();
    trigger(`/api/fba/${fbid}`);
    router.push({ pathname: "/furniturebanks", query: { message } });
  }

  async function removeAdmin(id) {
    mutate(`/api/user`, [fb]);
    await fetch(`/api/user`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ userId: id }),
    });
    trigger(`/api/fba/${fbid}`);
    setEditing(false);
  }

  const day = moment(fb?.createdAt);
  const now = moment();

  return fb ? (
    <>
      <Alert
        style={{
          marginLeft: "2em",
          marginBottom: "2em",
          width: "96%",
          display: messageC === undefined ? "none" : "block",
        }}
        message={messageC}
        type="success"
        showIcon
        closable
      />

      <Card>
        <Row>
          <Col span={12}>
            {editing ? <Input {...name} /> : <Title>{fb?.name}</Title>}
            <p>
              Added&nbsp;
              {day.calendar(now, {
                sameDay: "[Today] [at] hh:mm a",
                nextDay: "[Tomorrow]",
                nextWeek: "dddd [at] hh:mm a",
                lastDay: "dddd [at] hh:mm a",
                lastWeek: "dddd",
                sameElse: "MMMM DD, YYYY",
              })}
            </p>
          </Col>
          <Col
            span={12}
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingRight: "2em",
            }}
          >
            <Link href="/furniturebanks">
              <Button
                style={{ background: "#010101", color: "#ffffff" }}
                shape="circle"
                icon={<FontAwesomeIcon icon={faTimes} />}
              />
            </Link>
          </Col>
        </Row>
        <Divider />
        <Row style={{ marginBottom: "3em" }}>
          <Col span={editing ? 3 : 2} lg={4} xxl={2} xl={4}>
            {editing ? (
              <Button
                onClick={() => setEditing(!editing)}
                style={{
                  backgroundColor: "white",
                  height: "40px",
                  color: "#f5222d",
                  border: "1px solid #f5222d",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                {editing ? (
                  <FontAwesomeIcon icon={faTimes} />
                ) : (
                  <FontAwesomeIcon icon={faEdit} />
                )}{" "}
                &nbsp;
                {editing ? "Cancel" : "   Edit info"}
              </Button>
            ) : (
              <Button
                onClick={() => setEditing(!editing)}
                style={{
                  backgroundColor: "#0050b3",
                  height: "40px",
                  color: "white",
                  border: "1px solid #0050b3",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                {editing ? (
                  <FontAwesomeIcon icon={faTimes} />
                ) : (
                  <FontAwesomeIcon icon={faEdit} />
                )}
                &nbsp;
                {editing ? "Cancel" : "   Edit info"}
              </Button>
            )}
          </Col>
          {editing && (
            <Col span={4} onClick={handleEdit}>
              <Button
                onClick={() => setEditing(!editing)}
                style={{
                  backgroundColor: "#1890ff",
                  height: "40px",
                  color: "white",
                  border: "1px solid #1890ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
              </Button>
            </Col>
          )}
          <Col span={editing ? 3 : 2}>
            <Popconfirm
              placement="right"
              title="Are you sure you want to delete？"
              okText="Delete"
              cancelText="Cancel"
              onConfirm={handleDelete}
            >
              <Button
                style={{
                  backgroundColor: "#f5222d",
                  height: "40px",
                  color: "white",
                  border: "1px solid #f5222d",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: editing ? "none" : "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faTrashAlt} />
                &nbsp; Delete
              </Button>
            </Popconfirm>
          </Col>
          <Col>
            {!fb?.owner?.active && (
              <Col
                span={editing ? 10 : 12}
                offset={editing ? 14 : 12}
                style={{ height: "3em" }}
              >
                <Alert
                  style={{ display: "flex", flexDirection: "column" }}
                  type="warning"
                  message="Pending Login"
                  description="An email has been sent to the Furniture Bank’s Administrator so that they may begin using their online Furniture Bank Catalogue. This message will disappear after their
                first login."
                  showIcon
                />
              </Col>
            )}
          </Col>
        </Row>
        <Row>
          <Col
            xxl={2}
            xl={2}
            lg={4}
            style={{ display: "flex", justifyContent: "center" }}
          >
            <b>Contact:</b>
          </Col>
          <Col span={5}>
            {editing ? (
              <Input {...address} />
            ) : (
              <p>
                {fb?.address}, {fb?.city}, {fb?.state} {fb?.zip}
              </p>
            )}
            {editing ? <Input {...phone} /> : <p>{fb?.phone}</p>}
            {editing ? <Input {...email} /> : <p>{fb?.email}</p>}
          </Col>
        </Row>
        <Row>
          <Col
            xxl={2}
            xl={2}
            lg={4}
            style={{ display: "flex", justifyContent: "center" }}
          >
            <b>Admin:</b>
          </Col>
          <Col span={5}>
            {fb?.owner && editing ? (
              <Popconfirm
                placement="right"
                title="Are you sure？"
                okText="Yes"
                cancelText="No"
                onConfirm={() => removeAdmin(fb?.owner?._id)}
              >
                <Button
                  style={{
                    backgroundColor: "white",
                    height: "40px",
                    color: "#f5222d",
                    border: "1px solid #f5222d",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                >
                  <FontAwesomeIcon icon={faMinus} /> &nbsp; Remove Admin
                </Button>
              </Popconfirm>
            ) : fb?.owner ? (
              <>
                <p>
                  {fb?.owner?.firstName} {fb.owner?.lastName}
                </p>
                <p>{fb?.owner?.phone}</p>
                <p>{fb?.owner?.email}</p>
              </>
            ) : (
              <Link
                href={{
                  pathname: `/furniturebanks/${fbid}/newadmin`,
                  query: { fbid: fbid },
                }}
              >
                <a>Add Admin</a>
              </Link>
            )}
          </Col>
        </Row>
        <br />
        <hr />
        <br />
        {!fb?.owner ? (
          <Row style={{ marginBottom: "2em" }}>
            <Col span={24}>
              <Alert
                type="warning"
                message="Admin Missing"
                description="In order to add a Partner Agency you must first add a Furniture Bank Admin."
                showIcon
              />
            </Col>
          </Row>
        ) : null}
        <Row style={{ marginBottom: "3em" }} justify="start" align="middle">
          <Col span={20}>
            <Title level={4}>Partner Agencies:</Title>
          </Col>
          <Col span={4} style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button
              style={{
                backgroundColor: "#52c41a",
                height: "40px",
                color: "white",
                border: "1px solid #52c41a",
                borderRadius: "3px",
                borderwidth: "1px",
                display: "inline-block",
                textAlign: "center",
                paddingLeft: "20px",
                paddingRight: "20px",
              }}
              disabled={!fb?.owner ? true : false}
              onClick={() => 
                
                router.push(`/furniturebanks/${fbid}/addagency?page=1`)}
            >
              <FontAwesomeIcon icon={faPlus} /> &nbsp; Add Partner Agency
            </Button>
          </Col>
        </Row>
        <Alert
          style={{
            marginBottom: "2em",
            width: "100%",
            display: "none",
          }}
          message="Partner Agency has been added to Furniture Bank."
          type="success"
          showIcon
          closable
          onClose={() => setMessage(undefined)}
        />
        <Row>
          <Col span={24}>
            <AgenciesTable
              data={fb.partnerAgencies}
              fbid={fbid}
              // mutate={mutatefb}
            />
          </Col>
        </Row>
      </Card>
    </>
  ) : (
    <Row justify="center" align="middle" style={{ height: "100%" }}>
      <Space size="middle">
        <Spin size="large" />
      </Space>
    </Row>
  );
}

export default DetailFB;
