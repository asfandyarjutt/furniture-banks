import React, { useState, useEffect } from "react";
import {
  Layout,
  Row,
  Col,
  Typography,
  Form,
  Input,
  Button,
  message as ms,
} from "antd";
import Link from "next/link";
import { useCurrentUser, useOneAgency } from "../../../lib/hooks";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle, faCheck } from "@fortawesome/free-solid-svg-icons";
import { emailRegx, phoneRegx } from "../../../constants";
const { Content } = Layout;
const { Text } = Typography;

const addCaseManager = () => {
  const router = useRouter();
  const [user] = useCurrentUser();
  const [partnerAgency, mutate, trigger] = useOneAgency(user?.partnerAgency);
  const [manager, setManager] = useState({
    firstName: "",
    lastName: "",
    phone: "",
    email: "",
  });
  const [agencyId, setMyagency] = useState();

  useEffect(() => {
    setMyagency(partnerAgency?._id);
  });
  const handleChange = ({ target: { value, name } }) => {
    setManager({ ...manager, [name]: value });
  };
  const onFinish = async () => {
    Object.values(manager).some((el) => {
      if (el == "") {
        return ms.warning("Missing Fields");
      }
    });
    mutate(`/api/pa/${partnerAgency?._id}`, [manager], false);
    const res = await fetch(`/api/pa/${agencyId}/addcasemanager`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(manager),
    });
    trigger(`/api/pa/${partnerAgency?._id}`);

    const user = await res.json();
    const { message } = user;
    ms.success(message);
    if (res.status >= 200 && res.status <= 300) {
      router.push({ pathname: "/casemanagers/", query: { message } });
    }
  };

  return (
    <div>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <Row justify="space-between" style={{ marginBottom: "3em" }}>
          <Col>
            <Text strong style={{ fontSize: "20px" }}>
              Add New Case Manager
            </Text>{" "}
          </Col>
          <Col>
            <Link href={`/casemanagers`}>
              <a>
                <FontAwesomeIcon icon={faTimesCircle} size="2x" />
              </a>
            </Link>
          </Col>
        </Row>
        <Form layout="vertical" onFinish={onFinish} size="large">
          <Row justify="space-around">
            <Col span={10}>
              <Form.Item
                label="First Name"
                name="firstName"
                rules={[
                  {
                    required: true,
                    message: "First Name is required.",
                  },
                ]}
              >
                <Input
                  name="firstName"
                  type="text"
                  onChange={handleChange}
                  placeholder="First Name"
                />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item
                label="Last Name"
                name="lastName"
                rules={[
                  {
                    required: true,
                    message: "Last Name is required.",
                  },
                ]}
              >
                <Input
                  name="lastName"
                  type="text"
                  onChange={handleChange}
                  placeholder="Last Name"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row justify="center" style={{ marginBottom: "4em" }}>
            <Col span={7}>
              <Form.Item
                label="Phone Number:"
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Phone Number is required.",
                  },
                  {
                    pattern: phoneRegx,
                    message: "Please enter a valid Phone Number.",
                  },
                ]}
              >
                <Input
                  name="phone"
                  type="text"
                  onChange={handleChange}
                  placeholder="555-123-4444"
                  maxLength="10"
                />
              </Form.Item>
            </Col>
            <Col offset={2} span={13}>
              <Form.Item
                label="E-Mail"
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Email is required.",
                  },
                  {
                    pattern: emailRegx,
                    message: "Please enter a valid Email.",
                  },
                ]}
              >
                <Input
                  name="email"
                  type="text"
                  onChange={handleChange}
                  placeholder="E-Mail"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col offset={21} span={8}>
              <Button
                htmlType="submit"
                style={{
                  backgroundColor: "#1890ff",
                  height: "40px",
                  color: "white",
                  border: "1px solid #1890ff",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
              </Button>
            </Col>
          </Row>
        </Form>
      </Content>
    </div>
  );
};

export default addCaseManager;
