import React, { useState, useEffect } from "react";
import Router from "next/router";
import { OrderDetails } from "../../../constants";
import { useOneOrder } from "../../../lib/hooks";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import {
  Descriptions,
  Layout,
  Button,
  Row,
  Col,
  Table,
  Typography,
  Space,
} from "antd";
import Dinero from "dinero.js";

const { Content } = Layout;
const { Title, Text } = Typography;

const orderDetails = ({ orderId }) => {
  const [order] = useOneOrder(orderId);
  let filtered;
  let tableHeading = [];
  let date = moment(order?.createdAt).format("dddd, MMMM D, YYYY, hh:mm a");
  useEffect(() => {
    Router.prefetch("/orders");
  });

  let data = order?.items?.map((item, idx) => {
    tableHeading = {
      key: idx,
      item: item?.item[0]?.description,
      sku: item.sku,
      quantity: item.quantity,
      amount: "N/A",
    };
    return tableHeading;
  });

  if (order?.type) {
    const paymentInfo = order?.type;
    data.push({
      item: paymentInfo,
      key: data.lenght + 1,
      amount: Dinero({ amount: Number(order?.total + "00") }).toFormat(
        "$0,0.00"
      ),
    });
  }

  const columns = [
    {
      title: "Item",
      dataIndex: "item",
    },
    {
      title: "SKU",
      dataIndex: "sku",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
    },
    {
      title: "Amount",
      dataIndex: "amount",
    },
  ];

  return (
    <div>
      <Row
        style={{
          marginBottom: "3em",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Space value="middle" size={40} direction="horizontal">
          <Button
            onClick={() => Router.back()}
            style={{
              backgroundColor: "white",
              height: "40px",
              color: "#69c0ff",
              border: "1px solid #69c0ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            <FontAwesomeIcon
              style={{ color: "#69c0ff" }}
              icon={faChevronLeft}
            />
            <span style={{ color: "#69c0ff" }}> &nbsp; Back</span>
          </Button>
        </Space>
      </Row>
      {order && (
        <Content
          className="site-layout-background"
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
            background: "#ffffff",
          }}
        >
          <Row
            style={{
              marginTop: "2em",
              display: "flex",
            }}
            gutter={[8, 16]}
          >
            <Col span={12}>
              <Col span={16}>
                <Title level={4}>{`Order ${order?.number}`}</Title>
              </Col>
            </Col>
            <Col span={12}>
              <Col span={18}>
                <Title level={4}>{date}</Title>
              </Col>
            </Col>
          </Row>
          <Row gutter={[8, 16]}>
            <Col span={12}>
              <Descriptions size="middle">
                <Descriptions.Item span={4} label="Client">
                  {order?.clientName}
                </Descriptions.Item>
                <Descriptions.Item span={4} label="Client Address">
                  {order?.clientAddress} - {order.clientAppartment},{" "}
                  {order?.clientCity}, {order.clientState} {order.clientZip}{" "}
                  <br />
                </Descriptions.Item>
                <Descriptions.Item span={4} label="Client Email">
                  {order?.clientEmail} <br />
                </Descriptions.Item>
                <Descriptions.Item label="Client Phone">
                  {order?.clientPhone}
                </Descriptions.Item>
              </Descriptions>
            </Col>
            <Col span={12}>
              <Descriptions>
                <Descriptions.Item span={4} label="Partner Agency">
                  {order?.agency.name}
                </Descriptions.Item>
                <Descriptions.Item span={4} label="Case Manager">
                  {order?.cmName}
                </Descriptions.Item>
                <Descriptions.Item span={4} label="Case Manager E-mail">
                  {order?.cmEmail}
                </Descriptions.Item>
                <Descriptions.Item label="Case Manager Phone">
                  {order?.cmPhone}
                </Descriptions.Item>
              </Descriptions>
            </Col>
          </Row>

          <Row>
            <Col span={6}>
              <Descriptions
                title={
                  order?.type === "Pick Up"
                    ? "To be Pick Up by:"
                    : "Delivery to be received by:"
                }
              ></Descriptions>
            </Col>
            <Col span={8}>
              <Descriptions.Item>
                {`${order?.receiverName},  Phone: ${order?.receiverPhone}`}
              </Descriptions.Item>
            </Col>
          </Row>

          <Row
            style={{
              marginTop: "2em",
              display: "flex",
            }}
            gutter={[8, 16]}
          >
            <Col span={12}>
              <Descriptions title={OrderDetails.House_Info}></Descriptions>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Descriptions size="middle">
                {Object.keys(order?.houseHold).map((key) => {
                  let covertedKey = key.charAt(0).toUpperCase() + key.slice(1);

                  return (
                    <Row
                      label={covertedKey.replace(/([a-z])([A-Z])/g, "$1 $2")}
                    >
                      <Col span={12}> {order.houseHold[key]}</Col>
                    </Row>
                  );
                })}
              </Descriptions>
            </Col>
          </Row>

          {order && order?.bg && (
            <>
              <Row
                style={{
                  marginTop: "2em",
                  display: "flex",
                }}
                gutter={[8, 16]}
              >
                <Col span={12}>
                  <Descriptions title={OrderDetails.BG_Info}></Descriptions>
                </Col>
              </Row>
              <Row gutter={[8, 16]}>
                <Col span={24}>
                  <Descriptions size="middle">
                    {order &&
                      order?.bg &&
                      Object.keys(order?.bg).map((key) => {
                        return (
                          <Descriptions.Item span={4} label={key}>
                            {Array.isArray(order.bg[key])
                              ? order.bg[key].map((value, i) => {
                                  return (
                                    <span
                                      key={i}
                                      style={{
                                        margin: "2px",
                                      }}
                                    >
                                      {value}
                                    </span>
                                  );
                                })
                              : order.bg[key]}
                          </Descriptions.Item>
                        );
                      })}
                  </Descriptions>
                </Col>
              </Row>
            </>
          )}
          <Row style={{ marginBottom: "3em" }}>
            <Col span={20}>
              <Table
                style={{ borderBottom: "1px solid black" }}
                columns={columns}
                dataSource={data}
                pagination={false}
              />
            </Col>
          </Row>
          <Row gutter={[16, 16]} style={{ marginBottom: "2em" }}>
            <Col span={16}>
              <Title level={4}>{OrderDetails.Order_Total}</Title>
            </Col>
            <Col span={8}>
              <Title level={4}>
                {Dinero({ amount: Number(order?.total + "00") }).toFormat(
                  "$0,0.00"
                )}
              </Title>
            </Col>
          </Row>
          <Row gutter={[24, 48]}>
            <Col offset={6} span={4}>
              <Text strong>{OrderDetails.Client_Amount}</Text>
            </Col>
            <Col offset={3} span={5}>
              <Text>{order?.clientPaymentType}</Text>
            </Col>
            <Col>
              <Text>
                {Dinero({
                  amount: Number(order?.clientAmount + "00"),
                }).toFormat("$0,0.00")}
              </Text>
            </Col>
          </Row>
          <Row gutter={[24, 48]}>
            <Col offset={6} span={4}>
              <Text strong>{OrderDetails.Agency_Amount}</Text>
            </Col>
            <Col offset={3} span={5}>
              <Text>
                {order &&
                order?.agencyPaymentType &&
                order?.agencyPaymentType?.charAt(0)?.toUpperCase() +
                  order?.agencyPaymentType?.slice(1) ===
                  "Heartland"
                  ? "Credit Card"
                  : order?.agencyPaymentType?.charAt(0)?.toUpperCase() +
                    order?.agencyPaymentType?.slice(1)}
              </Text>
            </Col>
            <Col>
              <Text>
                {Dinero({
                  amount: Number(order?.agencyAmount + "00"),
                }).toFormat("$0,0.00")}
              </Text>
            </Col>
          </Row>
        </Content>
      )}
    </div>
  );
};

export default orderDetails;
