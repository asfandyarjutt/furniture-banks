import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Steps,
  Form,
  Input,
  Button,
  Typography,
  Layout,
  message,
  Spin,
  Tooltip,
} from "antd";
import { InputNumber } from "antd";
import { useRouter } from "next/router";
import Link from "next/link";
import {
  faCheck,
  faChevronRight,
  faChevronLeft,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { emailRegx, phoneRegx, textRegx, zipRegx } from "../utils/regex";

const { Step } = Steps;
const { TextArea } = Input;
const { Content } = Layout;
const { Title } = Typography;
function AddPA() {
  const router = useRouter();
  const [step, setStep] = useState(0);

  const [fstepValues, setFstepsValues] = useState({
    name: "",
    address: "",
    zip: 0,
    city: "",
    state: "",
    phone: "",
    email: "",
    services: "",
    lemail: "",
    lphone: "",
    lfn: "",
    lln: "",
  });
  const [load, setLoad] = useState(false);
  const [stepValues, setStepValues] = useState({});
  const [error, setError] = useState(false);
  let passArray = [];

  const handleChange = ({ target: { value, name } }) => {
    setFstepsValues({
      ...fstepValues,
      [name]: value,
    });
  };
  passArray.every(Boolean);
  const layout = {
    labelCol: { span: 14 },
    wrapperCol: { span: 24 },
  };
  useEffect(() => {
    if (step === 0) {
      setStepValues({
        name: fstepValues.name,
        address: fstepValues.address,
        zip: fstepValues.zip,
        city: fstepValues.city,
        state: fstepValues.state,
        phone: fstepValues.phone,
        email: fstepValues.email,
      });
    }
    if (step === 1) {
      setStepValues({
        services: fstepValues.services,
      });
    }
    if (step === 2) {
      setStepValues({
        email: fstepValues && fstepValues.lemail,
        phone: fstepValues && fstepValues.lphone,
        lfn: fstepValues.lfn,
        lln: fstepValues.lln,
      });
    }
  }, [step, fstepValues]);

  const handleCheck = async (e, value) => {
    e.preventDefault();
    const { email } = value;
    let res = await fetch(`/api/pa/checkemail`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email }),
    });
    if (res.status === 403) {
      setError(true);
      message.warning("Email already in use");
    } else {
      setError(false);
      if (step == 0) {
        setStep(step + 1);
      } else {
        onFinish();
      }
    }
  };

  const onFinish = async () => {
    setLoad(true);
    Object.values(fstepValues).some((el) => {
      if (el == "") {
        setLoad(false);
        message.warning("Check for missing fields on previous pages.");
      }
      return;
    });
    const { lemail, lphone, lfn, lln } = fstepValues;
    const data = {
      ...fstepValues,
      user: {
        email: lemail,
        phone: lphone,
        firstName: lfn,
        lastName: lln,
        liaison: true,
      },
    };

    let res = await fetch("/api/pa", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    if (res.status === 500) {
      res = await fetch("/api/pa", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data),
      });
    }
    const {
      partnerAgency: { _id },
      info,
    } = await res.json();
    setLoad(false);
    router.push({ pathname: `/agencies/${_id}`, query: { info } });
  };

  const onFinishFailed = () => {
    message.warning(`Complete all required fields.`);
  };
  const validateNext = (formValues) => {
    let rev = Object.values(formValues);
    passArray = rev.map((value) => {
      if (step === 0) {
        if (
          value === "" ||
          !emailRegx.test(formValues.email) ||
          !phoneRegx.test(formValues.phone) ||
          !zipRegx.test(formValues.zip) ||
          !textRegx.test(formValues.city) ||
          !textRegx.test(formValues.state)
        ) {
          return false;
        } else if (value === "string" || Number) {
          return true;
        }
      } else if (step === 2) {
        if (
          value === "" ||
          !emailRegx.test(formValues.email) ||
          !phoneRegx.test(formValues.phone) ||
          !textRegx.test(formValues.city) ||
          !textRegx.test(formValues.state)
        ) {
          return false;
        } else if (value === "string" || Number) {
          return true;
        }
      } else {
        if (value === "") {
          return false;
        } else if (value === "string" || Number) {
          return true;
        }
      }
    });
  };
  validateNext(stepValues);
  return (
    <>
      <Content
        className="site-layout-background"
        style={{
          padding: 24,
          margin: 0,
          minHeight: 280,
          background: "#ffffff",
        }}
      >
        <Spin spinning={load} delay={500} tip="Adding New Partner Agency...">
          <Row style={{ marginBottom: "2em" }}>
            <Col span={12}>
              <Title level={3}>Add Partner Agency</Title>
            </Col>
            <Col
              span={12}
              style={{
                display: "flex",
                justifyContent: "flex-end",
                paddingRight: "2em",
              }}
            >
              <Link href="/agencies">
                <Button
                  style={{ background: "#010101", color: "#ffffff" }}
                  shape="circle"
                  icon={<FontAwesomeIcon icon={faTimes} />}
                />
              </Link>
            </Col>
          </Row>
          <Row style={{ marginBottom: "3em" }}>
            <Col span={12} offset={6}>
              <Steps current={step}>
                <Step title="General Info" />
                <Step title="Services Provided" />
                <Step title="Liaison Info" />
              </Steps>
            </Col>
          </Row>
          <br />
          <hr />
          <br />
          <Form
            {...layout}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            style={{ width: "100%", marginTop: "2em" }}
          >
            {step === 0 && (
              <>
                {" "}
                <Row
                  gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
                  align="middle"
                  justify="space-around"
                >
                  <Col span={24}>
                    <Form.Item
                      label="Partner Agency Name:"
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Partner agency name is required.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        name="name"
                        value={fstepValues.name}
                        onChange={handleChange}
                        placeholder="Partner Agency Name"
                        rules={[
                          {
                            required: true,
                            message: "Name is required.",
                          },
                        ]}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <br />
                <Row type="flex">
                  <Col span={10}>
                    <Form.Item
                      label="Address:"
                      name="address"
                      rules={[
                        {
                          required: true,
                          message: "Address is required.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        name="address"
                        value={fstepValues.address}
                        onChange={handleChange}
                        placeholder="Address"
                      />
                    </Form.Item>
                  </Col>
                  <Col
                    span={12}
                    offset={2}
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Col xl={6} lg={7}>
                      <Form.Item
                        label="Zip Code:"
                        name="zip"
                        min="0"
                        rules={[
                          {
                            required: true,
                            message: "Zip code is required.",
                          },
                          {
                            pattern: zipRegx,
                            message: "Enter valid zip code.",
                          },
                        ]}
                      >
                        <Input
                          maxLength="5"
                          onKeyDown={(evt) => {
                            if (evt.key === "e") {
                              evt.preventDefault();
                            }
                            if (evt.key === "+") {
                              evt.preventDefault();
                            }
                            if (evt.key === "-") {
                              evt.preventDefault();
                            }
                          }}
                          size="large"
                          name="zip"
                          value={fstepValues.zip}
                          onChange={handleChange}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={7}>
                      <Form.Item
                        label="City:"
                        name="city"
                        rules={[
                          { required: true, message: "City is required." },
                          {
                            pattern: textRegx,
                            message: "Please enter a valid City name.",
                          },
                        ]}
                      >
                        <Input
                          size="large"
                          name="city"
                          value={fstepValues.city}
                          onChange={handleChange}
                          placeholder="City"
                        />
                      </Form.Item>
                    </Col>
                    <Col span={7}>
                      <Form.Item
                        label="State:"
                        name="state"
                        rules={[
                          { required: true, message: "State is required." },
                          {
                            pattern: textRegx,
                            message: "Please enter a valid State name.",
                          },
                        ]}
                      >
                        <Input
                          size="large"
                          name="state"
                          value={fstepValues.state}
                          onChange={handleChange}
                          placeholder="State"
                        />
                      </Form.Item>
                    </Col>
                  </Col>
                </Row>
                <br />
                <Row gutter={16}>
                  <Col span={10}>
                    <Form.Item
                      label="Phone Number:"
                      name="phone"
                      rules={[
                        {
                          required: true,
                          message: "Phone Number is required.",
                        },
                        {
                          pattern: phoneRegx,
                          message: "please enter a valid phone number.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        name="phone"
                        maxLength="10"
                        value={fstepValues.phone}
                        onChange={handleChange}
                        placeholder="555-123-4444"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12} offset={2}>
                    <Form.Item
                      label="E-mail:"
                      name="email"
                      rules={[
                        { required: true, message: "Email is required." },
                        {
                          pattern: emailRegx,
                          message: "Please enter a valid email",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        name="email"
                        value={fstepValues.email}
                        onChange={handleChange}
                        placeholder="furniture@bank.com"
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col
                    offset={18}
                    span={6}
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Button
                      size="large"
                      onClick={(e) => handleCheck(e, stepValues)}
                      style={{
                        backgroundColor: "white",
                        height: "40px",
                        color: "#69c0ff",
                        border: "1px solid #69c0ff",
                        borderRadius: "3px",
                        borderwidth: "1px",
                        display: "inline-block",
                        textAlign: "center",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                      }}
                      disabled={!passArray.every(Boolean)}
                    >
                      Next &nbsp;{" "}
                      <FontAwesomeIcon
                        style={{ color: "#69c0ff" }}
                        icon={faChevronRight}
                      />
                    </Button>
                  </Col>
                </Row>
              </>
            )}
            {step === 1 && (
              <>
                <Row style={{ marginBottom: "5em" }}>
                  <Col span={24}>
                    <Form.Item
                      label="Services Provided:"
                      name="services"
                      rules={[
                        { required: true, message: "Services are required." },
                      ]}
                    >
                      <TextArea
                        rows={4}
                        name="services"
                        value={fstepValues.services}
                        onChange={handleChange}
                        placeholder="Briefly describe or list the services provided by the Partner Agency."
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <br />
                <Row
                  style={{
                    width: "100%",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Button
                    style={{
                      backgroundColor: "white",
                      height: "40px",
                      color: "#69c0ff",
                      border: "1px solid #69c0ff",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                    onClick={() => setStep(step - 1)}
                  >
                    <FontAwesomeIcon
                      style={{ color: "#69c0ff" }}
                      icon={faChevronLeft}
                    />{" "}
                    &nbsp; Previous
                  </Button>
                  <Button
                    onClick={() => setStep(step + 1)}
                    style={{
                      backgroundColor: "white",
                      height: "40px",
                      color: "#69c0ff",
                      border: "1px solid #69c0ff",
                      borderRadius: "3px",
                      borderwidth: "1px",
                      display: "inline-block",
                      textAlign: "center",
                      paddingLeft: "20px",
                      paddingRight: "20px",
                    }}
                    disabled={!passArray.every(Boolean)}
                  >
                    Next &nbsp;{" "}
                    <FontAwesomeIcon
                      style={{ color: "#69c0ff" }}
                      icon={faChevronRight}
                    />
                  </Button>
                </Row>
              </>
            )}
            {step === 2 && (
              <>
                <Row>
                  <Col span={10}>
                    <Form.Item
                      label="Liaison First Name:"
                      name="lfn"
                      rules={[
                        {
                          required: true,
                          message: "Liaison first name is required.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        onChange={handleChange}
                        name="lfn"
                        value={fstepValues.lfn}
                        placeholder="First Name"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12} offset={2}>
                    <Form.Item
                      label="Liaison Last Name:"
                      name="lln"
                      rules={[
                        {
                          required: true,
                          message: "Liaison last name is required.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        onChange={handleChange}
                        name="lln"
                        value={fstepValues.lln}
                        placeholder="Last Name"
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <br />
                <Row gutter={16}>
                  <Col span={10}>
                    <Form.Item
                      label="Phone Number:"
                      name="lphone"
                      rules={[
                        { required: true, message: "Phone is required." },
                        {
                          pattern: phoneRegx,
                          message: "please enter a valid phone number.",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        maxLength="10"
                        placeholder="555-123-4444"
                        onChange={handleChange}
                        name="lphone"
                        value={fstepValues.lphone}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={12} offset={2}>
                    <Form.Item
                      label="E-mail:"
                      name="lemail"
                      rules={[
                        {
                          required: true,
                          message: "Email is required.",
                        },
                        {
                          pattern: emailRegx,
                          message: "Please enter a valid email",
                        },
                      ]}
                    >
                      <Input
                        size="large"
                        placeholder="furniture@admin.com"
                        onChange={handleChange}
                        name="lemail"
                        value={fstepValues.lemail}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col span={6}>
                    <Button
                      style={{
                        backgroundColor: "white",
                        height: "40px",
                        color: "#69c0ff",
                        border: "1px solid #69c0ff",
                        borderRadius: "3px",
                        borderwidth: "1px",
                        display: "inline-block",
                        textAlign: "center",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                      }}
                      onClick={() => setStep(step - 1)}
                    >
                      <FontAwesomeIcon
                        style={{ color: "#69c0ff" }}
                        icon={faChevronLeft}
                      />{" "}
                      &nbsp; Previous
                    </Button>
                  </Col>
                  <Col
                    offset={12}
                    span={6}
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Button
                      htmlType="submit"
                      type="primary"
                      onClick={(e) => handleCheck(e, stepValues)}
                      disabled={!passArray.every(Boolean)}
                      style={{
                        height: "40px",
                        color: "white",
                        borderRadius: "3px",
                        borderwidth: "1px",
                        display: "inline-block",
                        textAlign: "center",
                        paddingLeft: "20px",
                        paddingRight: "20px",
                      }}
                    >
                      <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
                    </Button>
                  </Col>
                </Row>
              </>
            )}
          </Form>
        </Spin>
      </Content>
    </>
  );
}

export default AddPA;
