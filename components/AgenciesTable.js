import React, { useEffect } from "react";
import { Button, Table, Popconfirm, message } from "antd";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useOneAgency, useOneFB } from "../lib/hooks";
import Link from "next/link";

function AgenciesTable({ agencyDetail, data, fbid, agencyId }) {
  const [agency, mutateAgency, triggerAgency] = useOneAgency(agencyId);
  const [furnitureBank, mutate, trigger] = useOneFB(fbid);
  useEffect(() => {}, [data]);
  async function removeAgency(id) {
    if (!agencyDetail) {
      mutate(
        `/api/fba/${fbid}`,
        furnitureBank?.partnerAgencies.filter((agency) => agency.id !== id)
      );
      const res = await fetch(`/api/fba/${fbid}/agencies/${id}`, {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
      });
      const { info } = await res.json();
      trigger(`/api/fba/${fbid}`);
      message.success(info);
    } else {
      mutateAgency(
        `/api/pa/${agencyId}`,
        agency?.caseManagers.filter((user) => user.id !== id)
      );
      await fetch(`/api/user`, {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ userId: id }),
      });
      triggerAgency(`/api/pa/${agencyId}`);
    }
  }
  const columns = [
    {
      title: "Name",
      key: "name",
      render: (record) => {
        let link;
        if (agencyId == undefined) {
          link = `/agencies/${record._id}`;
        } else {
          link = `/agencies/${agencyId}/${record._id}`;
        }

        return (
          <Link href={link}>
            <a>{record.name}</a>
          </Link>
        );
      },
    },
    {
      title: "",
      key: "action",
      width: 60,
      render: (record) => (
        <Popconfirm
          placement="right"
          title="Are you sure want to delete？"
          okText="Delete"
          cancelText="Cancel"
          onConfirm={() => removeAgency(record._id)}
        >
          <Button
            style={{
              backgroundColor: "#f5222d",
              height: "40px",
              color: "white",
              border: "1px solid #f5222d",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
            type="text"
          >
            <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Delete
          </Button>
        </Popconfirm>
      ),
    },
  ];
  return <Table columns={columns} dataSource={data} />;
}

export default AgenciesTable;
