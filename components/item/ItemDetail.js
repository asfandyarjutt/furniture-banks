import React, { useState, useEffect } from "react";
import Link from "next/link";
import {
  Card,
  Row,
  Col,
  Button,
  Input,
  Typography,
  Select,
  message,
  Popconfirm,
  Form,
} from "antd";
import {
  faCheck,
  faTrashAlt,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import { useCurrentUser, useOneItem } from "../../lib/hooks";
import useInput from "../../lib/useInput";
import UploadPhotos from "../UploadPhotos";

const { Title } = Typography;
const { Option } = Select;

function ItemDetail({ itemId }) {
  const [description, setdescription] = useInput("");
  const [photos, setPhotos] = useState([]);
  const [images, setimages] = useState([]);
  const [category, setCategory] = useState("");
  const [quantity, setquantity] = useInput("");
  const [color, setcolor] = useInput("");
  const [material, setmaterial] = useInput("");
  const [high, sethigh] = useInput("");
  const [wide, setwide] = useInput("");
  const [deep, setdeep] = useInput("");
  const [sku, setsku] = useInput("");
  const router = useRouter();
  const [item] = useOneItem(itemId);
  const [currentUser] = useCurrentUser();

  function addPhoto(photo) {
    setPhotos([...photos, photo]);
  }

  useEffect(() => {
    if (item) {
      setimages(item?.images);
      setdescription(item?.description);
      setCategory(item?.category);
      setquantity(item?.quantity);
      setcolor(item?.color);
      setsku(item?.sku);
      setmaterial(item.material);
      sethigh(item?.dimensions?.high);
      setwide(item?.dimensions?.wide);
      setdeep(item?.dimensions?.deep);
    }
  }, [item]);

  const handleSelect = (value) => {
    setCategory(value);
  };

  async function createItem() {
    const data = {
      description: description?.value,
      category: category,
      quantity: quantity?.value,
      images: [...photos, ...images],
      color: color?.value,
      material: material?.value,
      dimensions: {
        high: high?.value,
        wide: wide?.value,
        deep: deep?.value,
      },
    };

    await fetch(`/api/furniture/${item._id}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    message.success("Changes Saved");
    router.push("/dashboard");
  }
  return (
    <Card>
      <Row justify="end" align="middle" style={{ marginBottom: "2em" }}>
        <Col span={12}>
          <Title level={3}>Edit Item</Title>
        </Col>
        <Col
          span={12}
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingRight: "2em",
          }}
        >
          <Link href="/dashboard">
            <Button
              style={{ background: "#010101", color: "#ffffff" }}
              shape="circle"
              icon={<FontAwesomeIcon icon={faTimes} />}
            />
          </Link>
        </Col>
      </Row>
      <UploadPhotos
        setPhotos={addPhoto}
        photos={photos}
        placeholder="Upload Photo"
      />
      <Row>
        {images.map((image, i) => (
          <Col span={4} key={i}>
            <img
              src={image}
              style={{
                objectPosition: "center",
                OObjectFit: "contain",
                width: "100%",
              }}
            />
            <Popconfirm
              title="Are you sure？"
              okText="Yes"
              cancelText="No"
              onConfirm={() => setimages(images.filter((el) => image != el))}
            >
              <Button
                style={{
                  backgroundColor: "#f5222d",
                  height: "40px",
                  color: "white",
                  border: "1px solid #f5222d",
                  borderRadius: "3px",
                  borderwidth: "1px",
                  display: "inline-block",
                  textAlign: "center",
                  paddingLeft: "20px",
                  paddingRight: "20px",
                }}
              >
                <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Delete
              </Button>
            </Popconfirm>
          </Col>
        ))}
      </Row>
      <br />
      <br />
      <Row style={{ marginBottom: "2em" }}>
        <p>Item Description:</p>
        <Input
          size="large"
          placeholder="Wooden Bed Frame"
          name="description"
          {...description}
        />
      </Row>
      <br />
      <Row gutter={16} style={{ marginBottom: "2em" }}>
        <Col span={14}>
          <p>Category: </p>
          <Select
            size="large"
            style={{ width: "100%" }}
            onChange={handleSelect}
            value={category}
          >
            {currentUser?.furnitureBank?.categories?.map((category, key) => (
              <Option key={key} value={category}>
                {category}
              </Option>
            ))}
          </Select>
        </Col>
        <Col span={5}>
          <p>Quantity: </p>
          <Input
            size="large"
            {...quantity}
            min={1}
            defaultValue={1}
            type="number"
          />
        </Col>
        <Col span={5}>
          <p>SKU: </p>
          <Input size="large" {...sku} />
        </Col>
      </Row>
      <br />
      <Row gutter={16} style={{ marginBottom: "2em" }}>
        <Col span={12}>
          <p style={{ margin: 0 }}>Color:</p>
          <Input size="large" placeholder="White" {...color} />
        </Col>
        <Col span={12}>
          <p style={{ margin: 0 }}>Material:</p>
          <Input size="large" placeholder="Vinil" {...material} />
        </Col>
      </Row>
      <br />
      <p>Dimensions (inches):</p>
      <Row align="middle" gutter={16} style={{ marginBottom: "2em" }}>
        <Col span={4}>
          <Form.Item label="High">
            <Input size="large" placeholder="high" {...high} type="number" />
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item label="Wide">
            <Input size="large" placeholder="wide" {...wide} type="number" />
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item label="Deep">
            <Input size="large" placeholder="deep" {...deep} type="number" />
          </Form.Item>
        </Col>
      </Row>
      <br />
      <Row style={{ padding: "2em" }}>
        <Col
          offset={18}
          span={6}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          <Button
            style={{
              backgroundColor: "white",
              height: "40px",
              color: "#f5222d",
              border: "1px solid #f5222d",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
              marginRight: "10px",
            }}
            onClick={() => router.push(`/dashboard`)}
          >
            <FontAwesomeIcon icon={faTimes} /> &nbsp; Cancel
          </Button>
          <Button
            disabled={
              !description.value ||
              !category ||
              !quantity.value ||
              !high.value ||
              !wide.value ||
              !deep.value
            }
            onClick={createItem}
            style={{
              backgroundColor: "#1890ff",
              height: "40px",
              color: "white",
              border: "1px solid #1890ff",
              borderRadius: "3px",
              borderwidth: "1px",
              display: "inline-block",
              textAlign: "center",
              paddingLeft: "20px",
              paddingRight: "20px",
            }}
          >
            <FontAwesomeIcon icon={faCheck} /> &nbsp; Save
          </Button>
        </Col>
      </Row>
    </Card>
  );
}

export default ItemDetail;
