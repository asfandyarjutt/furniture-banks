import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import {
  Card,
  Button,
  Row,
  Col,
  Select,
  Input,
  Carousel,
  Typography,
  Badge,
  Spin,
  message,
  Alert,
  Popconfirm,
} from "antd";
import useInput from ".././../lib/useInput";
import Link from "next/link";
import { useCurrentUser } from "../../lib/hooks";
import { useCartValue } from "../../lib/state";
import usePersistedState from "../../lib/usePersist";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  faPlus,
  faEdit,
  faMinus,
  faSearch,
  faTrashAlt,
  faBan,
  faGrinHearts,
} from "@fortawesome/free-solid-svg-icons";
import ReactPaginate from "react-paginate";
import {
  GO_CHECKOUT,
  RELOAD_PAGE,
  EDIT_CATEGORY,
  ADD_ITEM,
  TOTAL_CATEGORY_REACHED,
  ITEM_ADDED,
  ITEM_DELETED,
  NOT_DELETED,
  NO_PURCHASE_LIMIT,
  NOT_ADDED,
  Role,
  ADD_CART,
  LIMIT_REACHED,
} from "../../constants";
import { catalog } from "../../constants";
import styles from "./styles";
import { getLocal } from "../Forms/functions";

const { Option } = Select;
const { Title } = Typography;

function Catalog() {
  const [currentUser] = useCurrentUser();
  const router = useRouter();
  const { page, keyword } = router.query;
  const [{ items: cartItems }] = useCartValue();
  const [limit, setLimit] = useState([]);
  const [purchase, setPurchase] = useState(false);
  let values;
  useEffect(() => {
    values = getLocal("State");
    if (values) {
      setLimit(values.limit);
      setPurchase(values.purchase);
    }
  }, []);
  const [items, setItems] = useState([]);
  const [search, setsearch] = useInput("");
  const [searching, setSearching] = useState("");
  const [category, setcategory] = useInput("", true);
  const [filtered, setfiltered] = useState([]);
  const [bycategory, setbycategory] = useState([]);
  const [spining, setSpin] = useState(false);
  const [msg, setMsg] = useState(undefined);
  const [myFurniture, setMyFurniture] = useState();
  const [totalObj, setTotalObj] = useState({});
  const [quantity, setQuantity] = useState(0);
  const [itemQty, setItemQty] = useState(0);
  const [message, setMessage] = useState("");
  const [maxPages, setMaxpages] = useState();
  useEffect(() => {
    cartItems.length > 0 &&
      setTotalObj(
        cartItems.reduce(
          (acc, item) => ({ ...acc, [item.item]: item.quantity }),
          {}
        )
      );
  }, []);

  async function fetchCatalogue(keyword = null) {
    setSpin(true);

    let url = `/api/fba/catalogue?page=${page}`;
    if (keyword) {
      url = `/api/fba/catalogue?page=1&keyword=${keyword}`;
      setSpin(false);
    }
    let res = await fetch(url);
    if (res.status === 200) {
      const { catalogue, furnitureBank, maxPage, curPage, perPage } =
        await res.json();
      setMyFurniture(furnitureBank);
      setItems(catalogue);
      setMaxpages(maxPage);
    } else if (res.status === 500) {
      res = await fetch("api/fba/catalogue");
      const { catalogue, furnitureBank, maxPage, curPage, perPage } =
        await res.json();
      setMyFurniture(furnitureBank);
      setItems(catalogue);
      setMaxpages(maxPage);
    } else {
      setMessage(`${RELOAD_PAGE}`);
    }

    setSpin(false);
  }

  useEffect(() => {
    fetchCatalogue();
    setMsg(router.query.message);
  }, [page]);

  function handleSearch({ target: { value } }) {
    setsearch(value);
    const filteredd = items.filter((item) =>
      item.description.toLowerCase().includes(value.toLowerCase())
    );
    setfiltered(filteredd);
  }

  function handleCategory(e) {
    setsearch("");
    setcategory(e), setbycategory(items.filter((item) => item.category === e));
  }

  const onchangeInput = ({ target }, id) => {
    setItemQty(target.value);
    setTotalObj((prev) => {
      return { ...prev, [id]: target.value };
    });
  };

  useEffect(() => {
    const itemsNumber = Object.values(totalObj).reduce(
      (acc, item) => (acc += Number(item)),
      0
    );
    setQuantity(itemsNumber);
  }, [totalObj]);

  const handlePagination = (page) => {
    const path = router.pathname;
    const query = router.query;
    query.page = page.selected + 1;
    router.push({
      pathname: path,
      query: query,
    });
  };

  function displayforPal() {
    return (
      <div>
        <Row justify="space-between" align="middle" gutter={[16, 32]}>
          <Col span={10}>
            <Title level={4} style={{ display: "flex", alignItems: "center" }}>
              {myFurniture?.name}
            </Title>
          </Col>
          <Col span={12} offset={2}>
            <Input
              size="large"
              onChange={(e) => fetchCatalogue(e.target.value)}
              prefix={<FontAwesomeIcon icon={faSearch} />}
              placeholder="Search"
            />
          </Col>
        </Row>
        <Row
          gutter={[16, 16]}
          justify="space-between"
          align="middle"
          style={{ marginBottom: "2em", width: "-webkit-fill-available" }}
        >
          <Col xxl={10} xl={8} lg={8} md={12} sm={12}>
            <Select
              defaultValue="All Categories"
              style={{ width: "100%" }}
              onChange={handleCategory}
              size="large"
            >
              <Option value="">All Categories</Option>
              {currentUser?.furnitureBank?.categories?.map((category, key) => (
                <Option key={key} value={category}>
                  {category}
                </Option>
              ))}
            </Select>
          </Col>
          <Col
            xxl={12}
            xl={14}
            lg={14}
            md={12}
            sm={12}
            offset={2}
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <Link href="/purchase">
              <a href="">
                <Button
                  style={{
                    backgroundColor: purchase ? "white" : "none",
                    color: purchase ? "#69c0ff" : "none",
                    border: purchase ? "1px solid #69c0ff" : "none",
                    borderRadius: "3px",
                    borderwidth: "1px",
                    display: "inline-block",
                    textAlign: "center",
                    paddingLeft: "20px",
                    paddingRight: "20px",
                  }}
                  size="large"
                  disabled={!purchase}
                >
                  Client Appointment Form
                </Button>
              </a>
            </Link>
            <Link href="/purchase">
              <a>
                <Button
                  style={{
                    ...styles.startNewP,
                    backgroundColor: !purchase ? "white" : "none",
                    color: !purchase ? "#69c0ff" : "none",
                    border: !purchase ? "1px solid #69c0ff" : "none",
                  }}
                  size="large"
                  disabled={purchase}
                >
                  Start New Purchase
                </Button>
              </a>
            </Link>
            {cartItems.length > 0 ? (
              <Badge count={Number(cartItems.length)}>
                <Link href="/checkout">
                  <a>
                    <Button
                      style={{
                        ...styles.goCheckout,
                        backgroundColor: purchase ? "white" : "none",
                        color: purchase ? "#69c0ff" : "none",
                        border: purchase ? "1px solid #69c0ff" : "none",
                      }}
                      size="large"
                      disabled={!purchase}
                      ghost={!purchase}
                    >
                      {GO_CHECKOUT}
                    </Button>
                  </a>
                </Link>
              </Badge>
            ) : (
              <Button
                style={{
                  ...styles.goToCheckout,
                  backgroundColor: purchase ? "white" : "none",
                  color: purchase ? "#69c0ff" : "none",
                  border: purchase ? "1px solid #69c0ff" : "none",
                }}
                size="large"
                disabled={true}
              >
                Go to Checkout
              </Button>
            )}
          </Col>
        </Row>
      </div>
    );
  }

  function displayforFba() {
    return (
      <Row justify="space-between" align="middle">
        <Col span={7} style={{ marginRight: "0em" }}>
          <Select
            defaultValue="All Categories"
            style={{ width: "100%" }}
            onChange={handleCategory}
            size="large"
          >
            <Option value="">All Categories</Option>
            {currentUser?.furnitureBank?.categories?.map((category, key) => (
              <Option key={key} value={category}>
                {category}
              </Option>
            ))}
          </Select>
        </Col>
        <Col span={5} style={{ display: "flex", justifyContent: "center" }}>
          <Link
            href={`/furniturebanks/${currentUser?.furnitureBank?._id}/categories`}
          >
            <a>
              <Button style={styles.editCategory}>
                <FontAwesomeIcon icon={faEdit} /> &nbsp;{EDIT_CATEGORY}
              </Button>
            </a>
          </Link>
        </Col>
        <Col span={5} style={{ display: "flex", justifyContent: "center" }}>
          <Link href={`/item/add`}>
            <a>
              <Button style={styles.addItem}>
                <FontAwesomeIcon icon={faPlus} /> &nbsp; {ADD_ITEM}
              </Button>
            </a>
          </Link>
        </Col>
        <Col span={6}>
          <Input
            size="large"
            onChange={(e) => {
              setSearching(e.target.value);
              fetchCatalogue(e.target.value);
            }}
            prefix={<FontAwesomeIcon icon={faSearch} />}
            placeholder="Search"
          />
        </Col>
      </Row>
    );
  }

  return (
    <>
      <Spin spinning={spining} tip="Loading...">
        {currentUser?.role === Role.PACM ? displayforPal() : null}
        {currentUser?.role === Role.PAL ? displayforPal() : null}
        {currentUser?.role === Role.FBA ? displayforFba() : null}
        <Row style={{ padding: "0 13%", marginTop: "2em" }}>
          <Col span={24}>
            <Alert
              style={{
                marginBottom: "2em",
                width: "100%",
                display: msg === undefined ? "none" : "block",
              }}
              message={msg}
              type="success"
              showIcon
              closable
              onClose={() => setMsg(undefined)}
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={24}>
            {!limit && <Alert message={NO_PURCHASE_LIMIT} type="error" />}
            <br />
          </Col>
          {search.value !== ""
            ? filtered?.map((item) => (
                <ItemCard
                  isActive={item?.isActive}
                  totalObj={totalObj}
                  total={quantity}
                  fetchCatalogue={fetchCatalogue}
                  onchangeInput={onchangeInput}
                  item={item}
                  key={item._id}
                  quantity={itemQty}
                />
              ))
            : category.value !== ""
            ? bycategory?.map((item) => (
                <ItemCard
                  isActive={item?.isActive}
                  totalObj={totalObj}
                  fetchCatalogue={fetchCatalogue}
                  total={quantity}
                  onchangeInput={onchangeInput}
                  item={item}
                  key={item._id}
                  quantity={itemQty}
                />
              ))
            : items?.map((item) => (
                <ItemCard
                  isActive={item?.isActive}
                  fetchCatalogue={fetchCatalogue}
                  router={router}
                  totalObj={totalObj}
                  total={quantity}
                  onchangeInput={onchangeInput}
                  item={item}
                  key={item._id}
                  quantity={itemQty}
                />
              ))}
          <div style={{ width: "100%" }}>
            {items.length <= 0 ? (
              <div>
                <h2>No Items Found</h2>
              </div>
            ) : (
              <ReactPaginate
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                previousLabel={"previous"}
                nextLabel={"next"}
                breakLabel={"..."}
                initialPage={0}
                pageCount={maxPages}
                onPageChange={handlePagination}
                containerClassName={"paginate-wrap"}
                subContainerClassName={"paginate-inner"}
                pageClassName={"paginate-li"}
                pageLinkClassName={"paginate-a"}
                activeClassName={"paginate-active"}
                nextLinkClassName={"paginate-next-a"}
                previousLinkClassName={"paginate-prev-a"}
                breakLinkClassName={"paginate-break-a"}
              />
            )}
          </div>
        </Row>
      </Spin>
    </>
  );
}

function ItemCard({
  item,
  totalObj,
  fetchCatalogue,
  router,
  isActive,
  onchangeInput,
  total,
  quantity,
}) {
  const [currentUser] = useCurrentUser();
  const { limitper } = currentUser?.furnitureBank;
  const [
    {
      purchase: _purchase,
      limitReach: _limitReach,
      items: _items,
      limit: _limits,
    },
    dispatch,
  ] = useCartValue();
  const [items, setItems] = useState([]);
  const [limit, setLimit] = useState([]);
  const [purchase, setPurchase] = useState(false);
  let values;

  useEffect(() => {
    values = getLocal("State");
    if (values) {
      setLimit(values.limit);
      setPurchase(values.purchase);
      setItems(values.items);
    }
  }, []);
  const [onButton, setActive] = usePersistedState(item._id, true);
  const [itemTotal, setItemTotal] = useState(0);
  const [purchaseLimit, setPurchaseLimit] = useState({});
  const [qty] = useInput(0, item.quantity);
  const {
    CATEGORY,
    TOTAL,
    TOTAL_PURCHASE,
    NOT_ADD,
    TOTAL_LIMIT_REACHED,
    ADD_TO_CART,
    TOTAL_ITEM_LIMIT_REACHED,
    REMOVE,
    DELETE,
    CANCEL,
    DELETE_TITLE,
    EDIT_ITEM,
  } = catalog;
  useEffect(() => {
    if (limit.length > 0) {
      let purchaseLimit;
      switch (limitper) {
        case CATEGORY:
          purchaseLimit = limit?.find((limit) => !limit.total);
          break;
        case TOTAL:
          purchaseLimit = limit?.find((limit) => limit.total);
          break;
        default:
          null;
      }
      setPurchaseLimit(purchaseLimit);
    }
  }, [limit]);

  useEffect(() => {
    const itemTotal = items.reduce(
      (prev, current) => Number(prev) + Number(current.quantity),
      0
    );
    setItemTotal(itemTotal);
  }, [...items, itemTotal]);

  function addToCartCate() {
    let add = true;
    if (limitper === CATEGORY) {
      Object.entries(purchaseLimit?.bycategory).forEach((el) => {
        if (item.category === el[0] && qty.value > Number(el[1])) {
          message.warning(`Limit of ${el[1]} ${el[0]}`);
          add = false;
        }
        _items.forEach((itemF) => {
          if (
            item.category === itemF.category &&
            item.category === el[0] &&
            Number(itemF.quantity) + Number(qty.value) > el[1]
          ) {
            message.warning(`Limit of ${el[1]} ${el[0]}`);
            add = false;
          }
        });
      });
    }
    if (add && Number(qty.value) !== 0) {
      dispatch({
        type: "ADD",
        payload: {
          quantity: qty.value,
          item: item._id,
          sku: item.sku,
          name: item.description,
          category: item.category,
          amount: "N/A",
        },
      });
      setActive(false);
      message.success(`${ITEM_ADDED}`);
    } else {
      message.warning(`${NOT_ADDED}`);
    }
  }

  function addToCart() {
    if (total !== 0 && total <= Number(purchaseLimit?.total)) {
      dispatch({
        type: "ADD",
        payload: {
          quantity: quantity,
          item: item._id,
          sku: item.sku,
          name: item.description,
          category: item.category,
          amount: "N/A",
        },
      });
      setActive(false);
      message.success(`${ITEM_ADDED}`);
    } else {
      message.warning(`${TOTAL_PURCHASE} ${purchaseLimit?.total}`);
      message.warning(`${NOT_ADD}`);
    }
  }
  function removeFromCart(id) {
    dispatch({ type: "REMOVE", payload: id });
    setActive(true);
    message.warning(`${ITEM_DELETED}`);
  }
  async function deleteItem(itemId) {
    const res = await fetch(`/api/furniture/${itemId}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });
    const { info } = await res.json();
    if (res.status === 200) {
      message.success(`${info}`);
      setTimeout(() => {
        window.location.reload(false);
      }, 2000);
    } else {
      message.error(`${NOT_DELETED}`);
    }
  }
  const inaActiveItem = async (id, key) => {
    const data = { ...item, isActive: key ? true : false };
    await fetch(`/api/furniture/${id}`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });

    if (key) {
      message.success("item Activated");
    } else message.success("item Inactived");

    fetchCatalogue();
  };

  return (
    <Col
      xs={{ span: 24 }}
      sm={{ span: 18, offset: 3 }}
      md={{ span: 18, offset: 3 }}
    >
      {isActive && (
        <Col>
          <Card>
            <Row gutter={16} style={styles.itemContainer}>
              <Col span={8}>
                <div
                  style={item.quantity <= 0 ? { opacity: 0.5 } : { opacity: 1 }}
                >
                  <Carousel autoplay>
                    {item?.images?.map((image) => (
                      <img
                        key={image}
                        src={image}
                        alt="item"
                        style={styles.carousel}
                      />
                    ))}
                  </Carousel>
                </div>
              </Col>
              <Col
                span={8}
                style={item.quantity <= 0 ? { opacity: 0.5 } : { opacity: 1 }}
              >
                <Row style={styles.itemDescription}>
                  <Title level={4}>{item.description}</Title>
                </Row>
                <p>
                  <b>{item.category}</b>
                </p>
                <p>
                  Items Available: <b>{item.quantity}</b>
                </p>
                <p>
                  SKU: <b>{item.sku}</b>
                </p>
                <p>
                  Color: <b>{item.color}</b>
                </p>
                <p>
                  Material: <b>{item.material}</b>
                </p>
                <p>
                  Dimensions:{" "}
                  <b>
                    {item?.dimensions?.high}" High x {item?.dimensions?.wide}"
                    Wide x&nbsp;
                    {item?.dimensions?.deep}" Deep
                  </b>
                </p>
              </Col>
              {currentUser?.role === "FBA" && (
                <Col xl={8} lg={12} md={4} sm={8} style={styles.userContainer}>
                  <Row justify="end">
                    <Col
                      xxl={10}
                      xl={10}
                      lg={6}
                      md={5}
                      sm={5}
                      style={styles.editButtonContainer}
                    >
                      <Link href={`/item/${item?._id}/edit`}>
                        <a>
                          <Button style={styles.editButton}>
                            <FontAwesomeIcon icon={faEdit} /> &nbsp; {EDIT_ITEM}
                          </Button>
                        </a>
                      </Link>
                    </Col>
                    <Col xxl={10} xl={10} lg={6} md={5} sm={5} offset={2}>
                      <Popconfirm
                        placement="right"
                        title={DELETE_TITLE}
                        okText={DELETE}
                        cancelText={CANCEL}
                        onConfirm={() => deleteItem(item?._id)}
                      >
                        <Button style={styles.deleteButton}>
                          <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; {DELETE}
                        </Button>
                      </Popconfirm>
                    </Col>
                  </Row>
                  <Col
                    xxl={10}
                    xl={10}
                    lg={6}
                    md={5}
                    sm={5}
                    offset={2}
                    style={{ paddingLeft: "0px", marginTop: "10px" }}
                  >
                    <Button
                      style={styles.inactive}
                      onClick={() => inaActiveItem(item?._id)}
                    >
                      <FontAwesomeIcon icon={faBan} /> &nbsp; Inactive
                    </Button>
                  </Col>
                </Col>
              )}
              {currentUser?.role !== Role.FBA && (
                <Col xl={8} lg={12} md={4} sm={8} style={styles.userContainer}>
                  {onButton ? (
                    <Row>
                      <Col xl={6} lg={8} md={4}>
                        {purchaseLimit?.total ? (
                          <Input
                            type="number"
                            max={item.quantity}
                            min={0}
                            value={totalObj[item._id] || 0}
                            size="large"
                            disabled={
                              !purchase ||
                              purchaseLimit === undefined ||
                              !purchaseLimit?.total
                            }
                            style={{ width: 58 }}
                            onChange={(e) => {
                              if (
                                item.quantity < e.target.value ||
                                e.target.value < 0
                              ) {
                                message.warning("Item Quantity is invalid");
                              } else {
                                onchangeInput(e, item._id);
                              }
                            }}
                          />
                        ) : (
                          <Input
                            type="number"
                            max={item.quantity}
                            min={0}
                            size="large"
                            {...qty}
                            disabled={
                              !purchase ||
                              purchaseLimit === undefined ||
                              !purchaseLimit?.bycategory
                            }
                            style={{ width: 58 }}
                          />
                        )}
                      </Col>
                      <Col xl={7} lg={8} md={4} sm={8} offset={2}>
                        <div>
                          {limitper === CATEGORY ? (
                            <>
                              {purchaseLimit?.bycategory !== undefined ? (
                                <Button
                                  style={styles.addToCart}
                                  disabled={item.quantity <= 0}
                                  onClick={addToCartCate}
                                >
                                  <FontAwesomeIcon icon={faPlus} /> &nbsp;{" "}
                                  {ADD_TO_CART}
                                </Button>
                              ) : (
                                <p>{TOTAL_LIMIT_REACHED}</p>
                              )}
                            </>
                          ) : limitper === TOTAL ? (
                            <>
                              {purchaseLimit?.total ? (
                                <Button
                                  style={styles.addToCart}
                                  disabled={!purchase}
                                  onClick={addToCart}
                                >
                                  <FontAwesomeIcon icon={faPlus} /> &nbsp;{" "}
                                  {ADD_TO_CART}
                                </Button>
                              ) : (
                                <p>{TOTAL_ITEM_LIMIT_REACHED}</p>
                              )}
                            </>
                          ) : null}
                        </div>
                      </Col>
                    </Row>
                  ) : (
                    <Button
                      style={styles.removeFromCart}
                      onClick={() => removeFromCart(item._id)}
                    >
                      <FontAwesomeIcon icon={faMinus} /> &nbsp; {REMOVE}
                    </Button>
                  )}
                </Col>
              )}
            </Row>
          </Card>
        </Col>
      )}
      {currentUser?.role === Role.FBA && !isActive && (
        <Card>
          <Row gutter={16} style={styles.itemContainer}>
            <Col span={8}>
              <div style={!isActive ? { opacity: 0.5 } : { opacity: 1 }}>
                <Carousel autoplay>
                  {item?.images?.map((image) => (
                    <img
                      key={image}
                      src={image}
                      alt="item"
                      style={styles.carousel}
                    />
                  ))}
                </Carousel>
              </div>
            </Col>
            <Col
              span={8}
              style={item.quantity <= 0 ? { opacity: 0.5 } : { opacity: 1 }}
            >
              <Row style={styles.itemDescription}>
                <Title level={4}>{item.description}</Title>
              </Row>
              <p>
                <b>{item.category}</b>
              </p>
              <p>
                Items Available: <b>{item.quantity}</b>
              </p>
              <p>
                SKU: <b>{item.sku}</b>
              </p>
              <p>
                Color: <b>{item.color}</b>
              </p>
              <p>
                Material: <b>{item.material}</b>
              </p>
              <p>
                Dimensions:{" "}
                <b>
                  {item?.dimensions?.high}" High x {item?.dimensions?.wide}"
                  Wide x&nbsp;
                  {item?.dimensions?.deep}" Deep
                </b>
              </p>
            </Col>
            {currentUser?.role === "FBA" && (
              <Col xl={8} lg={12} md={4} sm={8} style={styles.userContainer}>
                <Row justify="end">
                  <Col
                    xxl={10}
                    xl={10}
                    lg={6}
                    md={5}
                    sm={5}
                    style={styles.editButtonContainer}
                  >
                    <Link href={`/item/${item?._id}/edit`}>
                      <a>
                        <Button style={styles.editButton}>
                          <FontAwesomeIcon icon={faEdit} /> &nbsp;{EDIT_ITEM}
                        </Button>
                      </a>
                    </Link>
                  </Col>
                  <Col xxl={10} xl={10} lg={6} md={5} sm={5} offset={2}>
                    <Popconfirm
                      placement="right"
                      title={DELETE_TITLE}
                      okText={DELETE}
                      cancelText={CANCEL}
                      onConfirm={() => deleteItem(item?._id)}
                    >
                      <Button style={styles.deleteButton}>
                        <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; {DELETE}
                      </Button>
                    </Popconfirm>
                  </Col>
                </Row>
                {item.quantity >= 0 && !isActive && (
                  <Col
                    xxl={10}
                    xl={10}
                    lg={6}
                    md={5}
                    sm={5}
                    offset={-2}
                    style={{ padding: "0px", marginTop: "10px" }}
                  >
                    <Button
                      style={{
                        ...styles.activate,
                        backgroundColor:
                          item.quantity > 0 ? "#52c41a" : "#E5E4E2",
                        border:
                          item.quantity > 0
                            ? "1px solid #52c41a"
                            : "1px solid #E5E4E2",
                      }}
                      disabled={!item.quantity > 0}
                      onClick={() => {
                        let key = "activate";
                        inaActiveItem(item?._id, key);
                      }}
                    >
                      <FontAwesomeIcon icon={faGrinHearts} /> &nbsp; Activate
                    </Button>
                  </Col>
                )}
              </Col>
            )}
          </Row>
        </Card>
      )}
    </Col>
  );
}

export default Catalog;
