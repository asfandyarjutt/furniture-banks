import React, { useState } from "react";
import { Card, Input, Button, Typography, Row, Col, Form, message } from "antd";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import { AddManagers } from "../../constants";
import styles from "./styles";
import {
  emailAddress,
  firstName,
  lastName,
  phoneNumber,
} from "../../utils/formRules";
const { COMPLETE_FIELD, LIAISON, ADMIN, CASE_MANGER, SAVE } = AddManagers;

const { Title } = Typography;

function AddManager({ agencyId, liaison, fba }) {
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    partnerAgency: fba ? null : agencyId,
    liaison,
    fba,
  });
  const router = useRouter();

  async function saveUser() {
    try {
      if (!fba) {
        const resFba = await fetch(`/api/pa/${agencyId}/user`, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(user),
        });
        const { message } = await resFba.json();
        router.push({ pathname: `/agencies/${agencyId}`, query: { message } });
      } else {
        const res = await fetch(`/api/fba/${agencyId}/user`, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(user),
        });
        const { message } = await res.json();
        router.push({
          pathname: `/furniturebanks/${agencyId}`,
          query: { message },
        });
      }
    } catch (err) {
      throw err;
    }
  }
  const handleChange = ({ target: { value, name } }) => {
    setUser({ ...user, [name]: value });
  };

  const onFinishFailed = () => {
    message.warning(`${COMPLETE_FIELD}`);
  };

  return (
    <Card>
      <Row justify="end" align="middle" style={{ marginBottom: "2em" }}>
        <Col span={12}>
          <Title level={4}>
            Add {liaison ? LIAISON : fba ? ADMIN : CASE_MANGER}
          </Title>
        </Col>
        <Col span={12} style={styles.cancelButtonContainer}>
          <Button
            style={styles.cancelButton}
            shape="circle"
            icon={<FontAwesomeIcon icon={faTimes} />}
            onClick={() => router.back()}
          />
        </Col>
      </Row>
      <Form onFinish={saveUser} onFinishFailed={onFinishFailed}>
        <Row gutter={16} style={{ marginBottom: "2em" }}>
          <Col span={12}>
            <p>First Name:</p>
            <Form.Item name="firstName" rules={firstName}>
              <Input
                size="large"
                name="firstName"
                onChange={handleChange}
                placeholder="First Name"
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <p>Last Name:</p>
            <Form.Item name="lastName" rules={lastName}>
              <Input
                size="large"
                name="lastName"
                onChange={handleChange}
                placeholder="Last Name"
              />
            </Form.Item>
          </Col>
        </Row>
        <br />
        <Row gutter={16} style={{ marginBottom: "2em" }}>
          <Col span={8}>
            <p>Phone Number: </p>
            <Form.Item name="phone" rules={phoneNumber}>
              <Input
                size="large"
                name="phone"
                onChange={handleChange}
                maxLength="10"
                placeholder="555-123-4444"
              />
            </Form.Item>
          </Col>
          <Col span={16}>
            <p>E-mail: </p>
            <Form.Item name="email" rules={emailAddress}>
              <Input
                size="large"
                name="email"
                onChange={handleChange}
                placeholder="E-mail"
              />
            </Form.Item>
          </Col>
        </Row>
        <br />
        <Row style={{ paddingTop: "2em" }}>
          <Col span={6} offset={18} style={styles.saveButtonContainer}>
            <Button htmlType="submit" style={styles.saveButton}>
              <FontAwesomeIcon icon={faCheck} /> &nbsp; {SAVE}
            </Button>
          </Col>
        </Row>
      </Form>
    </Card>
  );
}

export default AddManager;
