const styles = {
  cancelButtonContainer: {
    display: "flex",
    justifyContent: "flex-end",
    paddingRight: "2em",
  },
  cancelButton: { background: "#010101", color: "#ffffff" },
  saveButtonContainer: { display: "flex", justifyContent: "flex-end" },
  saveButton: {
    backgroundColor: "#1890ff",
    height: "40px",
    color: "white",
    border: "1px solid #1890ff",
    borderRadius: "3px",
    borderwidth: "1px",
    display: "inline-block",
    textAlign: "center",
    paddingLeft: "20px",
    paddingRight: "20px",
  },
};
export default styles;
