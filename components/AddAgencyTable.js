import React, { useState } from "react";
import { Table } from "antd";
import { useRouter } from "next/router";

function AddAgencyTable({ data, setAgencies}) {
  const router = useRouter();
  const {fbid}=router.query
  const [selectionType, setSelectionType] = useState("checkbox");
  const [selectedRows, setDeleteRows] = useState(); const newData = data.filter(item => !item.furnitureBank);
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "City",
      dataIndex: "city",
    },
    {
      title: "State",
      dataIndex: "state",
    },
    {
      title: "Liaison",
      dataIndex: "liaison",
    },
  ];
  const reco = newData.map(item => {
    let some = {
      key: item._id,
      id: item._id,
      name: item.name,
      city: item.city,
      state: item.state,
      liaison: item.liaison ? `${item.liaison.firstName} ${item.liaison.lastName}` : 'None'
    }
    return some;
  })

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setAgencies(selectedRows)
    },

    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User',
      name: record.name,
    }),
  };


  return (
    <div>
    <Table
      rowSelection={{
        type: selectionType,
        ...rowSelection,
      }}
      columns={columns}
      dataSource={reco}
    />
    
    </div>
  )
}

export default AddAgencyTable;
