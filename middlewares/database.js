import mongoose from "mongoose";
import { formsSchema } from "../models/Forms";
import { furnitureBankSchema } from "../models/FurnitureBank";
import { furnitureItemSchema } from "../models/FurnitureItem";
import { userSchema } from "../models/User";
import { partnerAgencySchema } from "../models/PartnerAgency";
import { ticketSchema } from "../models/Ticket";
import { timesSchema } from "../models/Times";
import { tokenSchema } from "../models/Token";
import { orderSchema } from "../models/Order";

export async function setUpDB() {
  return mongoose.connect(process.env.furnidb, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    serverSelectionTimeoutMS: 5000,
    socketTimeoutMS: 30000,
    keepAlive: true,
    reconnectTries: 30000,
    poolSize: 1,
  });
}

export default async function database(req, _, next) {
  if (mongoose.connection.readyState == 0) {
    await setUpDB();
    mongoose.model("Forms", formsSchema);
    mongoose.model("FurnitureBank", furnitureBankSchema);
    mongoose.model("FurnitureItem", furnitureItemSchema);
    mongoose.model("User", userSchema);
    mongoose.model("PartnerAgency", partnerAgencySchema);
    mongoose.model("Ticket", ticketSchema);
    mongoose.model("Times", timesSchema);
    mongoose.model("Token", tokenSchema);
    mongoose.model("Order", orderSchema);
  }
  return next();
}
