import { session, expressSession } from "next-session";
import mongoose from "mongoose";

const MongoStore = require("connect-mongo")(expressSession);

export default function (req, res, next) {
  const mongoStore = new MongoStore({
    mongooseConnection: mongoose.connection,
    stringify: false,
  });

  return session({
    store: mongoStore,
  })(req, res, next);
}
