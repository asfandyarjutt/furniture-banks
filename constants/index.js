export const title = "Check Out";
export const stepZeroTitle =
  "Client payment can be made by check or money order and is due at time of pick up or delivery.";
export const stepOneTitle =
  "Your agency will receive an invoice from the Furniture Bank with the following information.";
export const pickUpTitle = "Choose a date to pick up.";
export const deliveryTitle = "Choose a date to receive your delivery.";
export const pickUpToolTip =
  "Today's appointment limit exceeded, Please select other date.";
export const deliveryToolTip =
  "Today's appointment limit exceeded, Please select other date.";
export const pickUpQuestion = "Who will pick the furniture items?";
export const deliveryQuestion = "Who will receive your delivery?";
export const Order_Text =
  "Click on an order number below to view more details.";
export const OTHER = "other";
export const stepOne = "Client Payment";
export const stepTwo = "Agency Payment";
export const stepThree = "Pick Up and Delivery";
export const Bg_Updated = "Background Question Updated";
export const Bg_Added = "Background Question Added";
export const Bg_Deleted = "Background Question Deleted";
export const IN_COMPLETE = "In-Complete Question Removed ";
export const ANSWERS_KEY = "answers";
export const OPTION_ERROR = "Please input Option or delete this field.";
export const ADD_OPTION = "Add Option";
export const DELETE_QUESTION = "Delete Question";
export const ADD_QUESTION = "Add Question";
export const OrderDetails = {
  Agency_Amount: "Agency Amount",
  Client_Amount: "Client Amount",
  Order_Total: "Order Total",
  BG_Info: "Background Information",
  House_Info: "Household Information",
};
export const Field = {
  Client_Name: "Client Name",
  Client_Address: "Client Address",
  Client_Email: "Client Email",
  Client_Phone: "Client Phone",
};
export const Role = {
  FBA: "FBA",
  PAL: "PAL",
  PACM: "PACM",
  ADMIN: "ADMIN",
};

export const emailRegx =
  /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
export const phoneRegx = /^\d{10}$/;
export const zipRegx = /^[0-9]{5}(-[0-9]{4})?$/;
export const zipCodeRegx = /^\d{5}(?:[ ,-]\d{5})*$/;
export const duplicateZipRegx = /^(?!.*\b(\d+)\b.*?,\1\b)/;
export const numberRegx = /^\d+$/;
export const BG_Info = "Background Information";
export const ErrorMSg = "Please Fill the empty fields";
export const ClientContactInfo = "Client Contact Information";
export const Not_Editable_Txt =
  "This section is not editable. The following information will be requested of all clients.";
export const CM_Info = "Case Manager Information";
export const Deliver_Info = "Delivery and Payment Information";
export const reciver_furniture = "How will you recieve the furniture items?";
export const not_applicable_txt =
  "Not Applicable (Please input $0 for client amount.)";
export const payment_change = "Payment Changed According to Delivery Cost";
export const curbs_deli = "Curbside Delivery";
export const instal_deli = "Installation Delivery";
export const HEADING_BG_INFO =
  " Add multiple choice or all that apply questions regarding client background information";
export const MULTI_CHOICE = "Multiple Choice";
export const ALL_APPLY = "All That Apply";
export const CHECKBOX = "checkbox";
export const SELECT_REASON =
  "Select the top 1 or 2 reasons for needing assistance at this time";
export const MS_STATUS = "Military Service Status";
export const ANNUAL_HH = "Annual Household Income";
export const ZIP_CODE = "The Furniture Bank doesn't deliver to your zip code.";
export const INFO_SAVED = "Information Saved";
export const SELECT_PICK_UP = (
  <p>
    The Furniture Bank Does Not Deliver To Your Zipcode.
    <br />
    Please Enter a Different Zip Code or Select "Pick Up"
  </p>
);
export const EMAIL_CONFORMATION_ORDER =
  " Write a message that you wish to appear in the e-mail confirmation to the Client. The message will be appear above the order summary.";
export const EMAIL_CONFORMATION_CM =
  " Write a message that you wish to appear in the e-mail confirmation to the Case Manager. The message will be appear above the order summary.";
export const EMAIL_CONFORMATION_PA =
  "Write a message that you wish to appear in the e-mail confirmation to the Partner Agency. The message will be appear above the order summary.";
export const HOUSEHOLD_INFORMATION = "Household Information";
export const Save_info = "Did you save your information?";
export const FILL_FIELDS = "Please Fill Empty Fields";
export const CLIENT_APP_FORM = "Client Appointment Form";
export const GO_CHECKOUT = "Go to Checkout";
export const RELOAD_PAGE = "Something went wrong Please Reload the Page";
export const EDIT_CATEGORY = "Edit Categories";
export const ADD_ITEM = "Add New Item";
export const TOTAL_CATEGORY_REACHED = "Total Items by Category Reached";
export const ITEM_ADDED = "Item Added to Cart";
export const URL_HDTICKETS = "/api/hdtickets";
export const URL_AUTH = "/api/auth";
export const HELP_DESK = "Help Desk";
export const NOT_ADDED = "Cannot Add Item to Cart";
export const ITEM_DELETED = "item Removed from Cart";
export const NOT_DELETED = "item has not been deleted";
export const NO_PURCHASE_LIMIT =
  "No purchase limits, contact your furniture bank.";
export const ADD_CART = "Add to Cart";
export const LIMIT_REACHED = "Total Items Limit Reached";
export const FALSE = "false";
export const paymentForm = {
  EDIT_COST: "Edit cost of pick up and delivery services.",
  PICK_UP: "Pick Up",
  CURBSIDE_DELIVERY: "Curbside Delivery",
  INSTALLATION_DELIVERY: "Installation Delivery",
};

export const calendarCard = {
  DELIVERY: "Delivery",
  TYPE_DELIVERY: "delivery",
  SELF_HAUL: "Self Haul",
  TYPE_SELF_HAUL: "selfHaul",
  PICK_UP: "Pick Up",
  PICK_UP_PURCHASE: "pick up their purchase",
  RECEIVE_DELIVERY: "receive their delivery",
};

export const ticketDeleteModal = {
  SELECTED_ROW_TITLE: "Are you sure you want to delete message(s)?",
  NO_ROW_SELECTED_TITLE: "No Tickets selected",
  SELECTED_ROW_CONTENT: "Messages cannot be recovered after being deleted.",
  NO_ROW_SELECTED_CONTENT: "You must select some row(s).",
  SELECTED_ROW_OK_TEXT: "Delete",
  NO_ROW_SELECTED_OK_TEXT: "Ok",
  SELECTED_ROW_OK_TYPE: "danger",
  NO_ROW_SELECTED_OK_TYPE: "default",
  SELECTED_ROW_CANCEL_TEXT: "Cancel",
  NO_ROW_SELECTED_CANCEL_TEXT: "Cancel",
};

export const furnitureBanks = {
  DELETE_MESSAGE:
    "Furniture Bank has been deleted. You can retrieve this information by going to the Deleted Furniture Banks section.",
};

export const catalog = {
  CATEGORY: "category",
  TOTAL: "total",
  TOTAL_PURCHASE: "Total purchase limit is",
  NOT_ADD: "Cannot Add Item to Cart",
  TOTAL_LIMIT_REACHED: "Total Items Limit Reached",
  ADD_TO_CART: "Add to Cart",
  TOTAL_ITEM_LIMIT_REACHED: "Total Items by total Reached",
  REMOVE: "Remove",
  DELETE: "Delete",
  CANCEL: "Cancel",
  DELETE_TITLE: "Are you sure you want to delete？",
  EDIT_ITEM: "Edit Item",
};

export const AddManagers = {
  COMPLETE_FIELD: "Complete all required fields.",
  LIAISON: "Liaison",
  ADMIN: "Admin",
  CASE_MANGER: "Case Manager",
  SAVE: "Save",
  REQUIRED_FIRST_NAME: "First Name is required.",
  REQUIRED_LAST_NAME: "Last Name is required.",
  REQUIRED_PHONE: "Phone Number is required.",
  VALID_PHONE: "please enter a valid phone number.",
  REQUIRED_EMAIL: "Email is required.",
  VALID_EMAIL: "Please enter a valid email",
};

export const purchaseLimit = {
  ADD: "add",
  EDIT: "edit",
};

export const RequiredZip = "Zip Code is Required";

export const Validzip = "please enter a valid Zip Code.";

export const invoice = {
  CLICK_TO_VIEW: "Click on a row below to view more details.",
  SEARCH_PLACEHOLDER: "Search by order number, agency name, or case manager.",
  NO_RECORD: "No Record Found",
};

export const invoiceDetail = {
  CLIENT: "Client",
  CLIENT_ADDRESS: "Client Address",
  CLIENT_EMAIL: "Client Email",
  CLIENT_PHONE: "Client Phone",
  PARTNER_AGENCY: "Partner Agency",
  CASE_MANGER: "Case Manager",
  CASE_MANGER_EMAIL: "Case Manager E-mail",
  CASE_MANGER_PHONE: "Case Manager Phone",
  DELIVERY_RECEIVED_BY: "Delivery to be received by:",
  PHONE_NUMBER: "Phone Number:",
};
